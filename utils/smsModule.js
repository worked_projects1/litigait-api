const AWS = require('aws-sdk');

async function sendSMS(to, subject, content) {
    AWS.config.region = 'us-west-2';
    const sns = new AWS.SNS();
    const params = {
        Message: content,
        MessageStructure: 'string',
        PhoneNumber: `${to}`,
        Subject: subject,
    };
    await sns.publish(params).promise();
}

module.exports = {
    sendSMS,
};
