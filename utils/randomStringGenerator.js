function generateRandomString(count) {
    const digits = '0123456789abcdefghijklmnopqrstuvwxyz';
    const otpLength = count || 8;
    let otp = '';
    for (let i = 1; i <= otpLength; i += 1) {
        const index = Math.floor(Math.random() * (digits.length));

        otp += digits[index];
    }

    return otp.toUpperCase();
}

module.exports = {
    generateRandomString,
};
