// format: Thu Jan 24 2019 08:10:09 GMT-0800 (Pacific Standard Time)
exports.fullTextTime = function () {
    return Date();
};

// format: 2019-01-24T16:12:02.781Z
exports.isoTime = function () {
    const date = new Date();
    return date.toISOString();
};

// format: 2019-01-24 16:12:33
exports.utcTime = function () {
    const date = new Date();
    return date.toISOString().replace(/T/, ' ').replace(/\..+/, ' - ');
};

// format: 1/24/2019 8:10:22a
exports.localTime = function () {
    const date = new Date();
    return date.toLocaleString().replace(/,/, '').replace(/ AM/, 'a - ').replace(/ PM/, 'p - ');
};

exports.unixTimeStamptoDateTime = (unixTimestamp) => {

    const date = new Date(unixTimestamp * 1000);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();
    const time = `${padTo2Digits(hours)}:${padTo2Digits(minutes)}:${padTo2Digits(seconds)}`;      
    const year = date.getFullYear();
    const month = padTo2Digits(date.getMonth() + 1);
    const day = padTo2Digits(date.getDate());
    const dateTime = `${year}-${month}-${day} ${time}`;
    return dateTime;
}

exports.unixTimeStamptoDate = (unixTimestamp) => {

    const date = new Date(unixTimestamp * 1000);
    const year = date.getFullYear();
    const month = padTo2Digits(date.getMonth() + 1);
    const day = padTo2Digits(date.getDate());
    const dateTime = `${month}/${day}/${year}`;
    return dateTime;
}

function padTo2Digits(num) {
    return num.toString().padStart(2, '0');
}

exports.getCurrentDate = () =>{
    const today = new Date();
    const date = today.getFullYear()+'-'+(padTo2Digits(today.getMonth()+1))+'-'+padTo2Digits(today.getDate());
    return date;
};
/* 
The padStart() method pads the current string with another string (multiple times, if needed) 
until the resulting string reaches the given length. 
*/

exports.dateDiffInDays = (a, b) => {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc1 - utc2) / _MS_PER_DAY);
}

exports.customDate = (period) => {
    //let filter_query = {};
    if (period == 'this_month') {
        const date = new Date();
        let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        return customDateandTime(firstDay);
    } else if (period == 'this_quarter') {
        const date = new Date();
        date.setMonth(date.getMonth() - 4);
        return customDateandTime(date);
    } else if (period == 'this_year') {
        const date = new Date(new Date().getFullYear(), 0, 1);
        return customDateandTime(date);
    } else if (period == 'last_month') {
        const date = new Date();
        date.setMonth(date.getMonth() - 1);
        let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        firstDay = customDateandTime(firstDay);
        lastDay = customDateandTime(lastDay);
        let obj = {firstDay , lastDay};
        return obj;
    } else if (period == 'last_quarter') {
        const date = new Date();
        date.setMonth(date.getMonth() - 4);
        let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        const currentDay = new Date();
        let firstDayOfCurrentDay = new Date(currentDay.getFullYear(), currentDay.getMonth(), 1);
        firstDay = customDateandTime(firstDay);
        firstDayOfCurrentDay = customDateandTime(firstDayOfCurrentDay);
        let obj = {firstDay , lastDay:firstDayOfCurrentDay};
        return obj;
    } else if (period == 'last_year') {
        const date = new Date();
        date.setMonth(date.getMonth() - 12);
        return customDateandTime(date);
    }else if(period == 'last_30_days'){
        const d = new Date();
        d.setMonth(d.getMonth() - 1);
        return customDateandTime(d);
    }
}

function customDateandTime (filterDate) {

    const date = new Date(filterDate);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();
    const time = `${padTo2Digits(hours)}:${padTo2Digits(minutes)}:${padTo2Digits(seconds)}`;      
    const year = date.getFullYear();
    const month = padTo2Digits(date.getMonth() + 1);
    const day = padTo2Digits(date.getDate());
    const dateTime = `${year}-${month}-${day} ${time}`;
    return dateTime;
}