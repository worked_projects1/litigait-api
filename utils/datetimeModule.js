const moment = require('moment');

const customDateandTime = (filter) =>{
    let from_date, to_date;
    if (filter == 'last_30_days') {
        from_date = moment().subtract(30, 'days').format('YYYY-MM-DD 00:00:00');
        to_date = moment().format('YYYY-MM-DD 23:59:59');
    } else if (filter == 'this_month') {
        from_date = moment().clone().startOf('month').format('YYYY-MM-DD 00:00:00');
        to_date = moment().clone().endOf('month').format('YYYY-MM-DD 23:59:59');
    } else if (filter == 'this_quarter') {
        from_date = moment().startOf('quarter').format('YYYY-MM-DD 00:00:00');
        to_date = moment().endOf('quarter').format('YYYY-MM-DD 23:59:59');
    } else if (filter == 'this_year') {
        from_date = moment().startOf('year').format('YYYY-MM-DD 00:00:00');
        to_date = moment().endOf('year').format('YYYY-MM-DD 23:59:59');
    } else if (filter == 'last_month') {
        from_date = moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD 00:00:00');
        to_date = moment().subtract(1, 'months').endOf('month').format('YYYY-MM-DD 23:59:59');
    } else if (filter == 'last_quarter') {
        from_date = moment().subtract(1, 'quarter').startOf('quarter').format('YYYY-MM-DD 00:00:00');
        to_date = moment().subtract(1, 'quarter').endOf('quarter').format('YYYY-MM-DD 23:59:59');
    } else if (filter == 'last_year') {
        from_date = moment().subtract(1, 'year').startOf('year').format('YYYY-MM-DD 00:00:00');
        to_date = moment().subtract(1, 'year').endOf('year').format('YYYY-MM-DD 23:59:59');
    }
    return {from_date,to_date};
}

module.exports = {
    customDateandTime
}