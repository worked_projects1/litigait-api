
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Update Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }
  test('update superAdmin password by superAdmin', async () => {
    const response = await server.put('/users/specific_superAdmin_user_for_test')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        password: 'new_password',
      });
    expect(response.statusCode).toBe(200);

    const secondResponse = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'secondsuperAdmin@test.com',
        password: 'new_password',
      });
    expect(secondResponse.statusCode).toBe(200);
    expect(secondResponse.body.authToken).not.toBe('');
  });

  test('update superAdmin role to operator by superAdmin', async () => {
    const response = await server.put('/users/specific_superAdmin_user_for_test')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        role: 'operator',
      });
    expect(response.statusCode).toBe(200);
    expect(response.body.role).toBe('operator');
    
  });
});
