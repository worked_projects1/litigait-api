

const request = require('supertest');
const { config } = require('../global');
const EnvVariables = require('../../configs/secrets.test.json');

process.env = Object.assign(process.env, EnvVariables);
const connectToDatabase = require('../../db');

describe('Clean up', () => {
  test('Clean Up Orders Table', async () => {
    const { Orders, sequelize } = await connectToDatabase();

    const result = await Orders.destroy({ where: { } });

    expect(result).not.toBe(null);
    const count = await Orders.count({
      where: {
      },
    });
    expect(count).toBe(0);
    //sequelize.close();
  });
});
