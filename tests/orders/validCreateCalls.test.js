
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }



  test('create orders with only required fields (order_date, status, practice_id, user_id, document_id, document_type, amount_charged, charge_id)', async () => {
    const response = await server.post('/orders')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        order_date: new Date(),
        status: 'completed',
        practice_id: 'sample_practice_id',
        user_id: 'sample_user_id',
        case_id: 'sample_case_id',
        document_id: 'sample_document_id',
        document_type: 'FROGS',
        amount_charged: 12,
        charge_id: 'abcdefrgsj',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create specific order id with only required fields (order_date, status, practice_id, user_id, document_id, document_type, amount_charged, charge_id)', async () => {
    const response = await server.post('/orders')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: 'specific_order_for_test',
        order_date: new Date(),
        status: 'completed',
        practice_id: 'sample_practice_id_2',
        user_id: 'sample_user_id_2',
        case_id: 'sample_case_id_2',
        document_id: 'sample_document_id_2',
        document_type: 'FROGS',
        amount_charged: 12,
        charge_id: 'abcdefrgssxsj',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create order with all fields at min value/length', async () => {
    const response = await server.post('/orders')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        order_date: new Date(),
        status: 'completed',
        practice_id: config.text4,
        user_id: config.text4,
        case_id: config.text4,
        document_id: config.text4,
        document_type: 'FROGS',
        amount_charged: 12,
        charge_id: config.text4,
      });
    expect(response.statusCode).toBe(200);
  });

  test('create order with all fields at max value/length', async () => {
    const response = await server.post('/orders')
        .set('content-type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', process.env.authToken)
        .send({
          order_date: new Date(),
          status: 'completed',
          practice_id: config.text64,
          user_id: config.text64,
          case_id: config.text64,
          document_id: config.text64,
          document_type: 'FROGS',
          amount_charged: 12,
          charge_id: config.text64,
        });
    expect(response.statusCode).toBe(200);
  });
});
