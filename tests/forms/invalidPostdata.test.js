
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }


  test('create form with missing name', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        address: 'justanyuser@test.com',
        logoFile: 'testlink',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with missing case_id', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        document_type: 'FROGS',
        question_number: 100,
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with invalid fieldname', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'abc',
        document_type: 'FROGS',
        question_number: 100,
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with missing document_type', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        question_number: 100,
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with invalid fieldname', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        document_type: 'abc',
        question_number: 100,
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with invalid question_number', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        document_type: 'FROGS',
        question_number: 'nan',
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with invalid question_text', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        document_type: 'FROGS',
        question_number: 1,
        question_text: 'abc',
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });
  test('create form with invalid lawyer_response_text', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        document_type: 'FROGS',
        question_number: 1,
        question_text: 'cacasca',
        lawyer_response_text: 'abc',
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with invalid lawyer_response_status', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        document_type: 'FROGS',
        question_number: 1,
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'abc', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with invalid client_response_text', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        document_type: 'FROGS',
        question_number: 1,
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: 'abc',
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with invalid client_response_status', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        document_type: 'FROGS',
        question_number: 1,
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'abc', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with invalid last_updated_by_lawyer', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        document_type: 'FROGS',
        question_number: 1,
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: 'invalid date',
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(400);
  });

  test('create form with invalid last_updated_by_client', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        document_type: 'FROGS',
        question_number: 1,
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: 'invalid date',
      });
    expect(response.statusCode).toBe(400);
  });
});
