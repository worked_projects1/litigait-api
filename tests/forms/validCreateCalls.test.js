
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }


  // case_id: type.STRING,
  // document_type: type.STRING, // FROGS, SPROGS, RFPD, RFA
  // question_number: type.FLOAT,
  // question_text: type.STRING,
  // lawyer_response_text: type.STRING,
  // lawyer_response_status: type.STRING, // (NotStarted, Draft, Final)
  // client_response_text: type.STRING,
  // client_response_status: type.STRING, // (NotSetToClient, SentToClient, ClientResponseAvailable)
  // last_updated_by_lawyer: type.DATE,
  // last_updated_by_client: type.DATE,

  test('create forms with only required fields (case_id, document_type)', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id',
        document_type: 'FROGS',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create forms with specific id and only required fields (id, case_id, document_type)', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: 'specific_form_for_test',
        case_id: 'sample_case_id_special_1',
        document_type: 'FROGS',
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at min value/length', async () => {
    const response = await server.post('/forms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
        document_type: 'FROGS',
        question_number: 100,
        question_text: config.text4,
        lawyer_response_text: config.text4,
        lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
        client_response_text: config.text4,
        client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
        last_updated_by_lawyer: new Date(),
        last_updated_by_client: new Date(),
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at max value/length', async () => {
    const response = await server.post('/forms')
        .set('content-type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', process.env.authToken)
        .send({
          case_id: 'sample_case_id_2',
          document_type: 'FROGS',
          question_number: 100,
          question_text: config.text64,
          lawyer_response_text: config.text64,
          lawyer_response_status: 'Draft', // (NotStarted, Draft, Final)
          client_response_text: config.text64,
          client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
          last_updated_by_lawyer: new Date(),
          last_updated_by_client: new Date(),
        });
    expect(response.statusCode).toBe(200);
  });
});
