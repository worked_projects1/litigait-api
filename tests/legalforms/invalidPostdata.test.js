
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }


  test('create user with missing practice_id', async () => {
    const response = await server.post('/legalforms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_1',
        client_id: 'sample_client_id_1',
        form_id: 'sample_form_id_1',
        status: 'pending', // pending, complete
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with invalid practice_id', async () => {
    const response = await server.post('/legalforms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        practice_id: 'abc',
        case_id: 'sample_case_id_1',
        client_id: 'sample_client_id_1',
        form_id: 'sample_form_id_1',
        status: 'pending', // pending, complete
      });
    expect(response.statusCode).toBe(400);
  });


  test('create user with missing case_id', async () => {
    const response = await server.post('/legalforms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        practice_id: 'sample_practice_id_1',
        client_id: 'sample_client_id_1',
        form_id: 'sample_form_id_1',
        status: 'pending', // pending, complete
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with invalid client_id', async () => {
    const response = await server.post('/legalforms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        practice_id: 'sample_practice_id_1',
        case_id: 'abc',
        client_id: 'sample_client_id_1',
        form_id: 'sample_form_id_1',
        status: 'pending', // pending, complete
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with missing form_id', async () => {
    const response = await server.post('/legalforms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        practice_id: 'sample_practice_id_1',
        case_id: 'sample_case_id_1',
        client_id: 'sample_client_id_1',
        status: 'pending', // pending, complete
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with invalid form_id', async () => {
    const response = await server.post('/legalforms')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        practice_id: 'sample_practice_id_1',
        case_id: 'sample_case_id_1',
        client_id: 'sample_client_id_1',
        form_id: 'abc',
        status: 'pending', // pending, complete
      });
    expect(response.statusCode).toBe(400);
  });
});
