
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Update Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }
  test('update legalform case_id by superAdmin', async () => {
    const response = await server.put('/legalforms/specific_legalform_for_test')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_3',
      });
    expect(response.statusCode).toBe(200);
  });

  test('update legal form client_id to operator by superAdmin', async () => {
    const response = await server.put('/legalforms/specific_legalform_for_test')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        client_id: 'sample_client_id_4',
      });
    expect(response.statusCode).toBe(200);
  });
});
