

const request = require('supertest');
const { config } = require('../global');
const EnvVariables = require('../../configs/secrets.test.json');

process.env = Object.assign(process.env, EnvVariables);
const connectToDatabase = require('../../db');

describe('Clean up', () => {
  test('Clean Up Practices Table', async () => {
    const { Practices, sequelize } = await connectToDatabase();

    const result = await Practices.destroy({ where: { } });

    expect(result).not.toBe(null);
    const count = await Practices.count({
      where: {
      },
    });
    expect(count).toBe(0);
    //sequelize.close();
  });
});
