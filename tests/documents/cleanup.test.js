

const request = require('supertest');
const { config } = require('../global');
const EnvVariables = require('../../configs/secrets.test.json');

process.env = Object.assign(process.env, EnvVariables);
const connectToDatabase = require('../../db');

describe('Clean up', () => {
  test('Clean Up Document Table', async () => {
    const { Documents, sequelize } = await connectToDatabase();

    const result = await Documents.destroy({ where: { } });

    expect(result).not.toBe(null);
    const count = await Documents.count({
      where: {
      },
    });
    expect(count).toBe(0);
    //sequelize.close();
  });
});
