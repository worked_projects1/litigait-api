
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Invalid Params', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }


  test('read document with invalid offset and proper limit', async () => {
    const response = await server.get('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .query({
        offset: 'nan',
        limit: 1,
      });
    expect(response.statusCode).toBe(400);
  });

  test('read document with valid offset and invalid limit', async () => {
    const response = await server.get('/documents')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .query({
        offset: 1,
        limit: 'nan',
      });
    expect(response.statusCode).toBe(400);
  });
});
