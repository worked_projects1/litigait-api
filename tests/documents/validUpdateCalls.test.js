
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Update Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }
  test('update document id by superAdmin', async () => {
    const response = await server.put('/documents/specific_document_for_test')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        document_id: 'sample_document_id_3',
      });
    expect(response.statusCode).toBe(200);
  });

  test('update case id role to operator by superAdmin', async () => {
    const response = await server.put('/documents/specific_document_for_test')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        case_id: 'sample_case_id_2',
      });
    expect(response.statusCode).toBe(200);
  });
});
