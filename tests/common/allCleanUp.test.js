

const request = require('supertest');
const { config } = require('../global');
const EnvVariables = require('../../configs/secrets.test.json');

process.env = Object.assign(process.env, EnvVariables);
const connectToDatabase = require('../../db');

describe('Clean up everything', () => {
  test('Clean Up all Table', async () => {
    const { Practices, Users, sequelize } = await connectToDatabase();
    const result = await Users.destroy({ where: { } });
    const result2 = await Practices.destroy({ where: { } });
    expect(result).not.toBe(null);
    expect(result2).not.toBe(null);
    const count = await Users.count({
      where: {
      },
    });
    const count2 = await Practices.count({
      where: {
      },
    });
    expect(count).toBe(0);
    expect(count2).toBe(0);
    sequelize.close();
  });
});
