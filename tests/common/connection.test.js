
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Test the root path', () => {
    test('It should response the GET method', async () => {
    const response = await server.get('/');
    expect(response.statusCode).toBe(200);
    expect(response.body.message).toBe('Connection successful.');
  });
});
