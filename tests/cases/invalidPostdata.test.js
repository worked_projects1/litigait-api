
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }


  test('create user with missing name', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        address: 'justanyuser@test.com',
        logoFile: 'testlink',
      });
    expect(response.statusCode).toBe(400);
  });


  test('create user with invalid name', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        client_id: 'ab',
        start_date: new Date(),
        case_title: 'Sample Case 1',
        case_number: 'xyzcase123',
        status: 'new',
      });
    expect(response.statusCode).toBe(400);
  });


  test('create user with missing client_id', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        start_date: new Date(),
        case_title: 'Sample Case 1',
        case_number: 'xyzcase123',
        status: 'new',
      });
    expect(response.statusCode).toBe(400);
  });


  test('create user with invalid start_date', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        client_id: 'client_id_1',
        start_date: 'invalid date',
        case_title: 'Sample Case 1',
        case_number: 'xyzcase123',
        status: 'new',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with missing start_date', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        client_id: 'client_id_1',
        case_title: 'Sample Case 1',
        case_number: 'xyzcase123',
        status: 'new',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with invalid case_title', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        client_id: 'client_id_1',
        start_date: new Date(),
        case_title: 'ab',
        case_number: 'xyzcase123',
        status: 'new',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with missing case_title', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        client_id: 'client_id_1',
        start_date: new Date(),
        case_number: 'xyzcase123',
        status: 'new',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with invalid case_number', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        client_id: 'client_id_1',
        start_date: new Date(),
        case_title: 'Sample Case 1',
        case_number: 'ab',
        status: 'new',
      });
    expect(response.statusCode).toBe(400);
  });

  test('create user with missing case_title', async () => {
    const response = await server.post('/cases')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        client_id: 'client_id_1',
        start_date: new Date(),
        case_title: 'Sample Case 1',
        case_number: 'xyzcase123',
      });
    expect(response.statusCode).toBe(400);
  });
});
