
const request = require('supertest');
const { config } = require('../global');

const server = request(config.server_url);

describe('Valid Create Calls', () => {
  if (!process.env.viaAll) {
    test('login-in a user as not running part of all', async () => {
      const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: 'sandip.sandip.das11@gmail.com',
        password: 'simple12345',
      });
      expect(response.statusCode).toBe(200);
      expect(response.body.authToken).not.toBe('');
      process.env.authToken = response.body.authToken;
    });
  }

  test('create clients with only required fields (name, email, phone, dob)', async () => {
    const response = await server.post('/clients')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: 'Requiredname',
        email: 'anyclient@test.com',
        dob: new Date(),
      });
    expect(response.statusCode).toBe(200);
  });

  test('create clients with only required fields (name, address)', async () => {
    const response = await server.post('/clients')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: 'specific_client_for_test',
        name: 'Requiredname',
        email: 'anyclient@test.com',
        dob: new Date(),
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at min value/length', async () => {
    const response = await server.post('/clients')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        name: config.shortName,
        email: 'a@t.com',
        phone: 1234567,
        dob: new Date(),
        hipaa_acceptance_status: true,
        hipaa_acceptance_date: new Date(),
        fee_acceptance_status: true,
        fee_acceptance_date: new Date(),
      });
    expect(response.statusCode).toBe(200);
  });

  test('create user with all fields at max value/length', async () => {
    const response = await server.post('/clients')
        .set('content-type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', process.env.authToken)
        .send({
          name: config.text64,
          email: 'a@t.com',
          phone: 1234567,
          dob: new Date(),
          hipaa_acceptance_status: true,
          hipaa_acceptance_date: new Date(),
          fee_acceptance_status: true,
          fee_acceptance_date: new Date(),
        });
    expect(response.statusCode).toBe(200);
  });
});
