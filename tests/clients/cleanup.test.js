

const request = require('supertest');
const { config } = require('../global');
const EnvVariables = require('../../configs/secrets.test.json');

process.env = Object.assign(process.env, EnvVariables);
const connectToDatabase = require('../../db');

describe('Clean up', () => {
  test('Clean Up Clients Table', async () => {
    const { Clients, sequelize } = await connectToDatabase();

    const result = await Clients.destroy({ where: { } });

    expect(result).not.toBe(null);
    const count = await Clients.count({
      where: {
      },
    });
    expect(count).toBe(0);
    //sequelize.close();
  });
});
