const sequelize = require("sequelize");

module.exports = (sequelize, type) => sequelize.define('OtherParties', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    case_id: type.STRING,
    practice_id: type.STRING,
    name: type.STRING,
    first_name: type.STRING,
    middle_name: type.STRING,
    last_name: type.STRING,
    email: type.STRING,
    phone: type.STRING,
    address: type.STRING,
    street: type.STRING,
    city: type.STRING,
    state: type.STRING,
    zip_code: type.STRING,
    country: type.STRING,
    dob: type.DATE,
    is_deleted: type.BOOLEAN,
});