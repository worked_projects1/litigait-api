const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateCreateOrders = function (data) {
    const rules = {
        order_date: 'required|date',
        status: 'required|min:4|max:64',
        practice_id: 'required|min:4|max:64',
        case_id: 'required|min:4|max:64',
        document_id: 'min:4|max:64',
        document_type: 'required|min:3|max:64',
        amount_charged: 'required',
        charge_id: 'required|min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateGetOneOrder = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateUpdateOrders = function (data) {
    const rules = {
        order_date: 'date',
        status: 'min:4|max:64',
        practice_id: 'min:4|max:64',
        case_id: 'min:4|max:64',
        document_id: 'min:4|max:64',
        document_type: 'min:3|max:64',
        amount_charged: 'min:4|max:64',
        charge_id: 'min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDeleteOrder = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateReadOrders = function (data) {
    const rules = {
        offset: 'numeric',
        limit: 'numeric',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
