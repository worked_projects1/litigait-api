module.exports = (sequelize, type) => sequelize.define('Orders', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    order_date: type.DATE,
    status: type.STRING, // completed, pending
    practice_id: type.STRING,
    user_id: type.STRING,
    case_id: type.STRING,
    client_id: type.STRING,
    legalforms_id: type.STRING,
    propoundforms_id: type.STRING,
    filename: type.STRING,
    document_id: type.STRING,
    document_type: type.STRING, // (FROGS, SPROGS, RFPD, RFA)
    document_generation_type: type.STRING, // template or final_doc
    amount_charged: type.FLOAT,
    charge_id: type.STRING,
    is_deleted: type.BOOLEAN,
    client_name: type.STRING,
    case_title: type.STRING,
    order_id: {
        type: type.INTEGER,
        autoIncrement: true,
        allowNull: false,
        unique: true,
    },
    pages: type.INTEGER,
    plan_type: type.STRING,
},
{
    indexes: [
        {
            name: 'created_doc_gen_type',
            fields: ['createdAt', 'document_generation_type']
        },
        {
            name: 'document_type',
            fields: ['document_type']
        },
        {
            name: 'docgen_type',
            fields: ['document_generation_type']
        },
    ]
});
