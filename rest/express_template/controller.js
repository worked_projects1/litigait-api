const uuid = require('uuid');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const {
    validateCreateCustomerObjections,
    validateGetOneCustomerObjection,
    validateUpdateCustomerObjections,
    validateDeleteCustomerObjection,
    validateReadCustomerObjections,
} = require('./validation');

const create = async (event) => {
    try {
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (event.user.practice_id) {
            input.practice_id = event.user.practice_id;
        }
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, {id: id || uuid.v4()});
        validateCreateCustomerObjections(dataObject);
        const {CustomerObjections} = await connectToDatabase();
        const customerObjectionss = await CustomerObjections.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionss),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the customerObjectionss.'}),
        };
    }
};


const getOne = async (event) => {
    try {
        validateGetOneCustomerObjection(event.pathParameters);
        const {CustomerObjections} = await connectToDatabase();
        const customerObjectionss = await CustomerObjections.findOne({where: {id: event.pathParameters.id}});
        if (!customerObjectionss) throw new HTTPError(404, `CustomerObjections with id: ${event.pathParameters.id} was not found`);
        const plainCustomerObjection = customerObjectionss.get({plain: true});
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainCustomerObjection),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the CustomerObjections.'}),
        };
    }
};

const getAll = async (event) => {
    try {
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query.order = [
            ['createdAt', 'DESC'],
        ];
        validateReadCustomerObjections(query);
        const {CustomerObjections} = await connectToDatabase();
        const customerObjectionsss = await CustomerObjections.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionsss),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the customerObjectionsss.'}),
        };
    }
};

const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        validateUpdateCustomerObjections(input);
        const {CustomerObjections} = await connectToDatabase();
        const customerObjectionss = await CustomerObjections.findOne({where: {id: event.pathParameters.id}});
        if (!customerObjectionss) throw new HTTPError(404, `CustomerObjections with id: ${event.pathParameters.id} was not found`);
        const updatedModel = Object.assign(customerObjectionss, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the CustomerObjections.'}),
        };
    }
};

const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateDeleteCustomerObjection(params);
        const {CustomerObjections} = await connectToDatabase();
        const customerObjectionsDetails = await CustomerObjections.findOne({where: {id: params.id}});
        if (!customerObjectionsDetails) throw new HTTPError(404, `CustomerObjections with id: ${params.id} was not found`);
        await customerObjectionsDetails.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionsDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could destroy fetch the CustomerObjections.'}),
        };
    }
};

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;
