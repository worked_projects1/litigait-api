const uuid = require('uuid');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const { dateDiffInDays } = require('../../utils/timeStamp');
const subscriptionPlans = [
    "monthly",
    "yearly",
    "propounding_monthly_199",
    "propounding_yearly_2199",
    "responding_monthly_349",
    "responding_monthly_495",
    "responding_yearly_3490",
    "responding_yearly_5100",
  ];

const create = async (event) => {
    try {
        const { DiscountCode, Op } = await connectToDatabase();

        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (!input.discount_code) throw new HTTPError(400, `Discount code or percentage is missing.`);
        if (!input.plan_type) throw new HTTPError(400, `Plantype not found.`);
        if (!input.discount_percentage) throw new HTTPError(400, `Offer percentage not found.`);

        const applicablePlans = input.plan_type.split(',');
        const is_payablePlans = applicablePlans.some(e => subscriptionPlans.includes(e));
        let name = input.discount_code.split(' ').join('').toUpperCase();
        const discountCodeDeatails = await DiscountCode.findOne({ where: { name: name }, raw: true });
        if (discountCodeDeatails?.id) throw new HTTPError(400, `Discount code ${input.discount_code} already exists.`);

        const dataObject = Object.assign(input, { id: uuid.v4(), name: name, discount_code: name, discount_percentage: input.discount_percentage, createdBy: event.user.id });
        if (is_payablePlans) {
            const couponName = input.discount_code.split(' ').join('').toLowerCase();
            let coupon_id = couponName.split(' ').join('-');

            const coupon = await stripe.coupons.create({
                id: coupon_id,
                name: name,
                percent_off: input.discount_percentage,
                duration: 'forever',
            });
            dataObject.discount_code = name.toUpperCase();
            dataObject.stripe_obj = JSON.stringify(coupon);
        }

        const discountCodeObj = await DiscountCode.create(dataObject);
        const plaintext = discountCodeObj.get({ plain: true });
        plaintext?.stripe_obj ? JSON.parse(plaintext.stripe_obj) : null;

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plaintext),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the discount.' }),
        };
    }
}
const getAll = async (event) => {
    try {
        const {DiscountCode, Op} = await connectToDatabase();

        const headers = event.headers;
        const query = {};
        if (headers.offset) query.offset = parseInt(headers.offset, 10);
        if (headers.limit) query.limit = parseInt(headers.limit, 10);
        query.where = {};
        query.where.is_deleted = {[Op.not]:true};
        query.order = [['createdAt', 'DESC']];
        const discountCode = await DiscountCode.findAll(query);
        let discountdatas = discountCode.map(e => {
            e.stripe_obj = e?.stripe_obj ? JSON.parse(e.stripe_obj) : null;
            return e;
        });
        const discountCodeCount = await DiscountCode.count();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': discountCodeCount

            },
            body: JSON.stringify(discountdatas),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the discount details.'}),
        };
    }
}
const getOne = async (event) => {
    try {
        const {DiscountCode, Op} = await connectToDatabase();
        const params = event.params || event.pathParameters;
        const discountCodeDeatails = await DiscountCode.findOne({where: {id: params.id, is_deleted : {[Op.not]:true}}, raw: true});
        if (!discountCodeDeatails) throw new HTTPError(400, `Discount code id ${params.id} was not found.`);
        if(discountCodeDeatails?.stripe_obj){
            discountCodeDeatails.stripe_obj = JSON.parse(discountCodeDeatails.stripe_obj);
        }
        
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(discountCodeDeatails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch discount details.'}),
        };
    }
}
const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {DiscountCode, Op} = await connectToDatabase();
        const params = event.params || event.pathParameters;
        const discountCodeDeatails = await DiscountCode.findOne({where: {id: params.id, is_deleted: {[Op.not]:true}}});
        if (!discountCodeDeatails) throw new HTTPError(400, `Discount code id ${params.id} was not found.`);
        const updateObj = Object.assign(discountCodeDeatails,{plan_type : input.plan_type});
        const updateRow = await updateObj.save();
        const plaintext = updateRow.get({plain: true});

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plaintext),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch discount details.'}),
        };
    }
}
const destroy = async (event) => {
    try {
        const {DiscountCode, Op} = await connectToDatabase();
        const params = event.params || event.pathParameters;
        const discountCodeObj = await DiscountCode.findOne({where: {id: params.id,is_deleted:{[Op.not]:true}}});
        if (!discountCodeObj) throw new HTTPError(400, `discount code with ${event.user.id} was not found.`);
        if(discountCodeObj?.stripe_obj){
            let stripe_obj = JSON.parse(discountCodeObj.stripe_obj);
            console.log(stripe_obj);
            const deletedObj = await stripe.coupons.del(stripe_obj.id);
            if(!deletedObj.deleted) throw new HTTPError(400, `Could not delete the coupon code.`);
        }        
        await discountCodeObj.destroy();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: "discount code destroyed"}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: "could not destroy discount code."}),
        };
    }
}
const validateDiscountCode = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {DiscountCode, Op, Practices, Plans, Subscriptions} = await connectToDatabase();
        const params = event.query;
        const discountCodeDeatails = await DiscountCode.findOne({where: {discount_code: params.code}, raw: true,logging:console.log});
        if (!discountCodeDeatails) throw new HTTPError(400, `Discount code ${params.code} was not found.`);
        const is_eligible_for = discountCodeDeatails.plan_type.split(',');
        
        const discount_details = [];

        
        const plansdetails = await Plans.findOne({where:{plan_id:input.plan_id,active:true},raw:true});
        const monthlyPropounding = await Plans.findOne({where:{plan_type:'propounding_monthly_199'},raw:true});
        const yearlyPropounding = await Plans.findOne({where:{plan_type:'propounding_yearly_2199'},raw:true});
        if(!plansdetails) throw new HTTPError(400, `Invalid plan detail. please check`);
        let type, actual_price, discount_price, discount_percentage, actual_discount_price;
        discount_percentage = discountCodeDeatails.discount_percentage;
        const percentage = 1 - (discountCodeDeatails.discount_percentage/100);  
        if(['responding'].includes(params.type)){
            if(is_eligible_for.includes(plansdetails.plan_type)){            
                type = plansdetails.plan_type;
                actual_price = plansdetails.price;
                let discount_price = (percentage * actual_price).toFixed(2).toString(); 
                let discount_info = Object.assign({}, { type, actual_price, discount_price, discount_percentage, actual_discount_price: discount_price });
                if(discount_info?.type)discount_details.push(discount_info);
            }
            if(is_eligible_for.includes('activation_fee')){
                actual_price = 249;
                type = 'activation_fee';
                discount_price = Math.ceil((percentage * actual_price).toFixed(2)).toString();
                let discount_info = Object.assign({}, { type, actual_price, discount_price, discount_percentage, actual_discount_price: discount_price });
                if(discount_info?.type)discount_details.push(discount_info);
            }
            if(!is_eligible_for.includes(plansdetails.plan_type) && !is_eligible_for.includes('activation_fee')){
                throw new HTTPError(400, `Invalid coupon code. please check`);
            }
        }else if(['propounding'].includes(params.type)){
            const subscriptionDeatils = await Subscriptions.findOne({
                where: {
                    practice_id: event.user.practice_id, plan_category: 'responding', plan_id: { [Op.not]: null }, stripe_product_id: { [Op.not]: null },
                }, raw: true, logging: console.log
            });
            if (!subscriptionDeatils) throw new HTTPError(400, `Responding plan is not found.`);
            const monthly_days = monthlyPropounding.validity;
            const yearly_days = yearlyPropounding.validity;
            const monthly_price = monthlyPropounding.price;
            const yearly_price = yearlyPropounding.price;
            const current_date = new Date();
            const responding_start_date = new Date(subscriptionDeatils.subscribed_on);
            let days_difference = dateDiffInDays(current_date, responding_start_date);
            let subscription_price = 0;
            if (is_eligible_for.includes(plansdetails.plan_type)) {
                if ((plansdetails.plan_type == 'propounding_monthly_199' && days_difference == 0) || (plansdetails.plan_type == 'propounding_yearly_2199' && days_difference == 0)) {
                    actual_discount_price = discount_price = (percentage * plansdetails.price).toFixed(2).toString();
                    actual_price = plansdetails.price;
                }else if (plansdetails.plan_type == 'propounding_monthly_199' && days_difference > 0 ) {
                    days_difference = monthly_days - days_difference;
                    actual_price = Math.ceil((monthly_price / monthly_days) * days_difference);
                    discount_price = subscription_price = Math.ceil( actual_price * percentage);
                    actual_discount_price = (percentage * plansdetails.price).toFixed(2).toString();
                } else if (plansdetails.plan_type == 'propounding_yearly_2199' && days_difference > 0) {

                    days_difference = yearly_days - days_difference;
                    actual_price = Math.ceil((yearly_price / yearly_days) * days_difference);
                    discount_price = subscription_price = Math.ceil( actual_price * percentage);
                    actual_discount_price = (percentage * plansdetails.price).toFixed(2).toString();
                }
                let discount_info = Object.assign({}, { 
                    type: plansdetails.plan_type, actual_price, discount_price, discount_percentage, days_difference, actual_discount_price 
                });
                if (discount_info?.type) discount_details.push(discount_info);
            }else{
                throw new HTTPError(400, `Invalid coupon code. please check`);
            }
        }
        
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(discount_details),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'could not find discount code details.'}),
        };
    }
}
const getAllHistory = async (event) => {
    try {
        const {DiscountHistory, Practices, Op} = await connectToDatabase();
        const headers = event.headers;
        const query = {};
        if (headers.offset) query.offset = parseInt(headers.offset, 10);
        if (headers.limit) query.limit = parseInt(headers.limit, 10);
        // query.where = {};
        // query.where.is_deleted = {[Op.not]:true};
        query.raw = true;
        query.order = [['createdAt', 'DESC']];
        const discountCodeHistoryObj = await DiscountHistory.findAll(query);
        for (let i = 0; i < discountCodeHistoryObj.length; i++) {
            const practiceObj = await Practices.findOne({where:{id:discountCodeHistoryObj[i].practice_id},raw:true});
            if(practiceObj?.id){
                discountCodeHistoryObj[i].name = practiceObj.name;
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(discountCodeHistoryObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch discount history details.'}),
        };
    }
}
module.exports.create = create;
module.exports.getAll = getAll;
module.exports.getOne = getOne;
module.exports.destroy = destroy;
module.exports.update = update;
module.exports.getAllHistory = getAllHistory;
module.exports.validateDiscountCode = validateDiscountCode;
