const uuid = require("uuid");
const middy = require("@middy/core");
const doNotWaitForEmptyEventLoop = require("@middy/do-not-wait-for-empty-event-loop");
const connectToDatabase = require("../../db");
const { HTTPError } = require("../../utils/httpResp");
const authMiddleware = require("../../auth");
const AWS = require("aws-sdk");
const s3 = new AWS.S3();
const { sendEmail } = require("../../utils/mailModule");
const { QueryTypes } = require("sequelize");
const {
  validateCreatePracticeTemplates,
  validateModifiedTemplate,
} = require("./validation");

const create = async (event) => {
  try {
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const params = event.query || event.params || event.pathParameters;
    const { PracticesTemplates, Practices, Op } = await connectToDatabase();
    const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
    if (!input) throw new HTTPError(404, "input not found");
    const dataObject = Object.assign(input, {
      id: uuid.v4(),
      practice_id: event.user.practice_id,
      status: "Processing",
      template_type: params.type,
    });

    if (process.env.CODE_ENV == "staging" || process.env.CODE_ENV == "local") {
      email = "k.bhuvaneshwari@rifluxyss.com,aravinthrifluxyss@gmail.com,prithiviraj.rifluxyss@gmail.com";
    } else {
      email = process.env.SUPPORT_EMAIL_RECEIPIENT;
    }
    let type;
    if (params.type == "responding") { type = "Responding" }
    else if (params.type == "propounding") { type = "Propounding" } 
    else{ type = "" }
    await sendEmail(
      email,
      `Custom Template Request - ${practiceObject.name}`,
      `
        User Email : ${event.user.email}<br/>
        Practice Name : ${practiceObject.name}<br/>
        File Name : ${input.file_name}<br/>
        State : ${input.state}<br/>
        File Url : ${input.custom_template}<br/>
        Template type : ${type}<br/>
    `,'EsquireTek'
    );

    const practiceTemplateData = await PracticesTemplates.create(dataObject);

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(practiceTemplateData),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not create practice template details.",
      }),
    };
  }
};

const update = async (event) => {
  try {

    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { PracticesTemplates, Practices, Users, Op } =
      await connectToDatabase();
    const query = event.query || event.pathParameters;
    const params = event.params || event.pathParameters;
    //validateModifiedTemplate(input);
    const existingTemplate = await PracticesTemplates.findOne({
      where: { id: params.id, template_type: query.type },
    });
    if (!existingTemplate)
      throw new HTTPError(
        404,
        `Practice Templatae with id: ${event.pathParameters.id} was not found`
      );
    const updatedModel = Object.assign(existingTemplate, input);
    const updatePractice = await updatedModel.save();
    if (input.status == "Applied") {
      const practiceDetails = await Practices.findOne({
        where: { id: input.practice_id },
      });
      const query = {};
      query.where = {};
      query.where.practice_id = input.practice_id;
      query.where.is_deleted = { [Op.not]: true };
      query.order = [["createdAt", "ASC"]];
      query.raw = true;
      const userDetail = await Users.findOne(query);
      const Body =
        "Hi " +
        practiceDetails.name +
        ",<br><br>" +
        "We have attached the custom document template for your practice. Your documents will be generated using the custom document template which you have uploaded.<br><br>" +
        "Notification from EsquireTek.";
       await sendEmail( userDetail.email, "Your Document Template Is Ready To Use", Body,'EsquireTek');
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(updatePractice),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could Update practiceTemplate Details.",
      }),
    };
  }
};
const getOne = async (event) => {
  try {
    const query = event.query || event.pathParameters;
    const params = event.params || event.pathParameters;
    const { PracticesTemplates, Practices } = await connectToDatabase();
    const PracticesTemplatesObj = await PracticesTemplates.findOne({
      where: { id: params.id, template_type: query.type },
    });
    if (!PracticesTemplatesObj)
      throw new HTTPError(
        404,
        `Practice Template with id: ${params.id} was not found`
      );
    const practicetemplate = PracticesTemplatesObj.get({ plain: true });
    const practice = await Practices.findOne({
      where: { id: practicetemplate.practice_id },
    });
    practicetemplate.practice_name = practice.name;
    if (!practicetemplate.state) { practicetemplate.state = ""; };
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(practicetemplate),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not get  practiceTemplate Details.",
      }),
    };
  }
};
const getAll = async (event) => {
  try {
    const query = event.headers;
    const params = event.query || event.params || event.pathParameters;
    const userDetails = event.user;
    let searchKey = "";
    const sortKey = {};
    let sortQuery = "";
    let searchQuery = "";

    if (!query.offset && !query.limit) {
      return getAllWithOutHeader(event);
    }

    if (userDetails.practice_id) {
      return getAllByPracticeId(event);
    }

    const non_practice_roles = [
      "superAdmin",
      "manager",
      "operator",
      "medicalExpert",
      "QualityTechnician",
    ];
    if (!non_practice_roles.includes(event.user.role))
      throw new HTTPError(500, "Invalid User Details");
    const { sequelize, PracticesTemplates, Practices, Op } =
      await connectToDatabase();

    if (query.offset) query.offset = parseInt(query.offset, 10);
    if (query.limit) query.limit = parseInt(query.limit, 10);
    if (query.search) searchKey = query.search;
    query.raw = true;
    if (!query.where) {
      query.where = {};
    }
    let codeSnip = "";
    let codeSnip2 = "";

    let where = `WHERE PracticesTemplates.is_deleted IS NOT true AND PracticesTemplates.template_type = '${params.type}' `;

    /**Sort**/
    if (query.sort == "false") {
      sortQuery += ` ORDER BY PracticesTemplates.createdAt DESC`;
    } else if (query.sort != "false") {
      query.sort = JSON.parse(query.sort);
      sortKey.column = query.sort.column;
      sortKey.type = query.sort.type;
      if (sortKey.column != "all" && sortKey.column != "" && sortKey.column) {
        sortQuery += ` ORDER BY ${sortKey.column}`;
      }
      if (sortKey.type != "all" && sortKey.type != "" && sortKey.type) {
        sortQuery += ` ${sortKey.type}`;
      }
    }

    /**Search**/
    if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
      searchQuery = ` PracticesTemplates.file_name LIKE '%${searchKey}%' OR PracticesTemplates.state LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%'`;
    }

    if (searchQuery != "" && sortQuery != "") {
      codeSnip =
        where +
        " AND (" +
        searchQuery +
        ")" +
        sortQuery +
        ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where + " AND (" + searchQuery + ")" + sortQuery;
    } else if (searchQuery == "" && sortQuery != "") {
      codeSnip =
        where + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where + sortQuery;
    } else if (searchQuery != "" && sortQuery == "") {
      codeSnip =
        where +
        " AND (" +
        searchQuery +
        ")" +
        ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where + " AND (" + searchQuery + ")";
    } else if (searchQuery == "" && sortQuery == "") {
      codeSnip = where + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where;
    }

    let sqlQueryWithPagination =
      "SELECT PracticesTemplates.id, PracticesTemplates.practice_id, Practices.name AS practice_name, PracticesTemplates.file_name, PracticesTemplates.template_type, PracticesTemplates.custom_template_s3_file_key, PracticesTemplates.modified_template_s3_file_key_frogs, PracticesTemplates.modified_template_s3_file_key_sprogs, PracticesTemplates.modified_template_s3_file_key_rfpd, PracticesTemplates.modified_template_s3_file_key_rfa, PracticesTemplates.custom_template, PracticesTemplates.template, PracticesTemplates.state, PracticesTemplates.modified_template_frogs, PracticesTemplates.modified_template_sprogs, PracticesTemplates.modified_template_rfpd, PracticesTemplates.modified_template_rfa, PracticesTemplates.status, PracticesTemplates.is_deleted, PracticesTemplates.createdAt, PracticesTemplates.updatedAt " +
      "FROM PracticesTemplates " +
      "INNER JOIN Practices ON PracticesTemplates.practice_id = Practices.id " +
      codeSnip;

    let sqlQueryForAllData =
      "SELECT PracticesTemplates.id, PracticesTemplates.practice_id, Practices.name AS practice_name, PracticesTemplates.file_name, PracticesTemplates.template_type, PracticesTemplates.custom_template_s3_file_key, PracticesTemplates.modified_template_s3_file_key_frogs, PracticesTemplates.modified_template_s3_file_key_sprogs, PracticesTemplates.modified_template_s3_file_key_rfpd, PracticesTemplates.modified_template_s3_file_key_rfa, PracticesTemplates.custom_template, PracticesTemplates.template, PracticesTemplates.state, PracticesTemplates.modified_template_frogs, PracticesTemplates.modified_template_sprogs, PracticesTemplates.modified_template_rfpd, PracticesTemplates.modified_template_rfa, PracticesTemplates.status, PracticesTemplates.is_deleted, PracticesTemplates.createdAt, PracticesTemplates.updatedAt " +
      "FROM PracticesTemplates " +
      "INNER JOIN Practices ON PracticesTemplates.practice_id = Practices.id " +
      codeSnip2;

    const dataWithPagination = await sequelize.query(sqlQueryWithPagination, {
      type: QueryTypes.SELECT,
    });

    for (let i = 0; i < dataWithPagination.length; i++) {
      if (!dataWithPagination[i].state) { dataWithPagination[i].state = ""; };
    }

    const getAllData = await sequelize.query(sqlQueryForAllData, {
      type: QueryTypes.SELECT,
    });

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
        "Access-Control-Expose-Headers": "totalPageCount",
        "totalPageCount": getAllData.length,
      },
      body: JSON.stringify(dataWithPagination),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not get All practiceTemplate Details.",
      }),
    };
  }
};

const getAllByPracticeId = async (event) => {
  try {
    const query = event.headers;
    const practice_id = event.user.practice_id;
    let searchKey = "";
    const sortKey = {};
    let sortQuery = "";
    let searchQuery = "";
    const { sequelize, PracticesTemplates, Practices, Users, Op } =
      await connectToDatabase();
    if (query.offset) query.offset = parseInt(query.offset, 10);
    if (query.limit) query.limit = parseInt(query.limit, 10);
    if (query.search) searchKey = query.search;
    query.raw = true;
    if (!query.where) {
      query.where = {};
    }
    let codeSnip = "";
    let codeSnip2 = "";

    const params = event.query || event.params || event.pathParameters;

    const practice_roles = ["lawyer", "paralegal"];
    if (!practice_roles.includes(event.user.role))
      throw new HTTPError(500, "Invalid User Details");

    if (practice_id) {
      const practice_data = await Users.findOne({
        where: {
          practice_id: practice_id,
          is_deleted: { [Op.not]: true },
          role: { [Op.in]: practice_roles },
        },
      });
      if (!practice_data) {
        throw new HTTPError(400, `invalid Practice ${event.user.practice_id} `);
      } else {
        query.where.practice_id = event.user.practice_id;
      }
    }
    if (params && params.status) {
      query.where.status = { [Op.ne]: params.status };
    }

    let where = `WHERE PracticesTemplates.practice_id = '${practice_id}' AND PracticesTemplates.is_deleted IS NOT true AND PracticesTemplates.template_type = '${params.type}' `;

    /**Sort**/
    if (query.sort == "false") {
      sortQuery += ` ORDER BY PracticesTemplates.createdAt DESC`;
    } else if (query.sort != "false") {
      query.sort = JSON.parse(query.sort);
      sortKey.column = query.sort.column;
      sortKey.type = query.sort.type;
      if (sortKey.column != "all" && sortKey.column != "" && sortKey.column) {
        sortQuery += ` ORDER BY ${sortKey.column}`;
      }
      if (sortKey.type != "all" && sortKey.type != "" && sortKey.type) {
        sortQuery += ` ${sortKey.type}`;
      }
    }

    /**Search**/
    if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
      searchQuery = ` PracticesTemplates.file_name LIKE '%${searchKey}%' OR PracticesTemplates.state LIKE '%${searchKey}%'`;
    }

    if (searchQuery != "" && sortQuery != "") {
      codeSnip =
        where +
        " AND (" +
        searchQuery +
        ")" +
        sortQuery +
        ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where + " AND (" + searchQuery + ")" + sortQuery;
    } else if (searchQuery == "" && sortQuery != "") {
      codeSnip =
        where + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where + sortQuery;
    } else if (searchQuery != "" && sortQuery == "") {
      codeSnip =
        where +
        " AND (" +
        searchQuery +
        ")" +
        ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where + " AND (" + searchQuery + ")";
    } else if (searchQuery == "" && sortQuery == "") {
      codeSnip = where + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where;
    }

    let sqlQueryWithPagination =
      "SELECT PracticesTemplates.id, PracticesTemplates.practice_id, Practices.name AS practice_name, PracticesTemplates.file_name, PracticesTemplates.template_type, PracticesTemplates.custom_template_s3_file_key, PracticesTemplates.modified_template_s3_file_key_frogs, PracticesTemplates.modified_template_s3_file_key_sprogs, PracticesTemplates.modified_template_s3_file_key_rfpd, PracticesTemplates.modified_template_s3_file_key_rfa, PracticesTemplates.custom_template, PracticesTemplates.template, PracticesTemplates.state, PracticesTemplates.modified_template_frogs, PracticesTemplates.modified_template_sprogs, PracticesTemplates.modified_template_rfpd, PracticesTemplates.modified_template_rfa, PracticesTemplates.status, PracticesTemplates.is_deleted, PracticesTemplates.createdAt, PracticesTemplates.updatedAt " +
      "FROM PracticesTemplates " +
      "INNER JOIN Practices ON PracticesTemplates.practice_id = Practices.id " +
      codeSnip;

    let sqlQueryForAllData =
      "SELECT PracticesTemplates.id, PracticesTemplates.practice_id, Practices.name AS practice_name, PracticesTemplates.file_name, PracticesTemplates.template_type, PracticesTemplates.custom_template_s3_file_key, PracticesTemplates.modified_template_s3_file_key_frogs, PracticesTemplates.modified_template_s3_file_key_sprogs, PracticesTemplates.modified_template_s3_file_key_rfpd, PracticesTemplates.modified_template_s3_file_key_rfa, PracticesTemplates.custom_template, PracticesTemplates.template, PracticesTemplates.state, PracticesTemplates.modified_template_frogs, PracticesTemplates.modified_template_sprogs, PracticesTemplates.modified_template_rfpd, PracticesTemplates.modified_template_rfa, PracticesTemplates.status, PracticesTemplates.is_deleted, PracticesTemplates.createdAt, PracticesTemplates.updatedAt " +
      "FROM PracticesTemplates " +
      "INNER JOIN Practices ON PracticesTemplates.practice_id = Practices.id " +
      codeSnip2;

    const dataWithPagination = await sequelize.query(sqlQueryWithPagination, {
      type: QueryTypes.SELECT,
    });

    for (let i = 0; i < dataWithPagination.length; i++) {
      if (!dataWithPagination[i].state) { dataWithPagination[i].state = ""; };
    }

    const getAllData = await sequelize.query(sqlQueryForAllData, {
      type: QueryTypes.SELECT,
    });
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
        "Access-Control-Expose-Headers": "totalPageCount",
        "totalPageCount": getAllData.length,
      },
      body: JSON.stringify(dataWithPagination),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not get all practice template details.",
      }),
    };
  }
};

const getAllWithOutHeader = async (event) => {
  try {
    const query = event.queryStringParameters || {};
    const params = event.query || event.params || event.pathParameters;

    if (query.where) query.where = JSON.parse(query.where);
    query.raw = true;

    if (!query.where) {
      query.where = {};
    }

    if (params.status == "Applied") {
      query.where.status = "Applied";
    }

    if (params.type == "responding") {
      query.where.template_type = "responding";
    } else if (params.type == "propounding") {
      query.where.template_type = "propounding";
    } else {
      throw new HTTPError(400, `Practices Templates type was not found`);
    }

    query.where.practice_id = event.user.practice_id;
    query.order = [["createdAt", "DESC"]];
    const { PracticesTemplates, Practices } = await connectToDatabase();
    const practiceTemplateObj = await PracticesTemplates.findAll(query);

    for (let i = 0; i < practiceTemplateObj.length; i++) {
      if (practiceTemplateObj[i].practice_id) {
        const practice = await Practices.findOne({
          where: { id: practiceTemplateObj[i].practice_id },
        });
        practiceTemplateObj[i].practice_name = practice.name;
        if (!practiceTemplateObj[i].state) { practiceTemplateObj[i].state = ""; };
      }
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(practiceTemplateObj),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not get All practiceTemplate Details.",
      }),
    };
  }
};

const destroy = async (event) => {
  try {
    const params = event.params || event.pathParameters;
    const { PracticesTemplates } = await connectToDatabase();
    const PracticesTemplatesDetails = await PracticesTemplates.findOne({
      where: { id: params.id },
    });
    if (!PracticesTemplatesDetails)
      throw new HTTPError(
        404,
        `Practices Templates with id: ${params.id} was not found`
      );
    await PracticesTemplatesDetails.destroy();
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ message: "Template Removed Successfully" }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not get All practiceTemplate Details.",
      }),
    };
  }
};
const getUploadURL = async (input) => {
  let fileExtention = "";
  if (input.file_name) {
    fileExtention = `.${input.file_name.split(".").pop()}`;
  }
  let s3_key = "";
  if (input.document_type) {
    s3_key = `${input.practice_id}/${input.document_type}/${input.file_name}.${input.practice_id}${input.templates_count}.${fileExtention}`;
  } else {
    s3_key = `${input.practice_id}/${input.file_name}.${input.practice_id}${input.templates_count}.${fileExtention}`;
  }
  const s3Params = {
    Bucket: process.env.S3_BUCKET_FOR_TEMPLATES,
    Key: s3_key,
    ContentType: input.content_type,
    ACL: "public-read",
  };

  return new Promise((resolve, reject) => {
    const uploadURL = s3.getSignedUrl("putObject", s3Params);
    resolve({
      uploadURL,
      s3_file_key: s3Params.Key,
      public_url: `https://${s3Params.Bucket}.s3.amazonaws.com/${s3Params.Key}`,
    });
  });
};
const UploadTemplateFile = async (event) => {
  try {
    const input = event.body;
    if (!input.practice_id) {
      input.practice_id = event.user.practice_id;
    }
    if (!input.practice_id)
      throw new HTTPError(404, "Practice Id was not found.");
    const { PracticesTemplates } = await connectToDatabase();
    const templatesCount = await PracticesTemplates.count({
      where: { practice_id: input.practice_id },
    });
    input.templates_count = templatesCount;
    const uploadURLObject = await getUploadURL(input);
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        uploadURL: uploadURLObject.uploadURL,
        s3_file_key: uploadURLObject.s3_file_key,
        public_url: uploadURLObject.public_url,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not upload the Practice Templates.",
      }),
    };
  }
};


module.exports.create = create;
module.exports.getAll = getAll;
module.exports.getOne = getOne;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.UploadTemplateFile = UploadTemplateFile;
// module.exports.getAllByPracticeId = getAllByPracticeId;
