module.exports = (sequelize, type) => sequelize.define('PracticesTemplates', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    file_name: type.TEXT,
    custom_template_s3_file_key: type.TEXT, // practice
    modified_template_s3_file_key_frogs: type.TEXT,
    modified_template_s3_file_key_sprogs: type.TEXT,
    modified_template_s3_file_key_rfpd: type.TEXT,
    modified_template_s3_file_key_rfa: type.TEXT,
    custom_template: type.TEXT,
    template: type.TEXT,
    template_type: type.STRING,
    template_s3_file_key: type.TEXT,
    state: type.STRING,
    modified_template_frogs: type.TEXT,
    modified_template_sprogs: type.TEXT,
    modified_template_rfpd: type.TEXT,
    modified_template_rfa: type.TEXT,
    status: type.STRING,  //processing/applied - applied when having values in modified_template
    is_deleted: type.BOOLEAN,
});