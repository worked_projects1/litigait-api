const uuid = require('uuid');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const authMiddleware = require('../../auth');


const getSettings = async (event) => {
    try {
        const input = event.query;
        const {PracticeSettings, Settings} = await connectToDatabase();
        let settingsObject = await PracticeSettings.findOne({
            where: {
                practice_id: input.practice_id,
            }
        });
        if (!settingsObject) {
            settingsObject = await Settings.findOne({
                where: {
                    key: 'global_settings',
                }
            });
        }
        let settings;
        if (settingsObject) {
            const plainSettingsObject = settingsObject.get({plain: true});
            settings = JSON.parse(plainSettingsObject.value);
            if (!settings.medical_history_per_page_price) {
                settings.medical_history_per_page_price = 0;
            }
            if (!settings.medical_history_pricing_tier) {
                settings.medical_history_pricing_tier = [];
            }
            if (!settings.no_of_medical_history_free_pages) {
                settings.no_of_medical_history_free_pages = 0;
            }
            if (!settings.medical_history_minimum_pricing) {
                settings.medical_history_minimum_pricing = 0;
            }
            if (!settings.billing_type) {
                settings.billing_type = 'global';
            }
        } else {
            settings = {
                no_of_docs_free_tier: 0,
                template_generation_price: {
                    FROGS: 0,
                    SPROGS: 0,
                    RFPD: 0,
                    RFA: 0,
                },
                final_doc_generation_price: {
                    FROGS: 0,
                    SPROGS: 0,
                    RFPD: 0,
                    RFA: 0,
                },
                medical_history_per_page_price: 0,
                no_of_medical_history_free_pages: 0,
                medical_history_minimum_pricing: 0,
                session_timeout: 0,
                billing_type: 'global',
            };
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(settings),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Practices.'}),
        };
    }
};


const setSettings = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {PracticeSettings} = await connectToDatabase();
        const settingsObject = await PracticeSettings.findOne({
            where: {
                practice_id: input.practice_id,
            }
        });
        if (settingsObject) {
            settingsObject.value = JSON.stringify(input);
            await settingsObject.save();
        } else {
            const dataObject = Object.assign({
                practice_id: input.practice_id,
                value: JSON.stringify(input),
            }, {id: uuid.v4()});
            await PracticeSettings.create(dataObject);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(input),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Practices.'}),
        };
    }
};

module.exports.getSettings = getSettings;
module.exports.setSettings = setSettings;
