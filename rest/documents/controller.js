const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const {
    validateCreateDocuments,
    validateGetOneDocument,
    validateUpdateDocuments,
    validateDeleteDocument,
    validateReadDocuments,
} = require('./validation');

const create = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, {id: id || uuid.v4()});
        validateCreateDocuments(dataObject);
        const {Documents} = await connectToDatabase();
        const documents = await Documents.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(documents),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the documents.'}),
        };
    }
};

const getOne = async (event) => {
    try {
        validateGetOneDocument(event.pathParameters);
        const {Documents} = await connectToDatabase();
        const documents = await Documents.findOne({where: {id: event.pathParameters.id}});
        if (!documents) throw new HTTPError(404, `Documents with id: ${event.pathParameters.id} was not found`);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(documents),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Documents.'}),
        };
    }
};

const getAll = async (event) => {
    try {
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        validateReadDocuments(query);
        const {Documents} = await connectToDatabase();
        const documentss = await Documents.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(documentss),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the documentss.'}),
        };
    }
};

const update = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateUpdateDocuments(input);
        const {Documents} = await connectToDatabase();
        const documents = await Documents.findOne({where: {id: event.pathParameters.id}});
        if (!documents) throw new HTTPError(404, `Documents with id: ${event.pathParameters.id} was not found`);
        const updatedModel = Object.assign(documents, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Documents.'}),
        };
    }
};

const destroy = async (event) => {
    try {
        validateDeleteDocument(event.pathParameters);
        const {Documents} = await connectToDatabase();
        const documents = await Documents.findOne({where: {id: event.pathParameters.id}});
        if (!documents) throw new HTTPError(404, `Documents with id: ${event.pathParameters.id} was not found`);
        await documents.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(documents),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could destroy fetch the Documents.'}),
        };
    }
};

module.exports.create = middy(create).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = middy(getAll).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());

