const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const AWS = require('aws-sdk');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const phoneUtil = require("google-libphonenumber").PhoneNumberUtil.getInstance();
const { sendEmail, sendEmailwithCC } = require("../../utils/mailModule");
AWS.config.update({ region: process.env.REGION || 'us-east-1' });
const s3 = new AWS.S3();
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const { QueryTypes } = require('sequelize');
const {
    validateCreateLegalForms,
    validateGetOneLegalForms,
    validateUpdateLegalForms,
    validateDeleteLegalForms,
    validateReadLegalForms,
    validateFilename,
    validateDeleteFormsDetails,
    validateResponseDate
} = require('./validation');
const { includes } = require('lodash');

const create = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4() });
        validateCreateLegalForms(dataObject);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(legalForms),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the legalForms.' }),
        };
    }
};
const getOne = async (event) => {
    try {
        validateGetOneLegalForms(event.pathParameters);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.findOne({ where: { id: event.pathParameters.id } });
        if (!legalForms) throw new HTTPError(404, `LegalForms with id: ${event.pathParameters.id} was not found`);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(legalForms),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the LegalForms.' }),
        };
    }
};
const getAll = async (event) => {
    try {
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        validateReadLegalForms(query);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(legalForms),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the legalForms.' }),
        };
    }
};
const update = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateUpdateLegalForms(input);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.findOne({ where: { id: event.pathParameters.id } });
        if (!legalForms) throw new HTTPError(404, `LegalForms with id: ${event.pathParameters.id} was not found`);
        const updatedModel = Object.assign(legalForms, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the LegalForms.' }),
        };
    }
};
const destroy = async (event) => {
    try {
        validateDeleteLegalForms(event.pathParameters);
        const { LegalForms } = await connectToDatabase();
        const legalForms = await LegalForms.findOne({ where: { id: event.pathParameters.id } });
        if (!legalForms) throw new HTTPError(404, `LegalForms with id: ${event.pathParameters.id} was not found`);
        await legalForms.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(legalForms),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy the LegalForms' }),
        };
    }
};
const getUploadURL = async (input, legalForms) => {
    let fileExtention = '';
    let documentType = '';
    if (input.file_name) {
        fileExtention = `.${input.file_name.split('.').pop()}`;
    }
    if (input.document_type) {
        documentType = `_${input.document_type}`;
    }
    const s3Params = {
        Bucket: process.env.S3_BUCKET_FOR_FORMS,
        Key: `${legalForms.id}${documentType}${fileExtention}`,
        ContentType: input.content_type,
        ACL: 'private',
        Metadata: {
            legal_form_id: legalForms.id,
            practice_id: legalForms.practice_id,
            case_id: legalForms.case_id,
            client_id: legalForms.client_id,
        },
    };
    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
        });
    });
};
const uploadLegalForm = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4(), status: 'pending' });
        validateCreateLegalForms(dataObject);
        const { LegalForms, Forms, OtherPartiesForms } = await connectToDatabase();
        const existing_legalforms_id = input.legalforms_id;
        if (existing_legalforms_id) {
            const existingLegalForm = await LegalForms.findOne({
                where: {
                    id: existing_legalforms_id,
                    practice_id: input.practice_id,
                    case_id: input.case_id,
                    client_id: input.client_id,
                }, logging: console.log, raw: true
            });
            if (existingLegalForm) {
                await Forms.destroy({
                    where: {
                        legalforms_id: existingLegalForm.id,
                        client_id: input.client_id,
                        case_id: input.case_id
                    }, logging: console.log
                });
                await OtherPartiesForms.destroy({
                    where: {
                        legalforms_id: existingLegalForm.id,
                        practice_id: input.practice_id,
                        case_id: input.case_id,
                    }, logging: console.log
                });
                await LegalForms.destroy({
                    where: {
                        id: existingLegalForm.id,
                        practice_id: input.practice_id,
                        case_id: input.case_id,
                        client_id: input.client_id,
                    }, logging: console.log
                });
            }
        }
        const legalForms = await LegalForms.create(dataObject);
        uploadURLObject = await getUploadURL(input, legalForms);
        legalForms.s3_file_key = uploadURLObject.s3_file_key;
        await legalForms.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                legalForms,
                uploadURL: uploadURLObject.uploadURL,
                s3_file_key: uploadURLObject.s3_file_key,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
};

const createLegalformDetailsforHelpRequest = async (event) => {
    try {
        const input = JSON.parse(event.body);

        const dataObject = Object.assign(input, { id: uuid.v4(), status: 'pending' });
        validateCreateLegalForms(dataObject);
        const { LegalForms, Forms, OtherPartiesForms } = await connectToDatabase();

        let existingLegalformID = input.legalforms_id;
        if (existingLegalformID) {
            await Forms.destroy({ where: { legalforms_id: existingLegalformID.id } });
            await OtherPartiesForms.destroy({ where: { legalforms_id: existingLegalformID.id } });
            await LegalForms.destroy({ where: { id: existingLegalformID.id } });
        }
        const legalForms = await LegalForms.create(dataObject);
        uploadURLObject = await getUploadURL(input, legalForms);
        legalForms.s3_file_key = uploadURLObject.s3_file_key;
        await legalForms.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                legalForms,
                uploadURL: uploadURLObject.uploadURL,
                s3_file_key: uploadURLObject.s3_file_key,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }

};
const saveFormData = async (event) => {
    try {
        const { Forms, LegalForms, HelpRequest, OtherPartiesForms, OtherParties, Users, Op } = await connectToDatabase();
        const input = JSON.parse(event.body);
        console.log(input);
        const queryParams = event.queryStringParameters || {};
        const legalForms = await LegalForms.findOne({
            where: {
                id: event.pathParameters.id,
                client_id: input.client_id,
                document_type: input.document_type
            }, logging: console.log
        });
        const query = {};
        query.raw = true;
        const question_encode = queryParams.question_encode;

        const formsBulkData = [];
        const otherPartiesFormsBulkData = [];


        if (!legalForms) throw new HTTPError(404, `LegalForms with id: ${event.pathParameters.id} was not found`);
        if (input.plaintiff_name) {
            legalForms.plaintiff_name = input.plaintiff_name;
        }
        if (input.defendant_practice_details) {
            legalForms.defendant_practice_details = input.defendant_practice_details;
        }
        if (input.defendant_name) {
            legalForms.defendant_name = input.defendant_name;
        }
        if ((input.plaintiff_name && input.defendant_name) || (input.defendant_practice_details)) {
            await legalForms.save();
        }

        const otherPartiesModel = await OtherParties.findAll({
            where: {
                case_id: legalForms.case_id,
                practice_id: legalForms.practice_id
            }
        });
        for (let i = 0; i < input.questions.length; i += 1) {
            let question_text = '';
            if (question_encode == 'true') {
                question_text = Buffer.from(input.questions[i].question_text, 'base64').toString('utf8')
            } else {
                question_text = input.questions[i].question_text;
            }
            let question_id = uuid.v4();
            const dataObject = {
                id: question_id,
                case_id: legalForms.case_id,
                practice_id: legalForms.practice_id,
                client_id: legalForms.client_id,
                legalforms_id: event.pathParameters.id,
                document_type: input.document_type, // FROGS, SPROGS, RFPD, RFA
                question_type: input.questions[i].question_type, // only for initial discloser forms (FROGS, SPROGS, RFPD, RFA)
                question_number: parseFloat(input.questions[i].question_number),
                question_number_text: input.questions[i].question_number,
                question_id: parseFloat(input.questions[i].question_id),
                question_text: question_text,
                question_section_id: input.questions[i].question_section_id,
                question_section: input.questions[i].question_section,
                question_section_text: input.questions[i].question_section_text,
                question_options: JSON.stringify(input.questions[i].question_options),
                is_consultation_set: input.questions[i].is_consultation_set,
                consultation_set_no: input.questions[i].consultation_set_no,
                lawyer_response_status: 'NotStarted', // (NotStarted, Draft, Final)
                client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
            };

            formsBulkData.push(dataObject);
            if (otherPartiesModel.length != 0) {
                console.log('--Inside Other parties--');
                // for(let n=0;n<otherPartiesModel.length;n++)
                for (let n = 0; n < otherPartiesModel.length; n++) //4
                {
                    let otherPartiesFormRow = Object.assign({}, dataObject, {
                        form_id: question_id,
                        id: uuid.v4(),
                        party_id: otherPartiesModel[n].id
                    });
                    otherPartiesFormsBulkData.push(otherPartiesFormRow);
                    // otherPartiesFormsBulkData[otherPartiesFormsBulkData.length] = otherPartiesFormRow;
                }
            }
        }
        await Forms.bulkCreate(formsBulkData);
        if (otherPartiesFormsBulkData.length > 0) {
            await OtherPartiesForms.bulkCreate(otherPartiesFormsBulkData);
        }
        const adminRoles = ['superAdmin', 'QualityTechnician'];
        if (input.action_by && adminRoles.includes(input.action_by)) {
            const helpRequestObject = await HelpRequest.findOne({
                where: {
                    case_id: legalForms.case_id,
                    document_type: input.document_type
                }
            });
            if (helpRequestObject) {
                const userObject = await Users.findOne({
                    where: {
                        email: helpRequestObject.user_email,
                        is_deleted: { [Op.not]: true }
                    }
                });
                if (userObject) {
                    let template = 'Hi';
                    if(userObject?.name) template = template+' '+userObject?.name;
                    await sendEmail(userObject.email, 'Important: Questions updated for your case', `${template},<br/>
          Your case, ${helpRequestObject.case_title} has been updated with the questions. <br/>
          Notification from EsquireTek`);
                    await HelpRequest.destroy({
                        where: {
                            case_id: legalForms.case_id,
                            document_type: input.document_type,
                            client_id: input.client_id,
                            legalforms_id: input.legalforms_id
                        }
                    });
                }
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                legal_form_id: event.pathParameters.id,
                message: 'Form data processing complete',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the LegalForms.' }),
        };
    }
};

const saveGeneratedDocument = async (event) => {
    try {
        const { LegalForms, Orders, Practices, Settings, Cases, Clients, Op, PracticeSettings, Subscriptions, Plans } = await connectToDatabase();
        let validSubscriptionFeatures = [];
        const existingSubscriptionDetails = await Subscriptions.findOne({
            where: { practice_id: event.user.practice_id, plan_category: 'responding' },
            order: [['createdAt', 'DESC']],
            logging:console.log,
            raw: true
        });
        if (existingSubscriptionDetails && existingSubscriptionDetails.stripe_subscription_data && existingSubscriptionDetails.plan_id) {
            const stripeSubscriptionDataObject = JSON.parse(existingSubscriptionDetails.stripe_subscription_data);
            console.log(stripeSubscriptionDataObject);
            const subscriptionValidity = new Date(parseInt(stripeSubscriptionDataObject.current_period_end) * 1000);
            console.log(subscriptionValidity);
            const today = new Date();
            console.log(today);
            if (subscriptionValidity > today) {
                console.log('inside checking plans');
                const planDetails = await Plans.findOne({
                    raw: true,
                    where: { stripe_product_id: existingSubscriptionDetails.stripe_product_id },
                });
                validSubscriptionFeatures = planDetails.features_included.split(',');
            }
        }
        const input = JSON.parse(event.body);

        let plan_type;
        if (input.plan_type == 'free_trial') {
            plan_type = 'FREE TRIAL';
        } else if (input.plan_type == 'pay_as_you_go' || input.plan_type == 'tek_as_you_go') {
            plan_type = 'TEK AS-YOU-GO';
        }else if(['responding_monthly_349','responding_monthly_495','monthly'].includes(input.plan_type)){
            plan_type= 'MONTHLY';
        }else if(['responding_yearly_5100','responding_yearly_3490','yearly'].includes(input.plan_type)){
            plan_type= 'YEARLY';
        }else if(['propounding_monthly_199'].includes(input.plan_type)){
            plan_type= 'MONTHLY PROPOUNDING';
        }else if(['propounding_yearly_2199'].includes(input.plan_type)){
            plan_type= 'YEARLY PROPOUNDING';
        }

        if (!plan_type) throw new HTTPError(400, `Plan Type not found.`);
        if (!input.legalforms_id) throw new HTTPError(400, 'Legalform id was not found');
        const legalForm = await LegalForms.findOne({ where: { id: input.legalforms_id } });

        if (!legalForm) throw new HTTPError(400, 'Relevant previously generated legal form data not found');

        if (input.document_generation_type === 'final') {
            input.document_generation_type = 'final_doc';
        }

        if (input.document_generation_type === 'final_doc') {
            legalForm.final_document = input.final_document;
            legalForm.final_document_s3_key = input.final_document_s3_key;
        } else if (input.document_generation_type === 'pos') {
            legalForm.pos_document = input.pos_document;
            legalForm.pos_document_s3_key = input.pos_document_s3_key;
        } else {
            input.document_generation_type = 'template';
            legalForm.generated_document = input.generated_document;
            legalForm.generated_document_s3_key = input.generated_document_s3_key;
        }
        legalForm.serving_attorney_name = input.serving_attorney_name;
        legalForm.serving_attorney_street = input.serving_attorney_street;
        legalForm.serving_attorney_city = input.serving_attorney_city;
        legalForm.serving_attorney_state = input.serving_attorney_state;
        legalForm.serving_attorney_zip_code = input.serving_attorney_zip_code;
        legalForm.serving_attorney_email = input.serving_attorney_email;
        legalForm.serving_date = input.serving_date;

        const CaseDetails = await Cases.findOne({ where: { id: input.case_id } });
        const clientDetails = await Clients.findOne({ where: { id: CaseDetails.client_id } });

        const orderData = {
            id: uuid.v4(),
            order_date: new Date(),
            status: 'completed', // completed, pending
            practice_id: event.user.practice_id,
            user_id: event.user.id,
            case_id: input.case_id,
            client_id: CaseDetails.client_id,
            document_type: input.document_type, // (FROGS, SPROGS, RFPD, RFA)
            document_generation_type: input.document_generation_type, // template,pos or final_doc
            amount_charged: 0,
            charge_id: '',
            case_title: CaseDetails.case_title,
            client_name: clientDetails.name,
            legalforms_id: input.legalforms_id,
            filename: legalForm.filename,
            plan_type: plan_type
        };

        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        const settingsObject = await Settings.findOne({ where: { key: 'global_settings' }, raw: true, });

        if (!settingsObject) throw new HTTPError(400, 'Settings data not found');
        let settings = JSON.parse(settingsObject.value);

        const practiceSettings = await PracticeSettings.findOne({ where: { practice_id: event.user.practice_id, }, raw: true, });
        if (practiceSettings) {
            const practiceSettingsObject = JSON.parse(practiceSettings.value);
            settings = Object.assign(settings, practiceSettingsObject);
        }

        const document_type = input.document_type;
        const document_generation_type = input.document_generation_type;

        const freeQuotaUseageCount = await Orders.count({
            where: {
                practice_id: event.user.practice_id,
                document_type: { [Op.in]: ['FROGS', 'SPROGS', 'RFPD', 'RFA'] },
                amount_charged: 0,
            }
        });

        let free_tier_available = true;
        if (freeQuotaUseageCount >= parseInt(settings.no_of_docs_free_tier, 10)) {
            free_tier_available = false;
        }

        let price = 0;
        if (document_generation_type == 'pos' && settings) {
            if (!settings.pos) throw new HTTPError(400, 'POS Data not found in Settings');
            price = settings.pos;
        } else {
            price = parseFloat(settings[`${document_generation_type}_generation_price`][document_type]);
        }

        let credit_card_details_available = false;

        if (practiceObject && practiceObject.stripe_customer_id) {
            credit_card_details_available = true;
        }
        if (validSubscriptionFeatures.includes('shell')) {
            price = 0;
        }
        if (validSubscriptionFeatures.includes('discovery')) {
            price = 0;
        }
        if (validSubscriptionFeatures.includes('pos')) {
            price = 0;
        }
        console.log(validSubscriptionFeatures);
        console.log('price : '+price);
        console.log('free_tier_available : '+free_tier_available);
        if (free_tier_available || price == 0) {
            await Orders.create(orderData);
        } else {
            if (!practiceObject.stripe_customer_id) {
                throw new HTTPError(400, 'Free tire exceeded but payment method is not added yet');
            }
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });
            if (!paymentMethods.data.length) {
                throw new HTTPError(400, 'Free tire exceeded but payment method not added');
            }

            if (price && price > 0 && price < 0.50) {
                price = 0.50;   //minimum pricing charge
            }

            const paymentMethod = paymentMethods.data[0];
            const paymentIntent = await stripe.paymentIntents.create({
                amount: parseInt(price * 100),
                currency: 'usd',
                customer: practiceObject.stripe_customer_id,
                payment_method: paymentMethod.id,
                off_session: true,
                confirm: true,
            });
            orderData.charge_id = paymentIntent.id;
            orderData.amount_charged = price;
            await Orders.create(orderData);
        }

        await legalForm.save();
        const legalFormRawText = await LegalForms.findOne({ where: { id: input.legalforms_id } });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                legalFormRawText,
                message: 'Generated document saved',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the LegalForms.' }),
        };
    }
};
const deleteForm = async (event) => {
    try {
        const input = JSON.parse(event.body);
        const { Forms, LegalForms, OtherPartiesForms } = await connectToDatabase();

        validateDeleteFormsDetails(input);
        const caseId = input.case_id;
        const legalforms_id = input.legalforms_id;
        const documentType = input.document_type;
        await Forms.destroy({ where: { case_id: caseId, document_type: documentType, legalforms_id: legalforms_id } });
        await LegalForms.destroy({ where: { case_id: caseId, document_type: documentType, id: legalforms_id } });
        await OtherPartiesForms.destroy({
            where: {
                case_id: caseId,
                document_type: documentType,
                legalforms_id: legalforms_id
            }
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'data removed successfully',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy the LegalForms' }),
        };
    }
};
const shredForm = async (event) => {
    try {
        const input = event.body;
        const { LegalForms } = await connectToDatabase();
        const LegalFormsDetails = await LegalForms.findOne({ where: { id: input.id } });
        if (!LegalFormsDetails) throw new HTTPError(404, `LegalForms with id: ${input.id} was not found`);
        if (LegalFormsDetails.final_document) {
            let legalFormsPlainText = LegalFormsDetails.get({plain: true});
            const s3Params = {
                Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
                Key: LegalFormsDetails.final_document_s3_key,
            };
            const aws_status = await s3.deleteObject(s3Params).promise();
            legalFormsPlainText.final_document_s3_key = null;
            legalFormsPlainText.final_document = null;
            if (LegalFormsDetails.pdf_s3_file_key) {
                 await s3.deleteObject({Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS, Key: LegalFormsDetails.pdf_s3_file_key,}).promise();
                 legalFormsPlainText.pdf_s3_file_key = null;
            }
            await LegalForms.update(legalFormsPlainText, { where: {id: input.id} });
        } else {
            throw new HTTPError(404, `Final Doucument not found this Case`);
        }
        const LegalFormsObj = await LegalForms.findOne({ where: { id: input.id } });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(LegalFormsObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the LegaForms.' }),
        };
    }
};
const updateFilename = async (event) => {
    try {
        const input = event.body;
        const { LegalForms } = await connectToDatabase();
        const legalforms_id = event.params.id;
        const filename = input.filename;
        validateFilename(input);
        const existingLegalform = await LegalForms.findAll({
            where: {
                practice_id: event.user.practice_id,
                case_id: input.case_id,
                filename: input.filename,
                document_type: input.document_type
            }
        });
        if (existingLegalform.length) throw new HTTPError(404, `File Name  ${input.filename} has been already existing in this Doument Type `);
        const legalformsObj = await LegalForms.update({ filename: filename }, { where: { id: legalforms_id } });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ success: 'Filename has been updated successfully' }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the LegaForms.' }),
        };
    }
};
const updateFormsResponsedeadlineDate = async (event) => {
    try {
        const input = event.body;
        validateResponseDate(input);
        const { LegalForms, sequelize, Op } = await connectToDatabase();
        if (!input.legalforms_id) throw new HTTPError(404, 'Legalforms id was  not found');
        const updateColumn = {};
        const updateQuery = {};
        updateColumn.response_deadline_startdate = input.start_date;
        updateColumn.response_deadline_enddate = input.end_date;
        updateQuery.id = input.legalforms_id;
        updateQuery.case_id = input.case_id;
        updateQuery.client_id = input.client_id;
        updateQuery.practice_id = input.practice_id;
        const form = await LegalForms.update(updateColumn, { where: updateQuery });
        let sqlQuery = 'SELECT Cases.*,Clients.name as client_name, ' +
            '(SELECT SUM(amount_charged) amount_charged FROM Orders WHERE case_id = ' + "'" + input.case_id + "'" + ' ) AS total_cost, ' +
            '(SELECT MIN(response_deadline_enddate) FROM LegalForms WHERE case_id = ' + "'" + input.case_id + "'" + ' AND date(response_deadline_enddate) >= date(now())) AS due_date ' +
            'FROM (Clients INNER JOIN Cases ON Clients.id = Cases.client_id) WHERE Cases.is_deleted IS NOT true AND Cases.practice_id =' + "'" + input.practice_id + "'" + ' order by createdAt desc limit 1';
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: "ok", due_date: serverData[0].due_date }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Forms.' }),
        };
    }
};

const sendEmailForms = async (event) => {
    try {
        const { LegalForms, Practices, Users, Cases, Op } = await connectToDatabase();

        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;

        const to_opposing_counsel_email_arr = input.to_opposing_counsel_email.split(",");
     
        const attach_documents = input.attach_documents;

        if(attach_documents.length > 0) {
        await LegalForms.update({attach_documents:JSON.stringify(input.attach_documents)},{
            where: {
              id: input.attach_document_id,
              practice_id: event.user.practice_id,
              case_id: input.case_id,
            },
          });
        }
        if(attach_documents.length == 0){
            await LegalForms.update({attach_documents:null},{
                where: {
                  id: input.attach_document_id,
                  practice_id: event.user.practice_id,
                  case_id: input.case_id,
                },
              });
        }

        let email_template,forms_obj;

        const attachments = [];
        const Frogs = [];
        const Rfpd = [];
        const Rfa= [];
        const Sprogs = [];

        const casesObj = await Cases.findOne({
            where: { id: input.case_id },
            is_deleted: { [Op.not]: true },
            logging: console.log,
            raw: true,
        });

        for (let arr = 0; arr < input.selected_forms.length; arr++) {
            if(input.selected_forms[arr].document_type == "frogs"){
                Frogs.push(input.selected_forms[arr]);
            }
            if(input.selected_forms[arr].document_type == "rfpd"){
                Rfpd.push(input.selected_forms[arr]);
            }
            if(input.selected_forms[arr].document_type == "rfa"){
                Rfa.push(input.selected_forms[arr]);
            }
            if(input.selected_forms[arr].document_type == "sprogs"){
                Sprogs.push(input.selected_forms[arr]);
            }
        }
            if (to_opposing_counsel_email_arr.length === 0) throw new HTTPError(400, "No email details found");

            const check_emailIdsExists = await Users.findAll({
                where: {
                    email: { [Op.in]: to_opposing_counsel_email_arr },
                    is_deleted: { [Op.not]: true },
                },
                logging: console.log,
                raw: true,
            });

            email_template = "<ul>";

            for (let j = 0; j < input.selected_forms.length; j++) {

                let email_template_file_name = "";

                forms_obj = await LegalForms.findOne({
                    where: {
                        id: input.selected_forms[j].id,
                        practice_id: event.user.practice_id,
                        case_id: input.case_id,
                    },
                });

                if (!forms_obj) {
                    throw new HTTPError(
                        404,
                        `form with id: ${input.selected_forms[j].id} was not found`
                    );
                }

                let public_url = undefined;

                if (forms_obj.pdf_s3_file_key) {
                    public_url = await s3.getSignedUrlPromise("getObject", {
                        Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
                        Expires: 60 * 60 * 1,
                        Key: forms_obj.pdf_s3_file_key,
                    });
                }

                if (!public_url) throw new HTTPError(400, "cannot generate public url or s3 file key not found");

        let plaintiff_defendant =
          casesObj &&
          casesObj.case_plaintiff_name &&
          casesObj.case_defendant_name;
        let case_defendant_name =
          casesObj &&
          casesObj.case_defendant_name &&
          casesObj.case_defendant_name.toUpperCase();
        let case_plaintiff_name =
          casesObj &&
          casesObj.case_plaintiff_name &&
          casesObj.case_plaintiff_name.toUpperCase();

        if (
          forms_obj &&
          forms_obj.document_type &&
          forms_obj.document_type.toLowerCase() == "frogs"
        ) {
        if(Frogs.length == 1){ input.selected_forms[j].orderId = "" };
          if (plaintiff_defendant) {
            email_template +=
              "<li>" +
              `DEFENDANT ${case_plaintiff_name}'S FORM INTERROGATORIES-GENERAL TO PLAINTIFF ${case_defendant_name} ${Frogs.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}` +
              "</li>";
            if (forms_obj.pdf_s3_file_key) {
              email_template_file_name = `DEFENDANT ${case_plaintiff_name}'S FORM INTERROGATORIES-GENERAL TO PLAINTIFF ${case_defendant_name} ${Frogs.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}.pdf`;
            } 
          } else {
            email_template += "<li>" + `FORM INTERROGATORIES-GENERAL ${Frogs.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}` + `</li>`;
            if (forms_obj.pdf_s3_file_key) {
              email_template_file_name = `FORM INTERROGATORIES-GENERAL ${Frogs.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}.pdf`;
            } 
          }
        }

        if (
          forms_obj &&
          forms_obj.document_type &&
          forms_obj.document_type.toLowerCase() == "sprogs"
        ) {
        if(Sprogs.length == 1){ input.selected_forms[j].orderId = "" };
          if (plaintiff_defendant) {
            email_template +=
              "<li>" +
              `DEFENDANT ${case_plaintiff_name}'S SPECIAL INTERROGATORIES TO PLAINTIFF ${case_defendant_name} ${Sprogs.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}` +
              "</li>";
            if (forms_obj.pdf_s3_file_key) {
              email_template_file_name = `DEFENDANT ${case_plaintiff_name}'S SPECIAL INTERROGATORIES TO PLAINTIFF ${case_defendant_name} ${Sprogs.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}.pdf`;
            } 
          } else {
            email_template += "<li>" + `SPECIAL INTERROGATORIES ${Sprogs.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}` + `</li>`;
            if (forms_obj.pdf_s3_file_key) {
              email_template_file_name = `SPECIAL INTERROGATORIES ${Sprogs.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}.pdf`;
            }
          }
        }

        if (
          forms_obj &&
          forms_obj.document_type &&
          forms_obj.document_type.toLowerCase() == "rfpd"
        ) {
        if(Rfpd.length == 1){ input.selected_forms[j].orderId = "" };
          if (plaintiff_defendant) {
            email_template +=
              "<li>" +
              `DEFENDANT ${case_plaintiff_name}'S REQUEST FOR PRODUCTION OF DOCUMENTS TO PLAINTIFF ${case_defendant_name} ${Rfpd.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}` +
              "</li>";
            if (forms_obj.pdf_s3_file_key) {
              email_template_file_name = `DEFENDANT ${case_plaintiff_name}'S REQUEST FOR PRODUCTION OF DOCUMENTS TO PLAINTIFF ${case_defendant_name} ${Rfpd.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}.pdf`;
            } 
          } else {
            email_template +=
              "<li>" + `REQUEST FOR PRODUCTION OF DOCUMENTS ${Rfpd.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}` + `</li>`;
            if (forms_obj.pdf_s3_file_key) {
              email_template_file_name = `REQUEST FOR PRODUCTION OF DOCUMENTS ${Rfpd.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}.pdf`;
            } 
          }
        }

        if (
          forms_obj &&
          forms_obj.document_type &&
          forms_obj.document_type.toLowerCase() == "rfa"
        ) {
        if(Rfa.length == 1){ input.selected_forms[j].orderId = "" };
          if (plaintiff_defendant) {
            email_template +=
              "<li>" +
              `DEFENDANT ${case_plaintiff_name}'S REQUEST FOR ADMISSION TO PLAINTIFF ${case_defendant_name} ${Rfa.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}` +
              "</li>";
            if (forms_obj.pdf_s3_file_key) {
              email_template_file_name = `DEFENDANT ${case_plaintiff_name}'S REQUEST FOR ADMISSION TO PLAINTIFF ${case_defendant_name} ${Rfa.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}.pdf`;
            } 
          } else {
            email_template += "<li>" + `REQUEST FOR ADMISSION ${Rfa.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}` + `</li>`;
            if (forms_obj.pdf_s3_file_key) {
              email_template_file_name = `REQUEST FOR ADMISSION ${Rfa.length > 1 ? "- FORM ":""}${input.selected_forms[j].orderId}.pdf`;
            } 
          }
        }

                if (forms_obj && !forms_obj.s3_file_key) {
                    throw new HTTPError(400, "Document details not found.");
                }
                forms_obj.email_sent_date = new Date();
                await forms_obj.save();

                let attachments_details = {};

                if (forms_obj.pdf_s3_file_key) {
                    attachments_details.filename = email_template_file_name;
                    attachments_details.href = public_url; // URL of document save in the cloud.
                    attachments_details.contentType = "application/pdf";
                }
                attachments.push(attachments_details);
            }

    for (let q = 0; q < attach_documents.length; q++) {
        let uploadfile = Object.assign({}, {
            filename: attach_documents[q].filename,
            href: attach_documents[q].public_url,
            contentType: attach_documents[q].contentType,
        })
        attachments.push(uploadfile);
    }

    email_template += "</ul>";

            const practice_details = await Practices.findOne({
                where: {
                    is_deleted: { [Op.not]: true },
                    id: event.user.practice_id,
                },
                raw: true,
            });

            const user_details = await Users.findOne({
                where: { id: event.user.id, is_deleted: { [Op.not]: true } },
                raw: true,
            });

            let cc_email = undefined;
            if (input.cc_responder_email) cc_email = input.cc_responder_email;

            let to_email_string = to_opposing_counsel_email_arr;

            let practice_address = practice_details.name + ",";
            if (practice_details.street)
                practice_address += "<br>" + practice_details.street;
            if (practice_details.city)
                practice_address += "<br>" + practice_details.city + ",";
            if (practice_details.state)
                practice_address += "&nbsp;" + practice_details.state;
            if (practice_details.zip_code)
                practice_address += "&nbsp;" + practice_details.zip_code;
            if (practice_details.phone) {
                const Mobile = phoneUtil.parseAndKeepRawInput(
                    practice_details.phone,
                    "US"
                );
                const MobileNumber = phoneUtil.formatInOriginalFormat(Mobile, "US");
                practice_address += "<br>" + "Telephone: " + MobileNumber;
            }
            if (practice_details.fax) {
                const Fax = phoneUtil.parseAndKeepRawInput(
                    practice_details.fax,
                    "US"
                );
                const FaxNumber = phoneUtil.formatInOriginalFormat(Fax, "US");
                practice_address += "<br>" + "Fax: " + FaxNumber;
            }
            if (user_details.email)
                practice_address += "<br>" + "Email: " + user_details.email;

    if (cc_email) {
        await sendEmailwithCC(
            to_email_string,
            cc_email,
            `Responding ${attachments.length > 1 ? "docs" : "doc"} for ${casesObj.case_title}`,
            `Hello,<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Please find attached for electronic service in the above referenced matter the below listed ${attachments.length > 1 ? "documents" : "document"
            }: ${email_template}Please do not hesitate to contact me should you have any questions, comments or concerns.<br><br>Thank you.<br>${practice_address}`,
            practice_details.name,
            attachments
        );
    } else {
        await sendEmail(
            to_email_string,
            `Responding ${attachments.length > 1 ? "docs" : "doc"} for ${casesObj.case_title}`,
            `Hello,<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Please find attached for electronic service in the above referenced matter the below listed ${attachments.length > 1 ? "documents" : "document"
            }: ${email_template}Please do not hesitate to contact me should you have any questions, comments or concerns.<br><br>Thank you.<br>${practice_address}`,
            practice_details.name,
            attachments
        );
    }
        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                status: "Ok",
                message: "Email sent successfully.",
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error:
                    err.message || "could not send propound forms details to responder.",
            }),
        };
    }
}

const saveS3FileKeyForPdf = async (event) => {
    try {
      const { LegalForms, Op } = await connectToDatabase();
      const input =
        typeof event.body === "string" ? JSON.parse(event.body) : event.body;
      const queryParams = event.pathParameters || event.params;

      const formsObj = await LegalForms.findOne({
        where: { id: queryParams.id },
      });

      if (!formsObj)
      throw new HTTPError( 404, `Legal forms id: ${queryParams.id} was not found`);

      //formsObj.s3_file_key = input.s3_file_key;
      if (input.pdf_s3_file_key) {
        formsObj.pdf_s3_file_key = input.pdf_s3_file_key;
      }
      
    const saveObj = await formsObj.save();
    const plaintext = saveObj.get({ plain: true });
    console.log(plaintext);
    plaintext.public_url = await s3.getSignedUrlPromise("getObject", {
      Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
      Expires: 60 * 60 * 1,
      Key: plaintext.pdf_s3_file_key,
    });
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(plaintext),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "could not fetch legal forms details.",
      }),
    };
  }
};

const QuickCreateFormsCreate = async(event)=>{
    try{
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const caseNumber = input?.case_number;
        const {Forms, Op, LegalForms, Cases, DocumentExtractionProgress} = await connectToDatabase();

        const CaseData = await Cases.findOne({where: {
            case_number: caseNumber,
            practice_id: event.user.practice_id,
        },order: [['createdAt', 'DESC']]}); 

        if(!CaseData){ throw new HTTPError(400,'case not found')};
        
        const legalFormsObject = Object.assign({}, {
            id: uuid.v4(),
            client_id: CaseData.client_id,
            case_id: CaseData.id,
            practice_id: event.user.practice_id,
            filename: input.legalform_filename,
            status: 'pending',
            document_type: input.document_type,
            s3_file_key: input.s3_file_key
            
        });
        
        const legalformsObj = await LegalForms.create(legalFormsObject);
        if(!input?.extraction_id) throw new HTTPError(400,'Extraction Id not found.');
        await DocumentExtractionProgress.update({legalforms_id:legalformsObj.id},{where:{id: input.extraction_id}});
    const questions_count = input?.questions?.length;
    const questions = input?.questions;
    for (let i = 0; i < questions_count; i++){
        const formsObject = Object.assign(questions[i],{
            id: uuid.v4(),
            client_id: CaseData.client_id,
            case_id: CaseData.id,
            practice_id: event.user.practice_id,
            document_type: input.document_type,
            legalforms_id: legalformsObj.id,
            lawyer_response_status: "NotStarted",
            client_response_status: "NotSetToClient"
        });
        const formData = await Forms.create(formsObject);
    }

  return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        case_id: legalformsObj.case_id,
        practice_id: legalformsObj.practice_id,
        client_id: legalformsObj.client_id,
        legalforms_id: legalformsObj.id,
        filename: legalformsObj.filename,
        document_type: legalformsObj.document_type,
        state: CaseData.state,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not create forms.",
      }),
    };
  }
}


module.exports.create = middy(create).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = middy(getAll).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.uploadLegalForm = middy(uploadLegalForm).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.saveFormData = saveFormData;
module.exports.saveGeneratedDocument = middy(saveGeneratedDocument).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.deleteForm = middy(deleteForm).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.shredForm = shredForm;
module.exports.updateFilename = updateFilename;
module.exports.updateFormsResponsedeadlineDate = updateFormsResponsedeadlineDate;
module.exports.sendEmailForms = sendEmailForms;
module.exports.saveS3FileKeyForPdf = saveS3FileKeyForPdf;
module.exports.QuickCreateFormsCreate = QuickCreateFormsCreate;