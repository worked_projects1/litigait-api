module.exports = (sequelize, type) => sequelize.define('LegalForms', {
        id: {
            type: type.STRING,
            primaryKey: true,
        },
        practice_id: type.STRING,
        case_id: type.STRING,
        client_id: type.STRING,
        form_id: type.STRING,
        filename: type.STRING,
        plaintiff_name: type.STRING,
        defendant_name: type.STRING,
        defendant_practice_details: type.TEXT,
        status: type.STRING, // pending, complete
        json_data: type.TEXT,
        document_type: type.STRING, // FROGS, SPROGS, RFPD, RFA
        set_number: type.STRING,
        response_deadline_startdate: type.DATEONLY,
        response_deadline_enddate: type.DATEONLY,
        s3_file_key: type.STRING,
        pdf_s3_file_key: type.STRING,
        attach_documents: type.TEXT('long'),
        generated_document: type.TEXT,
        generated_document_s3_key: type.TEXT,
        final_document: type.TEXT,
        final_document_s3_key: type.TEXT,
        pos_document: type.TEXT,
        pos_document_s3_key: type.TEXT,
        serving_attorney_name: type.STRING,
        serving_attorney_street: type.STRING,
        serving_attorney_city: type.STRING,
        serving_attorney_state: type.STRING,
        serving_attorney_zip_code: type.STRING,
        serving_attorney_email: type.STRING,
        serving_date: type.DATE,
    },
    {
        indexes: [
            {
                name: 'practice_document_type_createdAt_legalindex',
                fields: ['practice_id', 'case_id', 'client_id', 'document_type', 'createdAt']
            }
        ]
    });
