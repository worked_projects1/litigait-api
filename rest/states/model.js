module.exports = (sequelize, type) => sequelize.define('States', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    state_name: type.STRING,
    state_code: type.STRING,
    document_type: type.STRING,
    is_deleted: type.BOOLEAN,
});
