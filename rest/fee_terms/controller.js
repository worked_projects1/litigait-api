const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');


const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const authMiddleware = require('../../auth');


const getFeeTerms = async (event) => {
    try {
        const {FeeTerms} = await connectToDatabase();
        const feetermsObject = await FeeTerms.findOne({
            where: {
                practice_id: event.user.practice_id,
            }
        });
        let feeTermsJson = {
            practice_id: event.user.practice_id,
            terms_text: '',
        };
        if (feetermsObject) {
            feeTermsJson = feetermsObject.get({plain: true});
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(feeTermsJson),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Practices.'}),
        };
    }
};

const getFeeTermsByClientId = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {FeeTerms, Clients, Cases} = await connectToDatabase();
        const clientDetails = await Clients.findOne({
            where: {
                id: input.client_id,
            }
        });
        if (!clientDetails) throw new HTTPError(401, 'invalid Client id');
        const feetermsObject = await FeeTerms.findOne({
            where: {
                practice_id: clientDetails.practice_id,
            }
        });
        let feeTermsJson = {
            practice_id: clientDetails.practice_id,
            terms_text: '',
        };
        if (feetermsObject) {
            feeTermsJson = feetermsObject.get({plain: true});
        }
        let fee_acceptance_status = clientDetails.fee_acceptance_status;
        if (input.case_id) {
            const caseDetails = await Cases.findOne({where: {id: input.case_id}});
            if (caseDetails) {
                if (caseDetails.fee_terms) feeTermsJson.terms_text = caseDetails.fee_terms;
                fee_acceptance_status = caseDetails.fee_acceptance_status;
            }
        }
        feeTermsJson.fee_acceptance_status = fee_acceptance_status;
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(feeTermsJson),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Practices.'}),
        };
    }
};


const setFeeTerms = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {FeeTerms} = await connectToDatabase();
        const feetermsObject = await FeeTerms.findOne({
            where: {
                practice_id: event.user.practice_id,
            }
        });
        if (feetermsObject) {
            feetermsObject.terms_text = input.terms_text;
            await feetermsObject.save();
        } else {
            const dataObject = Object.assign({
                practice_id: event.user.practice_id,
                terms_text: input.terms_text,
            }, {id: uuid.v4()});
            await FeeTerms.create(dataObject);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Terms updated',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Practices.'}),
        };
    }
};

module.exports.getFeeTerms = middy(getFeeTerms).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.setFeeTerms = middy(setFeeTerms).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getFeeTermsByClientId = getFeeTermsByClientId;
module.exports.getFeeTermsExpress = getFeeTerms;
module.exports.setFeeTermsExpress = setFeeTerms;

