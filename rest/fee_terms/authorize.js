const {HTTPError} = require('../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const practiceRoles = ['lawyer', 'paralegal'];
const litigaitRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician', 'lawyer', 'paralegal'];

exports.authorizeGetFeeTermsByClientId = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetFeeTerms = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeSetFeeTerms = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};