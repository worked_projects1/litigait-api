const getResponseHistory = async (practice_id, case_id, form_id, legalforms_id, FormsResponseHistory) => {
    try {
        let lawyer_response = [];
        const response_history = await FormsResponseHistory.findOne({
            where: { practice_id: practice_id, case_id: case_id, form_id: form_id, legalforms_id: legalforms_id },
            raw: true
        });
        if (response_history?.response) {
            lawyer_response = JSON.parse(response_history.response);
            lawyer_response.sort((a, b) => b.version - a.version);
        }
        return lawyer_response;
    } catch (err) {
        console.log('Helper Function Error: getResponseHistory');
        console.log(err);
        throw new Error(err);
    }
}   

module.exports = {
    getResponseHistory
}