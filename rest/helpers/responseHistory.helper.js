const checkResponseHistoryCount = (arr , limit) => {
    try {
        if(arr.length>=limit){
            arr.shift();
        }
        return arr;
    } catch (err) {
        console.log('Helper Function Error: Objection Calculate arr');
        console.log(err);
        throw new Error(err);
    }
}   

module.exports = {
    checkResponseHistoryCount
}