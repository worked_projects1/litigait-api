const generateColorCode = async (practice_id, Users) => {
    
    let color,is_existed;
    do {
      // Generate a random hex color code
      color = '#' + Math.floor(Math.random() * 16777215).toString(16);
      // Exclude white, black and Same practice user have same color code.
      is_existed = await Users.findOne({where:{practice_id, color_code: color }, raw: true});
    } while (color === '#ffffff' || color === '#000000' || is_existed?.color_code);
    // Add the color to the set to ensure it's not generated again
    return color;
}

module.exports = {
    generateColorCode
}
