const uuid = require('uuid');
const AWS = require('aws-sdk');

AWS.config.update({region: process.env.REGION || 'us-east-1'});
const s3 = new AWS.S3();

const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const {json} = require('body-parser');

const create = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {MedicalDocumentProperty, Op, MedicalHistory, Cases} = await connectToDatabase();
        const casesObj = await Cases.findOne({where: {id: input.case_id, is_deleted: {[Op.not]: true}}, raw: true});
        if (!casesObj) throw new HTTPError(404, `Case with id: ${input.case_id} was not found`);
        let practice_id = casesObj.practice_id;
        let case_id = input.case_id;
        let client_id = input.client_id;
        let id = input.id;
        const findExistingDOcumentProperty = await MedicalDocumentProperty.findOne({
            where: {
                medical_history_id: id,
                case_id: case_id,
                client_id: client_id,
                practice_id: practice_id
            }
        });
        if (findExistingDOcumentProperty) {
            const existingMedicalDOcumentStatus = await MedicalHistory.findOne({
                where: {
                    id: id,
                    case_id: case_id,
                    client_id: client_id
                }
            });
            existingMedicalDOcumentStatus.keys = false;
            await existingMedicalDOcumentStatus.save();
            await MedicalDocumentProperty.destroy({
                where: {
                    medical_history_id: id,
                    case_id: case_id,
                    client_id: client_id,
                    practice_id: practice_id
                }
            });
        }
        const medicalHistoryObj = await MedicalHistory.findOne({
            where: {
                id: id,
                case_id: case_id,
                client_id: client_id
            }
        });
        if (!medicalHistoryObj) throw new HTTPError(404, `Medical HistoryObj with id: ${medicalHistoryObj.id} was not found`);
        medicalHistoryObj.order_date = input.order_date;
        medicalHistoryObj.keys = true;
        await medicalHistoryObj.save();
        let properties_arr = input.keys;
        let response = [];
        const obj = {};
        const medicalHistorySavedObj = await MedicalHistory.findOne({
            where: {
                id: input.id,
                case_id: case_id,
                client_id: client_id
            }, raw: true
        });
        for (let i = 0; i < properties_arr.length; i++) {
            let data = {
                id: uuid.v4(),
                practice_id: practice_id,
                case_id: case_id,
                client_id: client_id,
                medical_history_id: input.id,
                row_id: properties_arr[i].row_id,
                row_section_id: properties_arr[i].row_section_id,
                key: properties_arr[i].key,
                extracted_text: JSON.stringify(properties_arr[i].extracted_text),
                crop_image: JSON.stringify(properties_arr[i].crop_image),
            }
            const MedicalDocumentPropertyObj = await MedicalDocumentProperty.create(data);
            const MedicalDocumentPropertyText = MedicalDocumentPropertyObj.get({plain: true});
            if (MedicalDocumentPropertyText.extracted_text) {
                MedicalDocumentPropertyText.extracted_text = JSON.parse(MedicalDocumentPropertyText.extracted_text);
            }
            if (MedicalDocumentPropertyText.crop_image) {
                MedicalDocumentPropertyText.crop_image = JSON.parse(MedicalDocumentPropertyText.crop_image);
            }
            MedicalDocumentPropertyText.order_date = medicalHistorySavedObj.order_date;
            response.push(MedicalDocumentPropertyText);
        }

        obj.case_id = case_id;
        obj.client_id = client_id;
        obj.id = input.id;
        obj.order_date = medicalHistorySavedObj.order_date;
        obj.keys = response;
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(obj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({err, error: err.message || 'Could not create the medicalHistoryDetails.'}),
        };
    }
};
const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const {MedicalDocumentProperty, Op, MedicalHistory, Cases} = await connectToDatabase();
        let response = [];
        const obj = {};
        const medicalDocumentPropertyDetails = await MedicalDocumentProperty.findAll({
            where: {
                is_deleted: {[Op.not]: true},
                medical_history_id: params.id
            }, logging: console.log
        });
        const medicalDocumentPropertyDetailsCount = await MedicalDocumentProperty.count({
            where: {
                is_deleted: {[Op.not]: true},
                medical_history_id: params.id
            }, logging: console.log
        });
        if (!medicalDocumentPropertyDetailsCount) throw new HTTPError(404, `medicalDocumentProperty with id: ${params.id} was not found`);
        const plainmedicalDocumentProperty = medicalDocumentPropertyDetails;
        const medicalHistoryObj = await MedicalHistory.findOne({where: {id: params.id}});
        for (let i = 0; i < plainmedicalDocumentProperty.length; i++) {
            if (plainmedicalDocumentProperty[i].extracted_text) {
                plainmedicalDocumentProperty[i].extracted_text = JSON.parse(plainmedicalDocumentProperty[i].extracted_text);
            }
            if (plainmedicalDocumentProperty[i].crop_image) {
                plainmedicalDocumentProperty[i].crop_image = JSON.parse(plainmedicalDocumentProperty[i].crop_image);
            }
        }
        plainmedicalDocumentProperty.order_date = medicalHistoryObj.order_date;
        obj.keys = plainmedicalDocumentProperty;
        obj.case_id = plainmedicalDocumentProperty[0].case_id;
        obj.client_id = plainmedicalDocumentProperty[0].client_id;
        obj.id = params.id;
        obj.order_date = medicalHistoryObj.order_date;

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(obj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the medicalDocumentProperty.'}),
        };
    }
};
const getAll = async (event) => {
    try {
        const {MedicalDocumentProperty, Op, MedicalHistory, Cases} = await connectToDatabase();
        const params = event.params || event.pathParameters;
        /* let case_id = historyObj.case_id;
        let client_id = historyObj.client_id; */
        let case_id = params.caseId;
        let client_id = params.clientId;
        const documents = [];
        const getAllMedicalHistoryObj = await MedicalHistory.findAll({
            where: {case_id: case_id, client_id: client_id},
            order: [['createdAt', 'DESC']],
            raw: true,
            logging: console.log
        });
        for (let i = 0; i < getAllMedicalHistoryObj.length; i++) {
            let practice_id = getAllMedicalHistoryObj[i].practice_id;
            let obj = {};
            const medicalDocumentPropertyDetails = await MedicalDocumentProperty.findAll({
                where: {
                    is_deleted: {[Op.not]: true},
                    medical_history_id: getAllMedicalHistoryObj[i].id,
                    case_id: case_id,
                    client_id: client_id,
                    practice_id: practice_id
                },
                order: [['createdAt', 'ASC']], raw: true, logging: console.log
            });
            const medicalDocumentPropertyDetailsCount = await MedicalDocumentProperty.count({
                where: {
                    is_deleted: {[Op.not]: true},
                    medical_history_id: getAllMedicalHistoryObj[i].id,
                    case_id: case_id,
                    client_id: client_id,
                    practice_id: practice_id
                },
                order: [['createdAt', 'ASC']], raw: true, logging: console.log
            });

            if (medicalDocumentPropertyDetailsCount != 0) {
                for (let k = 0; k < medicalDocumentPropertyDetails.length; k++) {
                    if (medicalDocumentPropertyDetails[k].extracted_text) {
                        medicalDocumentPropertyDetails[k].extracted_text = JSON.parse(medicalDocumentPropertyDetails[k].extracted_text);
                    }
                    if (medicalDocumentPropertyDetails[k].crop_image) {
                        medicalDocumentPropertyDetails[k].crop_image = JSON.parse(medicalDocumentPropertyDetails[k].crop_image);
                    }
                    medicalDocumentPropertyDetails[k].order_date = getAllMedicalHistoryObj[i].order_date;
                }
                obj.keys = medicalDocumentPropertyDetails;
                obj.case_id = case_id;
                obj.client_id = client_id;
                obj.id = getAllMedicalHistoryObj[i].id;
                obj.order_date = getAllMedicalHistoryObj[i].order_date;
                obj.filename = getAllMedicalHistoryObj[i].file_name;
                obj.total_pages = getAllMedicalHistoryObj[i].pages;
                documents.push(obj);
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(documents),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the medicalDocumentPropertyDetails.'}),
        };
    }
};
const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        const {MedicalDocumentProperty, Op, MedicalHistory, Cases} = await connectToDatabase();
        let case_id = input.case_id;
        let client_id = input.client_id;
        let medical_history_id = params.id;
        await MedicalDocumentProperty.destroy({
            where: {
                medical_history_id: medical_history_id,
                case_id: case_id,
                client_id: client_id
            }
        });
        const casesObj = await Cases.findOne({where: {id: input.case_id, is_deleted: {[Op.not]: true}}, raw: true});
        let practice_id = casesObj.practice_id;
        if (!casesObj) throw new HTTPError(404, `Case with id: ${input.case_id} was not found`);
        const medicalHistoryObj = await MedicalHistory.findOne({
            where: {
                id: medical_history_id,
                case_id: case_id,
                client_id: client_id
            }
        });
        medicalHistoryObj.order_date = input.order_date;
        await medicalHistoryObj.save();
        let properties_arr = input.keys;
        let response = [];
        const obj = {};
        const medicalHistorySavedObj = await MedicalHistory.findOne({
            where: {
                id: medical_history_id,
                case_id: case_id,
                client_id: client_id
            }, raw: true
        });
        for (let i = 0; i < properties_arr.length; i++) {
            let data = {
                id: uuid.v4(),
                practice_id: practice_id,
                case_id: case_id,
                client_id: client_id,
                medical_history_id: medical_history_id,
                row_id: properties_arr[i].row_id,
                row_section_id: properties_arr[i].row_section_id,
                key: properties_arr[i].key,
                extracted_text: JSON.stringify(properties_arr[i].extracted_text),
                crop_image: JSON.stringify(properties_arr[i].crop_image),
            }
            const MedicalDocumentPropertyObj = await MedicalDocumentProperty.create(data);
            const MedicalDocumentPropertyText = MedicalDocumentPropertyObj.get({plain: true});
            if (MedicalDocumentPropertyText.extracted_text) {
                MedicalDocumentPropertyText.extracted_text = JSON.parse(MedicalDocumentPropertyText.extracted_text);
            }
            if (MedicalDocumentPropertyText.crop_image) {
                MedicalDocumentPropertyText.crop_image = JSON.parse(MedicalDocumentPropertyText.crop_image);
            }
            MedicalDocumentPropertyText.order_date = medicalHistorySavedObj.order_date;
            response.push(MedicalDocumentPropertyText);
        }
        obj.keys = response;
        obj.case_id = case_id;
        obj.client_id = client_id;
        obj.id = medical_history_id;
        obj.order_date = medicalHistorySavedObj.order_date;


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(obj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the MedicalHistory.'}),
        };
    }
};
const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const {MedicalDocumentProperty, Op} = await connectToDatabase();
        const MedicalDocumentPropertyDetails = await MedicalDocumentProperty.findOne({
            where: {
                is_deleted: {[Op.not]: true},
                id: params.id
            }
        });
        if (!MedicalDocumentPropertyDetails) throw new HTTPError(404, `MedicalDocumentProperty with id: ${params.id} was not found`);
        MedicalDocumentPropertyDetails.is_deleted = true;
        await MedicalDocumentPropertyDetails.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(MedicalDocumentPropertyDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could destroy fetch the MedicalDocumentProperty.'}),
        };
    }
};
const getSecuredPublicUrlForSummeryPrivateFile = async (event) => {
    try {
        const input = event.body;
        AWS.config.update({region: process.env.S3_BUCKET_FOR_DUPLICATE_MEDICAL_SUMMERY});
        const publicUrl = await s3.getSignedUrlPromise('getObject', {
            Bucket: process.env.S3_BUCKET_FOR_DUPLICATE_MEDICAL_SUMMERY,
            Expires: 60 * 60 * 1,
            Key: input.s3_file_key,
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                publicUrl,
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not get the Public Url.'}),
        };
    }
};

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.getSecuredPublicUrlForSummeryPrivateFile = getSecuredPublicUrlForSummeryPrivateFile;
