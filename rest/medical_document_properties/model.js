module.exports = (sequelize, type) => sequelize.define('MedicalDocumentProperty', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    client_id: type.STRING,
    case_id: type.STRING,
    medical_history_id: type.STRING,
    row_id: type.STRING,
    row_section_id: type.STRING,
    key: type.STRING,
    extracted_text: type.TEXT,
    crop_image: type.TEXT,
    is_deleted: type.BOOLEAN,
});
