const connectToDatabase = require('../../db');
const uuid = require('uuid');
const {HTTPError} = require('../../utils/httpResp');
const {QueryTypes} = require('sequelize');
const {generateRandomString} = require('../../utils/randomStringGenerator');
const {sendEmail} = require('../../utils/mailModule');
const {sendSMS} = require('../../utils/smsModule');
const {response} = require('express');

const caseReminderCron_old = async (event) => {
    try {
        // send all -> selected_Questions , sendall.
        // 498 - legalforms  group by selected_questions
        // 1057 - sendall
        // send all question -> send previous link to client.
        // selected_question -> combine same legalforms id as one + generate new link and send it to client.
        // Delete if users answered all the questions and last response date exist more than 7 days.

        const {
            Op,
            Forms,
            Formotp,
            Cases,
            LegalForms,
            Clients,
            Practices,
            Users,
            HelpRequest,
            Orders,
            sequelize
        } = await connectToDatabase();
        let TimeStamp = new Date();
        let responseEndDate = new Date();
        TimeStamp.setDate(TimeStamp.getDate() - 7);
        let cDate = TimeStamp.getDate();
        let Year = TimeStamp.getFullYear();
        let Month = TimeStamp.getMonth() + 1;
        if (Month < 10) Month = '0' + Month;
        if (cDate < 10) cDate = '0' + cDate;
        let endDate = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';

        let previousDate = new Date();
        previousDate.setDate(previousDate.getDate() - 1);
        let pDate = previousDate.getDate();
        let pYear = previousDate.getFullYear();
        let pMonth = previousDate.getMonth() + 1;
        if (pMonth < 10) pMonth = '0' + pMonth;
        if (pDate < 10) pDate = '0' + pDate;
        let responseValidityEndDate = pYear + '-' + pMonth + '-' + pDate + ' ' + '23:59:59';


        const fetchAllFormsOtpData = await Formotp.findAll({
            where: {
                // sending_type:{[Op.in]:['all','selected_questions']},
                sending_type: {[Op.in]: ['all']},
                scheduler_email: true,
                is_client_answered_all_questions: false,
                latest_client_response_date: {
                    [Op.not]: null,
                    [Op.lte]: responseValidityEndDate
                },
            },
            logging: console.log
        });

        for (let i = 0; i < fetchAllFormsOtpData.length; i++) {
            const otpRow = fetchAllFormsOtpData[i];
            let case_id = otpRow.case_id;
            let client_id = otpRow.client_id;
            let legalforms_id = otpRow.legalforms_id;
            let document_type = otpRow.document_type;
            let sending_type = otpRow.sending_type;
            let otp_code = otpRow.otp_code;
            let otp_secret = otpRow.otp_secret;
            let otp_row_id = otpRow.id;
            let TargetLanguageCode = otpRow.id;
            const otp_columns = {
                case_id,
                client_id,
                legalforms_id,
                document_type,
                otp_secret,
                otp_code,
                id: otp_row_id,
                TargetLanguageCode
            };

            if (sending_type == 'all') {
                await EmailAndSmsTemplate(otp_columns);
                await Formotp.update({scheduler_email: false}, {where: otp_columns, logging: console.log});
            } else if (sending_type == 'selected_questions') {
                const findSameFormsDatas = await Formotp.findAll({
                    where: {
                        case_id,
                        client_id,
                        legalforms_id,
                        document_type,
                        TargetLanguageCode,
                        scheduler_email: true,
                        is_client_answered_all_questions: false,
                        latest_client_response_date: {[Op.not]: null, [Op.lte]: responseValidityEndDate}
                    }
                });
                const findSameFormsSingleData = await Formotp.findOne({
                    where: {
                        case_id,
                        client_id,
                        legalforms_id,
                        document_type,
                        TargetLanguageCode,
                        scheduler_email: true,
                        is_client_answered_all_questions: false,
                        latest_client_response_date: {[Op.not]: null, [Op.lte]: responseValidityEndDate}
                    }, raw: true
                });

                let question_ids_collection = []
                for (let n = 0; n < findSameFormsDatas.length; n++) {
                    let otpRow = findSameFormsDatas[n];
                    let question_id = JSON.parse(otpRow.question_ids);
                    question_ids_collection.concat(question_id);
                }
                let otp_code = generateRandomString(8);
                let otp_secret = generateRandomString(4);
                const question_ids = [...new Set(question_ids_collection)]; // remove dublicate ids from question ids collection.
                let formsOtpColumn = {
                    id: uuid.v4(),
                    otp_code: otp_code,
                    otp_secret: otp_secret,
                    client_id: findSameFormsSingleData.client_id,
                    document_type: findSameFormsSingleData.document_type,
                    case_id: findSameFormsSingleData.case_id,
                    legalforms_id: findSameFormsSingleData.legalforms_id,
                    sending_type: findSameFormsSingleData.sending_type,
                    question_ids: JSON.stringify(question_ids),
                    sent_by: findSameFormsSingleData.sent_by,
                    scheduler_email: false,
                    TargetLanguageCode: findSameFormsSingleData.TargetLanguageCode,
                };
                const checkLatestClientResponse = await Forms.findAll({
                    where:
                        {
                            case_id,
                            client_id,
                            legalforms_id,
                            document_type,
                            id: {[Op.in]: question_ids},
                            client_response_status: 'ClientResponseAvailable'
                        },
                    attributes: [[sequelize.fn('max', sequelize.col('last_updated_by_client')), 'recent_response_date']],
                });
                const checkClientAnsweredQuestionsCount = await Forms.count({
                    where:
                        {
                            case_id,
                            client_id,
                            legalforms_id,
                            document_type,
                            id: {[Op.in]: question_ids},
                            client_response_status: 'SentToClient'
                        }
                });
                if (checkLatestClientResponse && checkLatestClientResponse.recent_response_date) {
                    formsOtpColumn.latest_client_response_date = checkLatestClientResponse.recent_response_date;
                }
                if (checkClientAnsweredQuestionsCount && checkClientAnsweredQuestionsCount == 0) {
                    formsOtpColumn.latest_client_response_date = true;
                } else {
                    formsOtpColumn.latest_client_response_date = false;
                }
                await Formotp.destroy({where: {case_id, client_id, legalforms_id, document_type, TargetLanguageCode}});
                const createOtpResponse = await Formotp.create(formsOtpColumn);
                const otpPlainText = createOtpResponse.get({plain: true});
                await EmailAndSmsTemplate(otpPlainText);
            }

        }

        await Formotp.destroy({
            where: {
                sending_type: {[Op.in]: ['all', 'selected_questions']},
                is_client_answered_all_questions: true,
                latest_client_response_date: {
                    [Op.not]: null,
                    [Op.lte]: responseValidityEndDate
                },
            },
            logging: console.log
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'Ok'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not send Medical History.'}),
        };
    }
}
const caseReminderCron = async (event) => {
    try {
        // send all -> selected_Questions , sendall.
        // 498 - legalforms  group by selected_questions
        // 1057 - sendall
        // send all question -> send previous link to client.
        // selected_question -> combine same legalforms id as one + generate new link and send it to client.
        // Delete if users answered all the questions and last response date exist more than 7 days.

        const { Op, Forms, Formotp, sequelize } = await connectToDatabase();
        let TimeStamp = new Date();
        let responseEndDate = new Date();
        TimeStamp.setDate(TimeStamp.getDate() - 7);
        let cDate = TimeStamp.getDate();
        let Year = TimeStamp.getFullYear();
        let Month = TimeStamp.getMonth() + 1;
        if (Month < 10) Month = '0' + Month;
        if (cDate < 10) cDate = '0' + cDate;
        let endDate = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';

        let previousDate = new Date();
        previousDate.setDate(previousDate.getDate() - 1);
        let pDate = previousDate.getDate();
        let pYear = previousDate.getFullYear();
        let pMonth = previousDate.getMonth() + 1;
        if (pMonth < 10) pMonth = '0' + pMonth;
        if (pDate < 10) pDate = '0' + pDate;
        let responseValidityEndDate = pYear + '-' + pMonth + '-' + pDate + ' ' + '23:59:59';


        const fetchAllFormsOtpData = await Formotp.findAll({
            where: {
                // sending_type:{[Op.in]:['all','selected_questions']},
                sending_type: {[Op.in]: ['all']},
                scheduler_email: true,
                is_client_answered_all_questions: false,
                latest_client_response_date: {
                    [Op.not]: null,
                    [Op.lte]: responseValidityEndDate
                },
            },
            logging: console.log
        });

        for (let i = 0; i < fetchAllFormsOtpData.length; i++) {
            const otpRow = fetchAllFormsOtpData[i];
            let case_id = otpRow.case_id;
            let client_id = otpRow.client_id;
            let legalforms_id = otpRow.legalforms_id;
            let document_type = otpRow.document_type;
            let sending_type = otpRow.sending_type;
            let otp_code = otpRow.otp_code;
            let otp_secret = otpRow.otp_secret;
            let otp_row_id = otpRow.id;
            let TargetLanguageCode = otpRow.id;
            const otp_columns = {
                case_id,
                client_id,
                legalforms_id,
                document_type,
                otp_secret,
                otp_code,
                id: otp_row_id,
                TargetLanguageCode
            };

            if (sending_type == 'all') {
                await EmailAndSmsTemplate(otp_columns);
                await Formotp.update({scheduler_email: false}, {where: otp_columns, logging: console.log});
            } else if (sending_type == 'selected_questions') {
                const findSameFormsDatas = await Formotp.findAll({
                    where: {
                        case_id,
                        client_id,
                        legalforms_id,
                        document_type,
                        TargetLanguageCode,
                        scheduler_email: true,
                        is_client_answered_all_questions: false,
                        latest_client_response_date: {[Op.not]: null, [Op.lte]: responseValidityEndDate}
                    }
                });
                const findSameFormsSingleData = await Formotp.findOne({
                    where: {
                        case_id,
                        client_id,
                        legalforms_id,
                        document_type,
                        TargetLanguageCode,
                        scheduler_email: true,
                        is_client_answered_all_questions: false,
                        latest_client_response_date: {[Op.not]: null, [Op.lte]: responseValidityEndDate}
                    }, raw: true
                });

                let question_ids_collection = []
                for (let n = 0; n < findSameFormsDatas.length; n++) {
                    let otpRow = findSameFormsDatas[n];
                    let question_id = JSON.parse(otpRow.question_ids);
                    question_ids_collection.concat(question_id);
                }
                let otp_code = generateRandomString(8);
                let otp_secret = generateRandomString(4);
                const question_ids = [...new Set(question_ids_collection)]; // remove dublicate ids from question ids collection.
                let formsOtpColumn = {
                    id: uuid.v4(),
                    otp_code: otp_code,
                    otp_secret: otp_secret,
                    client_id: findSameFormsSingleData.client_id,
                    document_type: findSameFormsSingleData.document_type,
                    case_id: findSameFormsSingleData.case_id,
                    legalforms_id: findSameFormsSingleData.legalforms_id,
                    sending_type: findSameFormsSingleData.sending_type,
                    question_ids: JSON.stringify(question_ids),
                    sent_by: findSameFormsSingleData.sent_by,
                    scheduler_email: false,
                    TargetLanguageCode: findSameFormsSingleData.TargetLanguageCode,
                };
                const checkLatestClientResponse = await Forms.findAll({
                    where:
                        {
                            case_id,
                            client_id,
                            legalforms_id,
                            document_type,
                            id: {[Op.in]: question_ids},
                            client_response_status: 'ClientResponseAvailable'
                        },
                    attributes: [[sequelize.fn('max', sequelize.col('last_updated_by_client')), 'recent_response_date']],
                });
                const checkClientAnsweredQuestionsCount = await Forms.count({
                    where:
                        {
                            case_id,
                            client_id,
                            legalforms_id,
                            document_type,
                            id: {[Op.in]: question_ids},
                            client_response_status: 'SentToClient'
                        }
                });
                /* prithivi changes start */
                if (checkLatestClientResponse && (checkLatestClientResponse.length > 0) && checkLatestClientResponse[0].recent_response_date) {
                    formsOtpColumn.latest_client_response_date = checkLatestClientResponse[0].recent_response_date;
                }
                /* prithivi changes ends */
                if (checkClientAnsweredQuestionsCount && checkClientAnsweredQuestionsCount == 0) {
                    formsOtpColumn.latest_client_response_date = true;
                } else {
                    formsOtpColumn.latest_client_response_date = false;
                }
                await Formotp.destroy({where: {case_id, client_id, legalforms_id, document_type, TargetLanguageCode}});
                const createOtpResponse = await Formotp.create(formsOtpColumn);
                const otpPlainText = createOtpResponse.get({plain: true});
                await EmailAndSmsTemplate(otpPlainText);
            }

        }

        await Formotp.destroy({
            where: {
                sending_type: {[Op.in]: ['all', 'selected_questions']},
                is_client_answered_all_questions: true,
                latest_client_response_date: {
                    [Op.not]: null,
                    [Op.lte]: responseValidityEndDate
                },
            },
            logging: console.log
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'Ok'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not send Medical History.'}),
        };
    }
}

const EmailAndSmsTemplate = async (otpDetails) => {
    try {
        const {OtherParties, Clients, Practices,} = await connectToDatabase();

        let questionsUrl = `${process.env.CARE_URL}/questionnaire/${otpDetails.otp_code}`;

        if (otpDetails.TargetLanguageCode) {
            questionsUrl = `${questionsUrl}?TargetLanguageCode=${otpDetails.TargetLanguageCode}`;
        }

        let flag = 0;
        let ClientDetails;
        if (otpDetails.client_id) {
            ClientDetails = await Clients.findOne({ where: { id: otpDetails.clientId }, });
        } else if (otpDetails.party_id) {
            ClientDetails = await OtherParties.findOne({ where: { id: otpDetails.clientId }, });
        }
        const practiceDetails = await Practices.findOne({where: {id: ClientDetails.practice_id}});
        const practiceName = practiceDetails.name;
        let message_body = '';
        let smstext_body = '';
        if (otpDetails.TargetLanguageCode && otpDetails.TargetLanguageCode != 'es' && otpDetails.TargetLanguageCode != 'vi') {
            message_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link to access the questionnaire.' + '<br/>' + questionsUrl;
            smstext_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link and enter the verification code ' + otpDetails.otp_code + ' to access the questionnaire. ' + questionsUrl;

            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Important: Response required for your lawsuit', message_body, practiceName);
                //await sendEmail('aravinthrifluxyss@gmail.com', 'Important: Response required for your lawsuit', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Important: Response required for your lawsuit', smstext_body);
            }
            flag = 1;
        } else if (otpDetails.TargetLanguageCode == 'es') {
            message_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace para acceder al cuestionario.' + '<br/>' + questionsUrl;
            smstext_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace e ingrese el código de verificación ' + otpDetails.otp_code + ' para acceder al cuestionario. ' + questionsUrl;
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Importante: Se requiere respuesta para su demanda', message_body, practiceName);
                //await sendEmail('aravinthrifluxyss@gmail.com', 'Importante: Se requiere respuesta para su demanda', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Importante: Se requiere respuesta para su demanda', smstext_body);
            }
            flag = 1;
        } else if (otpDetails.TargetLanguageCode == 'vi') {
            message_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết để truy cập bảng câu hỏi.' + '<br/>' + questionsUrl;
            smstext_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết và nhập mã xác minh ' + otpDetails.otp_code + ' để truy cập bảng câu hỏi. ' + questionsUrl;
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', message_body, practiceName);
                //await sendEmail('aravinthrifluxyss@gmail.com', 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', smstext_body);
            }
            flag = 1;
        }
    } catch (err) {
        console.log(err);

    }
}
const caseReminderCronold = async (event, context) => {
    try {
        const {
            Op,
            Forms,
            Formotp,
            Cases,
            LegalForms,
            Clients,
            Practices,
            Users,
            HelpRequest,
            Orders,
            sequelize
        } = await connectToDatabase();
        let TimeStamp = new Date();

        TimeStamp.setDate(TimeStamp.getDate() - 7);
        let cDate = TimeStamp.getDate();
        let Year = TimeStamp.getFullYear();
        let Month = TimeStamp.getMonth() + 1;
        if (Month < 10) Month = '0' + Month;
        if (cDate < 10) cDate = '0' + cDate;
        let endDate = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';
        console.log(endDate);
        const getAllFormOtps = await Formotp.findAll({
            where: {
                sending_type: {[Op.ne]: 'verification'},
                createdAt: {[Op.lte]: endDate},
                scheduler_email: true
            }, order: [['createdAt', 'DESC']], limit: 10, raw: true
        });
        if (getAllFormOtps.length != 0) {
            for (let i = 0; i < getAllFormOtps.length; i++) {
                const otpObject = getAllFormOtps[i];
                const sending_type = otpObject.sending_type;

                const casesObj = await Cases.findOne({
                    where: {
                        id: otpObject.case_id,
                        client_id: otpObject.client_id,
                        is_deleted: {[Op.not]: true},
                        is_archived: {[Op.not]: true},
                    }, raw: true,

                });

                if (casesObj) {
                    if (sending_type == 'selected_questions') {
                        const findDuplicateDatas = await Formotp.findAll({
                            where: {
                                sending_type: {[Op.like]: 'selected_questions'},
                                createdAt: {[Op.lte]: endDate},
                                scheduler_email: true,
                                client_id: otpObject.client_id,
                                case_id: otpObject.case_id,
                                legalforms_id: otpObject.legalforms_id,
                            }, order: [['createdAt', 'DESC']], raw: true
                        });
                        let languageType = [];
                        for (let j = 0; j < findDuplicateDatas.length; j++) {
                            const duplicateRow = findDuplicateDatas[j];
                            if (languageType.length == 0 || !languageType.includes(duplicateRow.TargetLanguageCode)) {
                                let newSelectedQuestionsOtp = {};
                                languageType.push(duplicateRow.TargetLanguageCode);
                                const getFormsData = await Forms.findAll({
                                    where: {
                                        legalforms_id: duplicateRow.legalforms_id,
                                        client_id: duplicateRow.client_id,
                                        case_id: duplicateRow.case_id,
                                        TargetLanguageCode: {[Op.like]: duplicateRow.TargetLanguageCode},
                                        client_response_status: {[Op.like]: 'SentToClient'}
                                    }, attributes: ['id'], raw: true
                                });
                                if (getFormsData.length != 0) {
                                    let questionIdsArr = [];
                                    for (let qid = 0; qid < getFormsData.length; qid++) {
                                        let nt = getFormsData[qid].id;
                                        questionIdsArr.push(nt);
                                    }
                                    newSelectedQuestionsOtp.id = uuid.v4();
                                    newSelectedQuestionsOtp.otp_code = generateRandomString(8);
                                    newSelectedQuestionsOtp.otp_secret = generateRandomString(4);
                                    newSelectedQuestionsOtp.client_id = duplicateRow.client_id;
                                    newSelectedQuestionsOtp.document_type = duplicateRow.document_type;
                                    newSelectedQuestionsOtp.question_type = duplicateRow.question_type;
                                    newSelectedQuestionsOtp.case_id = duplicateRow.case_id;
                                    newSelectedQuestionsOtp.legalforms_id = duplicateRow.legalforms_id;
                                    newSelectedQuestionsOtp.sending_type = duplicateRow.sending_type;
                                    newSelectedQuestionsOtp.question_ids = Stringify(questionIdsArr)
                                    newSelectedQuestionsOtp.sent_by = duplicateRow.sent_by;
                                    newSelectedQuestionsOtp.scheduler_email = false;
                                    newSelectedQuestionsOtp.TargetLanguageCode = duplicateRow.TargetLanguageCode;

                                    let emailandSmsObj = {
                                        clientId: duplicateRow.client_id,
                                        otpCode: newSelectedQuestionsOtp.otp_code,
                                        otpSecret: newSelectedQuestionsOtp.otp_secret,
                                        TargetLang: duplicateRow.TargetLanguageCode,
                                        LegalFormsId: duplicateRow.legalforms_id,
                                        sendingType: 'selected_questions',
                                        Table: 'Forms'
                                    };
                                    await Formotp.create(newSelectedQuestionsOtp);
                                    await sendCronEmailandSms(emailandSmsObj);
                                } else {
                                    await Formotp.destroy({
                                        where: {
                                            legalforms_id: duplicateRow.legalforms_id,
                                            sending_type: {[Op.like]: 'selected_questions'},
                                            TargetLanguageCode: duplicateRow.TargetLanguageCode,
                                            scheduler_email: true,
                                            client_id: duplicateRow.client_id,
                                            case_id: duplicateRow.case_id,
                                        }
                                    });
                                }
                            }
                        }
                    } else if (sending_type == 'all') {
                        const checkQuestionStatus = await Forms.findAll({
                            where: {
                                legalforms_id: otpObject.legalforms_id,
                                client_id: otpObject.client_id,
                                case_id: otpObject.case_id,
                                TargetLanguageCode: {[Op.like]: otpObject.TargetLanguageCode},
                                client_response_status: {[Op.like]: 'SentToClient'}
                            }
                        });
                        if (checkQuestionStatus.length != 0) {
                            let newSendAllQuestionsOtp = {};
                            newSendAllQuestionsOtp.id = uuid.v4();
                            newSendAllQuestionsOtp.otp_code = generateRandomString(8);
                            newSendAllQuestionsOtp.otp_secret = generateRandomString(4);
                            newSendAllQuestionsOtp.client_id = otpObject.client_id;
                            newSendAllQuestionsOtp.document_type = otpObject.document_type;
                            newSendAllQuestionsOtp.question_type = otpObject.question_type;
                            newSendAllQuestionsOtp.case_id = otpObject.case_id;
                            newSendAllQuestionsOtp.legalforms_id = otpObject.legalforms_id;
                            newSendAllQuestionsOtp.sending_type = otpObject.sending_type;
                            newSendAllQuestionsOtp.sent_by = otpObject.sent_by;
                            newSendAllQuestionsOtp.scheduler_email = false;
                            newSendAllQuestionsOtp.TargetLanguageCode = otpObject.TargetLanguageCode;

                            let emailandSmsforAllQuesObj = {
                                clientId: otpObject.client_id,
                                otpCode: newSendAllQuestionsOtp.otp_code,
                                otpSecret: newSendAllQuestionsOtp.otp_secret,
                                TargetLang: otpObject.TargetLanguageCode,
                                LegalFormsId: otpObject.legalforms_id,
                                sendingType: 'all',
                                Table: 'Forms'
                            };
                            await Formotp.create(newSendAllQuestionsOtp);
                            await sendCronEmailandSms(emailandSmsforAllQuesObj);
                        } else {
                            await Formotp.destroy({
                                where: {
                                    legalforms_id: otpObject.legalforms_id,
                                    sending_type: {[Op.like]: 'all'},
                                    TargetLanguageCode: otpObject.TargetLanguageCode,
                                    scheduler_email: true,
                                    client_id: otpObject.client_id,
                                    case_id: otpObject.case_id,
                                }
                            });
                        }
                    }
                } else {
                    console.log('Cases Deatails not found');
                }


            }
        }

        await caseReminderCronForOtherParties(event, context);
    } catch (err) {
        console.log(err);
        /* await sendEmail('aravinthrifluxyss@gmail.com', 'Forms Cron Function Err',err, 'Aravinth Litigait'); */
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the forms.'}),
        };
    }
};

const caseReminderCronForOtherParties = async (event, context) => {
    try {
        const {Op, OtherPartiesFormsOtp, OtherPartiesForms} = await connectToDatabase();
        let TimeStamp = new Date();

        TimeStamp.setDate(TimeStamp.getDate() - 7);
        let cDate = TimeStamp.getDate();
        let Year = TimeStamp.getFullYear();
        let Month = TimeStamp.getMonth() + 1;
        if (Month < 10) Month = '0' + Month;
        if (cDate < 10) cDate = '0' + cDate;
        let endDate = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';


        const getAllFormOtps = await OtherPartiesFormsOtp.findAll({
            where: {
                sending_type: {[Op.ne]: 'verification'},
                createdAt: {[Op.lte]: endDate},
                scheduler_email: true
            }, order: [['createdAt', 'DESC']], limit: 10, logging: console.log, raw: true
        });

        if (getAllFormOtps.length != 0) {
            for (let i = 0; i < getAllFormOtps.length; i++) {

                const otpObject = getAllFormOtps[i];
                const sending_type = otpObject.sending_type;

                const casesObj = await Cases.findOne({
                    where: {
                        id: otpObject.case_id,
                        is_deleted: {[Op.not]: true},
                        is_archived: {[Op.not]: true},
                    }, raw: true,
                    logging: console.log
                });

                if (casesObj) {
                    if (sending_type == 'selected_questions') {
                        const findDuplicateDatas = await OtherPartiesFormsOtp.findAll({
                            where: {
                                sending_type: {[Op.like]: 'selected_questions'},
                                createdAt: {[Op.lte]: endDate},
                                scheduler_email: true,
                                legalforms_id: otpObject.legalforms_id,
                                case_id: otpObject.case_id,
                            }, order: [['createdAt', 'DESC']], raw: true
                        });
                        let languageType = [];
                        for (let j = 0; j < findDuplicateDatas.length; j++) {
                            const duplicateRow = findDuplicateDatas[j];
                            if (languageType.length == 0 || !languageType.includes(duplicateRow.TargetLanguageCode)) {
                                let newSelectedQuestionsOtp = {};
                                languageType.push(duplicateRow.TargetLanguageCode);
                                const getFormsData = await OtherPartiesForms.findAll({
                                    where: {
                                        legalforms_id: duplicateRow.legalforms_id,
                                        case_id: duplicateRow.case_id,
                                        TargetLanguageCode: {[Op.like]: duplicateRow.TargetLanguageCode},
                                        client_response_status: {[Op.like]: 'SentToClient'}
                                    }, attributes: ['id']
                                });
                                if (getFormsData.length != 0) {
                                    let questionIdsArr = [];
                                    for (let qid = 0; qid < getFormsData.length; qid++) {
                                        let nt = getFormsData[qid].id;
                                        questionIdsArr.push(nt);
                                    }
                                    newSelectedQuestionsOtp.id = uuid.v4();
                                    newSelectedQuestionsOtp.otp_code = generateRandomString(8);
                                    newSelectedQuestionsOtp.otp_secret = generateRandomString(4);
                                    newSelectedQuestionsOtp.client_id = duplicateRow.client_id;
                                    newSelectedQuestionsOtp.document_type = duplicateRow.document_type;
                                    newSelectedQuestionsOtp.question_type = duplicateRow.question_type;
                                    newSelectedQuestionsOtp.case_id = duplicateRow.case_id;
                                    newSelectedQuestionsOtp.legalforms_id = duplicateRow.legalforms_id;
                                    newSelectedQuestionsOtp.sending_type = duplicateRow.sending_type;
                                    newSelectedQuestionsOtp.question_id = stringify(questionIdsArr);
                                    newSelectedQuestionsOtp.sent_by = duplicateRow.sent_by;
                                    newSelectedQuestionsOtp.scheduler_email = false;
                                    newSelectedQuestionsOtp.TargetLanguageCode = duplicateRow.TargetLanguageCode;

                                    let emailandSmsObj = {
                                        clientId: duplicateRow.client_id,
                                        otpCode: newSelectedQuestionsOtp.otp_code,
                                        otpSecret: newSelectedQuestionsOtp.otp_secret,
                                        TargetLang: duplicateRow.TargetLanguageCode,
                                        LegalFormsId: duplicateRow.legalforms_id,
                                        sendingType: 'selected_questions',
                                        Table: 'Otherparties'
                                    };
                                    await OtherPartiesFormsOtp.create(newSelectedQuestionsOtp);
                                    await sendCronEmailandSms(emailandSmsObj);
                                } else {
                                    console.log('selected else destroy');
                                    await OtherPartiesFormsOtp.destroy({
                                        where: {
                                            legalforms_id: duplicateRow.legalforms_id,
                                            case_id: duplicateRow.case_id,
                                            sending_type: {[Op.like]: 'selected_questions'},
                                            TargetLanguageCode: duplicateRow.TargetLanguageCode,
                                            scheduler_email: true
                                        }
                                    });
                                }
                            }
                        }
                    } else if (sending_type == 'all') {
                        const checkQuestionStatus = await OtherPartiesForms.findAll({
                            where: {
                                legalforms_id: otpObject.legalforms_id,
                                case_id: otpObject.case_id,
                                TargetLanguageCode: {[Op.like]: otpObject.TargetLanguageCode},
                                client_response_status: {[Op.like]: 'SentToClient'}
                            }
                        });
                        if (checkQuestionStatus.length != 0) {
                            let newSendAllQuestionsOtp = {};
                            const temparr = [];
                            newSendAllQuestionsOtp.id = uuid.v4();
                            newSendAllQuestionsOtp.otp_code = generateRandomString(8);
                            newSendAllQuestionsOtp.otp_secret = generateRandomString(4);
                            newSendAllQuestionsOtp.client_id = otpObject.client_id;
                            newSendAllQuestionsOtp.document_type = otpObject.document_type;
                            newSendAllQuestionsOtp.question_type = otpObject.question_type;
                            newSendAllQuestionsOtp.case_id = otpObject.case_id;
                            newSendAllQuestionsOtp.legalforms_id = otpObject.legalforms_id;
                            newSendAllQuestionsOtp.sending_type = otpObject.sending_type;
                            newSendAllQuestionsOtp.sent_by = otpObject.sent_by;
                            newSendAllQuestionsOtp.scheduler_email = false;
                            newSendAllQuestionsOtp.TargetLanguageCode = otpObject.TargetLanguageCode;

                            let emailandSmsforAllQuesObj = {
                                clientId: otpObject.client_id,
                                otpCode: newSendAllQuestionsOtp.otp_code,
                                otpSecret: newSendAllQuestionsOtp.otp_secret,
                                TargetLang: otpObject.TargetLanguageCode,
                                LegalFormsId: otpObject.legalforms_id,
                                sendingType: 'all',
                                Table: 'Otherparties'
                            };
                            await OtherPartiesFormsOtp.create(newSendAllQuestionsOtp);
                            await sendCronEmailandSms(emailandSmsforAllQuesObj);
                        } else {
                            await OtherPartiesFormsOtp.destroy({
                                where: {
                                    legalforms_id: otpObject.legalforms_id,
                                    case_id: otpObject.case_id,
                                    sending_type: {[Op.like]: 'all'},
                                    TargetLanguageCode: otpObject.TargetLanguageCode,
                                    scheduler_email: true
                                }
                            });
                        }
                    }
                } else {
                    console.log('Cases Deatails not found');
                }

            }
        }

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the forms.'}),
        };
    }
}

const sendCronEmailandSms = async (cronDeatails) => {
    try {
        const {Op, Formotp, OtherPartiesFormsOtp, OtherParties, Clients, Practices,} = await connectToDatabase();

        let questionsUrl = `${process.env.CARE_URL}/questionnaire/${cronDeatails.otpCode}`;
        let alreadyHadParameter = false;
        if (cronDeatails.TargetLang) {
            questionsUrl = `${questionsUrl}?TargetLanguageCode=${cronDeatails.TargetLang}`;
            alreadyHadParameter = true;
        }

        let flag = 0;
        let ClientDetails;
        if (cronDeatails.Table == 'Forms') {
            ClientDetails = await Clients.findOne({
                where: {id: cronDeatails.clientId},
            });
        } else if (cronDeatails.Table == 'Otherparties') {
            ClientDetails = await OtherParties.findOne({
                where: {id: cronDeatails.clientId},
            });
        }
        const practiceDetails = await Practices.findOne({where: {id: ClientDetails.practice_id}});
        const practiceName = practiceDetails.name;
        let message_body = '';
        let smstext_body = '';
        if (cronDeatails.TargetLang && cronDeatails.TargetLang != 'es' && cronDeatails.TargetLang != 'vi') {
            message_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link to access the questionnaire.' + '<br/>' + questionsUrl;
            smstext_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link and enter the verification code ' + cronDeatails.otpCode + ' to access the questionnaire. ' + questionsUrl;
        } else if (cronDeatails.TargetLang == 'es') {
            message_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace para acceder al cuestionario.' + '<br/>' + questionsUrl;
            smstext_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace e ingrese el código de verificación ' + cronDeatails.otpCode + ' para acceder al cuestionario. ' + questionsUrl;
        } else if (cronDeatails.TargetLang == 'vi') {
            message_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết để truy cập bảng câu hỏi.' + '<br/>' + questionsUrl;
            smstext_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết và nhập mã xác minh ' + cronDeatails.otpCode + ' để truy cập bảng câu hỏi. ' + questionsUrl;
        }


        if (cronDeatails.TargetLang && cronDeatails.TargetLang != 'es' && cronDeatails.TargetLang != 'vi') {
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Important: Response required for your lawsuit', message_body, practiceName);
                //await sendEmail('aravinthrifluxyss@gmail.com', 'Important: Response required for your lawsuit', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Important: Response required for your lawsuit', smstext_body);
            }
            flag = 1;
        } else if (cronDeatails.TargetLang == 'es') {
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Importante: Se requiere respuesta para su demanda', message_body, practiceName);
                //await sendEmail('aravinthrifluxyss@gmail.com', 'Importante: Se requiere respuesta para su demanda', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Importante: Se requiere respuesta para su demanda', smstext_body);
            }
            flag = 1;
        } else if (cronDeatails.TargetLang == 'vi') {
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', message_body, practiceName);
                //await sendEmail('aravinthrifluxyss@gmail.com', 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', smstext_body);
            }
            flag = 1;
        }

        const del_legalformsId = cronDeatails.LegalFormsId;
        const del_sendingType = cronDeatails.sendingType;
        const del_TargetLang = cronDeatails.TargetLang;
        if (flag == 1) {
            if (cronDeatails.Table == 'Forms') {
                await Formotp.destroy({
                    where: {
                        legalforms_id: del_legalformsId,
                        sending_type: {[Op.like]: del_sendingType},
                        TargetLanguageCode: del_TargetLang,
                        scheduler_email: true
                    }
                });
            }
            if (cronDeatails.Table == 'Otherparties') {
                await OtherPartiesFormsOtp.destroy({
                    where: {
                        legalforms_id: del_legalformsId,
                        sending_type: {[Op.like]: del_sendingType},
                        TargetLanguageCode: del_TargetLang,
                        scheduler_email: true
                    }
                });
            }
        }
    } catch (err) {
        console.log(err);
        /* await sendEmail('aravinthrifluxyss@gmail.com', 'CRON Email Function Err',err, 'Aravinth Litigait'); */
    }
}

const checkFormsQuestions = async () => {
    try {
        const {Op, OtherPartiesFormsOtp, OtherPartiesForms} = await connectToDatabase();
        const forms = await Forms.findAll({where: {}});
        const fileResponse = await readTxtFile();
        const jsonData = JSON.parse(fileResponse);
        const response = [];
        for (let i = 0; i < forms.length; i += 1) {
            let obj = {};
            let db_form_question_number_text = forms[i].question_number_text;
            let db_form_question_id = forms[i].question_id;
            let db_form_question_section = forms[i].question_section;
            let db_form_question_section_id = forms[i].question_section_id;
            for (const data of jsonData) {
                let json_question_number_text = data.question_number_text;
                let json_question_id = data.question_id;
                let json_question_section = data.question_section;
                let json_question_section_id = data.question_section_id;
                if (db_form_question_number_text == json_question_number_text) {
                    if (db_form_question_section) {
                        if (db_form_question_section_id == json_question_section_id) {
                            if (db_form_question_id != json_question_id) {
                                obj.form_id = forms[i].id;
                                obj.legalforms_id = forms[i].legalforms_id;
                                response.push(obj);
                            }
                            break;
                        }
                    } else {
                        if (db_form_question_id != json_question_id) {
                            obj.form_id = forms[i].id;
                            obj.legalforms_id = forms[i].legalforms_id;
                            response.push(obj);
                        }
                        break;
                    }
                }
            }
        }
    } catch (err) {
        console.log(err);
    }
};

const readTxtFile = () => {
    return new Promise(function (resolve, reject) {
        const s3BucketParams = {
            Bucket: process.env.S3_BUCKET_FOR_TRANSLATIONQUESTIONS,
            Key: "Json-data/disc001.json"
        };
        s3.getObject(s3BucketParams, function (err, data) {
            if (err) {
                reject(err.message);
            } else {
                var data = Buffer.from(data.Body).toString('utf8');
                resolve(data);
            }
        });
    });
}

const medicalHistoryReminder = async (event, context) => {
    try {
        const {Op, MedicalHistory, MedicalHistorySummery, Practices, Orders, Users} = await connectToDatabase();
        const todayDate = new Date();
        // let sample_context = `Your cron function for Medical Reminder"${context.functionName}" ran at ${todayDate}`;
        // await sendEmail('aravinthrifluxyss@gmail.com', 'Cron Schedular MedicalHistory Test', sample_context, 'EsquireTek');
        let TimeStamp = findBusinessDays(todayDate, -10);
        let cDate = TimeStamp.getDate();
        let Year = TimeStamp.getFullYear();
        let Month = TimeStamp.getMonth() + 1;
        if (Month < 10) Month = '0' + Month;
        if (cDate < 10) cDate = '0' + cDate;
        let createdDate = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';

        const medicalhistoryobj = await MedicalHistorySummery.findAll({
            where: {
                createdAt: {[Op.lte]: createdDate},
                status: {[Op.in]: ['Assigned', 'New Request', 'In Progress']},
                scheduler_email: true
            }, order: [['createdAt', 'DESC']], logging: console.log, raw: true
        });
        console.log(medicalhistoryobj.length);
        if (medicalhistoryobj.length != 0) {

            for (let i = 0; i < medicalhistoryobj.length; i++) {
                const updateMedicalObj = await MedicalHistorySummery.findOne({where: {id: medicalhistoryobj[i].id}});
                const OrdersObj = await Orders.findOne({where: {id: medicalhistoryobj[i].order_id}, raw: true});
                const usersObj = await Users.findOne({where: {id: OrdersObj.user_id}, raw: true});
                console.log(medicalhistoryobj[i].request_date);
                let requestDate = new Date(medicalhistoryobj[i].request_date);
                let convertedDate = requestDate.getDate();
                let convertedMonth = requestDate.getMonth() + 1;
                let convertedYear = requestDate.getFullYear();
                if (convertedDate < 10) convertedDate = '0' + convertedDate;
                if (convertedMonth < 10) convertedMonth = '0' + convertedMonth;
                let submitted_date = convertedMonth + '/' + convertedDate + '/' + convertedYear;

                const practicesObj = await Practices.findOne({
                    where: {id: medicalhistoryobj[i].practice_id},
                    raw: true
                });
                const message_body = 'Our team is hard at work creating the medical chronology for ' + practicesObj.name + ' that you submitted on ' + submitted_date + '. The file is being reviewed for accuracy and we anticipate it being completed in the  next few days.<br/><br/>If you have any questions please email <u>support@esquiretek.com</u>.Thank you.';
                console.log(usersObj.email);
                await sendEmail(usersObj.email, 'Important: Summary Document In Progress', message_body, 'EsquireTek');
                // await sendEmail('aravinthrifluxyss@gmail.com', 'Important: Summary Document In Progress', message_body, 'EsquireTek');
                updateMedicalObj.scheduler_email = false;
                await updateMedicalObj.save();
            }
        } else {
            console.log('No Medical History ' + createdDate);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'Ok'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not send Medical History.'}),
        };
    }
};

//pass postive value to get the after the nth business days
//pass negative value to get the before the nth business days
function findBusinessDays(d, n) {
    d = new Date(d.getTime());
    var day = d.getDay();
    d.setDate(d.getDate() + n + (day === 6 ? 2 : +!day) + (Math.floor((n - 1 + (day % 6 || 1)) / 5) * 2));
    return d;
};

const subscriptionPlanRemainder = async (event) => {
    try {
        const {Op, Practices, Users, SubscriptionHistory, sequelize} = await connectToDatabase();
        let currentDate = new Date();

        currentDate.setDate(currentDate.getDate() - 5);
        let cDate = currentDate.getDate();
        let Year = currentDate.getFullYear();
        let Month = currentDate.getMonth() + 1;
        if (Month < 10) Month = '0' + Month;
        if (cDate < 10) cDate = '0' + cDate;
        let startDate = Year + '-' + Month + '-' + cDate + ' ' + '00:00:00';
        let endDate = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';

        const subscriptionObj = await SubscriptionHistory.findAll({
            where: {
                plan_type: 'yearly',
                is_reminder_send: {[Op.not]: true},
                subscription_valid_till: {[Op.between]: [startDate, endDate]},
                is_reminder_send: {[Op.not]: true}
            }, logging: console.log
        });

        for (let i = 0; i < subscriptionObj.length; i++) {
            let subscriptionDeatils = subscriptionObj[i];
            const practiceObj = await Practices.findOne({
                where: {
                    id: subscriptionDeatils.practice_id,
                    is_deleted: {[Op.not]: true}
                }
            });
            if (practiceObj) {
                const usersObj = await Users.findAll({
                    where: {
                        practice_id: subscriptionDeatils.practice_id,
                        role: 'lawyer',
                        is_admin: true,
                        is_deleted: {[Op.not]: true}
                    }, raw: true
                });
                const usersEmail = [];
                for (let j = 0; j < usersObj.length; j++) {
                    usersEmail.push(usersObj[j].email);
                }

                let emailTemplate = 'Hi,' + '<br/><br/>' +
                    'This is an automatic email to remind you that your account is up for renewal in five days. The credit card on file will be charged for another yearly subscription unless we hear from you. Thank you for using EsquireTek and please email us at <u>gerry@esquiretek.com</u> if you have any questions. Thank you.' +
                    '<br/><br/>' +
                    'Signed,<br/>' +
                    'Gerry Espinoza<br/>' +
                    'Operations Manager<br/>';
                await sendEmail(usersEmail, 'Important: Yearly Subscription Reminder', emailTemplate, 'EsquireTek');
                const updateSubscriptionObj = await SubscriptionHistory.findOne({where: {id: subscriptionDeatils.id}});
                updateSubscriptionObj.is_reminder_send = true;
                await updateSubscriptionObj.save();
            }
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({message: "Reminders sent to client"}),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not send subscription remainder.'}),
        };
    }
}

const clientsQuestionsRemainderCron = async() => {
    try{

        const { Forms, Formotp, Op, sequelize, Cases} = await connectToDatabase();

        let TimeStamp = new Date();
        let Day = TimeStamp.getDate();
        let Year = TimeStamp.getFullYear();
        let Month = TimeStamp.getMonth() + 1;

        if (Month < 10) Month = '0' + Month;
        if (Day < 10) Day = '0' + Day;

        let date = Year+'-'+Month+'-'+Day;


        const sqlQuery = `SELECT id, otp_code, otp_secret, client_id, document_type, case_id, legalforms_id, sending_type, question_ids, sent_by, `+
         `scheduler_email, TargetLanguageCode, latest_client_response_date, is_client_answered_all_questions, reminder_meantime, `+
         `latest_reminder_send_date, reminder_till_date, `+
         `DATE_ADD(latest_reminder_send_date, INTERVAL reminder_meantime DAY) as upcoming_schedulare_date `+
         `FROM Formotps WHERE sending_type in ('all','selected_questions') and reminder_meantime IS NOT NULL and scheduler_email is true and `+
         `is_client_answered_all_questions is not true and reminder_till_date >= now() `+
         `having upcoming_schedulare_date <= CONCAT('${date}','23:59:59') and upcoming_schedulare_date >= CONCAT('${date}','00:00:00') `;
         console.log(sqlQuery);

        const fetchAllFormsOtpData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT,
        });

/*         const fetchAllFormsOtpData = await Formotp.findAll({
            where: {
                sending_type:{[Op.in]:['all','selected_questions']},
                scheduler_email: true, is_client_answered_all_questions: false, latest_client_response_date: {[Op.not]: null },
            },
            logging: console.log
        }); */

        for (let i = 0; i < fetchAllFormsOtpData.length; i++) {
            const Otpdata = fetchAllFormsOtpData[i];
            let { case_id, client_id, legalforms_id, TargetLanguageCode, otp_row_id, document_type,
                sending_type, latest_client_response_date, reminder_meantime,
                reminder_till_date, question_ids, sent_by
            } = Otpdata;

            const otp_columns = {
                case_id, client_id, legalforms_id, document_type, otp_secret, otp_code,
                id: otp_row_id, TargetLanguageCode
            };
            const casesObj = await Cases.findOne({ where: { id: case_id, is_deleted: { [Op.not]: true }, is_archived: { [Op.not]: true }, } });
            if (casesObj?.id) {
                let otp_code = generateRandomString(8);
                let otp_secret = generateRandomString(4);
                let formsOtpColumn = {
                    id: uuid.v4(), otp_code: otp_code, otp_secret: otp_secret,
                    client_id, document_type, case_id, legalforms_id, sending_type,
                    sent_by, TargetLanguageCode, reminder_meantime, reminder_till_date, latest_reminder_send_date: new Date(),
                    is_client_answered_all_questions: false, scheduler_email: true
                };
                let query = {case_id,client_id,legalforms_id,document_type,client_response_status:'ClientResponseAvailable'};

                if (sending_type == 'selected_questions') {
                    query.id = {[Op.in]:JSON.parse(question_ids)};
                    formsOtpColumn.question_ids = question_ids;
                }
                /* Get Latest client Response Date */
                const checkLatestClientResponse = await Forms.findAll({
                    where: query,
                    attributes: [[sequelize.fn('max', sequelize.col('last_updated_by_client')), 'recent_response_date']],
                });

                if (checkLatestClientResponse?.recent_response_date) {
                    formsOtpColumn.latest_client_response_date = checkLatestClientResponse.recent_response_date;
                }
                /* Get UnAnswered Questions Count */
                query.client_response_status = 'SentToClient';
                const checkClientAnsweredQuestionsCount = await Forms.count({ where: query });

                if (checkClientAnsweredQuestionsCount == 0) {
                    formsOtpColumn.is_client_answered_all_questions = true;
                    formsOtpColumn.scheduler_email = false;
                }
                await Formotp.destroy({ where: {
                    case_id, client_id, legalforms_id, document_type, TargetLanguageCode, otp_code: Otpdata.otp_code, otp_secret: Otpdata.otp_secret }
                });
                const createOtpResponse = await Formotp.create(formsOtpColumn);
                const otpPlainText = createOtpResponse.get({ plain: true });
                await EmailAndSmsTemplate(otpPlainText);
            } else {
                await Formotp.destroy({where:{id:case_id,client_id,}})
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'Ok'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not send form questions email remainder email'}),
        };
    }
}

const OtherPartiesQuestionsRemainderCron = async() => {
    try {
        const { Op, OtherPartiesFormsOtp, OtherPartiesForms, Cases, OtherParties, sequelize } = await connectToDatabase();

        let TimeStamp = new Date();
        let Day = TimeStamp.getDate();
        let Year = TimeStamp.getFullYear();
        let Month = TimeStamp.getMonth() + 1;

        if (Month < 10) Month = '0' + Month;
        if (Day < 10) Day = '0' + Day;

        let date = Year+'-'+Month+'-'+Day;

        const sqlQuery = `SELECT id, otp_code, otp_secret, party_id,document_type,case_id,legalforms_id,sending_type,question_id,sent_by, `+
        `scheduler_email,TargetLanguageCode, latest_client_response_date, is_client_answered_all_questions, reminder_meantime, `+
        `latest_reminder_send_date, reminder_till_date, `+
        `DATE_ADD(latest_reminder_send_date, INTERVAL reminder_meantime DAY) as upcoming_schedulare_date From `+
        `OtherPartiesFormsOtps where sending_type in ('all','selected_questions') and `+
        `reminder_meantime IS NOT NULL and scheduler_email is true and is_client_answered_all_questions is not true and `+
        `reminder_till_date >= now() having upcoming_schedulare_date <= CONCAT('${date}','23:59:59') and upcoming_schedulare_date >= CONCAT('${date}','00:00:00') `;
        console.log(sqlQuery);

       const getAllFormOtps = await sequelize.query(sqlQuery, {
           type: QueryTypes.SELECT,
       });



        for (let i = 0; i < getAllFormOtps.length; i++) {
            const Otpdata = getAllFormOtps[i];
            let { party_id, case_id, legalforms_id, document_type, sending_type,
                sent_by, scheduler_email, TargetLanguageCode,
                is_client_answered_all_questions, reminder_meantime, latest_reminder_send_date,
                reminder_till_date, question_id
            } = Otpdata;

            const casesObj = await Cases.findOne({ where: { id: case_id, is_deleted: { [Op.not]: true }, is_archived: { [Op.not]: true }, } });

            if (casesObj?.id) {
                let otp_code = generateRandomString(8);
                let otp_secret = generateRandomString(4);
                let formsOtpColumn = {
                    id: uuid.v4(), otp_code: otp_code, otp_secret: otp_secret,
                    party_id, document_type, case_id, legalforms_id, sending_type, sent_by,
                    scheduler_email: true, TargetLanguageCode, is_client_answered_all_questions,
                    reminder_meantime, latest_reminder_send_date: new Date(), reminder_till_date,
                }
                let query = { case_id, party_id, legalforms_id, document_type, client_response_status: 'ClientResponseAvailable' };

                if(sending_type == 'selected_questions'){
                    query.id = {[Op.in]:JSON.parse(question_id)}
                    formsOtpColumn.question_id = question_id;
                }
                /* Get Latest client Response Date */
                const checkLatestClientResponse = await OtherPartiesForms.findAll({
                    where: query,
                    attributes: [[sequelize.fn('max', sequelize.col('last_updated_by_client')), 'recent_response_date']],
                });

                if (checkLatestClientResponse?.recent_response_date) {
                    formsOtpColumn.latest_client_response_date = checkLatestClientResponse.recent_response_date;
                }
                /* Get UnAnswered Questions Count */
                query.client_response_status = 'SentToClient';
                const checkClientAnsweredQuestionsCount = await OtherPartiesForms.count({ where: query });

                if (checkClientAnsweredQuestionsCount == 0) {
                    formsOtpColumn.is_client_answered_all_questions = true;
                    formsOtpColumn.scheduler_email = false;
                }

                await OtherPartiesFormsOtp.destroy({ where: {
                    case_id, party_id, legalforms_id, document_type, TargetLanguageCode, otp_code: Otpdata.otp_code, otp_secret: Otpdata.otp_secret }
                });
                const createOtpResponse = await OtherPartiesFormsOtp.create(formsOtpColumn);
                const otpPlainText = createOtpResponse.get({ plain: true });
                await EmailAndSmsTemplate(otpPlainText);
            } else {
                await OtherPartiesFormsOtp.destroy({where:{case_id, party_id, legalforms_id, document_type,otp_code:Otpdata.otp_code,otp_secret:Otpdata.otp_secret}});
            }
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ message: 'Ok' }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not send other party form questions remainder email.' }),
        };
    }
}
const unansweredQuestionsRemainderCronForOtherParties_old = async() => {
    try{
        const { Op, OtherPartiesFormsOtp, OtherPartiesForms, Cases, OtherParties } = await connectToDatabase();

        const getAllFormOtps = await OtherPartiesFormsOtp.findAll({
            where: {
                sending_type:{[Op.in]:['all','selected_questions']},
                scheduler_email: true,
                is_client_answered_all_questions: false,
                latest_client_response_date: {[Op.not]: null },
            },  order: [['createdAt', 'DESC']],
        });
        if(getAllFormOtps.length != 0) {
            for (let i = 0; i < getAllFormOtps.length; i++) {

            const remainderTimeStamp = getAllFormOtps[i].latest_reminder_send_date;

            remainderTimeStamp.setDate(remainderTimeStamp.getDate() - getAllFormOtps[i].reminder_meantime);
            let cDate = remainderTimeStamp.getDate();
            let Year = remainderTimeStamp.getFullYear();
            let Month = remainderTimeStamp.getMonth() + 1;
            if (Month < 10) Month = '0' + Month;
            if (cDate < 10) cDate = '0' + cDate;
            let remainderDate = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';

            const Otpdata = getAllFormOtps[i];
            let {case_id , legalforms_id, document_type, sending_type, TargetLanguageCode } = otpObject;

                const casesObj = await Cases.findOne({
                    where: { id: case_id, is_deleted: {[Op.not]: true}, is_archived: {[Op.not]: true},}
                });

                if (casesObj) {
                    if (sending_type == 'selected_questions') {
                        const findDuplicateDatas = await OtherPartiesFormsOtp.findAll({
                            where: {
                                sending_type: {[Op.like]: 'selected_questions'},
                                scheduler_email: true, legalforms_id, document_type, case_id, TargetLanguageCode,
                                is_client_answered_all_questions: false,
                                reminder_till_date: { [Op.gt]: remainderDate },
                                latest_client_response_date: {[Op.not]: null}
                            }, order: [['createdAt', 'DESC']]
                        });
                        let languageType = [];
                        for (let j = 0; j < findDuplicateDatas.length; j++) {
                            const duplicateRow = findDuplicateDatas[j];

                            if (languageType.length == 0 || !languageType.includes(duplicateRow.TargetLanguageCode)) {
                                let newSelectedOtp = {};
                                languageType.push(duplicateRow.TargetLanguageCode);

                                const getFormsData = await OtherPartiesForms.findAll({
                                    where: {
                                        legalforms_id: duplicateRow.legalforms_id,
                                        case_id: duplicateRow.case_id,
                                        TargetLanguageCode: {[Op.like]: duplicateRow.TargetLanguageCode},
                                        client_response_status: {[Op.like]: 'SentToClient'}
                                    }, attributes: ['id'],
                                });
                                if (getFormsData.length != 0) {
                                    let questionIdsArr = [];
                                    for (let qid = 0; qid < getFormsData.length; qid++) {
                                        let nt = getFormsData[qid].id;
                                        questionIdsArr.push(nt);
                                    }

                                    newSelectedOtp.id = uuid.v4();
                                    newSelectedOtp.otp_code = generateRandomString(8);
                                    newSelectedOtp.otp_secret = generateRandomString(4);
                                    newSelectedOtp.party_id = duplicateRow.party_id;
                                    newSelectedOtp.document_type = duplicateRow.document_type;
                                    newSelectedOtp.question_type = duplicateRow.question_type;
                                    newSelectedOtp.case_id = duplicateRow.case_id;
                                    newSelectedOtp.legalforms_id = duplicateRow.legalforms_id;
                                    newSelectedOtp.sending_type = duplicateRow.sending_type;
                                    newSelectedOtp.question_id = JSON.stringify(questionIdsArr);
                                    newSendAllOtp.reminder_meantime = sendAllOtpObj.reminder_meantime;
                                    newSendAllOtp.reminder_till_date = sendAllOtpObj.reminder_till_date;
                                    newSendAllOtp.latest_reminder_send_date = new Date();
                                    newSelectedOtp.sent_by = duplicateRow.sent_by;
                                    newSelectedOtp.scheduler_email = true;
                                    newSelectedOtp.TargetLanguageCode = duplicateRow.TargetLanguageCode;

                                    let emailandSmsObj = {
                                        clientId: duplicateRow.party_id,
                                        otpCode: newSelectedOtp.otp_code,
                                        otpSecret: newSelectedOtp.otp_secret,
                                        TargetLang: duplicateRow.TargetLanguageCode,
                                        LegalFormsId: duplicateRow.legalforms_id,
                                        sendingType: 'selected_questions',
                                        Table: 'Otherparties'
                                    };
                                    await OtherPartiesFormsOtp.create(newSelectedOtp);
                                    await sendCronEmailandSms(emailandSmsObj);
                                } else {
                                    await OtherPartiesFormsOtp.destroy({
                                        where: {
                                            legalforms_id: duplicateRow.legalforms_id,
                                            case_id: duplicateRow.case_id,
                                            sending_type: {[Op.like]: 'selected_questions'},
                                            TargetLanguageCode: duplicateRow.TargetLanguageCode,
                                            scheduler_email: true
                                        }
                                    });
                                }
                            }
                        }
                    }else if (sending_type == 'all') {
                        const checkQuestionStatus = await OtherPartiesForms.findAll({
                            where: {
                                legalforms_id: otpObject.legalforms_id,
                                case_id: otpObject.case_id,
                                TargetLanguageCode: {[Op.like]: otpObject.TargetLanguageCode},
                                client_response_status: {[Op.like]: 'SentToClient'}
                            }
                        });
                        const sendAllOtpObj = await OtherPartiesFormsOtp.findOne({
                            where: {
                                sending_type: {[Op.like]: 'all'},
                                scheduler_email: true,
                                legalforms_id,
                                document_type,
                                case_id,
                                TargetLanguageCode,
                                is_client_answered_all_questions: false,
                                reminder_till_date: { [Op.gt]: remainderDate },
                                latest_client_response_date: {[Op.not]: null}
                            }, order: [['createdAt', 'DESC']]
                        });
                        if (checkQuestionStatus.length != 0 && sendAllOtpObj) {
                            let newSendAllOtp = {
                                id : uuid.v4(),
                                otp_code : generateRandomString(8),
                                otp_secret : generateRandomString(4),
                                party_id : sendAllOtpObj.party_id,
                                document_type : sendAllOtpObj.document_type,
                                question_type : sendAllOtpObj.question_type,
                                case_id : sendAllOtpObj.case_id,
                                legalforms_id : sendAllOtpObj.legalforms_id,
                                reminder_meantime : sendAllOtpObj.reminder_meantime,
                                reminder_till_date : sendAllOtpObj.reminder_till_date,
                                latest_reminder_send_date : new Date(),
                                sending_type : sendAllOtpObj.sending_type,
                                sent_by : sendAllOtpObj.sent_by,
                                scheduler_email : true,
                                TargetLanguageCode : sendAllOtpObj.TargetLanguageCode
                            }

                            let emailandSmsforAllQuesObj = {
                                clientId: sendAllOtpObj.party_id,
                                otpCode: newSendAllOtp.otp_code,
                                otpSecret: newSendAllOtp.otp_secret,
                                TargetLang: sendAllOtpObj.TargetLanguageCode,
                                LegalFormsId: sendAllOtpObj.legalforms_id,
                                sendingType: 'all',
                                Table: 'Otherparties'
                            };
                            await OtherPartiesFormsOtp.create(newSendAllOtp);
                            await sendCronEmailandSms(emailandSmsforAllQuesObj);
                        } else {
                            await OtherPartiesFormsOtp.destroy({
                                where: {
                                    legalforms_id: otpObject.legalforms_id,
                                    case_id: otpObject.case_id,
                                    sending_type: {[Op.like]: 'all'},
                                    TargetLanguageCode: otpObject.TargetLanguageCode,
                                    scheduler_email: true
                                }
                            });
                        }
                    }
                } else {
                    console.log('Cases Deatails not found');
                }

            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'Ok'}),
        };
    } catch(err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not send other party form questions remainder email.'}),
        };
    }
}
module.exports.caseReminderCron = caseReminderCron;
module.exports.medicalHistoryReminder = medicalHistoryReminder;
module.exports.subscriptionPlanRemainder = subscriptionPlanRemainder;
module.exports.clientsQuestionsRemainderCron = clientsQuestionsRemainderCron;
module.exports.OtherPartiesQuestionsRemainderCron = OtherPartiesQuestionsRemainderCron;
