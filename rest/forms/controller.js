const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const {generateRandomString} = require('../../utils/randomStringGenerator');
const {sendEmail} = require('../../utils/mailModule');
const {sendSMS} = require('../../utils/smsModule');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION || 'us-east-1'});
const s3 = new AWS.S3();
const {QueryTypes} = require('sequelize');
const {findQuestionDisk} = require('./forms_question_number');
const { validateCreateForms, validateGetOneForm, validateUpdateForms, validateDeleteForm, validateReadForms, validateSentQuestionToClient,
    validateClientQuestionAnswering, validateVerificationRequestToClient, validateResponseDate } = require('./validation');
const lodash = require('lodash');

const { getResponseHistory } = require('../helpers/forms.helper');
const { checkResponseHistoryCount } = require('../helpers/responseHistory.helper');

const create = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, {id: id || uuid.v4()});
        validateCreateForms(dataObject);
        const {Forms} = await connectToDatabase();
        const forms = await Forms.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(forms),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the forms.'}),
        };
    }
};
const getOne = async (event) => {
    try {
        validateGetOneForm(event.pathParameters);
        const {Forms} = await connectToDatabase();
        const forms = await Forms.findOne({where: {id: event.pathParameters.id}});
        if (!forms) throw new HTTPError(404, `Forms with id: ${event.pathParameters.id} was not found`);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(forms),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Forms.'}),
        };
    }
};
const getAll = async (event) => {
    try {
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        query.raw = true;
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        validateReadForms(query);
        const {Forms} = await connectToDatabase();
        const formss = await Forms.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(formss),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the forms'}),
        };
    }
};
const update = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateUpdateForms(input);
        const {Forms} = await connectToDatabase();
        const forms = await Forms.findOne({where: {id: event.pathParameters.id}});
        if (!forms) throw new HTTPError(404, `Forms with id: ${event.pathParameters.id} was not found`);
        const updatedModel = Object.assign(forms, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};
const destroy = async (event) => {
    try {
        validateDeleteForm(event.pathParameters);
        const {Forms} = await connectToDatabase();
        const forms = await Forms.findOne({where: {id: event.pathParameters.id}});
        if (!forms) throw new HTTPError(404, `Forms with id: ${event.pathParameters.id} was not found`);
        await forms.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(forms),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could destroy fetch the Forms.'}),
        };
    }
};

const getFormDataByCaseidAndDocumentType = async (event) => {
    try {
        const {Op, Forms, LegalForms, sequelize, FormsResponseHistory} = await connectToDatabase();
        const case_id = event.pathParameters.case_id;
        const document_type = event.pathParameters.document_type;
        const queryParams = event.queryStringParameters || {};
        const legalfromsObj = await LegalForms.findOne({where: {id: queryParams.legalforms_id}, raw: true});
        if (queryParams.party_id && queryParams.party_id !== undefined) {
            return await getOtherPartyFormDataByCaseidAndDocumentType(event);
        }
        const query = {};
        query.raw = true;
        query.logging = console.log;
        query.where = {case_id, document_type, practice_id: event.user.practice_id};
        if (queryParams.lawyer_response_status_filter) {
            if (queryParams.lawyer_response_status_filter.toLowerCase() !== 'all') {
                query.where.lawyer_response_status = queryParams.lawyer_response_status_filter;
            }
        }

        if (queryParams.client_response_status_filter) {
            if (queryParams.client_response_status_filter.toLowerCase() !== 'all') {
                if (queryParams.client_response_status_filter == 'SentToClient') {
                    query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable']};
                } else {
                    query.where.client_response_status = queryParams.client_response_status_filter;
                }
            }
        }

        query.order = [
            ['question_id', 'ASC'],
        ];
        if (queryParams.legalforms_id) {
            query.where.legalforms_id = queryParams.legalforms_id;
        }
        const forms = await Forms.findAll(query);

        const compare = (x1, x2) => {
            let a = x1.question_number_text;
            let b = x2.question_number_text;
            let section_id_a = parseInt(x1.question_section_id);
            let section_id_b = parseInt(x2.question_section_id);
            if (a === b) {
                if (section_id_a < section_id_b) {
                    return -1;
                }
                if (section_id_a > section_id_b) {
                    return 1;
                }

                return 0
            }
            ;
            const aArr = a.split("."), bArr = b.split(".");
            for (let i = 0; i < Math.min(aArr.length, bArr.length); i++) {
                if (parseInt(aArr[i]) < parseInt(bArr[i])) {
                    return -1
                }
                ;
                if (parseInt(aArr[i]) > parseInt(bArr[i])) {
                    return 1
                }
                ;
            }
            if (aArr.length < bArr.length) {
                return -1
            }
            ;
            if (aArr.length > bArr.length) {
                return 1
            }
            ;
            return 0;
        };
        forms.sort(compare);
        forms.map(obj => {
            obj.defendant_practice_details = legalfromsObj.defendant_practice_details;
        });


        let finalQuestions;
        if (document_type && document_type.toUpperCase() === 'FROGS') {

            const maxConsultationNo = await Forms.findAll({
                where: {
                    legalforms_id: queryParams.legalforms_id,
                    case_id,
                    document_type,
                    practice_id: event.user.practice_id,
                    is_consultation_set: true
                },
                attributes: [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']]
            });
            forms.map(obj => {
                obj.total_consultation_count = maxConsultationNo[0].consultation_set_no;
            });

            finalQuestions = await groupByFormsConsultation(forms);
        } else {
            finalQuestions = forms;
        }

        //selva changes, version history - start
        const responseHistories = await FormsResponseHistory.findAll({
            where: {
                practice_id: event.user.practice_id,
                case_id: case_id,
                document_type: document_type,
                legalforms_id: queryParams.legalforms_id
            },
            logging:console.log
        })
        const responseHistoriesDict = responseHistories.reduce((newObj, responseHistory) => {
            let parse_data = JSON.parse(responseHistory.response);
            parse_data.sort((a,b) => b.version - a.version);
            newObj[responseHistory.form_id] = parse_data;
            return newObj;
        }, {});
        const questionsWithResponseHistoryDetails = finalQuestions.map(question => {
            const responseHistoryDetails = responseHistoriesDict[question.id] || "";
            return { ...question, responseHistory: responseHistoryDetails };
        });
        //selva changes, version history - end

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(questionsWithResponseHistoryDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the forms'}),
        };
    }
};

const getOtherPartyFormDataByCaseidAndDocumentType = async (event) => {
    try {
        const {Op, OtherPartiesForms, Forms, LegalForms, sequelize, FormsResponseHistory} = await connectToDatabase();
        const case_id = event.pathParameters.case_id;
        const document_type = event.pathParameters.document_type;
        const queryParams = event.queryStringParameters || {};
        const query = {};
        //query.raw = true;
        const party_id = queryParams.party_id;
        const legalforms_id = queryParams.legalforms_id;
        const legalfromsObj = await LegalForms.findOne({where: {id: legalforms_id}, raw: true});
        if (!party_id) throw new HTTPError(404, `Other Parties Forms with id: ${party_id} was not found`);
        let lawyer_response_status = '';
        let client_response_status = '';
        if (queryParams.lawyer_response_status_filter) {
            if (queryParams.lawyer_response_status_filter.toLowerCase() !== 'all') {
                lawyer_response_status = queryParams.lawyer_response_status_filter;
            } else {
                lawyer_response_status = {[Op.in]: ['NotStarted', 'Draft', 'Final']};
            }
        }

        if (queryParams.client_response_status_filter) {
            if (queryParams.client_response_status_filter.toLowerCase() !== 'all') {

                if (queryParams.client_response_status_filter == 'SentToClient') {
                    client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable']};
                } else {
                    client_response_status = queryParams.client_response_status_filter;
                }
            } else {
                client_response_status = {[Op.in]: ['NotSetToClient', 'SentToClient', 'ClientResponseAvailable']};
            }
        }

        let order = [
            ['question_id', 'ASC'],
        ];
        const otherPartiesFormsPartyID = await OtherPartiesForms.findAll({
            where: {
                client_response_status: client_response_status,
                practice_id: event.user.practice_id,
                case_id,
                document_type,
                party_id,
                legalforms_id
            },
            include: [
                {
                    model: Forms, as: 'Form',
                    attributes: ['lawyer_response_status', 'lawyer_response_text'],
                    where: {
                        lawyer_response_status: lawyer_response_status
                    },
                    required: true
                }
            ],
            order: order,
            raw: true,
            logging: console.log

        });
        const response = [];
        for (let i = 0; i < otherPartiesFormsPartyID.length; i++) {
            let otherPartiesObject = otherPartiesFormsPartyID[i];
            const form = otherPartiesObject.Form;

            let obj = {
               ...otherPartiesObject
            };

            if (obj.Form) delete obj.Form;
            if(form?.dataValues?.lawyer_response_text)obj.lawyer_response_text = form.dataValues.lawyer_response_text;
            if(form?.dataValues?.lawyer_response_status)obj.lawyer_response_status = form.dataValues.lawyer_response_status;
            if(form?.dataValues?.last_updated_by_lawyer)obj.last_updated_by_lawyer= form.dataValues.last_updated_by_lawyer;
            response[i] = obj;
        }

        const compare = (x1, x2) => {
            let a = x1.question_number_text;
            let b = x2.question_number_text;
            let section_id_a = parseInt(x1.question_section_id);
            let section_id_b = parseInt(x2.question_section_id);
            if (a === b) {
                if (section_id_a < section_id_b) {
                    return -1;
                }
                if (section_id_a > section_id_b) {
                    return 1;
                }

                return 0
            }
            ;
            const aArr = a.split("."), bArr = b.split(".");
            for (let i = 0; i < Math.min(aArr.length, bArr.length); i++) {
                if (parseInt(aArr[i]) < parseInt(bArr[i])) {
                    return -1
                }
                ;
                if (parseInt(aArr[i]) > parseInt(bArr[i])) {
                    return 1
                }
                ;
            }
            if (aArr.length < bArr.length) {
                return -1
            }
            ;
            if (aArr.length > bArr.length) {
                return 1
            }
            ;
            return 0;
        };
        response.sort(compare);
        response.map(obj => {
            obj.defendant_practice_details = legalfromsObj.defendant_practice_details;
        });

        const responseHistories = await FormsResponseHistory.findAll({
            where: {
                practice_id: event.user.practice_id, case_id: case_id,
                document_type: document_type, legalforms_id: queryParams.legalforms_id
            },
            logging:console.log
        })
        const responseHistoriesDict = responseHistories.reduce((newObj, responseHistory) => {
            let parse_data = JSON.parse(responseHistory.response);
            parse_data.sort((a,b) => b.version - a.version);
            newObj[responseHistory.form_id] = parse_data;
            return newObj;
        }, {});

        const questionsWithResponseHistoryDetails = response.map(question => {
            const responseHistoryDetails = responseHistoriesDict[question.form_id] || "";
            return { ...question, responseHistory: responseHistoryDetails };
        });

        let finalQuestions;
        if (document_type && document_type.toUpperCase() === 'FROGS') {

            const maxConsultationNo = await OtherPartiesForms.findAll({
                where: { legalforms_id, case_id, document_type, party_id,
                    practice_id: event.user.practice_id, is_consultation_set: true
                },
                attributes: [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']]
            });
            questionsWithResponseHistoryDetails.map(obj => {
                obj.total_consultation_count = maxConsultationNo[0].consultation_set_no;
            });

            finalQuestions = await groupByFormsConsultation(questionsWithResponseHistoryDetails);
        } else {
            finalQuestions = questionsWithResponseHistoryDetails;
        }
        console.log(finalQuestions);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(finalQuestions),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the forms'}),
        };
    }
};
const UpdateFormDataByCaseidAndDocumentType = async (event) => {
    try {
        const case_id = event.pathParameters.case_id;
        const document_type = event.pathParameters.document_type;
        const input = JSON.parse(event.body);
        const {Forms, OtherPartiesForms, FormsResponseHistory, Practices, Cases, Op, Settings} = await connectToDatabase();

        const form_id = input?.client_id ? input?.id : input?.form_id; //get form's id.
        let updated_question_response, is_otherparties_form = false;
        if(!input?.client_id) is_otherparties_form = true;
        let [forms_response, practiceObj, casesObj] = await Promise.all([
            Forms.findOne({where: {id: form_id}}),
            Practices.findOne({ where: { id: event.user.practice_id }, raw: true }),
            Cases.findOne({where:{practice_id: event.user.practice_id,id:case_id}, raw: true})
        ]);

        /* Get Previous Response History */
        const formsResponseHistoryObj = await FormsResponseHistory.findOne({
            where: { practice_id: forms_response.practice_id, case_id, form_id, legalforms_id: forms_response.legalforms_id },
            logging: console.log
        });

        const otherparties_obj = await OtherPartiesForms.findOne({where: {id: input.id}});

        if(is_otherparties_form && !otherparties_obj?.id) throw new HTTPError(404, `Question id ${input.id} was not found`);
        if(is_otherparties_form && otherparties_obj?.id){
            forms_response = otherparties_obj.get({plain: true});
        }

        if(forms_response?.id){
            if(is_otherparties_form && otherparties_obj?.id){
                otherparties_obj.subgroup = input.subgroup;
                await otherparties_obj.save();
                }
            const saveFormsObj = await Forms.findOne({where: {id:form_id},logging:console.log});
            if(!is_otherparties_form && !otherparties_obj?.id) saveFormsObj.subgroup = input.subgroup;
                saveFormsObj.lawyer_response_text = input.lawyer_response_text;
                saveFormsObj.lawyer_response_status = input.lawyer_response_status;
                saveFormsObj.last_updated_by_lawyer = new Date();
                await saveFormsObj.save();

            const forms_obj = await Forms.findOne({where: {id: form_id},logging:console.log, raw: true});
            if (is_otherparties_form  && otherparties_obj?.id) {
                console.log('first if');
                updated_question_response = await OtherPartiesForms.findOne({where: {id: input.id}, raw: true});
                updated_question_response.lawyer_response_text = forms_obj.lawyer_response_text;
                updated_question_response.lawyer_response_status = forms_obj.lawyer_response_status;
                updated_question_response.last_updated_by_lawyer = forms_obj.last_updated_by_lawyer;
            } else {
                console.log('first else');
                updated_question_response = forms_obj;
            }
            }


        if (practiceObj?.global_attorney_response_tracking && casesObj?.attorney_response_tracking) {
            if (formsResponseHistoryObj?.id) {
                const settingsObj = await Settings.findOne({where:{key:'global_settings'},raw:true});
                const settings_value = JSON.parse(settingsObj.value);
                let response_history_limit;
                let responseHistory = JSON.parse(formsResponseHistoryObj.response);
                let version_no = responseHistory[responseHistory.length-1].version;
                response_history_limit = settings_value?.response_history_limit ? settings_value.response_history_limit : 100;
                responseHistory = checkResponseHistoryCount(responseHistory,response_history_limit);
                responseHistory.push({
                    version: version_no + 1,
                    response: input.lawyer_response_text,
                    user_id: event.user.id,
                    timestamp: new Date(),
                    color_code: input.color_code,
                    status: input.lawyer_response_status

                });
                formsResponseHistoryObj.response = JSON.stringify(responseHistory);
                await formsResponseHistoryObj.save();
            } else {
                let responseObj = [{
                    version: 1,
                    response: input.lawyer_response_text,
                    user_id: event.user.id,
                    timestamp: new Date(),
                    color_code: input.color_code,
                    status: input.lawyer_response_status
                }];
                let responseHistoryObj = {
                    id: uuid.v4(),
                    practice_id: input.practice_id,
                    form_id: input.id,
                    legalforms_id: input.legalforms_id,
                    document_type: input.document_type,
                    case_id: case_id,
                    response: JSON.stringify(responseObj)
                }
                await FormsResponseHistory.create(responseHistoryObj)
            }
        }
        const response_history =  await FormsResponseHistory.findOne({
            where: { practice_id: forms_response.practice_id, case_id, form_id, legalforms_id: forms_response.legalforms_id },
            raw: true
        });

        if (response_history?.response) {
            let lawyer_response = JSON.parse(response_history.response);
            lawyer_response.sort((a, b) => b.version - a.version);
            updated_question_response = Object.assign({}, { ...updated_question_response }, { responseHistory: lawyer_response });
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updated_question_response),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};
const updateSubgroupStatus = async (event) => {
    try {
        const input = JSON.parse(event.body);

        const {Forms, OtherPartiesForms} = await connectToDatabase();
        let response = [];
        let plaintText = undefined;
        let is_otherparties_form = false;
        for (let i = 0; i < input.length; i++) {
            let forms = undefined;
            forms = await Forms.findOne({where: {id: input[i].id}});
            if (!forms) {
                forms = await OtherPartiesForms.findOne({where: {id: input[i].id}});
                is_otherparties_form = true;
            }
            if (forms) {
                forms.subgroup = input[i].subgroup;
                await forms.save();
                if (is_otherparties_form) {
                    plaintText = await OtherPartiesForms.findOne({where: {id: input[i].id}});
                } else {
                    plaintText = await Forms.findOne({where: {id: input[i].id}});
                }
                response[i] = plaintText.get({plain: true});
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};
const saveAlllawyerResponseByCaseidAndDocumentTypeOld = async (event) => {
    try {
        const input = event.body;

        const {Forms, OtherPartiesForms} = await connectToDatabase();
        let response = [];
        let plaintText = undefined;
        let is_otherparties_form = false;
        for (let i = 0; i < input.length; i++) {
            let forms = undefined;
            forms = await Forms.findOne({where: {id: input[i].id}});
            if (!forms) {
                forms
                forms = await OtherPartiesForms.findOne({where: {id: input[i].id}});
                if (!forms) throw new HTTPError(404, `Question id ${input[i].id} was not found`);
                is_otherparties_form = true;
            }
            if (forms) {
                if (!is_otherparties_form) {
                    const otherPartiesFormsObject = await OtherPartiesForms.findOne({where: {form_id: input[i].id}});
                    if (otherPartiesFormsObject) {
                        otherPartiesFormsObject.subgroup = input[i].subgroup;
                        await otherPartiesFormsObject.save();
                    }
                    forms.lawyer_response_text = input[i].lawyer_response_text;
                    forms.lawyer_response_status = input[i].lawyer_response_status;
                    forms.last_updated_by_lawyer = new Date();
                }
                if (is_otherparties_form) {
                    const saveFormsObj = await Forms.findOne({where: {id: forms.form_id}});
                    saveFormsObj.lawyer_response_text = input[i].lawyer_response_text;
                    saveFormsObj.lawyer_response_status = input[i].lawyer_response_status;
                    saveFormsObj.last_updated_by_lawyer = new Date();
                    saveFormsObj.subgroup = input[i].subgroup;
                    await saveFormsObj.save();
                }
                forms.subgroup = input[i].subgroup;

                await forms.save();
                if (is_otherparties_form) {
                    plaintText = await OtherPartiesForms.findOne({where: {id: input[i].id}});
                    let formsobject = await Forms.findOne({where: {id: plaintText.form_id}});
                    plaintText.lawyer_response_text = formsobject.lawyer_response_text;
                    plaintText.lawyer_response_status = formsobject.lawyer_response_status;
                    plaintText.last_updated_by_lawyer = formsobject.last_updated_by_lawyer;
                } else {
                    plaintText = await Forms.findOne({where: {id: input[i].id}});
                }
                response[i] = plaintText.get({plain: true});
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(response),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};
const saveAlllawyerResponseByCaseidAndDocumentType = async (event) => {
    try {
        const input = event.body;

        const { Forms, OtherPartiesForms, FormsResponseHistory, Op, Practices, Cases, Settings } = await connectToDatabase();
        let response = undefined;
        let plaintText = undefined;
        let is_otherparties_form = false;
        let is_otherparties_have_form_qus = false;

        let legalforms_id, case_id, document_type, practice_id = undefined;
        for (let i = 0; i < input.length; i++) {
            /* By enclosing the left-hand side in parentheses, we are indicating to the interpreter that it should treat it as an expression, and not as a block statement. */
            const form_id = input[i]?.client_id ? input[i]?.id : input[i]?.form_id;
            ({ legalforms_id, case_id, document_type, practice_id } = input[i]);
            const whereCon = { id: form_id, legalforms_id, document_type, case_id };
            const otherPartieswhereCon = { form_id: form_id, legalforms_id, document_type, case_id, practice_id }

            const [formsQues, otherPartiesQues,formsResponseHistoryObj,practiceObj,casesObj] = await Promise.all([
                Forms.findOne({ where: whereCon }),
                OtherPartiesForms.findOne({ where: otherPartieswhereCon }),
                FormsResponseHistory.findOne({where: otherPartieswhereCon}),
                Practices.findOne({ where: { id: event.user.practice_id }, raw: true }),
                Cases.findOne({where:{practice_id: event.user.practice_id,id:case_id}, raw: true})
            ]);

            if (formsQues) {
                is_otherparties_form = false;
                is_otherparties_have_form_qus = false;
            } else if (otherPartiesQues) {
                is_otherparties_have_form_qus = true;
                    is_otherparties_form = true;
            } else {
                throw new HTTPError(404, `Question id ${form_id} was not found`);
            }


            if (!is_otherparties_form && !is_otherparties_have_form_qus) {
                /* Here Updateing forms Question */
                formsQues.lawyer_response_text = input[i].lawyer_response_text;
                formsQues.lawyer_response_status = input[i].lawyer_response_status;
                formsQues.last_updated_by_lawyer = new Date();
                formsQues.subgroup = input[i].subgroup;
                await formsQues.save();
            }
            else if (is_otherparties_form && is_otherparties_have_form_qus) {
                await OtherPartiesForms.update({ subgroup: input[i].subgroup }, { where: otherPartieswhereCon });
                        }
            if (practiceObj?.global_attorney_response_tracking && casesObj?.attorney_response_tracking) {
                if (formsResponseHistoryObj) {
                    const settingsObj = await Settings.findOne({where:{key:'global_settings'},raw:true});
                    const settings_value = JSON.parse(settingsObj.value);
                    let response_history_limit;
                    let responseHistory = JSON.parse(formsResponseHistoryObj.response);
                    let version_no = responseHistory[responseHistory.length-1].version;
                    response_history_limit = settings_value?.response_history_limit ? settings_value.response_history_limit : 100;
                    responseHistory = checkResponseHistoryCount(responseHistory,response_history_limit);

                    responseHistory.push({
                        version: version_no + 1,
                        response: input[i].lawyer_response_text,
                        user_id: event.user.id,
                        timestamp: new Date(),
                        color_code: input.color_code,
                        status: input[i].lawyer_response_status
                    })
                    formsResponseHistoryObj.response = JSON.stringify(responseHistory);
                    await formsResponseHistoryObj.save();
                } else {
                    const responseObj = [{
                        version: 1,
                        response: input[i].lawyer_response_text,
                        user_id: event.user.id,
                        timestamp: new Date(),
                        color_code: input.color_code,
                        status: input[i].lawyer_response_status
                    }];
                    let responseHistoryObj = {
                        id: uuid.v4(),
                        practice_id,
                        form_id: form_id,
                        legalforms_id,
                        document_type,
                        case_id,
                        response: JSON.stringify(responseObj)
                            }
                    await FormsResponseHistory.create(responseHistoryObj)
                                }
                        }
                    }

        if (is_otherparties_form && is_otherparties_have_form_qus) {
            response = await OtherPartiesForms.findAll({
                        where: {
                    practice_id: event.user.practice_id,
                    case_id,
                    document_type,
                    legalforms_id : input[i].legalforms_id
                },
                include: [
                    {
                        model: Forms, as: 'Form',
                        attributes: ['lawyer_response_status', 'lawyer_response_text','last_updated_by_lawyer'],
                        required: true
                            }
                ],
                logging: console.log,
                raw: true
                        });
            } else {
            response = await Forms.findAll({ where: { legalforms_id, case_id, document_type }, raw: true });
            }


        for (let i = 0; i < response.length; i++) {
            const form_id = response[i]?.client_id ? response[i]?.id : response[i]?.form_id;
            const practice_id = response[i].practice_id;
            let lawyer_response = await getResponseHistory(practice_id,case_id,form_id,legalforms_id,FormsResponseHistory);
            response[i].responseHistory = lawyer_response;
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(response),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};
const sendQuestionsToClient = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateSentQuestionToClient(input);
        const practice_id = event.user.practice_id;
        const sendingType = input.sending_type;
        const legalforms_id = input.legalforms_id;
        const client_id = input.client_id;
        const case_id = input.case_id;
        const document_type = input.document_type;
        const languageCode = input.TargetLanguageCode || 'en';
        const ids = input.ids || []; //question ids
        const otherparty_ids = input.party_ids || []; //question ids
        const reference_id = input.reference_id; //to check questions comes from cliet or otherparties.
        let is_questionsfrom_forms = false;
        let is_questionsfrom_otherparties = false;
        let forms_question_id = [];
        let other_parties_question_id = [];
        const custom_message = input.custom_message;
        const share_attorney_response = input?.share_attorney_response || false;

        //case 1: clent_id = reference_id -> questions id from Forms
        //case 2: party_ids array contains refernce_id -> questions id from otherpartiesForms
        const {Op, Forms, Practices, OtherPartiesForms, OtherParties} = await connectToDatabase();
        const practice_details = await Practices.findOne({where: {id: event.user.practice_id}, raw: true});

        if (!legalforms_id) throw new HTTPError(404, 'LegalForm id was not found');
        if (sendingType !== 'all' && !ids.length) throw new HTTPError(404, `question id's was not found`);
        if (!reference_id) throw new HTTPError(404, `reference id was not found`);
        if (input.client_id == false && input.party_ids.length == 0) throw new HTTPError(404, 'Client and Parties Details Not Found');

        if (client_id != false && client_id == reference_id) {
            is_questionsfrom_forms = true;
            is_questionsfrom_otherparties = false;
        } else if ((client_id != reference_id && (otherparty_ids.length == 0 || otherparty_ids.length != 0)) || (client_id == false && otherparty_ids.length != 0)) {
            is_questionsfrom_otherparties = true;
            is_questionsfrom_forms = false;
        } else {
            throw new HTTPError(404, `reference id was not found in clients and otherparties table`);
        }

        // Party_id . length = 0 -> No Other parties, 
        // client_id = false -> no need to send questions to client.

        // case 1 : client_id 'x-y-z' party_id : 0
        // case 2 : client_id 'x-y-z' party_id : [x,y,z]
        // case 3 : client_id false   party_id : [x,y,z] 
        // case 4 : client_id false   party_id : 0

        const updateColumn = {};
        const updateQuery = {};

        if(!share_attorney_response) updateQuery.client_response_status = {[Op.ne]: 'ClientResponseAvailable'};
        updateQuery.legalforms_id = legalforms_id;
        updateQuery.case_id = input.case_id;
        updateQuery.document_type = input.document_type;

        if (share_attorney_response || !share_attorney_response) { // resend question function don't have this node value.
            updateColumn.share_attorney_response = share_attorney_response;
        }

        updateColumn.client_response_status = 'SentToClient';
        updateColumn.TargetLanguageCode = input.TargetLanguageCode;
        updateColumn.is_client_answered_all_questions = false;

        const otherpartiesUpdateQuery = {
            legalforms_id
        };
        if(!share_attorney_response) otherpartiesUpdateQuery.client_response_status = {[Op.ne]: 'ClientResponseAvailable'};
        otherpartiesUpdateQuery.party_id = {[Op.in]: otherparty_ids};
        otherpartiesUpdateQuery.case_id = input.case_id;
        otherpartiesUpdateQuery.document_type = input.document_type;

        if (sendingType === 'selected_questions') {
            updateColumn.file_upload_status = false;
        } else {
            updateColumn.file_upload_status = true;
        }

        if (input.document_ids) {
            const file_upload_ids = input.document_ids;
            if (file_upload_ids.length != 0 && sendingType === 'selected_questions') {  //document_ids node only comes for RFPD type document request.
                updateColumn.file_upload_status = true;
            }
        }

        const otpDataObject = {
            document_type: input.document_type,
            case_id: input.case_id,
            legalforms_id: legalforms_id,
            sending_type: input.sending_type,
            sent_by: event.user.id,
            scheduler_email: true,
            TargetLanguageCode: languageCode,
            practice_name: practice_details.name,
            custom_message,
            practice_id: event.user.practice_id
        }
        if(input?.reminder_meantime) otpDataObject.reminder_meantime = input?.reminder_meantime;
        if(input?.reminder_till_date) otpDataObject.reminder_till_date = input?.reminder_till_date;
        if(input?.reminder_till_date && input?.reminder_meantime) otpDataObject.latest_reminder_send_date = new Date();

        if (client_id && is_questionsfrom_forms && !is_questionsfrom_otherparties) {

            if (sendingType === 'selected_questions') {
                updateQuery.id = {[Op.in]: ids};
            }
            forms_question_id = ids; //save question id's for sending Email.
            await Forms.update(updateColumn, {where: updateQuery});
            if (otherparty_ids.length > 0) {
                if (sendingType === 'selected_questions') {
                    otherpartiesUpdateQuery.form_id = {[Op.in]: ids};
                    let fetchOtherPartiesDatas = await OtherPartiesForms.findAll({where: otherpartiesUpdateQuery});
                    other_parties_question_id = await groupByOtherPartiesPartyID(fetchOtherPartiesDatas, 'party_id');
                }
                await OtherPartiesForms.update(updateColumn, {where: otherpartiesUpdateQuery});
            }
        } else if (client_id && !is_questionsfrom_forms && is_questionsfrom_otherparties) {
            if (sendingType === 'selected_questions') {
                const getFormsIds = await OtherPartiesForms.findAll({
                    where: {
                        case_id,
                        legalforms_id,
                        document_type,
                        id: {[Op.in]: ids}
                    }, raw: true
                });
                if (getFormsIds.length == 0) throw new HTTPError(404, `cannot get forms ids .`);
                const forms_ids = getFormsIds.map(row => {
                    return row.form_id
                });
                if (otherparty_ids.length > 0) {
                    forms_question_id = forms_ids;  //save question id's for sending Email.   
                    otherpartiesUpdateQuery.form_id = {[Op.in]: forms_ids};
                    let fetchOtherPartiesDatas = await OtherPartiesForms.findAll({
                        where: otherpartiesUpdateQuery,
                        logging: console.log
                    });
                    other_parties_question_id = await groupByOtherPartiesPartyID(fetchOtherPartiesDatas, 'party_id');
                    console.log(other_parties_question_id);
                    await OtherPartiesForms.update(updateColumn, {where: otherpartiesUpdateQuery});
                }
                updateQuery.id = {[Op.in]: forms_ids};
                await Forms.update(updateColumn, {where: updateQuery});
            } else {
                forms_question_id = [];
                await Forms.update(updateColumn, {where: updateQuery});
                if (otherparty_ids.length > 0) {
                    other_parties_question_id = [];
                    await OtherPartiesForms.update(updateColumn, {where: otherpartiesUpdateQuery});
                }
            }
        } else if (!client_id && !is_questionsfrom_forms && is_questionsfrom_otherparties) {
            if (sendingType === 'selected_questions') {
                let getFormsIds = await OtherPartiesForms.findAll({
                    where: {
                        case_id,
                        legalforms_id,
                        document_type,
                        id: {[Op.in]: ids}
                    }, raw: true
                });
                if (getFormsIds.length == 0) {
                    getFormsIds = await OtherPartiesForms.findAll({
                        where: {
                            case_id,
                            legalforms_id,
                            document_type,
                            form_id: {[Op.in]: ids}
                        }, raw: true
                    });
                }
                if (getFormsIds.length == 0) throw new HTTPError(404, `cannot get forms ids .`);
                const forms_ids = getFormsIds.map(row => {
                    return row.form_id
                });
                otherpartiesUpdateQuery.form_id = {[Op.in]: forms_ids};
                //In this scenario clinet_id was empty. we cant send questions from Forms table.
                let fetchOtherPartiesDatas = await OtherPartiesForms.findAll({where: otherpartiesUpdateQuery});
                other_parties_question_id = await groupByOtherPartiesPartyID(fetchOtherPartiesDatas, 'party_id');
                await OtherPartiesForms.update(updateColumn, {where: otherpartiesUpdateQuery});
            } else {
                forms_question_id = [];
                other_parties_question_id = [];
                await OtherPartiesForms.update(updateColumn, {where: otherpartiesUpdateQuery});
            }
        }

        if (client_id) {
            otpDataObject.otp_code = generateRandomString(8);
            otpDataObject.otp_secret = generateRandomString(4);
            otpDataObject.client_id = client_id;
            otpDataObject.question_ids = JSON.stringify(forms_question_id);
            otpDataObject.id = uuid.v4();
            await sendQuestionsEmailAndSMSTemplate(otpDataObject);
            delete otpDataObject.client_id;
            delete otpDataObject.question_ids;
        }

        for (let i = 0; i < otherparty_ids.length; i++) {
            const party_id = otherparty_ids[i];
            if (sendingType == 'selected_questions') {
                let temp = other_parties_question_id[party_id];
                otpDataObject.question_id = JSON.stringify(temp);
            } else {
                otpDataObject.question_id = JSON.stringify(other_parties_question_id);
            }
            otpDataObject.otp_code = generateRandomString(8);
            otpDataObject.otp_secret = generateRandomString(4);
            otpDataObject.party_id = party_id;
            otpDataObject.id = uuid.v4();
            await sendQuestionsEmailAndSMSTemplate(otpDataObject);
        }

        if (client_id) {
            const resp = {};
            const LastSetOfQuestions = await lastSetOfQuestions(input.case_id, input.legalforms_id, input.client_id, input.document_type);
            resp.QuestionsRespondedByClientLastSetofCount = LastSetOfQuestions.QuestionsRespondedByClientLastSetofCount;
            resp.QuestionsSentToClientLastSetofCount = LastSetOfQuestions.QuestionsSentToClientLastSetofCount;
            resp.legalformid = input.legalforms_id;
            resp.client_id = input.client_id;
            resp.document_type = input.document_type;
            resp.case_id = input.case_id;

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(resp),
            };
        } else {
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify({message: "Questions sent to client"}),
            };
        }

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not send the Forms.'}),
        };
    }
}
const groupByOtherPartiesPartyID = async (otherPartiesDatas, party_id) => {
    try {
        return otherPartiesDatas.reduce((datas, obj) => {
            const key = obj[party_id];
            if (!datas[key]) {
                datas[key] = [];
            }
            // Add object to list for given key's value
            datas[key].push(obj.id);
            return datas;
        }, {});
    } catch (err) {
        console.log(err);
    }
}
const sendQuestionsToClientOld = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateSentQuestionToClient(input);
        const practice_id = event.user.practice_id;
        const sendingType = input.sending_type;
        const legalforms_id = input.legalforms_id;
        const ids = input.ids || [];
        const {Op, Forms, Formotp, Clients, Practices, Users} = await connectToDatabase();

        if (!legalforms_id) throw new HTTPError(404, 'LegalForm id was not found Not Found');
        if (input.client_id == false && input.party_ids.length == 0) throw new HTTPError(404, 'Client and Parties Details Not Found');

        if (input.client_id != false) {
            const updateQuery = {
                practice_id,
            };
            const updateColumn = {};
            if (sendingType === 'selected_questions') {
                if (!ids.length) throw new HTTPError(404, 'question was not found');
                updateQuery.id = {[Op.in]: ids};
                updateColumn.client_response_status = 'SentToClient';

                const selectedFormsObj = await Forms.findAll({
                    where: {id: {[Op.in]: ids}},
                    raw: true,
                    logging: console.log
                });
                //if(selectedFormsObj.length == 0) throw new HTTPError(400, 'Questions Ids Not fount in Forms');

                updateColumn.file_upload_status = false;
                updateColumn.TargetLanguageCode = input.TargetLanguageCode;
            } else {
                updateQuery.case_id = input.case_id;
                updateQuery.document_type = input.document_type;
                updateColumn.file_upload_status = true;
                updateColumn.client_response_status = 'SentToClient';
                updateColumn.TargetLanguageCode = input.TargetLanguageCode;
            }

            updateQuery.client_response_status = {[Op.ne]: 'ClientResponseAvailable'};
            updateQuery.legalforms_id = legalforms_id;
            const otpCode = generateRandomString(8);
            const otpSecret = generateRandomString(4);

            await Forms.update(updateColumn, {where: updateQuery});

            if (input.document_ids) {
                const file_upload_ids = input.document_ids;
                if (file_upload_ids.length != 0 && sendingType === 'selected_questions') {  //RFPD
                    await Forms.update({file_upload_status: true}, {where: {id: {[Op.in]: file_upload_ids}}});
                }
            }
            let languageCode = '';
            if (!input.TargetLanguageCode) {
                languageCode = 'en';
            } else {
                languageCode = input.TargetLanguageCode;
            }

            const otdDataObject = {
                id: uuid.v4(),
                otp_code: otpCode,
                otp_secret: otpSecret,
                client_id: input.client_id,
                document_type: input.document_type,
                case_id: input.case_id,
                legalforms_id: legalforms_id,
                sending_type: input.sending_type,
                question_ids: JSON.stringify(input.ids),
                sent_by: event.user.id,
                scheduler_email: true,
                TargetLanguageCode: languageCode
            };
            const formotpModelObject = await Formotp.create(otdDataObject);

            const ClientDetails = await Clients.findOne({
                where: {id: input.client_id},
            });

            let practiceName = '';
            let userName = '';
            const practiceDetails = await Practices.findOne({where: {id: ClientDetails.practice_id}});
            const usersDetails = await Users.findOne({where: {id: event.user.id}});
            if (practiceDetails) {
                practiceName = practiceDetails.name;
            }
            if (usersDetails) {
                userName = usersDetails.name;
            }

            let questionsUrl = `${process.env.CARE_URL}/questionnaire/${formotpModelObject.otp_code}`;
            let alreadyHadParameter = false;
            if (input.TargetLanguageCode) {
                questionsUrl = `${questionsUrl}?TargetLanguageCode=${input.TargetLanguageCode}`;
                alreadyHadParameter = true;
            }

            if (input.SendVerification) {
                if (alreadyHadParameter) {
                    questionsUrl += '&';
                } else {
                    questionsUrl += '?';
                }
                questionsUrl += 'SendVerification=true';
            }

            let message_body = '';
            let smstext_body = '';
            if (input.custom_message && input.TargetLanguageCode != 'es' && input.TargetLanguageCode != 'vi') {
                message_body = input.custom_message + '\n' + 'Please click the link to access the questionnaire.' + '<br/>' + questionsUrl;
                smstext_body = input.custom_message + '\n' + 'Please click the link and enter the verification code ' + formotpModelObject.otp_secret + ' to access the questionnaire. ' + questionsUrl;
            } else if (input.custom_message && input.TargetLanguageCode == 'es') {
                message_body = input.custom_message + '\n' + 'Haga clic en el enlace para acceder al cuestionario.' + '<br/>' + questionsUrl;
                smstext_body = input.custom_message + '\n' + 'Haga clic en el enlace e ingrese el código de verificación ' + formotpModelObject.otp_secret + ' para acceder al cuestionario. ' + questionsUrl;
            } else if (input.custom_message && input.TargetLanguageCode == 'vi') {
                message_body = input.custom_message + '\n' + 'Vui lòng nhấp vào liên kết để truy cập bảng câu hỏi.' + '<br/>' + questionsUrl;
                smstext_body = input.custom_message + '\n' + 'Vui lòng nhấp vào liên kết và nhập mã xác minh ' + formotpModelObject.otp_secret + ' để truy cập bảng câu hỏi. ' + questionsUrl;
            } else if (!input.custom_message && input.TargetLanguageCode && input.TargetLanguageCode != 'es' && input.TargetLanguageCode != 'vi') {
                message_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link to access the questionnaire.' + '<br/>' + questionsUrl;
                smstext_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link and enter the verification code ' + formotpModelObject.otp_secret + ' to access the questionnaire. ' + questionsUrl;
            } else if (!input.custom_message && input.TargetLanguageCode == 'es') {
                message_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace para acceder al cuestionario.' + '<br/>' + questionsUrl;
                smstext_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace e ingrese el código de verificación ' + formotpModelObject.otp_secret + ' para acceder al cuestionario. ' + questionsUrl;
            } else if (!input.custom_message && input.TargetLanguageCode == 'vi') {
                message_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết để truy cập bảng câu hỏi.' + '<br/>' + questionsUrl;
                smstext_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết và nhập mã xác minh ' + formotpModelObject.otp_secret + ' để truy cập bảng câu hỏi. ' + questionsUrl;
            }


            if (input.TargetLanguageCode && input.TargetLanguageCode != 'es' && input.TargetLanguageCode != 'vi') {
                if (ClientDetails && ClientDetails.email) {
                    await sendEmail(ClientDetails.email, 'Important: Response required for your lawsuit', message_body, practiceName);
                }

                if (ClientDetails && ClientDetails.phone) {
                    const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                    await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Important: Response required for your lawsuit', smstext_body);
                }
            } else if (input.TargetLanguageCode == 'es') {
                if (ClientDetails && ClientDetails.email) {
                    await sendEmail(ClientDetails.email, 'Importante: Se requiere respuesta para su demanda', message_body, practiceName);
                }

                if (ClientDetails && ClientDetails.phone) {
                    const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                    await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Importante: Se requiere respuesta para su demanda', smstext_body);
                }
            } else if (input.TargetLanguageCode == 'vi') {
                if (ClientDetails && ClientDetails.email) {
                    await sendEmail(ClientDetails.email, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', message_body, practiceName);
                }

                if (ClientDetails && ClientDetails.phone) {
                    const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                    await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', smstext_body);
                }
            }
        }
        if (input.party_ids) {
            if (input.party_ids.length > 0) {
                return await sendQuestionToParties(event, input.party_ids);
            }
        }
        if (input.client_id != false) {
            const resp = {};
            const LastSetOfQuestions = await lastSetOfQuestions(input.case_id, input.legalforms_id, input.client_id, input.document_type);
            resp.QuestionsRespondedByClientLastSetofCount = LastSetOfQuestions.QuestionsRespondedByClientLastSetofCount;
            resp.QuestionsSentToClientLastSetofCount = LastSetOfQuestions.QuestionsSentToClientLastSetofCount;
            resp.legalformid = input.legalforms_id;
            resp.client_id = input.client_id;
            resp.document_type = input.document_type;
            resp.case_id = input.case_id;

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(resp),
            };
        } else {
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify({message: "Questions sent to client"}),
            };
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};

const sendQuestionToParties = async (event, party_ids) => {
    try {
        let input = JSON.parse(event.body);
        const practice_id = event.user.practice_id;
        const sendingType = input.sending_type;
        const legalforms_id = input.legalforms_id;
        let ids = input.ids || [];
        console.log(ids);
        input.selectedQuestionId = [];
        const {Op, OtherPartiesForms} = await connectToDatabase();
        let response = {};
        const updateQuery = {
            practice_id,
        };


        for (let party_id of party_ids) {
            const updateColumn = {};
            if (sendingType === 'selected_questions') {
                if (!ids.length) throw new HTTPError(404, 'question was not found');
                const selectedFormquery = {};
                selectedFormquery.id = ids;
                selectedFormquery.party_id = party_id;
                const selectedFormsObj = await OtherPartiesForms.findAll({
                    where: selectedFormquery,
                    raw: true,
                    logging: console.log
                });
                //if(selectedFormsObj.length == 0) throw new HTTPError(400, 'Questions Ids Not fount in Other Parties');
                input.selectedQuestionId.length = 0;
                for (let otherpariesform_id of selectedFormsObj) {
                    input.selectedQuestionId.push(otherpariesform_id.id);
                }
                console.log(input.selectedQuestionId);
                updateQuery.id = {[Op.in]: input.selectedQuestionId};
                updateColumn.client_response_status = 'SentToClient';
                updateColumn.file_upload_status = false;
                updateColumn.TargetLanguageCode = input.TargetLanguageCode;
                if (input.document_ids) {
                    const file_upload_ids = input.document_ids;
                    if (file_upload_ids.length != 0 && sendingType === 'selected_questions') {
                        await OtherPartiesForms.update({file_upload_status: true}, {
                            where: {
                                form_id: {[Op.in]: file_upload_ids},
                                legalforms_id: legalforms_id,
                                document_type: input.document_type
                            }
                        });
                    }
                }
            } else {
                updateQuery.case_id = input.case_id;
                updateQuery.document_type = input.document_type;
                updateQuery.party_id = party_id;
                updateColumn.file_upload_status = true;
                updateColumn.client_response_status = 'SentToClient';
                updateColumn.TargetLanguageCode = input.TargetLanguageCode;
            }
            updateQuery.client_response_status = {[Op.ne]: 'ClientResponseAvailable'};
            updateQuery.legalforms_id = legalforms_id;
            console.log(updateColumn);
            console.log(updateQuery);
            await OtherPartiesForms.update(updateColumn, {where: updateQuery});
            response = await sendEmailAndSms(party_id, input, event);
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: "Questions sent to Other Parties"}),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};
const sendEmailAndSms_new = async (party_id, input, event) => {

    const {OtherPartiesFormsOtp, OtherParties, OtherPartiesForms, Practices} = await connectToDatabase();
    const {sending_type, legalforms_id, client_id, reference_id, case_id, document_type, selectedQuestionId, custom_message, share_attorney_response } = input;

    const insert_id = sending_type === "selected_questions" ? selectedQuestionId : [];

    let responseObj = {};
    const otpCode = generateRandomString(8);
    const otpSecret = generateRandomString(4);
    const TargetLanguageCode = input?.TargetLanguageCode || 'en';

    const otdDataObject = {
        id: uuid.v4(),
        otp_code: otpCode,
        otp_secret: otpSecret,
        party_id: party_id,
        document_type,
        legalforms_id,
        case_id,
        sending_type,
        question_id: JSON.stringify(insert_id),
        sent_by: event.user.id,
        scheduler_email: true,
        TargetLanguageCode
    };
    const formotpModelObject = await OtherPartiesFormsOtp.create(otdDataObject);

    const otherParties = await OtherParties.findOne({ where: { id: party_id }, raw: true });
    const practiceDetails = await Practices.findOne({ where: { id: otherParties.practice_id }, raw: true });
    const practiceName = practiceDetails?.name || "";

    let questionsUrl = `${process.env.CARE_URL}/questionnaire/${formotpModelObject.otp_code}`;
    if (input.TargetLanguageCode) questionsUrl += `?TargetLanguageCode=${input.TargetLanguageCode}`;
    if (input.SendVerification)  questionsUrl += TargetLanguageCode ? "&SendVerification=true" : "?SendVerification=true";

    let message_body = '';
    let smstext_body = '';
    if (custom_message && TargetLanguageCode != 'es' && TargetLanguageCode != 'vi') {
        message_body = custom_message + '\n' + 'Please click the link to access the questionnaire.' + '<br/>' + questionsUrl;
        smstext_body = custom_message + '\n' + 'Please click the link and enter the verification code ' + formotpModelObject.otp_secret + ' to access the questionnaire. ' + questionsUrl;
    } else if (custom_message && TargetLanguageCode == 'es') {
        message_body = custom_message + '\n' + 'Haga clic en el enlace para acceder al cuestionario.' + '<br/>' + questionsUrl;
        smstext_body = custom_message + '\n' + 'Haga clic en el enlace e ingrese el código de verificación ' + formotpModelObject.otp_secret + ' para acceder al cuestionario. ' + questionsUrl;
    } else if (custom_message && TargetLanguageCode == 'vi') {
        message_body = custom_message + '\n' + 'Vui lòng nhấp vào liên kết để truy cập bảng câu hỏi.' + '<br/>' + questionsUrl;
        smstext_body = custom_message + '\n' + 'Vui lòng nhấp vào liên kết và nhập mã xác minh ' + formotpModelObject.otp_secret + ' để truy cập bảng câu hỏi. ' + questionsUrl;
    } else if (!custom_message && TargetLanguageCode && TargetLanguageCode != 'es' && TargetLanguageCode != 'vi') {
        message_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link to access the questionnaire.' + '<br/>' + questionsUrl;
        smstext_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link and enter the verification code ' + formotpModelObject.otp_secret + ' to access the questionnaire. ' + questionsUrl;
    } else if (!custom_message && TargetLanguageCode == 'es') {
        message_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace para acceder al cuestionario.' + '<br/>' + questionsUrl;
        smstext_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace e ingrese el código de verificación ' + formotpModelObject.otp_secret + ' para acceder al cuestionario. ' + questionsUrl;
    } else if (!custom_message && TargetLanguageCode == 'vi') {
        message_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết để truy cập bảng câu hỏi.' + '<br/>' + questionsUrl;
        smstext_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết và nhập mã xác minh ' + formotpModelObject.otp_secret + ' để truy cập bảng câu hỏi. ' + questionsUrl;
    }


    if (TargetLanguageCode && TargetLanguageCode != 'es' && TargetLanguageCode != 'vi') {
        if (otherParties && otherParties.email) {
            await sendEmail(otherParties.email, 'Important: Response required for your lawsuit', message_body, practiceName);
        }

        if (otherParties && otherParties.phone) {
            const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
            await sendSMS(`${phoneCountryCode}${otherParties.phone}`, 'Important: Response required for your lawsuit', smstext_body);
        }
    } else if (TargetLanguageCode == 'es') {
        if (otherParties && otherParties.email) {
            await sendEmail(otherParties.email, 'Importante: Se requiere respuesta para su demanda', message_body, practiceName);
        }

        if (otherParties && otherParties.phone) {
            const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
            await sendSMS(`${phoneCountryCode}${otherParties.phone}`, 'Importante: Se requiere respuesta para su demanda', smstext_body);
        }
    } else if (TargetLanguageCode == 'vi') {
        if (otherParties && otherParties.email) {
            await sendEmail(otherParties.email, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', message_body, practiceName);
        }

        if (otherParties && otherParties.phone) {
            const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
            await sendSMS(`${phoneCountryCode}${otherParties.phone}`, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', smstext_body);
        }
    }
    return responseObj;
};

const sendEmailAndSms = async (party_id, input, event) => {
    console.log(input.selectedQuestionId);
    const {OtherPartiesFormsOtp, OtherParties, OtherPartiesForms, Practices} = await connectToDatabase();
    let insert_id = [];
    if (input.sending_type === 'selected_questions') {
        insert_id.length = 0;
        insert_id = input.selectedQuestionId;
    } else {
        insert_id.length = 0;
    }

    let responseObj = {};
    const otpCode = generateRandomString(8);
    const otpSecret = generateRandomString(4);
    let languageCode = '';
    if (!input.TargetLanguageCode) {
        languageCode = 'en';
    } else {
        languageCode = input.TargetLanguageCode;
    }
    const otdDataObject = {
        id: uuid.v4(),
        otp_code: otpCode,
        otp_secret: otpSecret,
        party_id: party_id,
        document_type: input.document_type,
        legalforms_id: input.legalforms_id,
        case_id: input.case_id,
        sending_type: input.sending_type,
        question_id: JSON.stringify(insert_id),
        sent_by: event.user.id,
        scheduler_email: true,
        TargetLanguageCode: languageCode
    };
    if(input?.reminder_meantime) otdDataObject.reminder_meantime = input?.reminder_meantime;
    if(input?.reminder_till_date) otdDataObject.reminder_till_date = input?.reminder_till_date;
    if(input?.reminder_till_date && input?.reminder_meantime) otdDataObject.latest_reminder_send_date = new Date();
    const formotpModelObject = await OtherPartiesFormsOtp.create(otdDataObject);

    const otherParties = await OtherParties.findOne({
        where: {id: party_id},
    });

    let practiceName = '';
    const practiceDetails = await Practices.findOne({where: {id: otherParties.practice_id}});
    if (practiceDetails) {
        practiceName = practiceDetails.name;
    }

    let questionsUrl = `${process.env.CARE_URL}/questionnaire/${formotpModelObject.otp_code}`;
    let alreadyHadParameter = false;
    if (input.TargetLanguageCode) {
        questionsUrl = `${questionsUrl}?TargetLanguageCode=${input.TargetLanguageCode}`;
        alreadyHadParameter = true;
    }

    if (input.SendVerification) {
        if (alreadyHadParameter) {
            questionsUrl += '&';
        } else {
            questionsUrl += '?';
        }
        questionsUrl += 'SendVerification=true';
    }

    let message_body = '';
    let smstext_body = '';
    if (input.custom_message && input.TargetLanguageCode != 'es' && input.TargetLanguageCode != 'vi') {
        message_body = input.custom_message + '\n' + 'Please click the link to access the questionnaire.' + '<br/>' + questionsUrl;
        smstext_body = input.custom_message + '\n' + 'Please click the link and enter the verification code ' + formotpModelObject.otp_secret + ' to access the questionnaire. ' + questionsUrl;
    } else if (input.custom_message && input.TargetLanguageCode == 'es') {
        message_body = input.custom_message + '\n' + 'Haga clic en el enlace para acceder al cuestionario.' + '<br/>' + questionsUrl;
        smstext_body = input.custom_message + '\n' + 'Haga clic en el enlace e ingrese el código de verificación ' + formotpModelObject.otp_secret + ' para acceder al cuestionario. ' + questionsUrl;
    } else if (input.custom_message && input.TargetLanguageCode == 'vi') {
        message_body = input.custom_message + '\n' + 'Vui lòng nhấp vào liên kết để truy cập bảng câu hỏi.' + '<br/>' + questionsUrl;
        smstext_body = input.custom_message + '\n' + 'Vui lòng nhấp vào liên kết và nhập mã xác minh ' + formotpModelObject.otp_secret + ' để truy cập bảng câu hỏi. ' + questionsUrl;
    } else if (!input.custom_message && input.TargetLanguageCode && input.TargetLanguageCode != 'es' && input.TargetLanguageCode != 'vi') {
        message_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link to access the questionnaire.' + '<br/>' + questionsUrl;
        smstext_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link and enter the verification code ' + formotpModelObject.otp_secret + ' to access the questionnaire. ' + questionsUrl;
    } else if (!input.custom_message && input.TargetLanguageCode == 'es') {
        message_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace para acceder al cuestionario.' + '<br/>' + questionsUrl;
        smstext_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace e ingrese el código de verificación ' + formotpModelObject.otp_secret + ' para acceder al cuestionario. ' + questionsUrl;
    } else if (!input.custom_message && input.TargetLanguageCode == 'vi') {
        message_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết để truy cập bảng câu hỏi.' + '<br/>' + questionsUrl;
        smstext_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết và nhập mã xác minh ' + formotpModelObject.otp_secret + ' để truy cập bảng câu hỏi. ' + questionsUrl;
    }


    if (input.TargetLanguageCode && input.TargetLanguageCode != 'es' && input.TargetLanguageCode != 'vi') {
        if (otherParties && otherParties.email) {
            await sendEmail(otherParties.email, 'Important: Response required for your lawsuit', message_body, practiceName);
        }

        if (otherParties && otherParties.phone) {
            const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
            await sendSMS(`${phoneCountryCode}${otherParties.phone}`, 'Important: Response required for your lawsuit', smstext_body);
        }
    } else if (input.TargetLanguageCode == 'es') {
        if (otherParties && otherParties.email) {
            await sendEmail(otherParties.email, 'Importante: Se requiere respuesta para su demanda', message_body, practiceName);
        }

        if (otherParties && otherParties.phone) {
            const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
            await sendSMS(`${phoneCountryCode}${otherParties.phone}`, 'Importante: Se requiere respuesta para su demanda', smstext_body);
        }
    } else if (input.TargetLanguageCode == 'vi') {
        if (otherParties && otherParties.email) {
            await sendEmail(otherParties.email, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', message_body, practiceName);
        }

        if (otherParties && otherParties.phone) {
            const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
            await sendSMS(`${phoneCountryCode}${otherParties.phone}`, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', smstext_body);
        }
    }
    return responseObj;
};
const lastSetOfQuestions = async (cases_id, legalforms_ids, client_ids, document_types) => {
    try {
        const {Formotp, Forms, Op} = await connectToDatabase();
        const Formotps = await Formotp.findOne({
            where: { case_id: cases_id, legalforms_id: legalforms_ids, client_id: client_ids, document_type: document_types },
            order: [['createdAt', 'DESC']],
            raw: true,
        });
        const response = {};
        if (Formotps) {
            const query = {
                where: {
                    case_id: cases_id, legalforms_id: legalforms_ids, client_id: client_ids, document_type: document_types,
                    client_response_status: { [Op.in]: ['SentToClient', 'ClientResponseAvailable'] }
                },
                raw: true
            };
            if (Formotps?.sending_type == 'selected_questions') {
                const Formids = Formotps.question_ids;
                query.where.id = {[Op.in]: JSON.parse(Formids)};
            }
            const QuestionsSentToClientCount = await Forms.count(query);
            query.where.client_response_status = { [Op.like]: 'ClientResponseAvailable' };
            const QuestionResponsedQuesCount = await Forms.count(query);
            response.QuestionsRespondedByClientLastSetofCount = QuestionResponsedQuesCount;
            response.QuestionsSentToClientLastSetofCount = QuestionsSentToClientCount;
        } else {
            response.QuestionsRespondedByClientLastSetofCount = 0;
            response.QuestionsSentToClientLastSetofCount = 0;
        }
        return response;
    } catch (err) {
        console.log(err);
    }
};

const sendVerificationRequestToClient = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        validateVerificationRequestToClient(input);
        const { Op, Forms, Formotp, Clients, LegalForms, Practices, Orders, Settings, Cases, PracticeSettings,
            Subscriptions, Plans } = await connectToDatabase();
        const otpCode = generateRandomString(8);
        const otpSecret = generateRandomString(4);


        let plan_type ;
        if(input.plan_type == 'free_trial'){
            plan_type= 'FREE TRIAL';
        }else if(input.plan_type == 'pay_as_you_go' || input.plan_type == 'tek_as_you_go'){
            plan_type= 'TEK AS-YOU-GO';
        }else if(['responding_monthly_349','responding_monthly_495','monthly'].includes(input.plan_type)){
            plan_type= 'MONTHLY';
        }else if(['responding_yearly_5100','responding_yearly_3490','yearly'].includes(input.plan_type)){
            plan_type= 'YEARLY';
        }else if(['propounding_monthly_199'].includes(input.plan_type)){
            plan_type= 'MONTHLY PROPOUNDING';
        }else if(['propounding_yearly_2199'].includes(input.plan_type)){
            plan_type= 'YEARLY PROPOUNDING';
        }

        if(!plan_type) throw new HTTPError(400, `Plan Type not found.`);
        let planDetails = undefined;
        let validSubscriptionFeatures = [];
        const existingSubscriptionDetails = await Subscriptions.findOne({
            where: {practice_id: event.user.practice_id, plan_category: 'responding'},
            order: [['createdAt', 'DESC']],
            raw: true
        });
        if (existingSubscriptionDetails && existingSubscriptionDetails.stripe_subscription_data && existingSubscriptionDetails.plan_id) {
            const stripeSubscriptionDataObject = JSON.parse(existingSubscriptionDetails.stripe_subscription_data);
            const subscriptionValidity = new Date(parseInt(stripeSubscriptionDataObject.current_period_end) * 1000);
            const today = new Date();
            if (subscriptionValidity > today) {
                planDetails = await Plans.findOne({
                    raw: true,
                    where: {stripe_product_id: existingSubscriptionDetails.stripe_product_id},
                });
                validSubscriptionFeatures = planDetails.features_included.split(',');
            }
        }

        const legalformsDetails = await LegalForms.findOne({where: {id: input.legalforms_id}});
        /* const FormsDetails = await Forms.count({where:{
            legalforms_id:input.legalforms_id,
            TargetLanguageCode:'es'
        }}); */
        const otdDataObject = {
            id: uuid.v4(),
            otp_code: otpCode,
            otp_secret: otpSecret,
            client_id: input.client_id,
            document_type: input.document_type,
            case_id: input.case_id,
            sending_type: 'verification',
            sent_by: event.user.id,
            scheduler_email: false,
            legalforms_id: input.legalforms_id
        };
        if (input.TargetLanguageCode) {
            otdDataObject.TargetLanguageCode = input.TargetLanguageCode;
        }
        const formotpModelObject = await Formotp.create(otdDataObject);

        const ClientDetails = await Clients.findOne({
            where: {id: input.client_id},
        });

        let practiceName = '';
        const practiceDetails = await Practices.findOne({where: {id: ClientDetails.practice_id}});
        if (practiceDetails) {
            practiceName = practiceDetails.name;
        }

        let questionsUrl = `${process.env.CARE_URL}/lawyer-answer-verification/${formotpModelObject.otp_code}`;
        if (input.TargetLanguageCode) {
            questionsUrl = `${questionsUrl}?TargetLanguageCode=${input.TargetLanguageCode}`;
        }
        /* if (FormsDetails > 0) {
            questionsUrl = `${questionsUrl}?TargetLanguageCode=es`;
        } */


        const orderData = {
            id: uuid.v4(),
            order_date: new Date(),
            status: 'completed', // completed, pending
            practice_id: event.user.practice_id,
            user_id: event.user.id,
            case_id: input.case_id,
            client_id: input.client_id,
            document_type: 'TEKSIGN', // (FROGS, SPROGS, RFPD, RFA)
            document_generation_type: 'teksign', // template or final_doc
            amount_charged: 0,
            charge_id: '',
            case_title: input.case_title,
            client_name: ClientDetails.name,
            legalforms_id: input.legalforms_id,
            filename: legalformsDetails.filename,
            plan_type: plan_type
        };
        const settingsObject = await Settings.findOne({
            where: {key: 'global_settings'},
            raw: true,
        });

        if (!settingsObject) throw new HTTPError(400, 'Settings data not found');
        let settings = JSON.parse(settingsObject.value);

        const practiceSettings = await PracticeSettings.findOne({
            where: {
                practice_id: event.user.practice_id,
            },
            raw: true,
        });
        if (practiceSettings) {
            const practiceSettingsObject = JSON.parse(practiceSettings.value);
            settings = Object.assign(settings, practiceSettingsObject);
        }
        let price = 0;

        if (settings && settings.teksign) {
            price = settings.teksign;
        }
        if (validSubscriptionFeatures.includes('teksign')) {
            price = 0;
        }
        if (price <= 0) {
            const orderDataModel = await Orders.create(orderData);
            if (price < 0) {
                price = 0;
            }
            console.log('Order Data');
            console.log(orderDataModel);
        } else {
            if (!practiceDetails.stripe_customer_id) {
                throw new HTTPError(400, 'Free tire exceeded but payment method is not added yet');
            }
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceDetails.stripe_customer_id,
                type: 'card',
            });
            if (!paymentMethods.data.length) {
                throw new HTTPError(400, 'Free tire exceeded but payment method not added');
            }

            if (price && price > 0 && price < 0.50) {
                price = 0.50;
            }
            const existOrderModel = await Orders.findAll({
                where: {
                    status: 'completed',
                    practice_id: orderData.practice_id,
                    client_id: orderData.client_id,
                    legalforms_id: orderData.legalforms_id,
                    document_type: 'TEKSIGN',
                    document_generation_type: 'teksign'
                }
            });
            if (existOrderModel.length != 0 || (planDetails && planDetails.plan_id != 'free_trial')) {
                const orderDataModel = await Orders.create(orderData);
                /* await Formotp.destroy({where:{id:formotpModelObject.id}}); */
                console.log(formotpModelObject.id);
            } else {
                const paymentMethod = paymentMethods.data[0];
                const paymentIntent = await stripe.paymentIntents.create({
                    amount: parseInt(price * 100),
                    currency: 'usd',
                    customer: practiceDetails.stripe_customer_id,
                    payment_method: paymentMethod.id,
                    off_session: true,
                    confirm: true,
                });
                orderData.charge_id = paymentIntent.id;
                orderData.amount_charged = price;
                const orderDataModel = await Orders.create(orderData);
                console.log('Order New Teksign or other Data');
                console.log(orderData);
            }

        }


        let message_body = '';
        let smstext_body = '';
        if (input.TargetLanguageCode && input.TargetLanguageCode != 'es' && input.TargetLanguageCode != 'vi') {
            smstext_body = 'Your attorney has drafted response for your lawsuit. Please click the link and enter the verification code ' + formotpModelObject.otp_secret + ' to review and sign. ' + questionsUrl;
            message_body = 'Your attorney has drafted response for your lawsuit. Please click the link and enter the verification code to review and sign. ' + '<br />' + questionsUrl;
        } else if (input.TargetLanguageCode && input.TargetLanguageCode == 'es') {
            smstext_body = 'Su abogado ha redactado una respuesta para su demanda. Haga clic en el enlace e ingrese el código de verificación ' + formotpModelObject.otp_secret + ' para revisar y firmar. ' + questionsUrl;
            message_body = 'Su abogado ha redactado una respuesta para su demanda. Haga clic en el enlace e ingrese el código de verificación para revisar y firmar. ' + '<br />' + questionsUrl;
        } else if (input.TargetLanguageCode && input.TargetLanguageCode == 'vi') {
            smstext_body = 'Luật sư của bạn đã soạn thảo câu trả lời cho vụ kiện của bạn. Vui lòng nhấp vào liên kết và nhập mã xác minh ' + formotpModelObject.otp_secret + ' để xem xét và ký. ' + questionsUrl;
            message_body = 'Luật sư của bạn đã soạn thảo câu trả lời cho vụ kiện của bạn. Vui lòng nhấp vào liên kết và nhập mã xác minh để xem lại và ký. ' + '<br />' + questionsUrl;
        }

        if (input.TargetLanguageCode && input.TargetLanguageCode == 'es') {
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Importante: Se requiere respuesta para su demanda', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Importante: Se requiere respuesta para su demanda', smstext_body);
            }
        } else if (input.TargetLanguageCode && input.TargetLanguageCode == 'vi') {
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Quan trọng: Vụ kiện của bạn - Xem lại / Ký các phản hồi đã hoàn thiện', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Quan trọng: Vụ kiện của bạn - Xem lại / Ký các phản hồi đã hoàn thiện', smstext_body);
            }
        } else {
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Important:  Your lawsuit - Review/Sign finalized responses', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Important: Your lawsuit - Review/Sign finalized responses', smstext_body);
            }
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'Questions sent to client', otp_code: formotpModelObject.otp_code}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};


const mailTest = async (event) => {
    try {
        await sendEmail('sandip.sandip.das5@gmail.com', 'Important: Making sure mail function works', 'This is to confirm mail system working fine or not');
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'ok'}),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};

const getFormDataByOtp = async (event) => {
    try {
        const { Forms, Formotp, Op, QuestionsTranslations, OtherPartiesForms, OtherPartiesFormsOtp, Cases, sequelize } = await connectToDatabase();
        const queryParams = event.queryStringParameters || {};
        const otp_code = queryParams.otp_code;
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code query parameter');

        let otpDetails = await Formotp.findOne({ where: {otp_code},});

        let document_type;
        if (otpDetails) document_type = otpDetails.document_type;

        let otherPartyotpDetail = await OtherPartiesFormsOtp.findOne({ where: {otp_code},});

        if (!otpDetails && !otherPartyotpDetail) throw new HTTPError(404, 'Invalid OTP code');

        if (otherPartyotpDetail) {
            let res = await getOtherPartyFormDataByOtp(event);
            return res;
        }
        const caseDetails = await Cases.findOne({ where: {id: otpDetails.case_id},});

        const query = {};
        const query2 = {};
        query.raw = true;
        query2.raw = true;
        query.where = {
            case_id: otpDetails.case_id,
            legalforms_id: otpDetails.legalforms_id,
            document_type: otpDetails.document_type
        };
        query2.where = {
            case_id: otpDetails.case_id,
            legalforms_id: otpDetails.legalforms_id,
            document_type: otpDetails.document_type
        };
        query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable']};
        query2.where.client_response_status = {[Op.in]: ['SentToClient']};
        if (otpDetails.sending_type == 'selected_questions') {
            query.where.id = {[Op.in]: JSON.parse(otpDetails.question_ids)};
            query2.where.id = {[Op.in]: JSON.parse(otpDetails.question_ids)};
        }
        query.order = [['question_id', 'ASC'],];
        query2.order = [['question_id', 'ASC'],];

        query.logging = console.log;
        query2.logging = console.log;

        query.attributes = [[sequelize.fn('max', sequelize.col('last_updated_by_client')), 'recent_response_date']];
        query2.attributes = [[sequelize.fn('count', sequelize.col('client_response_status')), 'client_response_status']];

        const latestClientResponse = await Forms.findAll(query); // fetch latest client response date and time
        const pendingQuestionCount = await Forms.findAll(query2); // fetch pending questions count
        let finalDateforClientResponseEdit = latestClientResponse[0].recent_response_date;
        let currentDate = new Date();
        if (pendingQuestionCount == 0 && finalDateforClientResponseEdit && finalDateforClientResponseEdit != '') {
            finalDateforClientResponseEdit.setDate(finalDateforClientResponseEdit.getDate() + 7);
            let cDate = finalDateforClientResponseEdit.getDate();
            let Year = finalDateforClientResponseEdit.getFullYear();
            let Month = finalDateforClientResponseEdit.getMonth() + 1;
            if (Month < 10) Month = '0' + Month;
            if (cDate < 10) cDate = '0' + cDate;
            finalDateforClientResponseEdit = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';

            currentDate.setDate(currentDate.getDate() + 0);
            let cuDate = currentDate.getDate();
            let cYear = currentDate.getFullYear();
            let cuMonth = currentDate.getMonth() + 1;
            if (cuMonth < 10) cuMonth = '0' + cuMonth;
            if (cuDate < 10) cuDate = '0' + cuDate;
            currentDate = cYear + '-' + cuMonth + '-' + cuDate + ' ' + '23:59:59';
        }

        if (pendingQuestionCount == 0 && finalDateforClientResponseEdit && finalDateforClientResponseEdit < currentDate) {
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify([]),
            };
        }

        let consultationSetNo = 0;

        if (document_type.toUpperCase() === 'FROGS') {
            let consultaionQuery = query;
            if (otpDetails.sending_type == 'selected_questions') {
                delete consultaionQuery.where.id;
            }

            delete query.where.client_response_status;
            query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable', 'NotSetToClient']};

            consultaionQuery.attributes = [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']];
            consultationSetNo = await Forms.findAll(consultaionQuery);
            delete consultaionQuery.attributes;
            delete consultaionQuery.where.client_response_status;
            query.where.client_response_status = {[Op.in]: ['SentToClient']};
            consultationSetNo = consultationSetNo[0].consultation_set_no;

        } else {
            delete query.where.client_response_status;
            query.where.client_response_status = {[Op.in]: ['SentToClient']};
        }
        delete query.attributes;
        if (otpDetails.sending_type == 'selected_questions' && document_type.toUpperCase() === 'FROGS') {
            query.where.id = {[Op.in]: JSON.parse(otpDetails.question_ids)};
        }
        console.log(query);

        const forms = await Forms.findAll(query);

        if (forms.length <= 0) {

            let noPendingQuestions;
            if (document_type.toUpperCase() === 'FROGS') {
                noPendingQuestions = await groupByFormsConsultation(forms);
            } else {
                noPendingQuestions = forms;
            }

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(noPendingQuestions),
            };
        }
        for (let j = 0; j < forms.length; j += 1) {
            const formObject = forms[j];
            formObject.sending_type = otpDetails.sending_type;
            formObject.total_consultation_count = consultationSetNo;
            forms[j] = formObject;
        }
        let createdDate = forms[0].createdAt;
        const spanishFormDate = new Date('2021-07-11');
        let convertedDate = createdDate.getDate();
        let convertedYear = createdDate.getFullYear();
        let convertedMonth = createdDate.getMonth() + 1;
        if (convertedMonth < 10) convertedMonth = '0' + convertedMonth;
        let NewcreateDate = new Date(convertedYear + '-' + convertedMonth + '-' + convertedDate);

        console.log(queryParams);
        if ((queryParams.TargetLanguageCode != 'es' && queryParams.TargetLanguageCode != 'vi') || !queryParams.TargetLanguageCode) {

            let normalFormsQuestions;
            if (document_type.toUpperCase() === 'FROGS') {
                normalFormsQuestions = await groupByFormsConsultation(forms);
            } else {
                normalFormsQuestions = forms;
            }

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(normalFormsQuestions),
            };
        }

        if (NewcreateDate < spanishFormDate) {
            for (let i = 0; i < forms.length; i += 1) {
                const formObject = forms[i];
                const translatedObject = await QuestionsTranslations.findOne({
                    where: {
                        document_type: formObject.document_type,
                        question_number: formObject.question_number_text,
                        TargetLanguageCode: queryParams.TargetLanguageCode,
                    },
                });
                if (translatedObject) {
                    formObject.question_text = translatedObject.TargetLanguageText;
                }
                if (caseDetails) {
                    formObject.case_title = caseDetails.case_title;
                    formObject.county = caseDetails.county;
                }
                forms[i] = formObject;
            }
        } else {
            if (queryParams.TargetLanguageCode == 'es') {
                const fileResponse = await readTxtFile();
                const jsonData = JSON.parse(fileResponse);
                for (let i = 0; i < forms.length; i += 1) {
                    for (const data of jsonData) {
                        if (data.question_number_text == forms[i].question_number_text && data.question_section_id == forms[i].question_section_id) {
                            forms[i].question_text = data.question_text;
                            forms[i].question_section_text = data.question_section_text;
                            forms[i].question_section = data.question_section;
                            if (caseDetails) {
                                forms[i].case_title = caseDetails.case_title;
                                forms[i].county = caseDetails.county;
                            }
                            break;
                        }
                    }
                }
            } else if (queryParams.TargetLanguageCode == 'vi') {
                const findQuestion = forms[0].question_number_text;
                const vnFileName = findQuestionDisk(findQuestion);
                if (!vnFileName) throw new HTTPError(404, 'Cannot find Vietnamese Filename');
                const fileResponse = await readVietnameseTextFile(vnFileName);
                const jsonData = JSON.parse(fileResponse);
                for (let i = 0; i < forms.length; i += 1) {
                    for (const data of jsonData) {
                        if (data.question_number_text == forms[i].question_number_text && data.question_section_id == forms[i].question_section_id) {
                            forms[i].question_text = data.question_text;
                            forms[i].question_section_text = data.question_section_text;
                            forms[i].question_section = data.question_section;
                            if (caseDetails) {
                                forms[i].case_title = caseDetails.case_title;
                                forms[i].county = caseDetails.county;
                            }
                            break;
                        }
                    }
                }
            }
        }

        let translatedFormsQuestions;
        if (document_type.toUpperCase() === 'FROGS') {
            translatedFormsQuestions = await groupByFormsConsultation(forms);
        } else {
            translatedFormsQuestions = forms;
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(translatedFormsQuestions),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the forms'}),
        };
    }
};

const getOtherPartyFormDataByOtp = async (event) => {
    try {
        const { Op, QuestionsTranslations, OtherPartiesForms, Forms, OtherPartiesFormsOtp, Cases, sequelize } = await connectToDatabase();
        const queryParams = event.queryStringParameters || {};
        const otp_code = queryParams.otp_code;
        const otpTableDetails = await OtherPartiesFormsOtp.findAll({where: {otp_code: otp_code}, raw: true, logging:console.log});
        
        const otpDetails = otpTableDetails[0];

        const caseDetails = await Cases.findOne({ where: { id: otpDetails.case_id }, });

        const document_type = otpDetails.document_type;

        const query = {};
        const query2 = {};
        query.raw = true;
        query2.raw = true;
        query.where = { case_id: otpDetails.case_id, party_id: otpDetails.party_id,
            legalforms_id: otpDetails.legalforms_id, document_type: otpDetails.document_type
        };
        query2.where = {
            case_id: otpDetails.case_id, party_id: otpDetails.party_id,
            legalforms_id: otpDetails.legalforms_id, document_type: otpDetails.document_type
        };
        query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable']};
        query2.where.client_response_status = {[Op.in]: ['SentToClient']};

        if (otpDetails.sending_type == 'selected_questions') {
            query.where.id = {[Op.in]: JSON.parse(otpDetails.question_id)};
            query2.where.id = {[Op.in]: JSON.parse(otpDetails.question_id)};
        }
        query.order = [['question_id', 'ASC'],];
        query2.order = [['question_id', 'ASC'],];

        query.attributes = [[sequelize.fn('max', sequelize.col('last_updated_by_client')), 'recent_response_date']];
        query2.attributes = [[sequelize.fn('count', sequelize.col('client_response_status')), 'client_response_status']];

        // query.logging = console.log;
        // query2.logging = console.log;
        const latestClientResponse = await OtherPartiesForms.findAll(query);
        const pendingQuestionCount = await OtherPartiesForms.findAll(query2);
        let finalDateforClientResponseEdit = latestClientResponse[0].recent_response_date;
        let currentDate = new Date();

        if (pendingQuestionCount == 0 && finalDateforClientResponseEdit && finalDateforClientResponseEdit != '') {
            finalDateforClientResponseEdit.setDate(finalDateforClientResponseEdit.getDate() + 7);
            let cDate = finalDateforClientResponseEdit.getDate();
            let Year = finalDateforClientResponseEdit.getFullYear();
            let Month = finalDateforClientResponseEdit.getMonth() + 1;
            if (Month < 10) Month = '0' + Month;
            if (cDate < 10) cDate = '0' + cDate;
            finalDateforClientResponseEdit = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';

            currentDate.setDate(currentDate.getDate() + 0);
            let cuDate = currentDate.getDate();
            let cYear = currentDate.getFullYear();
            let cuMonth = currentDate.getMonth() + 1;
            if (cuMonth < 10) cuMonth = '0' + cuMonth;
            if (cuDate < 10) cuDate = '0' + cuDate;
            currentDate = cYear + '-' + cuMonth + '-' + cuDate + ' ' + '23:59:59';
        }

        if (pendingQuestionCount == 0 && finalDateforClientResponseEdit && finalDateforClientResponseEdit < currentDate) {
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify([]),
            };
        }

        let consultationSetNo = 0;

        if (document_type.toUpperCase() === 'FROGS') {

            let consultaionQuery = query;
            if (otpDetails.sending_type == 'selected_questions') {
                delete consultaionQuery.where.id;
            }


            delete query.where.client_response_status;
            query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable', 'NotSetToClient']};

            consultaionQuery.attributes = [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']];
            consultationSetNo = await OtherPartiesForms.findAll(consultaionQuery);
            delete consultaionQuery.attributes;
            delete consultaionQuery.where.client_response_status;
            query.where.client_response_status = {[Op.in]: ['SentToClient']};
            consultationSetNo = consultationSetNo[0].consultation_set_no;
        } else {
            delete query.where.client_response_status;
            query.where.client_response_status = {[Op.in]: ['SentToClient']};
        }

        delete query.attributes;
        if (otpDetails.sending_type == 'selected_questions' && document_type.toUpperCase() === 'FROGS') {
            query.where.id = {[Op.in]: JSON.parse(otpDetails.question_id)};
        }
        query.logging = console.log;
        const forms = await OtherPartiesForms.findAll(query);
        if (forms.length > 0) {
            for (let x = 0; x < forms.length; x++) {
                let otherPartiesRow = forms[x];
                let formsObj = await Forms.findOne({
                    where: {
                        case_id: otherPartiesRow.case_id,
                        legalforms_id: otherPartiesRow.legalforms_id,
                        document_type: otherPartiesRow.document_type,
                        id: otherPartiesRow.form_id
                    }, logging: console.log
                });
                forms[x].lawyer_response_text = formsObj.lawyer_response_text;
                forms[x].lawyer_response_status = formsObj.lawyer_response_status;
            }
        }
        if (forms.length <= 0) {
            let noPendingQuestions;
            if (document_type.toUpperCase() === 'FROGS') {
                noPendingQuestions = await groupByFormsConsultation(forms);
            } else {
                noPendingQuestions = forms;
            }

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(noPendingQuestions),
            };
        }
        for (let j = 0; j < forms.length; j += 1) {
            const formObject = forms[j];
            formObject.sending_type = otpDetails.sending_type;
            formObject.total_consultation_count = consultationSetNo;
            forms[j] = formObject;
        }
        let createdDate = forms[0].createdAt;
        const spanishFormDate = new Date('2021-07-11');
        let convertedDate = createdDate.getDate();
        let convertedYear = createdDate.getFullYear();
        let convertedMonth = createdDate.getMonth() + 1;
        if (convertedMonth < 10) convertedMonth = '0' + convertedMonth;
        let NewcreateDate = new Date(convertedYear + '-' + convertedMonth + '-' + convertedDate);
        if ((queryParams.TargetLanguageCode != 'es' && queryParams.TargetLanguageCode != 'vi') || !queryParams.TargetLanguageCode) {

            let normalFormsQuestions;
            if (document_type.toUpperCase() === 'FROGS') {
                normalFormsQuestions = await groupByFormsConsultation(forms);
            } else {
                normalFormsQuestions = forms;
            }

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(normalFormsQuestions),
            };
        }
        if (NewcreateDate < spanishFormDate) {
            for (let i = 0; i < forms.length; i += 1) {
                const formObject = forms[i];
                const translatedObject = await QuestionsTranslations.findOne({
                    where: {
                        document_type: formObject.document_type,
                        question_number: formObject.question_number_text,
                        TargetLanguageCode: queryParams.TargetLanguageCode,
                    },
                });
                if (translatedObject) {
                    formObject.question_text = translatedObject.TargetLanguageText;
                }
                if (caseDetails) {
                    formObject.case_title = caseDetails.case_title;
                    formObject.county = caseDetails.county;
                }
                forms[i] = formObject;
            }
        } else {
            if (queryParams.TargetLanguageCode == 'es') {
                const fileResponse = await readTxtFile();
                const jsonData = JSON.parse(fileResponse);
                for (let i = 0; i < forms.length; i += 1) {
                    for (const data of jsonData) {
                        if (data.question_number_text == forms[i].question_number_text && data.question_section_id == forms[i].question_section_id) {
                            forms[i].question_text = data.question_text;
                            forms[i].question_section_text = data.question_section_text;
                            forms[i].question_section = data.question_section;
                            if (caseDetails) {
                                forms[i].case_title = caseDetails.case_title;
                                forms[i].county = caseDetails.county;
                            }
                            break;
                        }
                    }
                }
            } else if (queryParams.TargetLanguageCode == 'vi') {
                const findQuestion = forms[0].question_number_text;
                const vnFileName = findQuestionDisk(findQuestion);
                if (!vnFileName) throw new HTTPError(404, 'Cannot find Vietnamese Filename');
                const fileResponse = await readVietnameseTextFile(vnFileName);
                const jsonData = JSON.parse(fileResponse);
                for (let i = 0; i < forms.length; i += 1) {
                    for (const data of jsonData) {
                        if (data.question_number_text == forms[i].question_number_text && data.question_section_id == forms[i].question_section_id) {
                            forms[i].question_text = data.question_text;
                            forms[i].question_section_text = data.question_section_text;
                            forms[i].question_section = data.question_section;
                            if (caseDetails) {
                                forms[i].case_title = caseDetails.case_title;
                                forms[i].county = caseDetails.county;
                            }
                            break;
                        }
                    }
                }
                /* console.log(forms); */
            }
        }

        let translatedFormsQuestions;
        if (document_type.toUpperCase() === 'FROGS') {
            translatedFormsQuestions = await groupByFormsConsultation(forms);
        } else {
            translatedFormsQuestions = forms;
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(translatedFormsQuestions),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the forms'}),
        };
    }
};

const getFormDataByOtpWithUnansweredQuestion = async (event) => {
    try {
        const {
            Forms,
            Formotp,
            Op,
            QuestionsTranslations,
            OtherPartiesForms,
            OtherPartiesFormsOtp,
            Cases,
            sequelize
        } = await connectToDatabase();
        const queryParams = event.query || {};
        const otp_code = queryParams.otp_code;
        /* console.log(otp_code);
        console.log(queryParams.TargetLanguageCode); */
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code query parameter');

        let otpDetails = await Formotp.findOne({
            where: {otp_code},
        });

        let document_type;
        if (otpDetails) {
            document_type = otpDetails.document_type;
        }

        let otherPartyotpDetail = await OtherPartiesFormsOtp.findOne({
            where: {otp_code},
        });

        if (!otpDetails && !otherPartyotpDetail) throw new HTTPError(404, 'Invalid OTP code');

        if (otherPartyotpDetail) {
            let res = await getOtherPartyFormDataByOtpWithUnansweredQuestion(event);
            return res;
        }
        const caseDetails = await Cases.findOne({
            where: {id: otpDetails.case_id},
        });

        const query = {};
        const query2 = {};
        query.raw = true;
        query2.raw = true;
        query.where = {
            case_id: otpDetails.case_id,
            legalforms_id: otpDetails.legalforms_id,
            document_type: otpDetails.document_type
        };
        query2.where = {
            case_id: otpDetails.case_id,
            legalforms_id: otpDetails.legalforms_id,
            document_type: otpDetails.document_type
        };
        query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable']};
        query2.where.client_response_status = {[Op.in]: ['SentToClient']};
        if (otpDetails.sending_type == 'selected_questions') {
            query.where.id = {[Op.in]: JSON.parse(otpDetails.question_ids)};
            query2.where.id = {[Op.in]: JSON.parse(otpDetails.question_ids)};
        }
        query.order = [['question_id', 'ASC'],];
        query2.order = [['question_id', 'ASC'],];

        query.logging = console.log;
        query2.logging = console.log;

        query.attributes = [[sequelize.fn('max', sequelize.col('last_updated_by_client')), 'recent_response_date']];
        query2.attributes = [[sequelize.fn('count', sequelize.col('client_response_status')), 'client_response_status']];

        const latestClientResponse = await Forms.findAll(query);
        const pendingQuestionCount = await Forms.findAll(query2);
        let finalDateforClientResponseEdit = latestClientResponse[0].recent_response_date;
        let currentDate = new Date();
        if (pendingQuestionCount == 0 && finalDateforClientResponseEdit && finalDateforClientResponseEdit != '') {
            finalDateforClientResponseEdit.setDate(finalDateforClientResponseEdit.getDate() + 7);
            let cDate = finalDateforClientResponseEdit.getDate();
            let Year = finalDateforClientResponseEdit.getFullYear();
            let Month = finalDateforClientResponseEdit.getMonth() + 1;
            if (Month < 10) Month = '0' + Month;
            if (cDate < 10) cDate = '0' + cDate;
            finalDateforClientResponseEdit = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';

            currentDate.setDate(currentDate.getDate() + 0);
            let cuDate = currentDate.getDate();
            let cYear = currentDate.getFullYear();
            let cuMonth = currentDate.getMonth() + 1;
            if (cuMonth < 10) cuMonth = '0' + cuMonth;
            if (cuDate < 10) cuDate = '0' + cuDate;
            currentDate = cYear + '-' + cuMonth + '-' + cuDate + ' ' + '23:59:59';
        }

        if (pendingQuestionCount == 0 && finalDateforClientResponseEdit && finalDateforClientResponseEdit < currentDate) {
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify([]),
            };
        }

        let consultationSetNo = 0;

        if (document_type.toUpperCase() === 'FROGS') {
            let consultaionQuery = query;
            if (otpDetails.sending_type == 'selected_questions') {
                delete consultaionQuery.where.id;
            }

            delete query.where.client_response_status;
            query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable', 'NotSetToClient']};

            consultaionQuery.attributes = [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']];
            consultationSetNo = await Forms.findAll(consultaionQuery);
            delete consultaionQuery.attributes;
            delete consultaionQuery.where.client_response_status;
            query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable']};
            consultationSetNo = consultationSetNo[0].consultation_set_no;

        }
        delete query.attributes;
        if (otpDetails.sending_type == 'selected_questions' && document_type.toUpperCase() === 'FROGS') {
            query.where.id = {[Op.in]: JSON.parse(otpDetails.question_ids)};
        }
        console.log(query);

        const forms = await Forms.findAll(query);

        if (forms.length <= 0) {

            let noPendingQuestions;
            if (document_type.toUpperCase() === 'FROGS') {
                noPendingQuestions = await groupByFormsConsultation(forms);
            } else {
                noPendingQuestions = forms;
            }

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(noPendingQuestions),
            };
        }
        for (let j = 0; j < forms.length; j += 1) {
            const formObject = forms[j];
            formObject.sending_type = otpDetails.sending_type;
            formObject.total_consultation_count = consultationSetNo;
            forms[j] = formObject;
        }
        let createdDate = forms[0].createdAt;
        const spanishFormDate = new Date('2021-07-11');
        let convertedDate = createdDate.getDate();
        let convertedYear = createdDate.getFullYear();
        let convertedMonth = createdDate.getMonth() + 1;
        if (convertedMonth < 10) convertedMonth = '0' + convertedMonth;
        let NewcreateDate = new Date(convertedYear + '-' + convertedMonth + '-' + convertedDate);

        console.log(queryParams);
        if ((queryParams.TargetLanguageCode != 'es' && queryParams.TargetLanguageCode != 'vi') || !queryParams.TargetLanguageCode) {

            let normalFormsQuestions;
            if (document_type.toUpperCase() === 'FROGS') {
                normalFormsQuestions = await groupByFormsConsultation(forms);
            } else {
                normalFormsQuestions = forms;
            }

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(normalFormsQuestions),
            };
        }

        if (NewcreateDate < spanishFormDate) {
            for (let i = 0; i < forms.length; i += 1) {
                const formObject = forms[i];
                const translatedObject = await QuestionsTranslations.findOne({
                    where: {
                        document_type: formObject.document_type,
                        question_number: formObject.question_number_text,
                        TargetLanguageCode: queryParams.TargetLanguageCode,
                    },
                });
                if (translatedObject) {
                    formObject.question_text = translatedObject.TargetLanguageText;
                }
                if (caseDetails) {
                    formObject.case_title = caseDetails.case_title;
                    formObject.county = caseDetails.county;
                }
                forms[i] = formObject;
            }
        } else {
            if (queryParams.TargetLanguageCode == 'es') {
                const fileResponse = await readTxtFile();
                const jsonData = JSON.parse(fileResponse);
                for (let i = 0; i < forms.length; i += 1) {
                    for (const data of jsonData) {
                        if (data.question_number_text == forms[i].question_number_text && data.question_section_id == forms[i].question_section_id) {
                            forms[i].question_text = data.question_text;
                            forms[i].question_section_text = data.question_section_text;
                            forms[i].question_section = data.question_section;
                            if (caseDetails) {
                                forms[i].case_title = caseDetails.case_title;
                                forms[i].county = caseDetails.county;
                            }
                            break;
                        }
                    }
                }
            } else if (queryParams.TargetLanguageCode == 'vi') {
                const findQuestion = forms[0].question_number_text;
                const vnFileName = findQuestionDisk(findQuestion);
                if (!vnFileName) throw new HTTPError(404, 'Cannot find Vietnamese Filename');
                const fileResponse = await readVietnameseTextFile(vnFileName);
                const jsonData = JSON.parse(fileResponse);
                for (let i = 0; i < forms.length; i += 1) {
                    for (const data of jsonData) {
                        if (data.question_number_text == forms[i].question_number_text && data.question_section_id == forms[i].question_section_id) {
                            forms[i].question_text = data.question_text;
                            forms[i].question_section_text = data.question_section_text;
                            forms[i].question_section = data.question_section;
                            if (caseDetails) {
                                forms[i].case_title = caseDetails.case_title;
                                forms[i].county = caseDetails.county;
                            }
                            break;
                        }
                    }
                }
            }
        }

        let translatedFormsQuestions;
        if (document_type.toUpperCase() === 'FROGS') {
            translatedFormsQuestions = await groupByFormsConsultation(forms);
        } else {
            translatedFormsQuestions = forms;
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(translatedFormsQuestions),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the forms'}),
        };
    }
}

const getOtherPartyFormDataByOtpWithUnansweredQuestion = async (event) => {
    try {
        const {
            Op,
            QuestionsTranslations,
            OtherPartiesForms,
            Forms,
            OtherPartiesFormsOtp,
            Cases,
            sequelize
        } = await connectToDatabase();
        /* const queryParams = event.params || event.pathParameters; */
        const queryParams = event.query || {};
        const otp_code = queryParams.otp_code;
        /* console.log(otp_code);
        console.log(queryParams.TargetLanguageCode); */
        const otpTableDetails = await OtherPartiesFormsOtp.findAll({where: {otp_code: otp_code}, raw: true});
        const otpDetails = otpTableDetails[0];

        const caseDetails = await Cases.findOne({
            where: {id: otpDetails.case_id},
        });

        const document_type = otpDetails.document_type;

        const query = {};
        const query2 = {};
        query.raw = true;
        query2.raw = true;
        query.where = {
            case_id: otpDetails.case_id,
            party_id: otpDetails.party_id,
            legalforms_id: otpDetails.legalforms_id,
            document_type: otpDetails.document_type
        };
        query2.where = {
            case_id: otpDetails.case_id,
            party_id: otpDetails.party_id,
            legalforms_id: otpDetails.legalforms_id,
            document_type: otpDetails.document_type
        };
        query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable']};
        query2.where.client_response_status = {[Op.in]: ['SentToClient']};
        if (otpDetails.sending_type == 'selected_questions') {
            query.where.id = {[Op.in]: JSON.parse(otpDetails.question_id)};
            query2.where.id = {[Op.in]: JSON.parse(otpDetails.question_id)};
        }
        query.order = [['question_id', 'ASC'],];
        query2.order = [['question_id', 'ASC'],];

        query.attributes = [[sequelize.fn('max', sequelize.col('last_updated_by_client')), 'recent_response_date']];
        query2.attributes = [[sequelize.fn('count', sequelize.col('client_response_status')), 'client_response_status']];

        // query.logging = console.log;
        // query2.logging = console.log;
        const latestClientResponse = await OtherPartiesForms.findAll(query);
        const pendingQuestionCount = await OtherPartiesForms.findAll(query2);
        let finalDateforClientResponseEdit = latestClientResponse[0].recent_response_date;
        let currentDate = new Date();

        if (pendingQuestionCount == 0 && finalDateforClientResponseEdit && finalDateforClientResponseEdit != '') {
            finalDateforClientResponseEdit.setDate(finalDateforClientResponseEdit.getDate() + 7);
            let cDate = finalDateforClientResponseEdit.getDate();
            let Year = finalDateforClientResponseEdit.getFullYear();
            let Month = finalDateforClientResponseEdit.getMonth() + 1;
            if (Month < 10) Month = '0' + Month;
            if (cDate < 10) cDate = '0' + cDate;
            finalDateforClientResponseEdit = Year + '-' + Month + '-' + cDate + ' ' + '23:59:59';

            currentDate.setDate(currentDate.getDate() + 0);
            let cuDate = currentDate.getDate();
            let cYear = currentDate.getFullYear();
            let cuMonth = currentDate.getMonth() + 1;
            if (cuMonth < 10) cuMonth = '0' + cuMonth;
            if (cuDate < 10) cuDate = '0' + cuDate;
            currentDate = cYear + '-' + cuMonth + '-' + cuDate + ' ' + '23:59:59';
        }

        console.log('pendingQuestionCount -> ' + pendingQuestionCount);
        console.log('finalDateforClientResponseEdit -> ' + finalDateforClientResponseEdit);
        console.log('currentDate -> ' + currentDate);
        if (pendingQuestionCount == 0 && finalDateforClientResponseEdit && finalDateforClientResponseEdit < currentDate) {
            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify([]),
            };
        }

        console.log('After IF');
        let consultationSetNo = 0;

        if (document_type.toUpperCase() === 'FROGS') {

            let consultaionQuery = query;
            if (otpDetails.sending_type == 'selected_questions') {
                delete consultaionQuery.where.id;
            }


            delete query.where.client_response_status;
            query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable', 'NotSetToClient']};

            consultaionQuery.attributes = [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']];
            consultationSetNo = await OtherPartiesForms.findAll(consultaionQuery);
            delete consultaionQuery.attributes;
            delete consultaionQuery.where.client_response_status;
            query.where.client_response_status = {[Op.in]: ['SentToClient', 'ClientResponseAvailable']};
            consultationSetNo = consultationSetNo[0].consultation_set_no;
        }

        delete query.attributes;
        query.logging = console.log;

        if (otpDetails.sending_type == 'selected_questions' && document_type.toUpperCase() === 'FROGS') {
            query.where.id = {[Op.in]: JSON.parse(otpDetails.question_id)};
        }
        const forms = await OtherPartiesForms.findAll(query);
        if (forms.length > 0) {
            for (let x = 0; x < forms.length; x++) {
                let otherPartiesRow = forms[x];
                let formsObj = await Forms.findOne({
                    where: {
                        case_id: otherPartiesRow.case_id,
                        legalforms_id: otherPartiesRow.legalforms_id,
                        document_type: otherPartiesRow.document_type,
                        id: otherPartiesRow.form_id
                    }
                });
                forms[x].lawyer_response_text = formsObj.lawyer_response_text;
                forms[x].lawyer_response_status = formsObj.lawyer_response_status;
            }
        }
        if (forms.length <= 0) {
            let noPendingQuestions;
            if (document_type.toUpperCase() === 'FROGS') {
                noPendingQuestions = await groupByFormsConsultation(forms);
            } else {
                noPendingQuestions = forms;
            }

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(noPendingQuestions),
            };
        }
        for (let j = 0; j < forms.length; j += 1) {
            const formObject = forms[j];
            formObject.sending_type = otpDetails.sending_type;
            formObject.total_consultation_count = consultationSetNo;
            forms[j] = formObject;
        }
        let createdDate = forms[0].createdAt;
        const spanishFormDate = new Date('2021-07-11');
        let convertedDate = createdDate.getDate();
        let convertedYear = createdDate.getFullYear();
        let convertedMonth = createdDate.getMonth() + 1;
        if (convertedMonth < 10) convertedMonth = '0' + convertedMonth;
        let NewcreateDate = new Date(convertedYear + '-' + convertedMonth + '-' + convertedDate);
        if ((queryParams.TargetLanguageCode != 'es' && queryParams.TargetLanguageCode != 'vi') || !queryParams.TargetLanguageCode) {

            let normalFormsQuestions;
            if (document_type.toUpperCase() === 'FROGS') {
                normalFormsQuestions = await groupByFormsConsultation(forms);
            } else {
                normalFormsQuestions = forms;
            }

            return {
                statusCode: 200,
                headers: {
                    'Content-Type': 'text/plain',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': true
                },
                body: JSON.stringify(normalFormsQuestions),
            };
        }
        if (NewcreateDate < spanishFormDate) {
            for (let i = 0; i < forms.length; i += 1) {
                const formObject = forms[i];
                const translatedObject = await QuestionsTranslations.findOne({
                    where: {
                        document_type: formObject.document_type,
                        question_number: formObject.question_number_text,
                        TargetLanguageCode: queryParams.TargetLanguageCode,
                    },
                });
                if (translatedObject) {
                    formObject.question_text = translatedObject.TargetLanguageText;
                }
                if (caseDetails) {
                    formObject.case_title = caseDetails.case_title;
                    formObject.county = caseDetails.county;
                }
                forms[i] = formObject;
            }
        } else {
            if (queryParams.TargetLanguageCode == 'es') {
                const fileResponse = await readTxtFile();
                const jsonData = JSON.parse(fileResponse);
                for (let i = 0; i < forms.length; i += 1) {
                    for (const data of jsonData) {
                        if (data.question_number_text == forms[i].question_number_text && data.question_section_id == forms[i].question_section_id) {
                            forms[i].question_text = data.question_text;
                            forms[i].question_section_text = data.question_section_text;
                            forms[i].question_section = data.question_section;
                            if (caseDetails) {
                                forms[i].case_title = caseDetails.case_title;
                                forms[i].county = caseDetails.county;
                            }
                            break;
                        }
                    }
                }
            } else if (queryParams.TargetLanguageCode == 'vi') {
                const findQuestion = forms[0].question_number_text;
                const vnFileName = findQuestionDisk(findQuestion);
                if (!vnFileName) throw new HTTPError(404, 'Cannot find Vietnamese Filename');
                const fileResponse = await readVietnameseTextFile(vnFileName);
                const jsonData = JSON.parse(fileResponse);
                for (let i = 0; i < forms.length; i += 1) {
                    for (const data of jsonData) {
                        if (data.question_number_text == forms[i].question_number_text && data.question_section_id == forms[i].question_section_id) {
                            forms[i].question_text = data.question_text;
                            forms[i].question_section_text = data.question_section_text;
                            forms[i].question_section = data.question_section;
                            if (caseDetails) {
                                forms[i].case_title = caseDetails.case_title;
                                forms[i].county = caseDetails.county;
                            }
                            break;
                        }
                    }
                }
                /* console.log(forms); */
            }
        }

        let translatedFormsQuestions;
        if (document_type.toUpperCase() === 'FROGS') {
            translatedFormsQuestions = await groupByFormsConsultation(forms);
        } else {
            translatedFormsQuestions = forms;
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(translatedFormsQuestions),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the forms'}),
        };
    }
}


const getLawyerResponseDataByOtp = async (event) => {
    try {
        const {Forms, Formotp, Op, QuestionsTranslations, Cases, Clients, ClientSignature} = await connectToDatabase();
        const queryParams = event.query || {};
        const otp_code = queryParams.otp_code;
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code query parameter');

        const otpDetails = await Formotp.findOne({
            where: {otp_code},
        });
        if (!otpDetails) throw new HTTPError(404, 'Invalid OTP code');
        const caseDetails = await Cases.findOne({
            where: {id: otpDetails.case_id},
        });
        const query = {};
        const client_id = otpDetails.client_id;
        query.raw = true;
        query.where = {
            case_id: otpDetails.case_id,
            legalforms_id: otpDetails.legalforms_id,
            document_type: otpDetails.document_type
        };
        query.where.lawyer_response_text = {[Op.ne]: null};
        query.where.lawyer_response_status = 'Final';
        query.order = [
            ['question_id', 'ASC'],
        ];

        const forms = await Forms.findAll(query);

        const clientSignatureObject = await ClientSignature.findOne({
            where: {
                case_id: otpDetails.case_id,
                document_type: otpDetails.document_type,
                legalforms_id: otpDetails.legalforms_id
            }
        });

        let client_signature_exist = false;
        if (clientSignatureObject) {
            client_signature_exist = true;
        }

        for (let i = 0; i < forms.length; i += 1) {
            const formObject = forms[i];
            const clinetDetails = await Clients.findOne({where: {id: client_id}});
            if (queryParams.TargetLanguageCode) {
                const translatedObject = await QuestionsTranslations.findOne({
                    where: {
                        document_type: formObject.document_type,
                        question_number: formObject.question_number_text,
                        TargetLanguageCode: queryParams.TargetLanguageCode,
                    },
                });
                if (translatedObject) {
                    formObject.question_text = translatedObject.TargetLanguageText;
                }
            }
            if (caseDetails) {
                formObject.case_title = caseDetails.case_title;
                formObject.county = caseDetails.county;
                formObject.client_name = clinetDetails.name;
                formObject.state = caseDetails.state;
                formObject.federal = caseDetails.federal;
                formObject.federal_district = caseDetails.federal_district;
            }
            forms[i].client_signature_exist = client_signature_exist;
            forms[i] = formObject;
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(forms),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the forms'}),
        };
    }
};


const saveClientAnswerByFormId = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.queryStringParameters || {};
        const validateInput = input[0];
        const otp_code = params.otp_code;
        Object.assign(validateInput, {otp_code: otp_code});
        validateClientQuestionAnswering(validateInput);

        const case_id = validateInput.case_id;
        const practice_id = validateInput.practice_id;
        const document_type = validateInput.document_type;
        const legalforms_id = validateInput.legalforms_id;


        if (input[0].party_id) {
            let res = await saveOtherpartyAnswerByFormId(event);
            return res;
        }
        const client_id = validateInput.client_id;
        const {Forms, Users, OtherPartiesForms, Clients, Formotp, sequelize, Op} = await connectToDatabase();

        const otpDetails = await Formotp.findOne({
            where: {otp_code, legalforms_id, document_type, case_id},
        });

        /*Here we get no of pending Questions count before update the Forms*/
        const QuestionsCountBeforeFormsUpdate = await Forms.count({
            where: {
                case_id: case_id,
                legalforms_id: legalforms_id,
                practice_id: practice_id,
                document_type: document_type,
                client_response_status: {[Op.in]: ['SentToClient']},
            }
        });

        for (let q = 0; q < input.length; q += 1) {
            const formsQuestion = input[q];
            const client_response_text = formsQuestion.client_response_text;

            let updateAtDate = new Date();
            const updateColumn = {
                client_response_text: client_response_text,
                client_response_status: formsQuestion.client_response_status,
                last_updated_by_client: updateAtDate,
                TargetLanguageCode: formsQuestion.TargetLanguageCode || '',
                is_the_client_response_edited: formsQuestion.is_the_client_response_edited,
                uploaded_documents: formsQuestion.uploaded_documents,
                client_modified_response: formsQuestion.client_modified_response
            }
            const updateQuery = {id: formsQuestion.id, case_id, client_id, practice_id, legalforms_id, document_type};

            await Forms.update(updateColumn, {where: updateQuery});
            otpDetails.latest_client_response_date = new Date();
            await otpDetails.save();
        }

        if (otpDetails && otpDetails.sent_by) {
            /* Here we get no of pending questions count after update the forms */
            const pendingQuestionsAfterFormsUpdate = await Forms.count({
                where: { legalforms_id, case_id, document_type, client_response_status: 'SentToClient'}
            });
            if (pendingQuestionsAfterFormsUpdate === 0) {
                otpDetails.is_client_answered_all_questions = true;
                await otpDetails.save();

                const userDetails = await Users.findOne({
                    where: { id: otpDetails.sent_by, is_deleted: {[Op.not]: true}}
                });
                const ClientDetails = await Clients.findOne({where: {id: client_id}});
                if (userDetails && ClientDetails) {
                    let docType = '';
                    if (validateInput.question_type == 'InitialDisclosure' && validateInput.document_type == 'FROGS') {
                        docType = 'Initial Disclosure';
                    } else if (validateInput.question_type == 'FamilyLawDisclosure' && validateInput.document_type == 'FROGS') {
                        docType = 'FamilyLaw Disclosure';
                    } else {
                        docType = otpDetails.document_type.toUpperCase();
                    }

                    if (QuestionsCountBeforeFormsUpdate != 0 && pendingQuestionsAfterFormsUpdate == 0) {
                        let template = 'Hi';
                        if(userDetails?.name) template = template+' '+userDetails.name;
                        await sendEmail(userDetails.email, `Response available from ${ClientDetails.name}`, `
                        ${template},  <br/><br/>
            
                        Your client, ${ClientDetails.name}, has responded to ${docType} questions. <br/><br/>
            
                        Notification from EsquireTek`, 'EsquireTek');
                    }
                }
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'Answer Updated successful'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};


const saveClientAnswerByFormIdold = async (event) => {
    try {
        const parseEvent = event;
        const inputBody = JSON.parse(event.body);
        const case_id = inputBody[0].case_id;
        const practice_id = inputBody[0].practice_id;
        const client_id = inputBody[0].client_id;
        const document_type = inputBody[0].document_type;
        const otp_code = parseEvent.queryStringParameters.otp_code;
        const legalforms_id = inputBody[0].legalforms_id;
        let isClientResponseIsEdited = undefined;

        let form_id = [];
        const {Forms, Users, OtherPartiesForms, Clients, Formotp, sequelize, Op} = await connectToDatabase();

        const form = await Forms.findOne({where: {id: inputBody[0].id, legalforms_id: inputBody[0].legalforms_id}});
        const otherPartyform = await OtherPartiesForms.findOne({where: {id: inputBody[0].id,}});

        if (!form && !otherPartyform) throw new HTTPError(404, 'question was not found');

        /* if(otherPartyform) {
            let res = await saveOtherpartyAnswerByFormId(event);
            return res;
        } */

        const otpDetails = await Formotp.findOne({
            where: {otp_code: otp_code, legalforms_id: legalforms_id,},
        });

        const pendingQuestionCount = await Forms.count({
            where: {
                case_id: case_id,
                legalforms_id: legalforms_id,
                practice_id: practice_id,
                document_type: document_type,
                client_response_status: {[Op.in]: ['SentToClient']},
            }
        });

        for (let q = 0; q < inputBody.length; q += 1) {
            let input = inputBody[q];
            let text = input.client_response_text;
            validateClientQuestionAnswering(input);
            if (!legalforms_id) throw new HTTPError(404, `LegalForm ID was not Found in Question Number ${q + 1}.`);
            if (!otp_code) throw new HTTPError(404, 'OTP Code was not Found');

            let stringId = '"' + input.id + '"';
            form_id.push(stringId);
            const formsUpdate = {
                client_response_text: text,
                client_response_status: input.client_response_status,
                last_updated_by_client: new Date(),
                TargetLanguageCode: input.TargetLanguageCode || '',
                is_the_client_response_edited: input.is_the_client_response_edited,
                uploaded_documents: input.uploaded_documents,
                client_modified_response: input.client_modified_response
            }
            await Forms.update(formsUpdate, {
                where: {
                    id: input.id,
                    case_id,
                    legalforms_id,
                    document_type,
                    practice_id,
                    client_id: input.client_id
                }
            });
            otpDetails.latest_client_response_date = new Date();
            await otpDetails.save();
        }


        if (otpDetails && otpDetails.sent_by) {
            const pendingQuestions = await Forms.count({
                where: {
                    legalforms_id: legalforms_id,
                    case_id: case_id,
                    document_type: document_type,
                    client_response_status: 'SentToClient'
                },
            });
            if (pendingQuestions === 0) {
                const userDetails = await Users.findOne({
                    where: {
                        id: otpDetails.sent_by,
                        is_deleted: {[Op.not]: true}
                    },
                });
                const ClientDetails = await Clients.findOne({
                    where: {id: client_id},
                });
                if (userDetails && ClientDetails) {
                    let docType = '';
                    if (form.question_type == 'InitialDisclosure' && form.document_type == 'FROGS') {
                        docType = 'Initial Disclosure';
                    } else if (form.question_type == 'FamilyLawDisclosure' && form.document_type == 'FROGS') {
                        docType = 'FamilyLaw Disclosure';
                    } else {
                        docType = otpDetails.document_type.toUpperCase();
                    }

                    if (pendingQuestionCount != 0 && pendingQuestions == 0) {
                        let template = 'Hi';
                        if(userDetails?.name) template = template+' '+userDetails.name;
                        await sendEmail(userDetails.email, `Response available from ${` ${ClientDetails.name}`}`, `
                        ${template},  <br/><br/>
            
                        Your client, ${ClientDetails.name}, has responded to ${docType} questions. <br/><br/>
            
                        Notification from EsquireTek`, 'EsquireTek');
                    }

                    //await Formotp.destroy({where: { otp_code: otp_code,legalforms_id: legalforms_id, }});
                }
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'Answer Updated successful'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};

const saveOtherpartyAnswerByFormId = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.queryStringParameters || {};
        const validateInput = input[0];
        const otp_code = params.otp_code;
        Object.assign(validateInput, {otp_code: otp_code});
        validateClientQuestionAnswering(validateInput);

        const case_id = validateInput.case_id;
        const practice_id = validateInput.practice_id;
        const document_type = validateInput.document_type;
        const legalforms_id = validateInput.legalforms_id;

        const party_id = validateInput.party_id;
        const form_id = validateInput.form_id;
        const {OtherPartiesFormsOtp, Users, OtherPartiesForms, OtherParties, Op} = await connectToDatabase();

        const otpDetails = await OtherPartiesFormsOtp.findOne({
            where: {otp_code, legalforms_id, document_type, case_id},
        });

        /*Here we get no of pending Questions count before update the Forms*/
        const QuestionsCountBeforeFormsUpdate = await OtherPartiesForms.count({
            where: {
                case_id,
                legalforms_id,
                practice_id,
                document_type,
                party_id,
                client_response_status: {[Op.in]: ['SentToClient']}
            }
        });

        for (let q = 0; q < input.length; q += 1) {
            const formsQuestion = input[q];
            const client_response_text = formsQuestion.client_response_text;
            let updateAtDate = new Date();
            const updateColumn = {
                client_response_text: client_response_text,
                client_response_status: formsQuestion.client_response_status,
                last_updated_by_client: updateAtDate,
                TargetLanguageCode: formsQuestion.TargetLanguageCode || '',
                is_the_client_response_edited: formsQuestion.is_the_client_response_edited,
                uploaded_documents: formsQuestion.uploaded_documents,
                client_modified_response: formsQuestion.client_modified_response
            }
            const updateQuery = {
                id: formsQuestion.id,
                case_id,
                party_id,
                practice_id,
                legalforms_id,
                document_type,
                form_id: formsQuestion.form_id
            };

            await OtherPartiesForms.update(updateColumn, {where: updateQuery});
            otpDetails.latest_client_response_date = new Date();

            await otpDetails.save();
        }

        if (otpDetails && otpDetails.sent_by) {
            /* Here we get no of pending questions count after update the forms */
            const pendingQuestionsAfterFormsUpdate = await OtherPartiesForms.count({
                where: {
                    legalforms_id,
                    party_id,
                    document_type,
                    client_response_status: 'SentToClient'
                }
            });
            if (pendingQuestionsAfterFormsUpdate === 0) {
                otpDetails.is_client_answered_all_questions = true;
                await otpDetails.save();

                const userDetails = await Users.findOne({
                    where: {
                        id: otpDetails.sent_by,
                        is_deleted: {[Op.not]: true}
                    }
                });
                const ClientDetails = await OtherParties.findOne({where: {id: party_id}});
                if (userDetails && ClientDetails) {
                    let docType = '';
                    if (validateInput.question_type == 'InitialDisclosure' && validateInput.document_type == 'FROGS') {
                        docType = 'Initial Disclosure';
                    } else if (validateInput.question_type == 'FamilyLawDisclosure' && validateInput.document_type == 'FROGS') {
                        docType = 'FamilyLaw Disclosure';
                    } else {
                        docType = otpDetails.document_type.toUpperCase();
                    }

                    if (QuestionsCountBeforeFormsUpdate != 0 && pendingQuestionsAfterFormsUpdate == 0) {
                        let template = 'Hi';
                        if(userDetails?.name) template = template+' '+userDetails.name;
                        await sendEmail(userDetails.email, `Response available from ${` ${ClientDetails.name}`}`, `
                        ${template},  <br/><br/>
            
                        Your client, ${ClientDetails.name}, has responded to ${docType} questions. <br/><br/>
            
                        Notification from EsquireTek`, 'EsquireTek');
                    }
                }
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'Answer Updated successful'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
}

const saveOtherpartyAnswerByFormIdold = async (event) => {
    try {
        const parseEvent = event;
        const inputBody = JSON.parse(event.body);
        const case_id = inputBody[0].case_id;
        const practice_id = inputBody[0].practice_id;
        const client_id = inputBody[0].client_id;
        const document_type = inputBody[0].document_type;
        const otp_code = parseEvent.queryStringParameters.otp_code;
        const legalforms_id = inputBody[0].legalforms_id;
        const party_id = inputBody[0].party_id;
        let otherPartiesForms_id = [];
        let isClientResponseIsEdited = undefined;

        const {OtherPartiesForms, Users, OtherParties, OtherPartiesFormsOtp, sequelize, Op} = await connectToDatabase();
        const singleQuestion = inputBody[0];

        const pendingQuestionCount = await OtherPartiesForms.count({
            where: {
                case_id: case_id,
                legalforms_id: legalforms_id,
                practice_id: practice_id,
                document_type: document_type,
                client_response_status: {[Op.in]: ['SentToClient']},
            }
        });

        const form = await OtherPartiesForms.findOne({where: {id: singleQuestion.id}});
        for (let q = 0; q < inputBody.length; q += 1) {
            let input = inputBody[q];
            let text = input.client_response_text;
            // console.log(input);
            validateClientQuestionAnswering(input);
            const otherpartiesFormObj = await OtherPartiesForms.findOne({where: {id: input.id}});
            console.log(input.id);
            if (!otherpartiesFormObj) throw new HTTPError(404, 'question was not found');
            if (!event.queryStringParameters.otp_code) throw new HTTPError(404, 'OTP Code was not Found');
            let stringId = '"' + input.id + '"';
            otherPartiesForms_id.push(stringId);
            const formsUpdate = {
                client_response_text: text,
                client_response_status: input.client_response_status,
                last_updated_by_client: new Date(),
                TargetLanguageCode: input.TargetLanguageCode || '',
                is_the_client_response_edited: input.is_the_client_response_edited,
                uploaded_documents: input.uploaded_documents,
                client_modified_response: input.client_modified_response
            }
            const updated = await OtherPartiesForms.update(formsUpdate, {where: {id: input.id}});
        }
        const otpTableDetails = await OtherPartiesFormsOtp.findOne({where: {otp_code: otp_code}, raw: true});
        const otpDetails = otpTableDetails;

        if (otpDetails && otpDetails.sent_by) {
            const pendingQuestions = await OtherPartiesForms.count({
                where: {
                    case_id: otpDetails.case_id,
                    legalforms_id: otpDetails.legalforms_id,
                    document_type: otpDetails.document_type,
                    client_response_status: 'SentToClient'
                }
            });

            if (pendingQuestions === 0) {
                const userDetails = await Users.findOne({
                    where: {
                        id: otpDetails.sent_by,
                        is_deleted: {[Op.not]: true}
                    }
                });
                const ClientDetails = await OtherParties.findOne({where: {id: party_id}});

                if (userDetails && ClientDetails) {
                    let docType = '';
                    if (form.question_type == 'InitialDisclosure' && form.document_type == 'FROGS') {
                        docType = 'Initial Disclosure';
                    } else if (form.question_type == 'FamilyLawDisclosure' && form.document_type == 'FROGS') {
                        docType = 'FamilyLaw Disclosure';
                    } else {
                        docType = otpDetails.document_type.toUpperCase();
                    }

                    if (pendingQuestionCount != 0 && pendingQuestions == 0) {
                        let template = 'Hi';
                        if(userDetails?.name) template = template+' '+userDetails.name;
                        await sendEmail(userDetails.email, `Response available from ${` ${ClientDetails.name}`}`, `
            ${template},  <br/><br/>

            Your recipient, ${ClientDetails.name}, has responded to ${docType} questions. <br/><br/>

            Notification from EsquireTek`, 'EsquireTek');
                        //await OtherPartiesFormsOtp.destroy({where: { otp_code: otp_code }});
                    }
                }
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: 'Answer Updated successful'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
}

const getUploadURL = async (input) => {
    let fileExtention = '';
    if (input.file_name) {
        fileExtention = `.${input.file_name.split('.').pop()}`;
    }

    const s3Params = {
        Bucket: 'esquiretek-public-assets',
        Key: `${input.file_name}.${uuid.v4()}.${fileExtention}`,
        ContentType: input.content_type,
        ACL: 'public-read',
    };

    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
            public_url: `https://${s3Params.Bucket}.s3.amazonaws.com/${s3Params.Key}`,
        });
    });
};

const userUploadFile = async (event) => {
    try {
        const input = event.body;
        const uploadURLObject = await getUploadURL(input);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                uploadURL: uploadURLObject.uploadURL,
                s3_file_key: uploadURLObject.s3_file_key,
                public_url: uploadURLObject.public_url,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not upload the legalForms.'}),
        };
    }
};
const sendPreviousSetQuestions = async (event) => {
    try {
        const newEvent = event;
        const body = event.body;
        if (!body.case_id) throw new HTTPError(404, 'Case id was Not Found');
        if (!body.client_id) throw new HTTPError(404, 'Client Id  was Not Found');
        if (!body.legalforms_id) throw new HTTPError(404, 'LegalForm id was Not Found');
        if (!body.document_type) throw new HTTPError(404, 'Document type was Not Found');
        const {Formotp, OtherPartiesFormsOtp, Forms, Op, sequelize, LegalForms} = await connectToDatabase();

        const Formotps = await Formotp.findOne({
            where: {
                case_id: body.case_id,
                legalforms_id: body.legalforms_id,
                client_id: body.client_id,
                document_type: body.document_type,
            },
            order: [
                ['createdAt', 'DESC']
            ],
            raw: true
        });

        const FormsDetails = await Forms.findOne({
            where: {
                case_id: body.case_id,
                legalforms_id: body.legalforms_id,
                client_id: body.client_id,
                document_type: body.document_type,
            },
            order: [
                ['updatedAt', 'DESC']
            ],
            raw: true
        });
        const newBody = {};
        newBody.case_id = body.case_id;
        newBody.client_id = body.client_id;
        newBody.legalforms_id = body.legalforms_id;
        newBody.document_type = body.document_type;
        newBody.reference_id = body.client_id;
        if (FormsDetails.TargetLanguageCode == null) {
            newBody.TargetLanguageCode = 'en'
        } else {
            newBody.TargetLanguageCode = FormsDetails.TargetLanguageCode;
        }

        newBody.party_ids = [];
        if (Formotps.sending_type == 'selected_questions') {
            newBody.sending_type = "selected_questions";
            newBody.ids = JSON.parse(Formotps.question_ids);
        } else if (Formotps.sending_type == 'all') {
            newBody.sending_type = "all";
        }

        newEvent.body = '';
        newEvent.body = JSON.stringify(newBody);
        console.log(newBody);
        /* await sendLastSetQuestionsToClientEmail(newEvent); */
        await sendQuestionsToClient(newEvent);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({message: "Questions sent to client"}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};
const sendLastSetQuestionsToClientEmail = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateSentQuestionToClient(input);
        const practice_id = event.user.practice_id;
        const sendingType = input.sending_type;
        const legalforms_id = input.legalforms_id;
        const ids = input.ids || [];
        const {Op, Forms, Formotp, Clients, Practices} = await connectToDatabase();

        if (!legalforms_id) throw new HTTPError(404, 'LegalForm id was not found Not Found');
        if (input.client_id == false && input.party_ids.length == 0) throw new HTTPError(404, 'Client and Parties Details Not Found');

        if (input.client_id != false) {
            const updateQuery = {
                practice_id,
            };
            const updateColumn = {};
            if (sendingType === 'selected_questions') {
                if (!ids.length) throw new HTTPError(404, 'question was not found');
                updateQuery.id = {[Op.in]: ids};
                updateColumn.client_response_status = 'SentToClient';
                updateColumn.file_upload_status = false;
                updateColumn.TargetLanguageCode = input.TargetLanguageCode;
            } else {
                updateQuery.case_id = input.case_id;
                updateQuery.document_type = input.document_type;
                updateColumn.file_upload_status = true;
                updateColumn.client_response_status = 'SentToClient';
                updateColumn.TargetLanguageCode = input.TargetLanguageCode;
            }

            updateQuery.client_response_status = {[Op.ne]: 'ClientResponseAvailable'};
            updateQuery.legalforms_id = legalforms_id;
            const otpCode = generateRandomString(8);
            const otpSecret = generateRandomString(4);

            await Forms.update(updateColumn, {where: updateQuery});

            if (input.document_ids) {
                const file_upload_ids = input.document_ids;
                if (file_upload_ids.length != 0 && sendingType === 'selected_questions') {
                    await Forms.update({file_upload_status: true}, {where: {id: {[Op.in]: file_upload_ids}}});
                }
            }
            let languageCode = '';
            if (!input.TargetLanguageCode) {
                languageCode = 'en';
            } else {
                languageCode = input.TargetLanguageCode;
            }
            const otdDataObject = {
                id: uuid.v4(),
                otp_code: otpCode,
                otp_secret: otpSecret,
                client_id: input.client_id,
                document_type: input.document_type,
                case_id: input.case_id,
                legalforms_id: legalforms_id,
                sending_type: input.sending_type,
                question_ids: JSON.stringify(input.ids),
                sent_by: event.user.id,
                scheduler_email: true,
                TargetLanguageCode: languageCode
            };

            const formotpModelObject = await Formotp.create(otdDataObject);

            const ClientDetails = await Clients.findOne({
                where: {id: input.client_id},
            });

            let practiceName = '';
            const practiceDetails = await Practices.findOne({where: {id: ClientDetails.practice_id}});
            if (practiceDetails) {
                practiceName = practiceDetails.name;
            }

            let questionsUrl = `${process.env.CARE_URL}/questionnaire/${formotpModelObject.otp_code}`;
            let alreadyHadParameter = false;
            if (input.TargetLanguageCode) {
                questionsUrl = `${questionsUrl}?TargetLanguageCode=${input.TargetLanguageCode}`;
                alreadyHadParameter = true;
            }

            if (input.SendVerification) {
                if (alreadyHadParameter) {
                    questionsUrl += '&';
                } else {
                    questionsUrl += '?';
                }
                questionsUrl += 'SendVerification=true';
            }

            if (input.TargetLanguageCode && input.TargetLanguageCode != 'es' && input.TargetLanguageCode != 'vi') {
                if (ClientDetails && ClientDetails.email) {
                    await sendEmail(ClientDetails.email, 'Important: Response required for your lawsuit', `Your attorney is in need of your responses. Please click the link ${questionsUrl} to access the questionnaire.`, practiceName);
                }

                if (ClientDetails && ClientDetails.phone) {
                    const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                    await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Important: Response required for your lawsuit', `Your attorney is in need of your responses. Please click the link ${questionsUrl} and enter the verification code ${formotpModelObject.otp_secret} to access the questionnaire.`);
                }
            } else if (input.TargetLanguageCode && input.TargetLanguageCode == 'es') {
                if (ClientDetails && ClientDetails.email) {
                    await sendEmail(ClientDetails.email, 'Importante: Se requiere respuesta para su demanda', `Su abogado necesita sus respuestas. Por favor, haga clic en el enlace ${questionsUrl} para acceder al cuestionario.`, practiceName);
                }

                if (ClientDetails && ClientDetails.phone) {
                    const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                    await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Importante: Se requiere respuesta para su demanda', `Su abogado necesita sus respuestas. Haga clic en el enlace ${questionsUrl} e introduzca el código de verificación ${formotpModelObject.otp_secret} para acceder al cuestionario.`);
                }
            } else if (input.TargetLanguageCode && input.TargetLanguageCode == 'vi') {
                if (ClientDetails && ClientDetails.email) {
                    await sendEmail(ClientDetails.email, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', `Luật sư của bạn đang cần phản hồi của bạn. Vui lòng nhấp vào liên kết ${questionsUrl} để truy cập bảng câu hỏi.`, practiceName);
                }

                if (ClientDetails && ClientDetails.phone) {
                    const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                    await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', `Luật sư của bạn đang cần phản hồi của bạn. Vui lòng nhấp vào liên kết ${questionsUrl} và nhập mã xác minh ${formotpModelObject.otp_secret} để truy cập bảng câu hỏi.`);
                }
            }
        }
        return "success";
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
};

const readTxtFile = () => {
    return new Promise(function (resolve, reject) {
        const s3BucketParams = {
            Bucket: process.env.S3_BUCKET_FOR_TRANSLATIONQUESTIONS,
            Key: "translation_questions.json"
        };
        s3.getObject(s3BucketParams, function (err, data) {
            if (err) {
                reject(err.message);
            } else {
                var data = Buffer.from(data.Body).toString('utf8');
                resolve(data);
            }
        });
    });
}

const readVietnameseTextFile = (filename) => {
    return new Promise(function (resolve, reject) {
        const s3BucketParams = {
            Bucket: process.env.S3_BUCKET_FOR_VIETNAMESE,
            Key: filename
        };
        s3.getObject(s3BucketParams, function (err, data) {
            if (err) {
                reject(err.message);
            } else {
                var data = Buffer.from(data.Body).toString('utf8');
                resolve(data);
            }
        });
    });
}
/* Consultation's */
const saveNewConsultation = async (event) => {
    try {
        const {Forms, Op, LegalForms, OtherPartiesForms, sequelize} = await connectToDatabase();
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

        console.log(input[0]);
        const case_id = input[0].case_id;
        const practice_id = input[0].practice_id;
        const legalforms_id = input[0].legalforms_id;
        const document_type = input[0].document_type;
        let is_ClientForms = false;
        let is_OtherPartiesForms = false;
        let client_id = input[0].client_id;
        let party_id = input[0].party_id;
        if (client_id) {
            is_ClientForms = true;
        }
        if (party_id) {
            is_OtherPartiesForms = true;
        }


        const legalfromsObj = await LegalForms.findOne({where: {id: legalforms_id}, logging: console.log, raw: true});
        client_id = legalfromsObj.client_id;


        for (let i = 0; i < input.length; i++) {
            let consultaionQuestions = input[i];
            let consultaionObj = Object.assign(consultaionQuestions, {
                id: id || uuid.v4(),
                consultation_createdby: event.user.id,
                client_id: client_id
            });
            let createdFormDetails = await Forms.create(consultaionObj);
            let createdFormDetailsPlainText = createdFormDetails.get({plain: true});

            let otherPartiesFormsObj = await OtherPartiesForms.findAll({
                where: {
                    case_id: case_id,
                    practice_id: practice_id,
                    legalforms_id: legalforms_id,
                    document_type: document_type
                },
                attributes: [
                    [sequelize.fn('DISTINCT', sequelize.col('party_id')), 'party_id'],
                ],
                logging: console.log
                , raw: true
            });

            for (let n = 0; n < otherPartiesFormsObj.length; n++) {
                let otherPartiesConsultaionObj = Object.assign(consultaionQuestions,
                    {
                        id: id || uuid.v4(),
                        party_id: otherPartiesFormsObj[n].party_id,
                        form_id: createdFormDetailsPlainText.id,
                        consultation_createdby: otherPartiesFormsObj[n].party_id
                    });
                await OtherPartiesForms.create(otherPartiesConsultaionObj);
            }
        }

        let formsobject;
        if (is_ClientForms) {
            formsobject = await Forms.findAll({
                where: {case_id, practice_id, client_id, legalforms_id, document_type},
                raw: true
            });
        }
        if (is_OtherPartiesForms) {
            let sql = "SELECT OtherPartiesForms.*, Forms.lawyer_response_text AS lawyer_response_text, " +
                "Forms.lawyer_response_status AS lawyer_response_status, Forms.last_updated_by_lawyer AS last_updated_by_lawyer " +
                "FROM OtherPartiesForms INNER JOIN " +
                "Forms ON OtherPartiesForms.form_id = Forms.id " +
                "WHERE OtherPartiesForms.case_id = " + "'" + case_id + "'" + " AND " +
                "OtherPartiesForms.practice_id =  " + "'" + practice_id + "'" + " AND OtherPartiesForms.party_id = " + "'" + party_id + "'" + " AND  " +
                "OtherPartiesForms.legalforms_id = " + "'" + legalforms_id + "'" + " AND OtherPartiesForms.document_type = " + "'" + document_type + "'";

            console.log(sql);

            formsobject = await sequelize.query(sql, {
                type: QueryTypes.SELECT
            });
        }

        console.log(formsobject);


        const compare = (x1, x2) => {
            let a = x1.question_number_text;
            let b = x2.question_number_text;
            let section_id_a = parseInt(x1.question_section_id);
            let section_id_b = parseInt(x2.question_section_id);
            if (a === b) {
                if (section_id_a < section_id_b) {
                    return -1;
                }
                if (section_id_a > section_id_b) {
                    return 1;
                }

                return 0
            }
            ;
            const aArr = a.split("."), bArr = b.split(".");
            for (let i = 0; i < Math.min(aArr.length, bArr.length); i++) {
                if (parseInt(aArr[i]) < parseInt(bArr[i])) {
                    return -1
                }
                ;
                if (parseInt(aArr[i]) > parseInt(bArr[i])) {
                    return 1
                }
                ;
            }
            if (aArr.length < bArr.length) {
                return -1
            }
            ;
            if (aArr.length > bArr.length) {
                return 1
            }
            ;
            return 0;
        };
        formsobject.sort(compare);
        formsobject.map(obj => {
            obj.defendant_practice_details = legalfromsObj.defendant_practice_details;
        });

        let finalQuestions;
        if (document_type.toUpperCase() === 'FROGS') {
            const maxConsultationNo = await Forms.findAll({
                where: {legalforms_id, case_id, document_type, practice_id, is_consultation_set: true},
                attributes: [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']]
            });
            formsobject.map(obj => {
                obj.total_consultation_count = maxConsultationNo[0].consultation_set_no;
            });
            finalQuestions = await groupByFormsConsultation(formsobject);
        } else {
            finalQuestions = formsobject;
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(finalQuestions),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not Create Consultaion set.'}),
        };
    }
}
const saveNewConsultationCreatedByClient = async (event) => {
    try {
        const {
            Forms,
            Op,
            LegalForms,
            Formotp,
            OtherPartiesFormsOtp,
            OtherPartiesForms,
            sequelize
        } = await connectToDatabase();
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

        const case_id = input[0].case_id;

        const practice_id = input[0].practice_id;
        const legalforms_id = input[0].legalforms_id;
        const document_type = input[0].document_type;
        const otpCode = input[0].otpCode;
        const new_consultation_no = input[0].consultation_set_no;


        let is_ClientForms = false;
        let is_OtherPartiesForms = false;
        let client_id = input[0].client_id;
        let party_id = input[0].party_id;

        if (client_id) {
            is_ClientForms = true;
        }
        if (party_id) {
            is_OtherPartiesForms = true;
        }

        const legalfromsObj = await LegalForms.findOne({where: {id: legalforms_id}, raw: true});
        const otherPartiesFormsPartyID = await OtherPartiesForms.findAll({
            where: {
                case_id: case_id,
                practice_id: practice_id,
                legalforms_id: legalforms_id,
                document_type: document_type
            },
            attributes: [
                [sequelize.fn('DISTINCT', sequelize.col('party_id')), 'party_id'],
            ],
            raw: true
        }); // 3 

        client_id = legalfromsObj.client_id;

        let question_ids_arr; //array or string


        let otpDetails = await Formotp.findOne({where: {otp_code: otpCode}, raw: true});
        let otherPartyotpDetails = await OtherPartiesFormsOtp.findOne({where: {otp_code: otpCode}, raw: true});

        if (!otpDetails && !otherPartyotpDetails) throw new HTTPError(404, "Invalid OTP Code");

        if (otpDetails && otpDetails.sending_type == 'selected_questions') {
            question_ids_arr = JSON.parse(otpDetails.question_ids);

        }

        if (otherPartyotpDetails && otherPartyotpDetails.sending_type == 'selected_questions') {
            question_ids_arr = JSON.parse(otherPartyotpDetails.question_id);

        }

        let client_response_status = '';
        let file_upload_status = '';
        let TargetLanguageCode = '';

        /* Here Questions are creating */
        for (let i = 0; i < input.length; i++)  //4
        {
            let consultaionQuestions = input[i]; // 1
            let newFormObj;


            if (otpDetails) {
                if (otpDetails.sending_type == 'selected_questions') {
                    file_upload_status = false;
                } else {
                    file_upload_status = true;
                }
                client_response_status = 'SentToClient';
                TargetLanguageCode = otpDetails.TargetLanguageCode;

                let consultaionObj = Object.assign(consultaionQuestions, {
                    id: id || uuid.v4(),
                    consultation_createdby: client_id,
                    client_response_status: client_response_status,
                    file_upload_status: file_upload_status,
                    TargetLanguageCode: TargetLanguageCode

                });
                console.log("*************");
                console.log(consultaionObj);
                newFormObj = await Forms.create(consultaionObj);

                typeof question_ids_arr === 'object' ? question_ids_arr.push(newFormObj.id) : question_ids_arr = undefined;

                if (otherPartiesFormsPartyID.length > 0) {

                    for (let n = 0; n < otherPartiesFormsPartyID.length; n++) {

                        let otherPartiesConsultaionObj = Object.assign(consultaionQuestions, {
                            id: id || uuid.v4(),
                            consultation_createdby: party_id,
                            party_id: otherPartiesFormsPartyID[n].party_id,
                            form_id: newFormObj.id,
                            client_response_text: '',
                            client_response_status: 'NotSetToClient'
                        });
                        console.log(otherPartiesConsultaionObj);
                        await OtherPartiesForms.create(otherPartiesConsultaionObj);
                    }

                }
            }
            if (otherPartyotpDetails) {

                let FormsConsultaionObj = Object.assign(consultaionQuestions, {
                    id: id || uuid.v4(),
                    consultation_createdby: legalfromsObj.client_id,
                    client_id: legalfromsObj.client_id,
                    client_response_status: 'NotSetToClient',
                    client_response_text: '',
                });

                newFormObj = await Forms.create(FormsConsultaionObj);

                if (otherPartyotpDetails.sending_type == 'selected_questions') {
                    file_upload_status = false;
                } else {
                    file_upload_status = true;
                }
                client_response_status = 'SentToClient'; // flag 1
                TargetLanguageCode = otherPartyotpDetails.TargetLanguageCode;

                for (let n = 0; n < otherPartiesFormsPartyID.length; n++) {
                    let consultaionObj = Object.assign(consultaionQuestions, {
                        id: id || uuid.v4(),
                        consultation_createdby: party_id,
                        form_id: newFormObj.id,
                        client_response_status: client_response_status,
                        file_upload_status: file_upload_status,
                        TargetLanguageCode: TargetLanguageCode,
                        party_id: otherPartiesFormsPartyID[n].party_id,
                    });
                    let otherPartiesFormsObj = await OtherPartiesForms.create(consultaionObj);
                    if (party_id == otherPartiesFormsPartyID[n].party_id) {
                        typeof question_ids_arr === 'object' ? question_ids_arr.push(otherPartiesFormsObj.id) : question_ids_arr = undefined;
                    }
                }
                // const getOtherPartyConsultationQuestion = await OtherPartiesForms.findAll({
                //     where: {
                //         case_id, practice_id, legalforms_id, document_type, party_id
                //     }
                // });
            }
        }


        if (otpDetails && otpDetails.sending_type == 'selected_questions') {
            let updateColumn = {
                question_ids: JSON.stringify(question_ids_arr)
            }
            await Formotp.update(updateColumn, {
                where: {otp_code: otpCode, legalforms_id, document_type, case_id},
                logging: console.log
            });
        }
        if (otherPartyotpDetails && otherPartyotpDetails.sending_type == 'selected_questions') {
            let updateColumn = {
                question_id: JSON.stringify(question_ids_arr)
            }
            await OtherPartiesFormsOtp.update(updateColumn, {
                where: {
                    otp_code: otpCode,
                    legalforms_id,
                    document_type,
                    case_id
                }, logging: console.log
            });
        }


        let created_questions_set;
        if (client_id && is_ClientForms) {
            console.log('Inside client Deatails');
            created_questions_set = await Forms.findAll({
                where: {
                    case_id, practice_id, legalforms_id, document_type, client_id,
                    consultation_set_no: new_consultation_no
                }, order: [
                    ['question_section', 'ASC'],
                ], raw: true, logging: console.log
            });

            const maxConsultationNo = await Forms.findAll({
                where: {
                    case_id, practice_id, legalforms_id, document_type, client_id,
                    is_consultation_set: true
                },
                attributes: [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']]
            });
            created_questions_set.map(obj => {
                obj.total_consultation_count = maxConsultationNo[0].consultation_set_no;
            });


        } else if (party_id && is_OtherPartiesForms) {
            console.log('Inside Other party Deatails')
            created_questions_set = await OtherPartiesForms.findAll({
                where: {
                    case_id, practice_id, legalforms_id, document_type, party_id,
                    consultation_set_no: new_consultation_no
                }, order: [
                    ['question_section', 'ASC'],
                ], raw: true, logging: console.log
            });


            const maxConsultationNo = await OtherPartiesForms.findAll({
                where: {
                    case_id, practice_id, legalforms_id, document_type, party_id,
                    is_consultation_set: true
                },
                attributes: [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']]
            });
            created_questions_set.map(obj => {
                obj.total_consultation_count = maxConsultationNo[0].consultation_set_no;
            });
        }

        if ((otpDetails && otpDetails.TargetLanguageCode == 'es') || (otherPartyotpDetails && otherPartyotpDetails.TargetLanguageCode == 'es')) {
            const fileResponse = await readTxtFile();
            const jsonData = JSON.parse(fileResponse);
            for (let i = 0; i < created_questions_set.length; i += 1) {
                for (const data of jsonData) {
                    if (data.question_number_text == created_questions_set[i].question_number_text && data.question_section_id == created_questions_set[i].question_section_id) {
                        created_questions_set[i].question_text = data.question_text;
                        created_questions_set[i].question_section_text = data.question_section_text;
                        created_questions_set[i].question_section = data.question_section;
                        break;
                    }
                }
            }
        } else if ((otpDetails && otpDetails.TargetLanguageCode == 'vi') || (otherPartyotpDetails && otherPartyotpDetails.TargetLanguageCode == 'vi')) {
            const findQuestion = created_questions_set[0].question_number_text;
            const vnFileName = findQuestionDisk(findQuestion);
            if (!vnFileName) throw new HTTPError(404, 'Cannot find Vietnamese Filename');
            const fileResponse = await readVietnameseTextFile(vnFileName);
            const jsonData = JSON.parse(fileResponse);
            for (let i = 0; i < created_questions_set.length; i += 1) {
                for (const data of jsonData) {
                    if (data.question_number_text == created_questions_set[i].question_number_text && data.question_section_id == created_questions_set[i].question_section_id) {
                        created_questions_set[i].question_text = data.question_text;
                        created_questions_set[i].question_section_text = data.question_section_text;
                        created_questions_set[i].question_section = data.question_section;
                        break;
                    }
                }
            }
        }

        // const formsDataByOtpCode = await getFormDataByOtp(obj);

        // return formsDataByOtpCode;
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(created_questions_set),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not Create Consultaion set.'}),
        };
    }
}
const removeConsultation = async (event) => {
    try {
        const {Forms, Op, LegalForms, OtherPartiesForms} = await connectToDatabase();
        let consultation_no = 2;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const case_id = input.case_id;
        const practice_id = input.practice_id;
        const legalforms_id = input.legalforms_id;
        const document_type = input.document_type;
        let is_question_exist_in_otherparties = false;

        //check forms

        const formObject = await Forms.findAll({
            where: {
                case_id,
                practice_id,
                legalforms_id,
                document_type,
                id: {[Op.in]: input.question_ids}
            }, raw: true
        });
        if (formObject.length > 0) {
            await Forms.destroy({
                where: {
                    case_id,
                    practice_id,
                    legalforms_id,
                    document_type,
                    id: {[Op.in]: input.question_ids}
                }
            });
            const checkQusExistinOtherpaty = await OtherPartiesForms.findAll({
                where: {
                    case_id,
                    practice_id,
                    legalforms_id,
                    document_type,
                    form_id: {[Op.in]: input.question_ids}
                }
            });
            if (checkQusExistinOtherpaty.length > 0) {
                is_question_exist_in_otherparties = true
                await OtherPartiesForms.destroy({
                    where: {
                        case_id,
                        practice_id,
                        legalforms_id,
                        document_type,
                        form_id: {[Op.in]: input.question_ids}
                    }
                });
            }
        } else {
            is_question_exist_in_otherparties = true;
            const otherpartyObj = await OtherPartiesForms.findAll({
                where: {
                    case_id,
                    practice_id,
                    legalforms_id,
                    document_type,
                    id: {[Op.in]: input.question_ids}
                }
            });
            let forms_id = [];
            for (let i = 0; i < otherpartyObj.length; i++) {
                let otherPartyRow = otherpartyObj[i];
                forms_id.push(otherPartyRow.form_id);
            }
            await OtherPartiesForms.destroy({
                where: {
                    case_id,
                    practice_id,
                    legalforms_id,
                    document_type,
                    form_id: {[Op.in]: forms_id}
                }
            });
            await Forms.destroy({where: {case_id, practice_id, legalforms_id, document_type, id: {[Op.in]: forms_id}}});
            is_question_exist_in_otherparties = true;
        }

        // After delete have to update consultation no

        const fetchAllConsultationData = await Forms.findAll({
            where: {
                case_id: case_id,
                practice_id: practice_id,
                legalforms_id: legalforms_id,
                document_type: document_type,
                is_consultation_set: true,
                consultation_set_no: {[Op.not]: 1}
            },
            attributes: [
                'consultation_set_no',],
            group: ['consultation_set_no'],
            order: [['consultation_set_no', 'ASC']],
            raw: true
        });


        for (let i = 0; i < fetchAllConsultationData.length; i++) {
            consultation_no += i;
            const consultationNo = fetchAllConsultationData[i].consultation_set_no;
            console.log(consultationNo);
            console.log(consultation_no);
            if (consultation_no != consultationNo) {
                const fetchDataPraticularConsultation = await Forms.findAll({
                    where: {
                        case_id: case_id,
                        practice_id: practice_id,
                        legalforms_id: legalforms_id,
                        document_type: document_type,
                        is_consultation_set: true,
                        consultation_set_no: consultationNo
                    }
                });
                for (let row = 0; row < fetchDataPraticularConsultation.length; row++) {
                    const formsRow = fetchDataPraticularConsultation[row];
                    let updateConsultationRow;
                    updateConsultationRow = await Forms.findOne({
                        where: {
                            id: formsRow.id,
                            case_id: formsRow.case_id,
                            practice_id: formsRow.practice_id,
                            legalforms_id: formsRow.legalforms_id,
                            document_type: formsRow.document_type,
                            is_consultation_set: formsRow.is_consultation_set,
                        },
                    });
                    if (updateConsultationRow) {
                        updateConsultationRow.consultation_set_no = consultation_no;
                        await updateConsultationRow.save();
                        //check otherparties have questions
                        const checkOtherpartiesForms = await OtherPartiesForms.findOne({
                            where: {
                                form_id: formsRow.id,
                                case_id: formsRow.case_id,
                                practice_id: formsRow.practice_id,
                                legalforms_id: formsRow.legalforms_id,
                                document_type: formsRow.document_type,
                                is_consultation_set: formsRow.is_consultation_set,
                                consultation_set_no: formsRow.consultation_set_no,
                            },
                        });
                        if (checkOtherpartiesForms) {
                            let updateColumn = {
                                consultation_set_no: consultation_no
                            };
                            let updateCondition = {
                                where: {
                                    form_id: formsRow.id,
                                    case_id: formsRow.case_id,
                                    practice_id: formsRow.practice_id,
                                    legalforms_id: formsRow.legalforms_id,
                                    document_type: formsRow.document_type,
                                    is_consultation_set: formsRow.is_consultation_set,
                                    consultation_set_no: formsRow.consultation_set_no,
                                }
                            };
                            console.log(updateCondition);
                            await OtherPartiesForms.update(updateColumn, updateCondition);
                        }
                    }
                }
            }
            consultation_no = 2;
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Consultaion Question removed successfully',
            }),
        };


    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not remove Consultaion set.'}),
        };
    }
};
const removeFromClientConsultation = async (event) => {
    try {
        const {Forms, Op, LegalForms, OtherPartiesForms, sequelize} = await connectToDatabase();

        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        let consultation_no = 2;
        console.log('inside function');
        const case_id = input.case_id;
        const practice_id = input.practice_id;
        const legalforms_id = input.legalforms_id;
        const document_type = input.document_type;
        let is_question_exist_in_otherparties = false;

        //check forms


        let client_id;
        let party_id;

        if (input.client_id) {
            client_id = input.client_id;
        }
        if (input.party_id) {
            party_id = input.party_id;
        }

        const formObject = await Forms.findAll({
            where: {
                case_id,
                practice_id,
                legalforms_id,
                document_type,
                id: {[Op.in]: input.question_ids}
            }, raw: true
        });
        if (formObject.length > 0) {
            await Forms.destroy({
                where: {
                    case_id,
                    practice_id,
                    legalforms_id,
                    document_type,
                    id: {[Op.in]: input.question_ids}
                }
            });
            const checkQusExistinOtherpaty = await OtherPartiesForms.findAll({
                where: {
                    case_id,
                    practice_id,
                    legalforms_id,
                    document_type,
                    form_id: {[Op.in]: input.question_ids}
                }
            });
            if (checkQusExistinOtherpaty.length > 0) {
                is_question_exist_in_otherparties = true
                await OtherPartiesForms.destroy({
                    where: {
                        case_id,
                        practice_id,
                        legalforms_id,
                        document_type,
                        form_id: {[Op.in]: input.question_ids}
                    }
                });
            }
        } else {
            is_question_exist_in_otherparties = true;
            const otherpartyObj = await OtherPartiesForms.findAll({
                where: {
                    case_id,
                    practice_id,
                    legalforms_id,
                    document_type,
                    id: {[Op.in]: input.question_ids}
                }
            });
            let forms_id = [];
            for (let i = 0; i < otherpartyObj.length; i++) {
                let otherPartyRow = otherpartyObj[i];
                forms_id.push(otherPartyRow.form_id);
            }
            await OtherPartiesForms.destroy({
                where: {
                    case_id,
                    practice_id,
                    legalforms_id,
                    document_type,
                    form_id: {[Op.in]: forms_id}
                }
            });
            await Forms.destroy({where: {case_id, practice_id, legalforms_id, document_type, id: {[Op.in]: forms_id}}});
            is_question_exist_in_otherparties = true;
        }

        // After delete have to update consultation no

        const fetchAllConsultationData = await Forms.findAll({
            where: {
                case_id: case_id,
                practice_id: practice_id,
                legalforms_id: legalforms_id,
                document_type: document_type,
                is_consultation_set: true,
                consultation_set_no: {[Op.not]: 1}
            },
            attributes: [
                'consultation_set_no',],
            group: ['consultation_set_no'],
            order: [['consultation_set_no', 'ASC']],
            raw: true
        });


        for (let i = 0; i < fetchAllConsultationData.length; i++) {
            consultation_no += i; // 2
            const consultationNo = fetchAllConsultationData[i].consultation_set_no;
            if (consultation_no != consultationNo) {
                const fetchDataPraticularConsultation = await Forms.findAll({
                    where: {
                        case_id: case_id,
                        practice_id: practice_id,
                        legalforms_id: legalforms_id,
                        document_type: document_type,
                        is_consultation_set: true,
                        consultation_set_no: consultationNo
                    }
                });
                for (let row = 0; row < fetchDataPraticularConsultation.length; row++) {
                    const formsRow = fetchDataPraticularConsultation[row];
                    let updateConsultationRow;
                    updateConsultationRow = await Forms.findOne({
                        where: {
                            id: formsRow.id,
                            case_id: formsRow.case_id,
                            practice_id: formsRow.practice_id,
                            legalforms_id: formsRow.legalforms_id,
                            document_type: formsRow.document_type,
                            is_consultation_set: formsRow.is_consultation_set,
                        },
                    });
                    if (updateConsultationRow) {
                        updateConsultationRow.consultation_set_no = consultation_no;
                        await updateConsultationRow.save();
                        //check otherparties have questions
                        const checkOtherpartiesForms = await OtherPartiesForms.findOne({
                            where: {
                                form_id: formsRow.id,
                                case_id: formsRow.case_id,
                                practice_id: formsRow.practice_id,
                                legalforms_id: formsRow.legalforms_id,
                                document_type: formsRow.document_type,
                                is_consultation_set: formsRow.is_consultation_set,
                                consultation_set_no: formsRow.consultation_set_no,
                            },
                        });
                        if (checkOtherpartiesForms) {
                            let updateColumn = {
                                consultation_set_no: consultation_no
                            };
                            let updateCondition = {
                                where: {
                                    form_id: formsRow.id,
                                    case_id: formsRow.case_id,
                                    practice_id: formsRow.practice_id,
                                    legalforms_id: formsRow.legalforms_id,
                                    document_type: formsRow.document_type,
                                    is_consultation_set: formsRow.is_consultation_set,
                                    consultation_set_no: formsRow.consultation_set_no,
                                }
                            };
                            await OtherPartiesForms.update(updateColumn, updateCondition);
                        }
                    }
                }
            }
            consultation_no = 2;
        }

        /* ===== */

        let new_consultation_set;
        if (client_id) {
            new_consultation_set = await Forms.findAll({
                where: {
                    case_id, practice_id, legalforms_id, document_type, client_id,
                    is_consultation_set: true
                }, order: [['consultation_set_no', 'ASC'], ['question_section', 'ASC']],
                logging: console.log,
                raw: true
            });

            const maxConsultationNo = await Forms.findAll({
                where: {
                    case_id, practice_id, legalforms_id, document_type, client_id,
                    is_consultation_set: true
                },
                attributes: [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']]
            });
            new_consultation_set.map(obj => {
                obj.total_consultation_count = maxConsultationNo[0].consultation_set_no;
            });


        } else if (party_id) {
            new_consultation_set = await OtherPartiesForms.findAll({
                where: {
                    case_id, practice_id, legalforms_id, document_type, party_id,
                    is_consultation_set: true
                }, order: [['consultation_set_no', 'ASC'], ['question_section', 'ASC']],
                logging: console.log, raw: true
            });

            const maxConsultationNo = await Forms.findAll({
                where: {
                    case_id, practice_id, legalforms_id, document_type, party_id,
                    is_consultation_set: true
                },
                attributes: [[sequelize.fn('max', sequelize.col('consultation_set_no')), 'consultation_set_no']]
            });
            new_consultation_set.map(obj => {
                obj.total_consultation_count = maxConsultationNo[0].consultation_set_no;
            });
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(new_consultation_set),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not remove Consultaion set.'}),
        };
    }
};

const groupByFormsConsultation = async (formsobject) => {
    try {
        const {Forms, Op, OtherPartiesForms} = await connectToDatabase();
        let questionNumberGroupData = lodash.groupBy(formsobject, "question_number_text");
        let finalQuestions = [];
        Object.keys(questionNumberGroupData).map((key) => {
            let val = questionNumberGroupData[key];
            if (val && val[0] && val[0][`consultation_set_no`]) {
                let res = lodash.groupBy(val, "consultation_set_no");
                Object.keys(res).map((k) => {
                    finalQuestions.push(...res[k]);
                });
            } else {
                finalQuestions.push(...val);
            }
        });
        return finalQuestions;
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could Group Consultaion set.'}),
        };
    }
}
const sendQuestionsEmailAndSMSTemplate = async (otpDetails) => {
    try {
        const {Op, Formotp, OtherPartiesFormsOtp, OtherParties, Clients, Practices,} = await connectToDatabase();
        let ClientDetails;
        let otpResponse;
        if (otpDetails.client_id) {
            otpResponse = await Formotp.create(otpDetails);
            ClientDetails = await Clients.findOne({
                where: {
                    id: otpDetails.client_id,
                    practice_id: otpDetails.practice_id
                }, logging: console.log
            });
            if (!otpResponse) throw new HTTPError(400, `cannot create send Question to client`);
        } else {
            otpResponse = await OtherPartiesFormsOtp.create(otpDetails);
            ClientDetails = await OtherParties.findOne({
                where: {
                    id: otpDetails.party_id,
                    practice_id: otpDetails.practice_id
                }, logging: console.log
            });
            if (!otpResponse) throw new HTTPError(400, `cannot create send Question to other parties`);
        }

        let questionsUrl = `${process.env.CARE_URL}/questionnaire/${otpDetails.otp_code}`;
        if (otpDetails.TargetLanguageCode) {
            questionsUrl = `${questionsUrl}?TargetLanguageCode=${otpDetails.TargetLanguageCode}`;
        }

        const practiceName = otpDetails.practice_name;


        let message_body = '';
        let smstext_body = '';
        if (otpDetails.custom_message && otpDetails.TargetLanguageCode != 'es' && otpDetails.TargetLanguageCode != 'vi') {
            message_body = otpDetails.custom_message + '\n' + 'Please click the link to access the questionnaire.' + '<br/>' + questionsUrl;
            smstext_body = otpDetails.custom_message + '\n' + 'Please click the link and enter the verification code ' + otpDetails.otp_secret + ' to access the questionnaire. ' + questionsUrl;
        } else if (otpDetails.custom_message && otpDetails.TargetLanguageCode == 'es') {
            message_body = otpDetails.custom_message + '\n' + 'Haga clic en el enlace para acceder al cuestionario.' + '<br/>' + questionsUrl;
            smstext_body = otpDetails.custom_message + '\n' + 'Haga clic en el enlace e ingrese el código de verificación ' + otpDetails.otp_secret + ' para acceder al cuestionario. ' + questionsUrl;
        } else if (otpDetails.custom_message && otpDetails.TargetLanguageCode == 'vi') {
            message_body = otpDetails.custom_message + '\n' + 'Vui lòng nhấp vào liên kết để truy cập bảng câu hỏi.' + '<br/>' + questionsUrl;
            smstext_body = otpDetails.custom_message + '\n' + 'Vui lòng nhấp vào liên kết và nhập mã xác minh ' + otpDetails.otp_secret + ' để truy cập bảng câu hỏi. ' + questionsUrl;
        } else if (!otpDetails.custom_message && otpDetails.TargetLanguageCode && otpDetails.TargetLanguageCode != 'es' && otpDetails.TargetLanguageCode != 'vi') {
            message_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link to access the questionnaire.' + '<br/>' + questionsUrl;
            smstext_body = 'Your Lawyers at ' + practiceName + ' are requesting your responses. Please click the link and enter the verification code ' + otpDetails.otp_secret + ' to access the questionnaire. ' + questionsUrl;
        } else if (!otpDetails.custom_message && otpDetails.TargetLanguageCode == 'es') {
            message_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace para acceder al cuestionario.' + '<br/>' + questionsUrl;
            smstext_body = 'Sus abogados de ' + practiceName + ' solicitan sus respuestas. Haga clic en el enlace e ingrese el código de verificación ' + otpDetails.otp_secret + ' para acceder al cuestionario. ' + questionsUrl;
        } else if (!otpDetails.custom_message && otpDetails.TargetLanguageCode == 'vi') {
            message_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết để truy cập bảng câu hỏi.' + '<br/>' + questionsUrl;
            smstext_body = 'Các Luật sư của bạn tại ' + practiceName + ' đang yêu cầu phản hồi của bạn. Vui lòng nhấp vào liên kết và nhập mã xác minh ' + otpDetails.otp_secret + ' để truy cập bảng câu hỏi. ' + questionsUrl;
        }

        if (otpDetails.TargetLanguageCode && otpDetails.TargetLanguageCode != 'es' && otpDetails.TargetLanguageCode != 'vi') {
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Important: Response required for your lawsuit', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Important: Response required for your lawsuit', smstext_body);
            }
        } else if (otpDetails.TargetLanguageCode == 'es') {
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Importante: Se requiere respuesta para su demanda', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Importante: Se requiere respuesta para su demanda', smstext_body);
            }
        } else if (otpDetails.TargetLanguageCode == 'vi') {
            if (ClientDetails && ClientDetails.email) {
                await sendEmail(ClientDetails.email, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', message_body, practiceName);
            }

            if (ClientDetails && ClientDetails.phone) {
                const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Quan trọng: Cần có phản hồi cho vụ kiện của bạn', smstext_body);
            }
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not send the Questions.'}),
        };
    }
}
/* Consultation's end. */
const updateClientResponseEditStatus = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {Forms, OtherPartiesForms, Op} = await connectToDatabase();

        const forms = await Forms.findOne({
            where: {
                id: input.id,
                case_id: input.case_id,
                legalforms_id: input.legalforms_id,
                practice_id: input.practice_id
            }
        });
        if (forms) {
            forms.is_the_client_response_edited = false
            await forms.save();
        }
        const otherPartiesFormsObj = await OtherPartiesForms.findOne({
            where: {
                id: input.id,
                case_id: input.case_id,
                legalforms_id: input.legalforms_id,
                practice_id: input.practice_id
            }
        });
        if (otherPartiesFormsObj) {
            otherPartiesFormsObj.is_the_client_response_edited = false
            await otherPartiesFormsObj.save();
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({status: 'ok', message: 'Clinet edited response status updated successfully.'}),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Clinet edited response.'}),
        };
    }
}
const lawyerResponseAutoSave = async (event) => {
    try {
        const input = event.body;
        let is_questions_from_clients_forms = false;
        let is_questions_from_otherParties_forms = false;
        const {Forms, OtherPartiesForms, Op, FormsResponseHistory, Practices, Cases, Settings} = await connectToDatabase();

        for (let i = 0; i < input.length; i++) {
            const formsrow = input[i];
            const form_id = formsrow?.client_id ? formsrow?.id : formsrow?.form_id;
            const { case_id, practice_id, legalforms_id, document_type, question_number_text, question_id } = formsrow;

            if (input[i].client_id) {
                is_questions_from_clients_forms = true;
            } else {
                is_questions_from_otherParties_forms = true;
            }
            const [formsObject, formsResponseHistoryObj,practiceObj,casesObj] = await Promise.all([
                Forms.findOne({
                    where: {
                        id: form_id, case_id, practice_id, legalforms_id, document_type,
                        question_id, question_number_text,
                    }
                }),
                FormsResponseHistory.findOne({ where: { form_id, legalforms_id, document_type, case_id, practice_id } }),
                Practices.findOne({ where: { id: event.user.practice_id }, raw: true }),
                Cases.findOne({where:{practice_id: event.user.practice_id,id:case_id}, raw: true})
            ]);

            if (is_questions_from_otherParties_forms) {
                await OtherPartiesForms.update({ subgroup: formsrow.subgroup }, {
                    where: {
                        id: formsrow.id, form_id: formsrow.form_id, case_id, practice_id, legalforms_id, document_type,
                        question_id: formsrow.question_id, question_number_text: formsrow.question_number_text,
                    }
                });
            }

            formsObject.lawyer_response_text = formsrow.lawyer_response_text;
            formsObject.lawyer_response_status = formsrow.lawyer_response_status;
            formsObject.last_updated_by_lawyer = new Date();
            formsObject.subgroup = formsrow.subgroup;
            let saveForms = await formsObject.save();

            if(practiceObj?.global_attorney_response_tracking && casesObj?.attorney_response_tracking){
                if(formsResponseHistoryObj){
                    const settingsObj = await Settings.findOne({where:{key:'global_settings'},raw:true});
                    const settings_value = JSON.parse(settingsObj.value);
                    let response_history_limit;
                    let responseHistory = JSON.parse(formsResponseHistoryObj.response);
                    let version_no = responseHistory[responseHistory.length-1].version;
                    response_history_limit = settings_value?.response_history_limit ? settings_value.response_history_limit : 100;
                    responseHistory = checkResponseHistoryCount(responseHistory,response_history_limit);

                    responseHistory.push({
                        version: version_no + 1,
                        response: formsrow.lawyer_response_text,
                        user_id: event.user.id,
                        timestamp: new Date(),
                        color_code: input.color_code,
                        status: 'auto_save'
                    })
                    formsResponseHistoryObj.response = JSON.stringify(responseHistory);
                    await formsResponseHistoryObj.save();
                } else {
                    const responseObj = [{
                        version: 1,
                        response: formsrow.lawyer_response_text,
                        user_id: event.user.id,
                        timestamp: new Date(),
                        color_code: input.color_code,
                        status: 'auto_save'
                    }];
                    let responseHistoryObj = {
                        id: uuid.v4(),
                        practice_id,
                        form_id,
                        legalforms_id,
                        document_type,
                        case_id,
                        response: JSON.stringify(responseObj)
                    }
                    await FormsResponseHistory.create(responseHistoryObj)
                }
            }


            input[i].lawyer_response_text = saveForms.lawyer_response_text;
            input[i].lawyer_response_status = saveForms.lawyer_response_status;
            input[i].last_updated_by_lawyer = saveForms.last_updated_by_lawyer;
            input[i].subgroup = saveForms.subgroup;
            let lawyer_response = await getResponseHistory(practice_id, case_id, form_id, legalforms_id, FormsResponseHistory);
            input[i].responseHistory = lawyer_response;

        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(input),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Forms.'}),
        };
    }
}

module.exports.create = middy(create).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = middy(getAll).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getFormDataByCaseidAndDocumentType = middy(getFormDataByCaseidAndDocumentType).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOtherPartyFormDataByCaseidAndDocumentType = middy(getOtherPartyFormDataByCaseidAndDocumentType).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.UpdateFormDataByCaseidAndDocumentType = middy(UpdateFormDataByCaseidAndDocumentType).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.sendQuestionsToClient = middy(sendQuestionsToClient).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getFormDataByOtp = getFormDataByOtp;
module.exports.saveClientAnswerByFormId = saveClientAnswerByFormId;
module.exports.mailTest = mailTest;
module.exports.sendVerificationRequestToClient = sendVerificationRequestToClient;
module.exports.getLawyerResponseDataByOtp = getLawyerResponseDataByOtp;
module.exports.userUploadFile = userUploadFile;
module.exports.sendPreviousSetQuestions = sendPreviousSetQuestions;
module.exports.updateSubgroupStatus = updateSubgroupStatus;
module.exports.saveAlllawyerResponseByCaseidAndDocumentType = saveAlllawyerResponseByCaseidAndDocumentType;
module.exports.saveNewConsultation = saveNewConsultation;
module.exports.removeConsultation = removeConsultation;
module.exports.removeFromClientConsultation = removeFromClientConsultation;
module.exports.saveNewConsultationCreatedByClient = saveNewConsultationCreatedByClient;
module.exports.updateClientResponseEditStatus = updateClientResponseEditStatus;
module.exports.getFormDataByOtpWithUnansweredQuestion = getFormDataByOtpWithUnansweredQuestion;
module.exports.lawyerResponseAutoSave = lawyerResponseAutoSave;