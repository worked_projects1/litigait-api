module.exports = (sequelize, type) => sequelize.define('DocumentUploadProgress', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    case_id: type.STRING,
    client_id: type.STRING,
    legalforms_id: type.STRING,
    status: type.STRING, // pending, complete
    document_type: type.STRING, // FROGS, SPROGS, RFPD, RFA
    s3_file_key: type.STRING,
});
