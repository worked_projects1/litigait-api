const uuid = require('uuid');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');

const getStatus = async (event) => {
    try {
        const input = event.query;
        const {DocumentUploadProgress} = await connectToDatabase();
        const legalforms_id = input.legalforms_id;
        if (!legalforms_id) throw new HTTPError(404, `Legalforms id: ${legalforms_id} was not found`);
        const uploadProgressStatusObject = await DocumentUploadProgress.findOne({
            where: {
                case_id: input.case_id,
                document_type: input.document_type,
                legalforms_id: input.legalforms_id
            },
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({status: 'ok', data: uploadProgressStatusObject, input}),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the AdminObjections.'}),
        };
    }
};

const setStatus = async (event) => {
    try {
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {DocumentUploadProgress, LegalForms} = await connectToDatabase();
        const legalforms_id = input.legalforms_id;
        const status = input.status;
        if (!legalforms_id) throw new HTTPError(404, `Legalforms id: ${legalforms_id} was not found`);
        let uploadProgressStatusObject = await DocumentUploadProgress.findOne({
            where: {
                case_id: input.case_id,
                client_id: input.client_id,
                document_type: input.document_type,
                legalforms_id: input.legalforms_id
            },

        });
        if (status == 'cancelled' || status == 'Cancelled') {
            const legalFormsObj = await LegalForms.findOne({where: {id: legalforms_id}});
            if (!legalFormsObj) throw new HTTPError(404, `LegalForms with id: ${legalforms_id} was not found`);
            await legalFormsObj.destroy();
        }
        if (uploadProgressStatusObject) {
            const updatedModel = Object.assign(uploadProgressStatusObject, input);
            await updatedModel.save();
        } else {
            if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
            const dataObject = Object.assign(input, {id: id || uuid.v4()});
            uploadProgressStatusObject = await DocumentUploadProgress.create(dataObject);
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({status: 'ok', data: uploadProgressStatusObject}),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the AdminObjections.'}),
        };
    }
};


module.exports.setStatus = setStatus;
module.exports.getStatus = getStatus;