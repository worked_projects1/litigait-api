const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const connectToDatabase = require('../../db');
const {HTTPError, resInvalidPostDataWithInfo} = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const {QueryTypes} = require('sequelize');

const getAll = async (event) => {
    try {
        const {sequelize} = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;

        let codeSnip = '';
        let codeSnip2 = '';

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY Formotps.createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = `where Formotps.document_type LIKE '%${searchKey}%' OR Formotps.otp_code LIKE '%${searchKey}%' OR Formotps.sending_type LIKE '%${searchKey}%' OR Practices.name LIKE '${searchKey}' OR Cases.case_title LIKE '${searchKey}' OR Cases.case_number LIKE '${searchKey}' OR Clients.name LIKE '${searchKey}'`;
        }

        if (searchQuery != '' && sortQuery != '') {
            codeSnip = searchQuery + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = searchQuery + sortQuery;
        } else if (searchQuery == '' && sortQuery != '') {
            codeSnip = sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = sortQuery;
        } else if (searchQuery != '' && sortQuery == '') {
            codeSnip = searchQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery == '') {
            codeSnip = ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = '';
        }

        let sqlQuery = ' SELECT Formotps.id,Formotps.case_id,' +
            ' Formotps.client_id,Formotps.legalforms_id,Formotps.document_type,Formotps.otp_code,Formotps.otp_secret,Formotps.createdAt,Formotps.TargetLanguageCode,' +
            ' Formotps.sending_type,Practices.name,Cases.case_title,Clients.name as client_name,Cases.case_number from Formotps INNER JOIN Cases ON Formotps.case_id = Cases.id' +
            ' INNER JOIN Clients ON Formotps.client_id = Clients.id' +
            ' INNER JOIN Practices ON Cases.practice_id = Practices.id ' + codeSnip;

        let sqlQueryCount = ' SELECT Formotps.id,Formotps.case_id,' +
            ' Formotps.client_id,Formotps.legalforms_id,Formotps.document_type,Formotps.otp_code,Formotps.otp_secret,Formotps.createdAt,Formotps.TargetLanguageCode,' +
            ' Formotps.sending_type,Practices.name,Cases.case_title,Clients.name as client_name,Cases.case_number from Formotps INNER JOIN Cases ON Formotps.case_id = Cases.id' +
            ' INNER JOIN Clients ON Formotps.client_id = Clients.id' +
            ' INNER JOIN Practices ON Cases.practice_id = Practices.id ' + codeSnip2;

        console.log(sqlQuery);
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Form Otps.'}),
        };
    }
};

module.exports.getAll = getAll;