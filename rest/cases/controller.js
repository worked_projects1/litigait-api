const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const AWS = require('aws-sdk');

const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const { sendEmail } = require('../../utils/mailModule');
const { QueryTypes } = require('sequelize');
const s3 = new AWS.S3({ region: 'us-east-1' });
const {
    validateCreateCases,
    validateGetOneCase,
    validateUpdateCases,
    validateDeleteCase,
    validateReadCases,
    validateCreateLegalForms,
    validateCreateParties,
    validateCreatePartiesForms,
    validateCreateClients,
    validateDuplicateCaseClientCreate
} = require('./validation');
const { response } = require('express');

const create = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        console.log(input);
        input.practice_id = event.user.practice_id;
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        let objvalues = Object.assign(input, { id: id || uuid.v4() });
        const dataObject = Object.assign(objvalues,
            { opposing_counsel: JSON.stringify(objvalues.opposing_counsel) },
            { propounder_opposing_counsel: JSON.stringify(objvalues.propounder_opposing_counsel) });

        validateCreateCases(dataObject);
        const { Cases, Clients, Users, Op, Practices } = await connectToDatabase();
        const practices = await Practices.findOne({where: { id: event.user.practice_id }});
        if(practices?.global_attorney_response_tracking){ dataObject.attorney_response_tracking = practices.global_attorney_response_tracking };
        const cases = await Cases.create(dataObject);
        const plainCase = cases.get({ plain: true });
        if (plainCase.attorneys != null && plainCase.attorneys) {
            let attorneysIds = plainCase.attorneys.split(",");
            let attorneyName = [];
            for (let l = 0; l < attorneysIds.length; l++) {
                if (attorneysIds[l]) {
                    const UsersObj = await Users.findAll({
                        where: { id: attorneysIds[l], is_deleted: { [Op.not]: true } },
                        raw: true
                    });
                    if (UsersObj.length != 0) attorneyName.push(UsersObj[0].name);
                }
            }
            plainCase.attorney_name = attorneyName.toString();
        }
        const clientObject = await Clients.findOne({ where: { id: plainCase.client_id } });
        if (clientObject) {
            plainCase.client_name = clientObject.name;
        }
        if (plainCase.opposing_counsel) {
            plainCase.opposing_counsel = JSON.parse(plainCase.opposing_counsel);
        }
        if (plainCase.propounder_opposing_counsel) {
            plainCase.propounder_opposing_counsel = JSON.parse(plainCase.propounder_opposing_counsel);
        }
        plainCase.opposing_counsel_email = JSON.stringify(plainCase.opposing_counsel_email);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify(plainCase),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ error: err.message || 'Could not create the cases.' }),
        };
    }
};

const getOne = async (event) => {
    try {
        validateGetOneCase(event.pathParameters);
        const { Cases, Clients } = await connectToDatabase();
        const cases = await Cases.findOne({ where: { id: event.pathParameters.id } });
        if (!cases) throw new HTTPError(404, `Cases with id: ${event.pathParameters.id} was not found`);
        const clients = await Clients.findOne({ where: { id: cases.client_id } });
        if (!clients) throw new HTTPError(404, `Clients with id: ${cases.client_id} was not found`);
        cases.client_name = clients.name;
        cases.client_phone = clients.phone;
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify(cases),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Cases.' }),
        };
    }
};

// const getAll = async (event) => {
//     try {
//         const header = event.headers;
//         const query = event.queryStringParameters || {};
//         if (header.offset) query.offset = parseInt(header.offset, 10);
//         if (header.limit) query.limit = parseInt(header.limit, 10);
//         if (query.where) query.where = JSON.parse(query.where);
//         query.raw = true;
//         if (!query.where) {
//             query.where = {};
//         }
//         query.where.practice_id = event.user.practice_id;
//         validateReadCases(query);
//         query.order = [
//             ['createdAt', 'DESC'],
//         ];
//         const { Cases, Clients, Orders, Op, Users, LegalForms } = await connectToDatabase();
//         query.where.is_deleted = { [Op.not]: true };
//         const casess = await Cases.findAll(query);
//         const clientIds = [];
//         for (let i = 0; i < casess.length; i += 1) {
//             if (!clientIds.includes(casess[i].client_id)) {
//                 clientIds.push(casess[i].client_id);
//             }
//             if(casess[i].attorneys != null && casess[i].attorneys ){
//                 let attorneysIds = casess[i].attorneys.split(",");
//                 let attorneyName = [];
//                 for(let l=0;l<attorneysIds.length;l++){
//                     if(attorneysIds[l]){
//                          const UsersObj = await Users.findAll({where:{id:attorneysIds[l]},raw:true,is_deleted:{[Op.not]:true}});    
//                          if(UsersObj) attorneyName.push(UsersObj[0].name);
//                     }
//                 }
//                 casess[i].attorney_name = attorneyName.toString();
//             }
//         }
//         const clients = await Clients.findAll({
//             where: {
//                 id: clientIds,
//             },
//             raw: true,
//         });

//         const orders = await Orders.findAll({
//             where: {
//                 practice_id: event.user.practice_id,
//             },
//             attributes: ['case_id', 'amount_charged'],
//             raw: true,
//         });


//         for (let i = 0; i < casess.length; i += 1) {
//             casess[i].total_cost = 0;
//             for (let j = 0; j < clients.length; j += 1) {
//                 if (casess[i].client_id == clients[j].id) {
//                     casess[i].client_name = clients[j].name;
//                 }
//             }
//             for (let k = 0; k < orders.length; k += 1) {
//                 if (casess[i].id == orders[k].case_id) {
//                     casess[i].total_cost += orders[k].amount_charged;
//                 }
//             }

//             const LegalformsObj = await LegalForms.findAll({where:{case_id:casess[i].id,response_deadline_enddate:{[Op.ne]:null}},raw: true,logging:console.log});
//             console.log(LegalformsObj);
//             LegalformsObj.sort(function compare(a, b) {
//                 var dateA = new Date(a.response_deadline_enddate);
//                 var dateB = new Date(b.response_deadline_enddate);
//                 return dateA - dateB;
//               });
//               if (LegalformsObj.length>0){
//                 casess[i].due_date = LegalformsObj[0].response_deadline_enddate;
//               }else{
//                   casess[i].due_date = '';
//               }
//         }

//         return {
//             statusCode: 200,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': true,
//             },
//             body: JSON.stringify(casess),
//         };
//     } catch (err) {
//         console.log('Errror Cases:');
//         console.log(err);
//         return {
//             statusCode: err.statusCode || 500,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': true,
//             },
//             body: JSON.stringify({ error: err.message || 'Could not fetch the casess.' }),
//         };
//     }
// };

/*list all new start*/
const getAll = async (event) => {
    try {
        const { sequelize, LegalForms, Op, Users, Cases, Clients } = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let filterQuery = '';
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;
        if (!query.offset && !query.limit) {
            let CasesGetAll = getAllCasesWithoutHeader(event);
            return CasesGetAll;
        }
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let practice_id = event.user.practice_id;
        if (!practice_id) throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
        let codeSnip = '';
        let codeSnip2 = '';
        let where = `WHERE Cases.is_deleted IS NOT true AND Cases.practice_id = '${practice_id}' AND Cases.is_archived IS NOT true`;
        /**Filter**/
        if (query.filter != 'false') {
            query.filter = JSON.parse(query.filter);
            filterKey.type = query.filter.attorneys;
            if (filterKey.type != 'all' && filterKey.type != '' && filterKey.type) {
                filterQuery += ` AND Cases.attorneys LIKE '%${filterKey.type}%'`;
            }
        }
        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY Cases.createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            /* searchQuery = ` Clients.name LIKE '%${searchKey}%' OR Cases.case_number LIKE '%${searchKey}%' OR Cases.case_title LIKE '%${searchKey}%' OR Cases.createdAt LIKE '%${searchKey}%' OR Cases.date_of_loss LIKE '%${searchKey}%' OR Users.name LIKE '%${searchKey}%'`; */
            searchQuery = ` Clients.name LIKE '%${searchKey}%' OR Cases.case_number LIKE '%${searchKey}%' OR Cases.case_title LIKE '%${searchKey}%' OR Cases.createdAt LIKE '%${searchKey}%' OR Cases.date_of_loss LIKE '%${searchKey}%' OR Cases.state LIKE '%${searchKey}%' `;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where + filterQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 = where;
        }


        let sqlQuery = 'SELECT Cases.id,Cases.case_number,Cases.case_title,Cases.claim_number,Cases.case_plaintiff_name, Cases.case_defendant_name,Cases.integration_case_id,' +
            'Cases.client_id,Cases.date_of_loss,Cases.matter_id,Cases.practice_id,Cases.state,Cases.attorneys,Cases.createdAt,Clients.name as client_name,Clients.phone as client_phone, ' +
            // '(SELECT SUM(amount_charged) amount_charged FROM Orders WHERE case_id = Cases.id ) AS total_cost, ' +
            '(SELECT MIN(response_deadline_enddate) FROM LegalForms WHERE case_id = Cases.id AND date(response_deadline_enddate) >= date(now())) AS due_date ' +
            'FROM (Clients INNER JOIN Cases ON Clients.id = Cases.client_id) ' + codeSnip;

        let totalCaseCount = await Cases.count({ where: { practice_id: event.user.practice_id, is_deleted: { [Op.not]: true }, is_archived: { [Op.not]: true } }, logging: console.log });

        // let sqlQueryCount = 'SELECT Cases.id,Cases.case_number,Cases.case_title,Cases.claim_number,Cases.case_plaintiff_name, Cases.case_defendant_name,Cases.filevine_project_id,Cases.filevine_case_created,Cases.mycase_case_id,Cases.mycase_case_created,'+
        //     'Cases.client_id,Cases.date_of_loss,Cases.matter_id,Cases.practice_id,Cases.state,Cases.attorneys,Cases.createdAt,Clients.name as client_name,Clients.phone as client_phone, ' +
        //     '(SELECT MIN(response_deadline_enddate) FROM LegalForms WHERE case_id = Cases.id AND date(response_deadline_enddate) >= date(now())) AS due_date ' +
        //     'FROM (Clients INNER JOIN Cases ON Clients.id = Cases.client_id) ' + codeSnip2;
        /* let sqlQuery = 'SELECT Cases.*,Clients.name AS client_name,Users.name AS attorney_name,(SELECT SUM(amount_charged) amount_charged FROM Orders Where case_id = Cases.id ) AS total_cost FROM Cases '+
            'INNER JOIN Clients ON  Cases.client_id = Clients.id '+
            'LEFT JOIN Users ON Cases.attorneys = Users.id '+ codeSnip ;
        console.log(sqlQuery);
        let sqlQueryCount = 'SELECT Cases.*,Clients.name AS client_name,Users.name AS attorney_name,(SELECT SUM(amount_charged) amount_charged FROM Orders Where case_id = Cases.id ) AS total_cost FROM Cases '+
            'INNER JOIN Clients ON  Cases.client_id = Clients.id '+
            'LEFT JOIN Users ON Cases.attorneys = Users.id '+ codeSnip2 ; */
        // console.log(sqlQuery);
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        // const TableDataCount = await sequelize.query(sqlQueryCount, {
        //     type: QueryTypes.SELECT
        // });

        for (let i = 0; i < serverData.length; i += 1) {
            if (serverData[i].attorneys != null && serverData[i].attorneys) {
                let attorneysIds = serverData[i].attorneys.split(",");
                let attorneyName = [];
                for (let l = 0; l < attorneysIds.length; l++) {
                    if (attorneysIds[l]) {
                        const UsersObj = await Users.findAll({
                            where: { id: attorneysIds[l] },
                            raw: true,
                            is_deleted: { [Op.not]: true }
                        });
                        if (UsersObj[0]?.name) attorneyName.push(UsersObj[0].name);
                    }
                }
                serverData[i].attorney_name = attorneyName.toString();
            } else {
                serverData[i].attorney_name = '';
            }
            // serverData[i].opposing_counsel = JSON.parse(serverData[i].opposing_counsel);
            // serverData[i].propounder_opposing_counsel = JSON.parse(serverData[i].propounder_opposing_counsel);
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': totalCaseCount
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
}
const getAllCasesWithoutHeader = async (event) => {
    try {
        const { sequelize, LegalForms, Op, Users } = await connectToDatabase();

        let practice_id = event.user.practice_id;
        if (!practice_id) throw new HTTPError(404, `Practice id: ${practice_id} was not found`);

        let where = `WHERE Cases.is_deleted IS NOT true AND Cases.practice_id = '${practice_id}' ORDER BY Cases.createdAt DESC`;

        let sqlQueryCount = 'SELECT Cases.*,Clients.name as client_name,Clients.phone as client_phone, ' +
            '(SELECT SUM(amount_charged) amount_charged FROM Orders WHERE case_id = Cases.id ) AS total_cost, ' +
            '(SELECT MIN(response_deadline_enddate) FROM LegalForms WHERE case_id = Cases.id AND date(response_deadline_enddate) >= date(now())) AS due_date ' +
            'FROM (Clients INNER JOIN Cases ON Clients.id = Cases.client_id) ' + where;

        const serverData = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });
        for (let i = 0; i < serverData.length; i += 1) {
            if (serverData[i].attorneys != null && serverData[i].attorneys) {
                let attorneysIds = serverData[i].attorneys.split(",");
                let attorneyName = [];
                for (let l = 0; l < attorneysIds.length; l++) {
                    if (attorneysIds[l]) {
                        const UsersObj = await Users.findAll({
                            where: { id: attorneysIds[l] },
                            raw: true,
                            is_deleted: { [Op.not]: true }
                        });
                        if (UsersObj[0]?.name) attorneyName.push(UsersObj[0].name);
                    }
                }
                serverData[i].attorney_name = attorneyName.toString();
            } else {
                serverData[i].attorney_name = '';
            }
            serverData[i].opposing_counsel = JSON.parse(serverData[i].opposing_counsel);
            serverData[i].propounder_opposing_counsel = JSON.parse(serverData[i].propounder_opposing_counsel);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the orders.' }),
        };
    }
}
/* list all optimize end */
const update = async (event) => {
    try {
        const input = JSON.parse(event.body);

        validateUpdateCases(input);
        const { Cases, Users, Op } = await connectToDatabase();
        const cases = await Cases.findOne({ where: { id: event.pathParameters.id } });
        if (!cases) throw new HTTPError(404, `Cases with id: ${event.pathParameters.id} was not found`);
        if(input?.integration_case_data) input.integration_case_data = JSON.stringify(input.integration_case_data);
        const caseDeatail = Object.assign(cases, input);
        const updatedModel = Object.assign(caseDeatail,
            { opposing_counsel: JSON.stringify(caseDeatail.opposing_counsel) },
            { propounder_opposing_counsel: JSON.stringify(caseDeatail.propounder_opposing_counsel) });
        await updatedModel.save();
        const plainCase = await updatedModel.get({ plain: true });
        if (plainCase.attorneys != null && plainCase.attorneys) {
            let attorneysIds = plainCase.attorneys.split(",");
            let attorneyName = [];
            for (let l = 0; l < attorneysIds.length; l++) {
                if (attorneysIds[l]) {
                    const UsersObj = await Users.findAll({
                        where: { id: attorneysIds[l], is_deleted: { [Op.not]: true } },
                        raw: true
                    });
                    if (UsersObj.length != 0) attorneyName.push(UsersObj[0].name);
                }
            }
            plainCase.attorney_name = attorneyName.toString();
        }
        if (plainCase.opposing_counsel) {
            plainCase.opposing_counsel = JSON.parse(plainCase.opposing_counsel);
        }
        if (plainCase.propounder_opposing_counsel) {
            plainCase.propounder_opposing_counsel = JSON.parse(plainCase.propounder_opposing_counsel);
        }
        if(plainCase.integration_case_data) plainCase.integration_case_data = JSON.parse(plainCase.integration_case_data);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify(plainCase),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Cases.' }),
        };
    }
};

const destroy = async (event) => {
    try {
        validateDeleteCase(event.pathParameters);
        const { Cases, Forms, LegalForms, OtherParties, OtherPartiesForms } = await connectToDatabase();
        const caseDetails = await Cases.findOne({ where: { id: event.pathParameters.id } });
        if (!caseDetails) throw new HTTPError(400, `Cases with id: ${event.pathParameters.id} was not found`);
        await Forms.destroy({ where: { case_id: caseDetails.id } });
        await LegalForms.destroy({ where: { case_id: caseDetails.id } });
        await OtherParties.destroy({ where: { case_id: caseDetails.id } });
        await OtherPartiesForms.destroy({ where: { case_id: caseDetails.id } });
        // await Orders.destroy({ where: { case_id: caseDetails.id } });
        // await Orders.update({ is_deleted: true }, { where: { case_id: caseDetails.id } });
        caseDetails.is_deleted = true;
        await caseDetails.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify(caseDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ error: err.message || 'Could destroy fetch the Cases.' }),
        };
    }
};

const getFormDates = async (case_id, document_type, legalforms_id) => {
    const { Op, Forms } = await connectToDatabase();
    let questionsUploadedDate = null;
    let questionSentToClientDate = null;
    let responsesByClientDate = null;
    let responsesFinalizedDate = null;

    const firstQuestionUploaded = await Forms.findOne({
        where: { case_id, document_type, legalforms_id },
        order: [
            ['createdAt', 'ASC'],
        ],
    });
    if (firstQuestionUploaded) {
        questionsUploadedDate = new Date(firstQuestionUploaded.createdAt);
    }

    const lastQuestionSentToClient = await Forms.findOne({
        where: {
            case_id,
            document_type,
            legalforms_id,
            client_response_status: { [Op.in]: ['SentToClient', 'ClientResponseAvailable'] }
        },
        order: [
            ['last_sent_to_client', 'DESC'],
        ],
    });
    if (lastQuestionSentToClient) {
        questionSentToClientDate = new Date(lastQuestionSentToClient.updatedAt);
    }

    const lastResponseByClient = await Forms.findOne({
        where: { case_id, document_type, legalforms_id, client_response_status: 'ClientResponseAvailable' },
        order: [
            ['last_updated_by_client', 'DESC'],
        ],
    });
    if (lastResponseByClient) {
        responsesByClientDate = new Date(lastResponseByClient.updatedAt);
    }

    const lastResponeFinalized = await Forms.findOne({
        where: { case_id, document_type, legalforms_id, lawyer_response_status: 'Final' },
        order: [
            ['last_updated_by_client', 'DESC'],
        ],
    });
    if (lastResponeFinalized) {
        responsesFinalizedDate = new Date(lastResponeFinalized.updatedAt);
    }

    return {
        questionsUploadedDate,
        questionSentToClientDate,
        responsesByClientDate,
        responsesFinalizedDate,
    };
};

const getDocumentUploadStatus = async (case_id, document_type) => {
    const { DocumentUploadProgress } = await connectToDatabase();
    const documentProgressObject = await DocumentUploadProgress.findOne({
        where: { case_id, document_type },
    });
    if (documentProgressObject) {
        return documentProgressObject.status;
    }
    return '';
};

const getClientSignature = async (case_id, document_type, legalforms_id) => {
    AWS.config.update({ region: process.env.S3_CLIENT_SIGNATURE_BUCKET_REGION });
    const { ClientSignature } = await connectToDatabase();
    const ClientSignatureObject = await ClientSignature.findOne({
        where: { case_id, document_type, legalforms_id },
        raw: true,
    });
    if (ClientSignatureObject) {
        const s3BucketParams = { Bucket: process.env.CLIENT_SIGNATURE_BUCKET, Expires: 60 * 60 * 1 };
        if (ClientSignatureObject.client_signature_s3_key) {
            s3BucketParams.Key = ClientSignatureObject.client_signature_s3_key;
            ClientSignatureObject.client_signature = await s3.getSignedUrlPromise('getObject', s3BucketParams);
        } else {
            ClientSignatureObject.client_signature = '';
        }
        if (ClientSignatureObject.client_verification_signature_s3_key) {
            s3BucketParams.Key = ClientSignatureObject.client_verification_signature_s3_key;
            ClientSignatureObject.client_verification_signature = await s3.getSignedUrlPromise('getObject', s3BucketParams);
        } else {
            ClientSignatureObject.client_verification_signature = '';
        }
        return ClientSignatureObject;
    }
    return {};
};


const getCaseDetails = async (event) => {
    const s3BucketParams = { Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS, Expires: 60 * 60 * 1 };
    try {
        if (!event.pathParameters && event.params) {
            event.pathParameters = event.params;
        }
        validateGetOneCase(event.pathParameters);
        const {
            Op,
            Forms,
            Cases,
            LegalForms,
            FeeTerms,
            HipaaTerms,
            Clients,
            MedicalHistory,
            MedicalHistorySummery,
            Practices
        } = await connectToDatabase();
        const cases = await Cases.findOne({ where: { id: event.pathParameters.id } });
        if (!cases) throw new HTTPError(404, `Cases with id: ${event.pathParameters.id} was not found`);
        const plainCase = cases.get({ plain: true });
        const practiceDetails = await Practices.findOne({
            where: {
                id: plainCase.practice_id,
            },
        });
        if (!plainCase.fee_terms) {
            const feetermsObject = await FeeTerms.findOne({
                where: {
                    practice_id: plainCase.practice_id,
                },
            });
            if (feetermsObject && feetermsObject.terms_text) {
                plainCase.fee_terms = feetermsObject.terms_text;
            }
        }
        plainCase.hipaa_terms = '';
        if (plainCase.practice_id) {
            const hipaatermsObject = await HipaaTerms.findOne({
                where: {
                    practice_id: plainCase.practice_id,
                },
            });
            if (hipaatermsObject) {
                plainCase.hipaa_terms = hipaatermsObject.terms_text;
            }
        }

        plainCase.phone = '';
        if (plainCase.client_id) {
            const clientDetails = await Clients.findOne({
                where: {
                    id: plainCase.client_id,
                },
            });
            if (clientDetails) {
                plainCase.phone = clientDetails.phone;
                plainCase.client_name = clientDetails.name;
            }
        }

        const MedicalHistorySummeryObject = await MedicalHistorySummery.findOne({
            where: {
                case_id: cases.id,
            },
        });

        const MedicalHistoryRecords = await MedicalHistory.findAll({
            where: { case_id: cases.id },
            order: [
                ['createdAt', 'DESC'],
            ],
            raw: true,
        });
        let pagesCount = 0;
        let documentStatus = 'No source documents';
        if (MedicalHistoryRecords.length) {
            documentStatus = 'Summary not requested';
        }
        let summery_url = '';
        if (MedicalHistorySummeryObject) {
            if (MedicalHistorySummeryObject.status == 'Complete') {
                documentStatus = 'Completed';
            } else {
                documentStatus = 'Work in progress';
            }
            summery_url = MedicalHistorySummeryObject.summery_url;
        }
        for (let i = 0; i < MedicalHistoryRecords.length; i += 1) {
            if (MedicalHistoryRecords[i].pages) {
                pagesCount += MedicalHistoryRecords[i].pages;
            }
            AWS.config.update({ region: process.env.S3_BUCKET_FOR_PRIVATE_ASSETS_REGION });
            MedicalHistoryRecords.secure_public_url = await s3.getSignedUrlPromise('getObject', {
                Bucket: process.env.S3_BUCKET_FOR_PRIVATE_ASSETS,
                Expires: 60 * 60 * 1,
                Key: MedicalHistoryRecords[i].s3_file_key,
            });
        }

        const legalFormsObject = await LegalForms.findAll({
            where: {
                practice_id: event.user.practice_id,
                case_id: cases.id,
                client_id: plainCase.client_id,
            },
            raw: true,
        });


        const FrogsArr = [];
        const SprogsArr = [];
        const RfpdArr = [];
        const RfaArr = [];
        /* For Start */

        for (let index = 0; index < legalFormsObject.length; index++) {
            const legalformid = legalFormsObject[index].id;
            const checkLegaformIsExist = await Forms.count({ where: { case_id: cases.id, legalforms_id: legalformid } });

            // FROGS, SPROGS, RFPD, RFA
            const frogsQuestionsCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    document_type: 'FROGS',
                    legalforms_id: legalformid
                }
            });
            const frogsQuestionsSentToClientCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'FROGS',
                    client_response_status: { [Op.in]: ['SentToClient', 'ClientResponseAvailable'] }
                }
            });

            const frogsQuestionsRespondedByClientCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'FROGS',
                    client_response_status: 'ClientResponseAvailable'
                }
            });
            const frogsQuestionsResponsesFinalizedCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'FROGS',
                    lawyer_response_status: 'Final'
                }
            });
            const sprogsQuestionsCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'SPROGS'
                }
            });
            const sprogsQuestionsSentToClientCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'SPROGS',
                    client_response_status: { [Op.in]: ['SentToClient', 'ClientResponseAvailable'] }
                }
            });
            const sprogsQuestionsRespondedByClientCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'SPROGS',
                    client_response_status: 'ClientResponseAvailable'
                }
            });
            const sprogsQuestionsResponsesFinalizedCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'SPROGS',
                    lawyer_response_status: 'Final'
                }
            });
            const rfpdQuestionsCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'RFPD'
                }
            });
            const rfpdQuestionsSentToClientCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'RFPD',
                    client_response_status: { [Op.in]: ['SentToClient', 'ClientResponseAvailable'] }
                }
            });
            const rfpdQuestionsRespondedByClientCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'RFPD',
                    client_response_status: 'ClientResponseAvailable'
                }
            });
            const rfpdQuestionsResponsesFinalizedCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'RFPD',
                    lawyer_response_status: 'Final'
                }
            });
            const rfaQuestionsCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'RFA'
                }
            });
            const rfaQuestionsSentToClientCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'RFA',
                    client_response_status: { [Op.in]: ['SentToClient', 'ClientResponseAvailable'] }
                }
            });
            const rfaQuestionsRespondedByClientCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'RFA',
                    client_response_status: 'ClientResponseAvailable'
                }
            });
            const rfaQuestionsResponsesFinalizedCount = await Forms.count({
                where: {
                    case_id: cases.id,
                    legalforms_id: legalformid,
                    document_type: 'RFA',
                    lawyer_response_status: 'Final'
                }
            });


            const frogsFormDates = await getFormDates(cases.id, 'FROGS', legalformid);
            const sprogsFormDates = await getFormDates(cases.id, 'SPROGS', legalformid);
            const rfpdFormDates = await getFormDates(cases.id, 'RFPD', legalformid);
            const rfaFormDates = await getFormDates(cases.id, 'RFA', legalformid);


            let generatedFrogsDocument = null;
            let generatedSprogsDocument = null;
            let generatedRfpdDocument = null;
            let generatedRfaDocument = null;
            let finalFrogsDocument = null;
            let finalSprogsDocument = null;
            let finalRfpdDocument = null;
            let finalRfaDocument = null;


            const generatedFrogsDocumentObject = await LegalForms.findOne({
                where: {
                    practice_id: event.user.practice_id,
                    case_id: cases.id,
                    id: legalformid,
                    document_type: 'FROGS',
                    [Op.or]: [{
                        generated_document: {
                            [Op.ne]: null,
                        },
                    }, {
                        final_document: {
                            [Op.ne]: null,
                        },
                    }],
                },
            });
            AWS.config.update({ region: process.env.S3_BUCKET_FOR_DOCUMENTS_REGION });
            if (generatedFrogsDocumentObject) {
                generatedFrogsDocument = generatedFrogsDocumentObject.generated_document;
                finalFrogsDocument = generatedFrogsDocumentObject.final_document;
                if (generatedFrogsDocumentObject.generated_document_s3_key) {
                    s3BucketParams.Key = generatedFrogsDocumentObject.generated_document_s3_key;
                    generatedFrogsDocument = await s3.getSignedUrlPromise('getObject', s3BucketParams);
                }
                if (generatedFrogsDocumentObject.final_document_s3_key) {
                    s3BucketParams.Key = generatedFrogsDocumentObject.final_document_s3_key;
                    finalFrogsDocument = await s3.getSignedUrlPromise('getObject', s3BucketParams);
                }
            }

            const generatedSprogsDocumentObject = await LegalForms.findOne({
                where: {
                    practice_id: event.user.practice_id,
                    case_id: cases.id,
                    id: legalformid,
                    document_type: 'SPROGS',
                    [Op.or]: [{
                        generated_document: {
                            [Op.ne]: null,
                        },
                    }, {
                        final_document: {
                            [Op.ne]: null,
                        },
                    }],
                },
            });
            if (generatedSprogsDocumentObject) {
                generatedSprogsDocument = generatedSprogsDocumentObject.generated_document;
                finalSprogsDocument = generatedSprogsDocumentObject.final_document;

                if (generatedSprogsDocumentObject.generated_document_s3_key) {
                    s3BucketParams.Key = generatedSprogsDocumentObject.generated_document_s3_key;
                    generatedSprogsDocument = await s3.getSignedUrlPromise('getObject', s3BucketParams);
                }
                if (generatedSprogsDocumentObject.final_document_s3_key) {
                    s3BucketParams.Key = generatedSprogsDocumentObject.final_document_s3_key;
                    finalSprogsDocument = await s3.getSignedUrlPromise('getObject', s3BucketParams);
                }
            }

            const generatedRfpdDocumentObject = await LegalForms.findOne({
                where: {
                    practice_id: event.user.practice_id,
                    case_id: cases.id,
                    id: legalformid,
                    document_type: 'RFPD',
                    [Op.or]: [{
                        generated_document: {
                            [Op.ne]: null,
                        },
                    }, {
                        final_document: {
                            [Op.ne]: null,
                        },
                    }],
                },
            });
            if (generatedRfpdDocumentObject) {
                generatedRfpdDocument = generatedRfpdDocumentObject.generated_document;
                finalRfpdDocument = generatedRfpdDocumentObject.final_document;

                if (generatedRfpdDocumentObject.generated_document_s3_key) {
                    s3BucketParams.Key = generatedRfpdDocumentObject.generated_document_s3_key;
                    generatedRfpdDocument = await s3.getSignedUrlPromise('getObject', s3BucketParams);
                }
                if (generatedRfpdDocumentObject.final_document_s3_key) {
                    s3BucketParams.Key = generatedRfpdDocumentObject.final_document_s3_key;
                    finalRfpdDocument = await s3.getSignedUrlPromise('getObject', s3BucketParams);
                }
            }

            const generatedRfaDocumentObject = await LegalForms.findOne({
                where: {
                    practice_id: event.user.practice_id,
                    case_id: cases.id,
                    id: legalformid,
                    document_type: 'RFA',
                    [Op.or]: [{
                        generated_document: {
                            [Op.ne]: null,
                        },
                    }, {
                        final_document: {
                            [Op.ne]: null,
                        },
                    }],
                },
            });


            let LegalFormFROGDetails;
            let LegalFormSFROGDetails;
            let LegalFormRFPDDetails;
            let LegalFormRFADetails;
            let LegalFormFROGId = null;
            let LegalFormSFROGId = null;
            let LegalFormRFPDId = null;
            let LegalFormRFAId = null;
            let Frogsfilename = null;
            let Sprogsfilename = null;
            let Rfpdfilename = null;
            let Rfafilename = null;
            let Frogs_response_deadline_startdate = null;
            let Sprogs_response_deadline_startdate = null;
            let Rfpd_response_deadline_startdate = null;
            let Rfa_response_deadline_startdate = null;
            let Frogs_response_deadline_enddate = null;
            let Sprogs_response_deadline_enddate = null;
            let Rfpd_response_deadline_enddate = null;
            let Rfa_response_deadline_enddate = null;

            LegalFormFROGDetails = await LegalForms.find({
                where: {
                    practice_id: event.user.practice_id,
                    case_id: cases.id,
                    client_id: plainCase.client_id,
                    document_type: 'FROGS',
                    id: legalformid,
                },
            });
            if (LegalFormFROGDetails) {
                LegalFormFROGId = LegalFormFROGDetails.id;
                Frogsfilename = LegalFormFROGDetails.filename;
                Frogs_response_deadline_startdate = LegalFormFROGDetails.response_deadline_startdate;
                Frogs_response_deadline_enddate = LegalFormFROGDetails.response_deadline_enddate;
            }

            LegalFormSFROGDetails = await LegalForms.find({
                where: {
                    practice_id: event.user.practice_id,
                    case_id: cases.id,
                    client_id: plainCase.client_id,
                    document_type: 'SPROGS',
                    id: legalformid,
                },
            });
            if (LegalFormSFROGDetails) {
                LegalFormSFROGId = LegalFormSFROGDetails.id;
                Sprogsfilename = LegalFormSFROGDetails.filename;
                Sprogs_response_deadline_startdate = LegalFormSFROGDetails.response_deadline_startdate;
                Sprogs_response_deadline_enddate = LegalFormSFROGDetails.response_deadline_enddate;
            }

            LegalFormRFPDDetails = await LegalForms.find({
                where: {
                    practice_id: event.user.practice_id,
                    case_id: cases.id,
                    client_id: plainCase.client_id,
                    document_type: 'RFPD',
                    id: legalformid,
                },
            });
            if (LegalFormRFPDDetails) {
                LegalFormRFPDId = LegalFormRFPDDetails.id;
                Rfpdfilename = LegalFormRFPDDetails.filename;
                Rfpd_response_deadline_startdate = LegalFormRFPDDetails.response_deadline_startdate;
                Rfpd_response_deadline_enddate = LegalFormRFPDDetails.response_deadline_enddate;
            }

            LegalFormRFADetails = await LegalForms.find({
                where: {
                    practice_id: event.user.practice_id,
                    case_id: cases.id,
                    client_id: plainCase.client_id,
                    document_type: 'RFA',
                    id: legalformid,
                },
            });
            if (LegalFormRFADetails) {
                LegalFormRFAId = LegalFormRFADetails.id;
                Rfafilename = LegalFormRFADetails.filename;
                Rfa_response_deadline_startdate = LegalFormRFADetails.response_deadline_startdate;
                Rfa_response_deadline_enddate = LegalFormRFADetails.response_deadline_enddate;
            }

            if (generatedRfaDocumentObject) {
                generatedRfaDocument = generatedRfaDocumentObject.generated_document;
                finalRfaDocument = generatedRfaDocumentObject.final_document;

                if (generatedRfaDocumentObject.generated_document_s3_key) {
                    s3BucketParams.Key = generatedRfaDocumentObject.generated_document_s3_key;
                    generatedRfaDocument = await s3.getSignedUrlPromise('getObject', s3BucketParams);
                }
                if (generatedRfaDocumentObject.final_document_s3_key) {
                    s3BucketParams.Key = generatedRfaDocumentObject.final_document_s3_key;
                    finalRfaDocument = await s3.getSignedUrlPromise('getObject', s3BucketParams);
                }
            }

            const frogsFormDocumentProgress = await getDocumentUploadStatus(cases.id, 'FROGS');
            const sprogsFormDocumentProgress = await getDocumentUploadStatus(cases.id, 'SPROGS');
            const rfpdFormDocumentProgress = await getDocumentUploadStatus(cases.id, 'RFPD');
            const rfaFormDocumentProgress = await getDocumentUploadStatus(cases.id, 'RFA');

            const frogsFormDocumentClientSignature = await getClientSignature(cases.id, 'FROGS');
            const sprogsFormDocumentClientSignature = await getClientSignature(cases.id, 'SPROGS');
            const rfpdFormDocumentClientSignature = await getClientSignature(cases.id, 'RFPD');
            const rfaFormDocumentClientSignature = await getClientSignature(cases.id, 'RFA');

            if (frogsQuestionsCount) {
                if (LegalFormFROGId) {
                    FrogsArr[FrogsArr.length] = {
                        QuestionsCount: frogsQuestionsCount,
                        QuestionsSentToClientCount: frogsQuestionsSentToClientCount,
                        QuestionsRespondedByClientCount: frogsQuestionsRespondedByClientCount,
                        QuestionsResponsesFinalizedCount: frogsQuestionsResponsesFinalizedCount,
                        generatedDocument: generatedFrogsDocument,
                        finalDocument: finalFrogsDocument,
                        questionsUploadedDate: frogsFormDates.questionsUploadedDate,
                        questionSentToClientDate: frogsFormDates.questionSentToClientDate,
                        responsesByClientDate: frogsFormDates.responsesByClientDate,
                        responsesFinalizedDate: frogsFormDates.responsesFinalizedDate,
                        upload_status: frogsFormDocumentProgress,
                        client_signature: frogsFormDocumentClientSignature.client_signature,
                        client_signature_date: frogsFormDocumentClientSignature.client_signature_date,
                        client_verification_signature: frogsFormDocumentClientSignature.client_verification_signature,
                        client_verification_signature_date: frogsFormDocumentClientSignature.client_verification_signature_date,
                        client_signature_data: frogsFormDocumentClientSignature.client_signature_data,
                        legalform_id: LegalFormFROGId,
                        filename: Frogsfilename,
                        response_deadline_startdate: Frogs_response_deadline_startdate,
                        response_deadline_enddate: Frogs_response_deadline_enddate,
                    };
                }
            }

            if (sprogsQuestionsCount) {
                if (LegalFormSFROGId) {
                    SprogsArr[SprogsArr.length] = {
                        QuestionsCount: sprogsQuestionsCount,
                        QuestionsSentToClientCount: sprogsQuestionsSentToClientCount,
                        QuestionsRespondedByClientCount: sprogsQuestionsRespondedByClientCount,
                        QuestionsResponsesFinalizedCount: sprogsQuestionsResponsesFinalizedCount,
                        generatedDocument: generatedSprogsDocument,
                        finalDocument: finalSprogsDocument,
                        questionsUploadedDate: sprogsFormDates.questionsUploadedDate,
                        questionSentToClientDate: sprogsFormDates.questionSentToClientDate,
                        responsesByClientDate: sprogsFormDates.responsesByClientDate,
                        responsesFinalizedDate: sprogsFormDates.responsesFinalizedDate,
                        upload_status: sprogsFormDocumentProgress,
                        client_signature: sprogsFormDocumentClientSignature.client_signature,
                        client_signature_date: sprogsFormDocumentClientSignature.client_signature_date,
                        client_verification_signature: sprogsFormDocumentClientSignature.client_verification_signature,
                        client_verification_signature_date: sprogsFormDocumentClientSignature.client_verification_signature_date,
                        client_signature_data: sprogsFormDocumentClientSignature.client_signature_data,
                        legalform_id: LegalFormSFROGId,
                        filename: Sprogsfilename,
                        response_deadline_startdate: Sprogs_response_deadline_startdate,
                        response_deadline_enddate: Sprogs_response_deadline_enddate,
                    };
                }
            }

            if (rfpdQuestionsCount) {
                if (LegalFormRFPDId) {
                    RfpdArr[RfpdArr.length] = {
                        QuestionsCount: rfpdQuestionsCount,
                        QuestionsSentToClientCount: rfpdQuestionsSentToClientCount,
                        QuestionsRespondedByClientCount: rfpdQuestionsRespondedByClientCount,
                        QuestionsResponsesFinalizedCount: rfpdQuestionsResponsesFinalizedCount,
                        generatedDocument: generatedRfpdDocument,
                        finalDocument: finalRfpdDocument,
                        questionsUploadedDate: rfpdFormDates.questionsUploadedDate,
                        questionSentToClientDate: rfpdFormDates.questionSentToClientDate,
                        responsesByClientDate: rfpdFormDates.responsesByClientDate,
                        responsesFinalizedDate: rfpdFormDates.responsesFinalizedDate,
                        upload_status: rfpdFormDocumentProgress,
                        client_signature: rfpdFormDocumentClientSignature.client_signature,
                        client_signature_date: rfpdFormDocumentClientSignature.client_signature_date,
                        client_verification_signature: rfpdFormDocumentClientSignature.client_verification_signature,
                        client_verification_signature_date: rfpdFormDocumentClientSignature.client_verification_signature_date,
                        client_signature_data: rfpdFormDocumentClientSignature.client_signature_data,
                        legalform_id: LegalFormRFPDId,
                        filename: Rfpdfilename,
                        response_deadline_startdate: Rfpd_response_deadline_startdate,
                        response_deadline_enddate: Rfpd_response_deadline_enddate,
                    };
                }
            }
            if (rfaQuestionsCount) {
                if (LegalFormRFAId) {
                    RfaArr[RfaArr.length] = {
                        QuestionsCount: rfaQuestionsCount,
                        QuestionsSentToClientCount: rfaQuestionsSentToClientCount,
                        QuestionsRespondedByClientCount: rfaQuestionsRespondedByClientCount,
                        QuestionsResponsesFinalizedCount: rfaQuestionsResponsesFinalizedCount,
                        generatedDocument: generatedRfaDocument,
                        finalDocument: finalRfaDocument,
                        questionsUploadedDate: rfaFormDates.questionsUploadedDate,
                        questionSentToClientDate: rfaFormDates.questionSentToClientDate,
                        responsesByClientDate: rfaFormDates.responsesByClientDate,
                        responsesFinalizedDate: rfaFormDates.responsesFinalizedDate,
                        upload_status: rfaFormDocumentProgress,
                        client_signature: rfaFormDocumentClientSignature.client_signature,
                        client_signature_date: rfaFormDocumentClientSignature.client_signature_date,
                        client_verification_signature: rfaFormDocumentClientSignature.client_verification_signature,
                        client_verification_signature_date: rfaFormDocumentClientSignature.client_verification_signature_date,
                        client_signature_data: rfaFormDocumentClientSignature.client_signature_data,
                        legalform_id: LegalFormRFAId,
                        filename: Rfafilename,
                        response_deadline_startdate: Rfa_response_deadline_startdate,
                        response_deadline_enddate: Rfa_response_deadline_enddate,
                    };
                }
            }


        }
        /* For   End */
        let casefrom;
        if(cases?.case_from){ casefrom = cases.case_from };
        if(cases && (cases.case_from == null)){casefrom = 'esquiretek'};
        if (cases?.integration_case_data) {
            cases.integration_case_data = JSON.parse(cases.integration_case_data);
        }
        const dataObject = Object.assign(plainCase, {
            frogs: FrogsArr,
            sprogs: SprogsArr,
            rfpd: RfpdArr,
            rfa: RfaArr,
            medical_history: {
                documentCount: MedicalHistoryRecords.length,
                pagesCount,
                documentStatus,
                uploadedDocuments: MedicalHistoryRecords,
                summery_url,
            },
            practice_name: practiceDetails.name,
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify(dataObject),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Cases.' }),
        };
    }
};
const DuplicateCaseClientCreate = async (event) => {
    try {
        let id;
        const input = event.body;
        validateDuplicateCaseClientCreate(input);
        if (event.user.practice_id) {
            input.practice_id = event.user.practice_id;
        }
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4() });
        validateCreateClients(dataObject);
        const { Cases, Clients, Forms, LegalForms, OtherParties, OtherPartiesForms, Op } = await connectToDatabase();
        let name = '', address;
            if(input?.first_name){
                name = input?.first_name;
            }
            if (input.middle_name) { 
                name = name + ' ' + input.middle_name; 
            }
            if (input.last_name) { 
                name = name + ' ' + input.last_name; 
            }
            dataObject.name = name;
            if(input.street){
            address = input.street + ', ' + input.city + ', ' + input.state + ' - ' + input.zip_code;
            dataObject.address = address;
            }
        const clients = await Clients.create(dataObject);
        const primaryCaseId = input.case_id;
        console.log(primaryCaseId);
        const CaseObj = await Cases.findOne({ where: { id: primaryCaseId } });
        const OtherPartiesObj = await OtherParties.findOne({ where: { case_id: primaryCaseId } });
        const OtherPartiesFormsObj = await OtherPartiesForms.findAll({ where: { case_id: primaryCaseId } });
        if (!CaseObj) throw new HTTPError(404, `Cases with id: ${input.case_id} was not found`);

        const cloningClientId = clients.id;
        console.log(cloningClientId);
        const dataObj = {
            id: uuid.v4(),
            client_id: cloningClientId,
            practice_id: CaseObj.practice_id,
            case_title: CaseObj.case_title,
            case_number: CaseObj.case_number,
            claim_number: CaseObj.claim_number,
            matter_id: CaseObj.matter_id,
            county: CaseObj.county,
            state: CaseObj.state,
            federal: CaseObj.federal,
            federal_district: CaseObj.federal_district,
            attorneys: CaseObj.attorneys,
            date_of_loss: CaseObj.date_of_loss,
            opposing_counsel: CaseObj.opposing_counsel,
            opposing_counsel_info: CaseObj.opposing_counsel_info,
            propounder_opposing_counsel: CaseObj.propounder_opposing_counsel,
        };

        const existing_cases = await Cases.findOne({
            where: {
                client_id: cloningClientId,
                practice_id: CaseObj.practice_id,
                case_title: CaseObj.case_title,
                case_number: CaseObj.case_number,
                is_deleted: { [Op.ne]: 1 },
            }
        });

        if (existing_cases) throw new HTTPError(404, `Case with client id: ${cloningClientId} is already exists.`);
        const cases = await Cases.create(dataObj);
        const plainCase = cases.get({ plain: true });
        const clientObject = await Clients.findOne({ where: { id: plainCase.client_id } });
        if (clientObject) {
            plainCase.client_name = clientObject.name;
        }

        await creatLegalFormsforCloningCases(primaryCaseId, plainCase);

        if (OtherPartiesObj && OtherPartiesFormsObj) {
            let otherPartiesInput = {
                id: uuid.v4(),
                case_id: plainCase.id,
                name: OtherPartiesObj.name,
                email: OtherPartiesObj.email,
                phone: OtherPartiesObj.phone,
                address: OtherPartiesObj.address,
                dob: OtherPartiesObj.dob,
                is_deleted: OtherPartiesObj.is_deleted,
                practice_id: OtherPartiesObj.practice_id
            };
            validateCreateParties(otherPartiesInput);
            const OtherPartiesNewObj = await OtherParties.create(otherPartiesInput);
            await cloneCaseOtherParties(OtherPartiesNewObj);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainCase),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the clients.' }),
        };
    }
};

const cloningCases = async (event) => {
    try {
        const input = event.body;
        const primaryCaseId = input.case_id;
        const cloningClientId = input.client_id;
        const { Cases, Clients, Forms, LegalForms, OtherParties, OtherPartiesForms, Op } = await connectToDatabase();

        const CaseObj = await Cases.findOne({ where: { id: primaryCaseId } });
        const ClinetObj = await Clients.findOne({ where: { id: cloningClientId } });
        const OtherPartiesObj = await OtherParties.findOne({ where: { case_id: primaryCaseId } });
        const OtherPartiesFormsObj = await OtherPartiesForms.findAll({ where: { case_id: primaryCaseId } });
        if (!CaseObj) throw new HTTPError(404, `Cases with id: ${input.case_id} was not found`);
        if (!ClinetObj) throw new HTTPError(404, `Client with id: ${input.client_id} was not found`);

        const dataObj = {
            id: uuid.v4(),
            client_id: cloningClientId,
            practice_id: CaseObj.practice_id,
            case_title: CaseObj.case_title,
            case_number: CaseObj.case_number,
            claim_number: CaseObj.claim_number,
            matter_id: CaseObj.matter_id,
            county: CaseObj.county,
            federal: CaseObj.federal,
            federal_district: CaseObj.federal_district,
            state: CaseObj.state,
            attorneys: CaseObj.attorneys,
            date_of_loss: CaseObj.date_of_loss,
            opposing_counsel: CaseObj.opposing_counsel,
            opposing_counsel_info: CaseObj.opposing_counsel_info,
            propounder_opposing_counsel: CaseObj.propounder_opposing_counsel,
        };

        const existing_cases = await Cases.findOne({
            where: {
                client_id: cloningClientId,
                practice_id: CaseObj.practice_id,
                case_title: CaseObj.case_title,
                case_number: CaseObj.case_number,
                is_deleted: { [Op.ne]: 1 },
            }
        });

        if (existing_cases) throw new HTTPError(404, `Case with client id: ${cloningClientId} is already exists.`);
        const cases = await Cases.create(dataObj);
        const plainCase = cases.get({ plain: true });
        const clientObject = await Clients.findOne({ where: { id: plainCase.client_id } });
        if (clientObject) {
            plainCase.client_name = clientObject.name;
        }

        await creatLegalFormsforCloningCases(primaryCaseId, plainCase);

        if (OtherPartiesObj && OtherPartiesFormsObj) {
            let otherPartiesInput = {
                id: uuid.v4(),
                case_id: plainCase.id,
                name: OtherPartiesObj.name,
                email: OtherPartiesObj.email,
                phone: OtherPartiesObj.phone,
                address: OtherPartiesObj.address,
                dob: OtherPartiesObj.dob,
                is_deleted: OtherPartiesObj.is_deleted,
                practice_id: OtherPartiesObj.practice_id
            };
            validateCreateParties(otherPartiesInput);
            const OtherPartiesNewObj = await OtherParties.create(otherPartiesInput);
            await cloneCaseOtherParties(OtherPartiesNewObj);
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify(plainCase),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ error: err.message || 'Could not clone these forms.' }),
        };
    }
};
const creatLegalFormsforCloningCases = async (primaryCaseId, cloningCase) => {
    try {
        const response = {};
        const { LegalForms, Cases, Forms } = await connectToDatabase();
        const legalFormsObject = await LegalForms.findAll({ where: { case_id: primaryCaseId }, raw: true });
        for (let i = 0; i < legalFormsObject.length; i++) {
            const input = {
                id: uuid.v4(),
                practice_id: cloningCase.practice_id,
                case_id: cloningCase.id,
                client_id: cloningCase.client_id,
                filename: legalFormsObject[i].filename,
                status: 'pending',
                document_type: legalFormsObject[i].document_type,
            };
            validateCreateLegalForms(input);

            const legalForms = await LegalForms.create(input);

            const uploadURLObject = await getUploadURL(input, legalForms);
            legalForms.s3_file_key = uploadURLObject.s3_file_key;
            await legalForms.save();

            const selectForms = await Forms.findAll({
                where: {
                    case_id: primaryCaseId,
                    document_type: legalFormsObject[i].document_type,
                    legalforms_id: legalFormsObject[i].id,
                },
                raw: true,
            });
            if (selectForms.length == 0) {
                await LegalForms.destroy({ where: { id: legalForms.id } });
            } else {
                for (let n = 0; n < selectForms.length; n++) {
                    const cloningFormDetails = {
                        id: uuid.v4(),
                        case_id: cloningCase.id,
                        practice_id: cloningCase.practice_id,
                        client_id: cloningCase.client_id,
                        legalforms_id: legalForms.id,
                        document_type: selectForms[n].document_type,
                        subgroup: selectForms[n].subgroup,
                        question_type: selectForms[n].question_type,
                        question_id: selectForms[n].question_id,
                        question_number: selectForms[n].question_number,
                        question_number_text: selectForms[n].question_number_text,
                        question_text: selectForms[n].question_text,
                        question_section_id: selectForms[n].question_section_id,
                        question_section: selectForms[n].question_section,
                        question_section_text: selectForms[n].question_section_text,
                        question_options: selectForms[n].question_options,
                        is_consultation_set: selectForms[n].is_consultation_set,
                        consultation_set_no: selectForms[n].consultation_set_no,
                        lawyer_response_status: 'NotStarted',
                        client_response_status: 'NotSetToClient',
                    };
                    let cloningFormsTable = await Forms.create(cloningFormDetails);
                    if (!cloningFormsTable) await LegalForms.destroy({ where: { id: legalForms.id } });
                    if (!cloningFormsTable) throw new HTTPError(404, `Cannot Create Forms.`);

                }
            }
        }
        return response;
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ error: err.message || 'Could not clone these forms.' }),
        };
    }
};
const decimalCount = num => {
    const numStr = String(num);
    // String Contains Decimal
    if (numStr.includes('.')) {
        let arr = numStr.split('.')[1].split('');
        arr = arr.filter(item => item != ' ');
        return arr.length;
    }
    ;
    // String Does Not Contain Decimal
    return 0;
}
const cloneCaseOtherParties = async (OtherPartiesNewObj) => {
    try {

        const { Forms, OtherPartiesForms, OtherParties } = await connectToDatabase();
        let FormsModelObj = await Forms.findAll({ where: { case_id: OtherPartiesNewObj.case_id } });

        FormsModelObj.forEach(async row => {
            /* let decimalLength = decimalCount(row.question_number_text);
            const question_number_sort = row.question_number_text * Math.pow(10, decimalLength); */
            let newData = {
                id: uuid.v4(),
                case_id: row.case_id,
                practice_id: row.practice_id,
                party_id: OtherPartiesNewObj.id,
                legalforms_id: row.legalforms_id,
                form_id: row.id,
                document_type: row.document_type, // FROGS, SPROGS, RFPD, RFA
                subgroup: row.subgroup, // FROGS, SPROGS, RFPD, RFA
                question_type: row.question_type, // only for initial discloser forms (FROGS, SPROGS, RFPD, RFA)
                question_id: row.question_id,
                question_number_text: row.question_number_text,
                question_number: row.question_number,
                question_text: row.question_text,
                question_section_id: row.question_section_id,
                question_section: row.question_section,
                question_section_text: row.question_section_text,
                question_options: row.question_options,
                is_consultation_set: row.is_consultation_set,
                consultation_set_no: row.consultation_set_no,
                //question_number_sort: question_number_sort,
                client_response_status: 'NotSetToClient', // (NotSetToClient, SentToClient, ClientResponseAvailable)
                TargetLanguageCode: row.TargetLanguageCode,
            }
            let newPartyQuestions = await OtherPartiesForms.create(newData);
            if (!newPartyQuestions) await OtherParties.destroy({ where: { case_id: row.case_id } });
            if (!newPartyQuestions) throw new HTTPError(404, `Other Parties Forms Cannot create.`);
        });
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ error: err.message || 'Could not clone Other Parties forms.' }),
        };
    }
};

const getUploadURL = async (input, legalForms) => {
    let fileExtention = '';
    let documentType = '';
    if (input.file_name) {
        fileExtention = `.${input.file_name.split('.').pop()}`;
    }
    if (input.document_type) {
        documentType = `_${input.document_type}`;
    }
    const s3Params = {
        Bucket: process.env.S3_BUCKET_FOR_FORMS,
        Key: `${legalForms.id}${documentType}${fileExtention}`,
        ContentType: input.content_type,
        ACL: 'private',
        Metadata: {
            legal_form_id: legalForms.id,
            practice_id: legalForms.practice_id,
            case_id: legalForms.case_id,
            client_id: legalForms.client_id,
        },
    };
    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
        });
    });
};

const getCaseDetailsOptimze = async (event) => {
    try {
        if (!event.pathParameters && event.params) {
            event.pathParameters = event.params;
        }
        validateGetOneCase(event.pathParameters);
        const {
            Op,
            Forms,
            Cases,
            sequelize,
            LegalForms,
            FeeTerms,
            Orders,
            PracticesTemplates,
            HipaaTerms,
            Clients,
            MedicalHistory,
            MedicalHistorySummery,
            Practices,
            FrogsTemplates
        } = await connectToDatabase();
        const cases = await Cases.findOne({ where: { id: event.pathParameters.id } });
        if (!cases) throw new HTTPError(404, `Cases with id: ${event.pathParameters.id} was not found`);
        const plainCase = cases.get({ plain: true });
        const practiceDetails = await Practices.findOne({
            where: {
                id: plainCase.practice_id,
            },
        });

        if (!plainCase.fee_terms) {
            const feetermsObject = await FeeTerms.findOne({
                where: {
                    practice_id: plainCase.practice_id,
                },
            });
            if (feetermsObject && feetermsObject.terms_text) {
                plainCase.fee_terms = feetermsObject.terms_text;
            }
        }
        plainCase.hipaa_terms = '';
        if (plainCase.practice_id) {
            const hipaatermsObject = await HipaaTerms.findOne({
                where: {
                    practice_id: plainCase.practice_id,
                },
            });
            if (hipaatermsObject) {
                plainCase.hipaa_terms = hipaatermsObject.terms_text;
            }
        }

        plainCase.phone = '';
        if (plainCase.client_id) {
            const clientDetails = await Clients.findOne({
                where: {
                    id: plainCase.client_id,
                },
            });
            if (clientDetails) {
                plainCase.client_phone = clientDetails.phone;
                plainCase.client_name = clientDetails.name;
            }
        }

        if (plainCase.opposing_counsel) {
            plainCase.opposing_counsel = JSON.parse(plainCase.opposing_counsel);
        }

        if (plainCase.propounder_opposing_counsel) {
            plainCase.propounder_opposing_counsel = JSON.parse(plainCase.propounder_opposing_counsel);
        }

        const MedicalHistorySummeryObject = await MedicalHistorySummery.findOne({
            where: {
                case_id: cases.id,
            },
        });

        const MedicalHistoryRecords = await MedicalHistory.findAll({
            where: { case_id: cases.id },
            order: [
                ['createdAt', 'DESC'],
            ],
            raw: true,
        });
        const totalamount = await Orders.findAll({
            where: { case_id: cases.id },
            attributes: [
                [sequelize.fn('sum', sequelize.col('amount_charged')), 'total_cost'],
            ],
            raw: true
        });
        let extractamount = 0;
        if (totalamount[0].total_cost) {
            extractamount = totalamount[0].total_cost;
        }
        let pagesCount = 0;
        let documentStatus = 'No source documents';
        if (MedicalHistoryRecords.length) {
            documentStatus = 'Summary not requested';
        }
        let summery_url = '';
        let summery_documents = '';
        if (MedicalHistorySummeryObject) {
            if (MedicalHistorySummeryObject.status == 'Complete') {
                documentStatus = 'Completed';
            } else {
                documentStatus = 'Work in progress';
            }
            summery_url = MedicalHistorySummeryObject.summery_url;
            if (MedicalHistorySummeryObject.summery_documents) {
                summery_documents = JSON.parse(MedicalHistorySummeryObject.summery_documents);
            }
        }
        for (let i = 0; i < MedicalHistoryRecords.length; i += 1) {
            if (MedicalHistoryRecords[i].pages) {
                pagesCount += MedicalHistoryRecords[i].pages;
            }
            AWS.config.update({ region: process.env.S3_BUCKET_FOR_PRIVATE_ASSETS_REGION });
            MedicalHistoryRecords.secure_public_url = await s3.getSignedUrlPromise('getObject', {
                Bucket: process.env.S3_BUCKET_FOR_PRIVATE_ASSETS,
                Expires: 60 * 60 * 1,
                Key: MedicalHistoryRecords[i].s3_file_key,
            });
        }

        const legalFormsObject = await LegalForms.findAll({
            where: {
                practice_id: event.user.practice_id,
                case_id: cases.id,
                client_id: plainCase.client_id,
            },
            order: [
                ['createdAt', 'ASC'],
            ],
            raw: true,
        });


        const FrogsArr = [];
        const SprogsArr = [];
        const RfpdArr = [];
        const RfaArr = [];
        /* For Start */

        for (let index = 0; index < legalFormsObject.length; index++) {
            const legalformid = legalFormsObject[index].id;
            const client_id = legalFormsObject[index].client_id;

            // FROGS, SPROGS, RFPD, RFA
            const Frogscount = {};
            Frogscount.case_id = cases.id;
            Frogscount.document_type = 'FROGS';
            Frogscount.legalforms_id = legalformid;
            Frogscount.practice_id = event.user.practice_id;
            const ResFrogscount = await caseDetailsCountSql(Frogscount);

            const Sprogscount = {};
            Sprogscount.case_id = cases.id;
            Sprogscount.document_type = 'SPROGS';
            Sprogscount.legalforms_id = legalformid;
            Sprogscount.practice_id = event.user.practice_id;
            const ResSprogscount = await caseDetailsCountSql(Sprogscount);

            const RFPDcount = {};
            RFPDcount.case_id = cases.id;
            RFPDcount.document_type = 'RFPD';
            RFPDcount.legalforms_id = legalformid;
            RFPDcount.practice_id = event.user.practice_id;
            const ResRFPDcount = await caseDetailsCountSql(RFPDcount);

            const RFAcount = {};
            RFAcount.case_id = cases.id;
            RFAcount.document_type = 'RFA';
            RFAcount.legalforms_id = legalformid;
            RFAcount.practice_id = event.user.practice_id;
            const ResRFAcount = await caseDetailsCountSql(RFAcount);

            const frogsFormDates = await getFormDates(cases.id, 'FROGS', legalformid);
            const sprogsFormDates = await getFormDates(cases.id, 'SPROGS', legalformid);
            const rfpdFormDates = await getFormDates(cases.id, 'RFPD', legalformid);
            const rfaFormDates = await getFormDates(cases.id, 'RFA', legalformid);

            AWS.config.update({ region: process.env.S3_BUCKET_FOR_DOCUMENTS_REGION });

            const FROGSGeneratedDocumentObject = await generateDocumentsObject(event.user.practice_id, cases.id, legalformid, 'FROGS');
            const SPROGSGeneratedDocumentObject = await generateDocumentsObject(event.user.practice_id, cases.id, legalformid, 'SPROGS');
            const RFPDGeneratedDocumentObject = await generateDocumentsObject(event.user.practice_id, cases.id, legalformid, 'RFPD');
            const RFAGeneratedDocumentObject = await generateDocumentsObject(event.user.practice_id, cases.id, legalformid, 'RFA');

            const LegalFormFROGDetails = await legalFormsDetails(legalformid, 'FROGS');
            const LegalFormSFROGDetails = await legalFormsDetails(legalformid, 'SPROGS');
            const LegalFormRFPDDetails = await legalFormsDetails(legalformid, 'RFPD');
            const LegalFormRFADetails = await legalFormsDetails(legalformid, 'RFA');

            const frogsFormDocumentProgress = await getDocumentUploadStatus(cases.id, 'FROGS');
            const sprogsFormDocumentProgress = await getDocumentUploadStatus(cases.id, 'SPROGS');
            const rfpdFormDocumentProgress = await getDocumentUploadStatus(cases.id, 'RFPD');
            const rfaFormDocumentProgress = await getDocumentUploadStatus(cases.id, 'RFA');

            const frogsFormDocumentClientSignature = await getClientSignature(cases.id, 'FROGS', legalformid);
            const sprogsFormDocumentClientSignature = await getClientSignature(cases.id, 'SPROGS', legalformid);
            const rfpdFormDocumentClientSignature = await getClientSignature(cases.id, 'RFPD', legalformid);
            const rfaFormDocumentClientSignature = await getClientSignature(cases.id, 'RFA', legalformid);

            const frogsLastSetOfQuestions = await lastSetOfQuestions(cases.id, legalformid, client_id, 'FROGS');
            const sprogsLastSetOfQuestions = await lastSetOfQuestions(cases.id, legalformid, client_id, 'SPROGS');
            const rfpdLastSetOfQuestions = await lastSetOfQuestions(cases.id, legalformid, client_id, 'RFPD');
            const rfaLastSetOfQuestions = await lastSetOfQuestions(cases.id, legalformid, client_id, 'RFA');

            if (ResFrogscount.legalformsCount != 0) {
                if (LegalFormFROGDetails) {
                    FrogsArr[FrogsArr.length] = {
                        QuestionsCount: ResFrogscount.legalformsCount,
                        QuestionsSentToClientCount: ResFrogscount.QuestionsSentToClientCount,
                        QuestionsRespondedByClientCount: ResFrogscount.QuestionsRespondedByClientCount,
                        QuestionsResponsesFinalizedCount: ResFrogscount.QuestionsResponsesFinalizedCount,
                        generatedDocument: FROGSGeneratedDocumentObject.generatedDocument,
                        finalDocument: FROGSGeneratedDocumentObject.finalDocument,
                        posDocument: FROGSGeneratedDocumentObject.pos_document,
                        discovery_s3_file_key: FROGSGeneratedDocumentObject.finalDocument_s3_file_key,
                        questionsUploadedDate: frogsFormDates.questionsUploadedDate,
                        questionSentToClientDate: frogsFormDates.questionSentToClientDate,
                        responsesByClientDate: frogsFormDates.responsesByClientDate,
                        responsesFinalizedDate: frogsFormDates.responsesFinalizedDate,
                        upload_status: frogsFormDocumentProgress,
                        client_signature: frogsFormDocumentClientSignature.client_signature,
                        client_signature_date: frogsFormDocumentClientSignature.client_signature_date,
                        client_verification_signature: frogsFormDocumentClientSignature.client_verification_signature,
                        client_verification_signature_date: frogsFormDocumentClientSignature.client_verification_signature_date,
                        client_signature_data: frogsFormDocumentClientSignature.client_signature_data,
                        legalform_id: LegalFormFROGDetails.id,
                        filename: LegalFormFROGDetails.filename,
                        defendant_practice_details: legalFormsObject[index].defendant_practice_details,
                        response_deadline_startdate: LegalFormFROGDetails.response_deadline_startdate,
                        response_deadline_enddate: LegalFormFROGDetails.response_deadline_enddate,
                        plaintiff_name: LegalFormFROGDetails.plaintiff_name,
                        defendant_name: LegalFormFROGDetails.defendant_name,
                        serving_attorney_name: LegalFormFROGDetails.serving_attorney_name,
                        serving_attorney_street: LegalFormFROGDetails.serving_attorney_street,
                        serving_attorney_city: LegalFormFROGDetails.serving_attorney_city,
                        serving_attorney_state: LegalFormFROGDetails.serving_attorney_state,
                        serving_attorney_zip_code: LegalFormFROGDetails.serving_attorney_zip_code,
                        serving_attorney_email: LegalFormFROGDetails.serving_attorney_email,
                        serving_date: LegalFormFROGDetails.serving_date,
                        QuestionsRespondedByClientLastSetofCount: frogsLastSetOfQuestions.QuestionsRespondedByClientLastSetofCount,
                        QuestionsSentToClientLastSetofCount: frogsLastSetOfQuestions.QuestionsSentToClientLastSetofCount,
                        attach_documents: ResFrogscount.attach_documents,
                        pdf_s3_file_key: ResFrogscount.pdf_s3_file_key
                    };
                }
            }

            if (ResSprogscount.legalformsCount != 0) {
                if (LegalFormSFROGDetails) {
                    SprogsArr[SprogsArr.length] = {
                        QuestionsCount: ResSprogscount.legalformsCount,
                        QuestionsSentToClientCount: ResSprogscount.QuestionsSentToClientCount,
                        QuestionsRespondedByClientCount: ResSprogscount.QuestionsRespondedByClientCount,
                        QuestionsResponsesFinalizedCount: ResSprogscount.QuestionsResponsesFinalizedCount,
                        generatedDocument: SPROGSGeneratedDocumentObject.generatedDocument,
                        finalDocument: SPROGSGeneratedDocumentObject.finalDocument,
                        posDocument: SPROGSGeneratedDocumentObject.pos_document,
                        discovery_s3_file_key: SPROGSGeneratedDocumentObject.finalDocument_s3_file_key,
                        questionsUploadedDate: sprogsFormDates.questionsUploadedDate,
                        questionSentToClientDate: sprogsFormDates.questionSentToClientDate,
                        responsesByClientDate: sprogsFormDates.responsesByClientDate,
                        responsesFinalizedDate: sprogsFormDates.responsesFinalizedDate,
                        upload_status: sprogsFormDocumentProgress,
                        client_signature: sprogsFormDocumentClientSignature.client_signature,
                        client_signature_date: sprogsFormDocumentClientSignature.client_signature_date,
                        client_verification_signature: sprogsFormDocumentClientSignature.client_verification_signature,
                        client_verification_signature_date: sprogsFormDocumentClientSignature.client_verification_signature_date,
                        client_signature_data: sprogsFormDocumentClientSignature.client_signature_data,
                        legalform_id: LegalFormSFROGDetails.id,
                        filename: LegalFormSFROGDetails.filename,
                        defendant_practice_details: legalFormsObject[index].defendant_practice_details,
                        response_deadline_startdate: LegalFormSFROGDetails.response_deadline_startdate,
                        response_deadline_enddate: LegalFormSFROGDetails.response_deadline_enddate,
                        plaintiff_name: LegalFormSFROGDetails.plaintiff_name,
                        defendant_name: LegalFormSFROGDetails.defendant_name,
                        serving_attorney_name: LegalFormSFROGDetails.serving_attorney_name,
                        serving_attorney_street: LegalFormSFROGDetails.serving_attorney_street,
                        serving_attorney_city: LegalFormSFROGDetails.serving_attorney_city,
                        serving_attorney_state: LegalFormSFROGDetails.serving_attorney_state,
                        serving_attorney_zip_code: LegalFormSFROGDetails.serving_attorney_zip_code,
                        serving_attorney_email: LegalFormSFROGDetails.serving_attorney_email,
                        serving_date: LegalFormSFROGDetails.serving_date,
                        QuestionsRespondedByClientLastSetofCount: sprogsLastSetOfQuestions.QuestionsRespondedByClientLastSetofCount,
                        QuestionsSentToClientLastSetofCount: sprogsLastSetOfQuestions.QuestionsSentToClientLastSetofCount,
                        attach_documents: ResSprogscount.attach_documents,
                        pdf_s3_file_key: ResSprogscount.pdf_s3_file_key

                    };
                }
            }

            if (ResRFPDcount.legalformsCount != 0) {
                if (LegalFormRFPDDetails) {
                    RfpdArr[RfpdArr.length] = {
                        QuestionsCount: ResRFPDcount.legalformsCount,
                        QuestionsSentToClientCount: ResRFPDcount.QuestionsSentToClientCount,
                        QuestionsRespondedByClientCount: ResRFPDcount.QuestionsRespondedByClientCount,
                        QuestionsResponsesFinalizedCount: ResRFPDcount.QuestionsResponsesFinalizedCount,
                        generatedDocument: RFPDGeneratedDocumentObject.generatedDocument,
                        finalDocument: RFPDGeneratedDocumentObject.finalDocument,
                        posDocument: RFPDGeneratedDocumentObject.pos_document,
                        discovery_s3_file_key: RFPDGeneratedDocumentObject.finalDocument_s3_file_key,
                        questionsUploadedDate: rfpdFormDates.questionsUploadedDate,
                        questionSentToClientDate: rfpdFormDates.questionSentToClientDate,
                        responsesByClientDate: rfpdFormDates.responsesByClientDate,
                        responsesFinalizedDate: rfpdFormDates.responsesFinalizedDate,
                        upload_status: rfpdFormDocumentProgress,
                        client_signature: rfpdFormDocumentClientSignature.client_signature,
                        client_signature_date: rfpdFormDocumentClientSignature.client_signature_date,
                        client_verification_signature: rfpdFormDocumentClientSignature.client_verification_signature,
                        client_verification_signature_date: rfpdFormDocumentClientSignature.client_verification_signature_date,
                        client_signature_data: rfpdFormDocumentClientSignature.client_signature_data,
                        legalform_id: LegalFormRFPDDetails.id,
                        filename: LegalFormRFPDDetails.filename,
                        defendant_practice_details: legalFormsObject[index].defendant_practice_details,
                        response_deadline_startdate: LegalFormRFPDDetails.response_deadline_startdate,
                        response_deadline_enddate: LegalFormRFPDDetails.response_deadline_enddate,
                        plaintiff_name: LegalFormRFPDDetails.plaintiff_name,
                        defendant_name: LegalFormRFPDDetails.defendant_name,
                        serving_attorney_name: LegalFormRFPDDetails.serving_attorney_name,
                        serving_attorney_street: LegalFormRFPDDetails.serving_attorney_street,
                        serving_attorney_city: LegalFormRFPDDetails.serving_attorney_city,
                        serving_attorney_state: LegalFormRFPDDetails.serving_attorney_state,
                        serving_attorney_zip_code: LegalFormRFPDDetails.serving_attorney_zip_code,
                        serving_attorney_email: LegalFormRFPDDetails.serving_attorney_email,
                        serving_date: LegalFormRFPDDetails.serving_date,
                        QuestionsRespondedByClientLastSetofCount: rfpdLastSetOfQuestions.QuestionsRespondedByClientLastSetofCount,
                        QuestionsSentToClientLastSetofCount: rfpdLastSetOfQuestions.QuestionsSentToClientLastSetofCount,
                        attach_documents: ResRFPDcount.attach_documents,
                        pdf_s3_file_key: ResRFPDcount.pdf_s3_file_key,
                    };
                }
            }
            if (ResRFAcount.legalformsCount != 0) {
                if (LegalFormRFADetails) {
                    RfaArr[RfaArr.length] = {
                        QuestionsCount: ResRFAcount.legalformsCount,
                        QuestionsSentToClientCount: ResRFAcount.QuestionsSentToClientCount,
                        QuestionsRespondedByClientCount: ResRFAcount.QuestionsRespondedByClientCount,
                        QuestionsResponsesFinalizedCount: ResRFAcount.QuestionsResponsesFinalizedCount,
                        generatedDocument: RFAGeneratedDocumentObject.generatedDocument,
                        finalDocument: RFAGeneratedDocumentObject.finalDocument,
                        posDocument: RFAGeneratedDocumentObject.pos_document,
                        discovery_s3_file_key: RFAGeneratedDocumentObject.finalDocument_s3_file_key,
                        questionsUploadedDate: rfaFormDates.questionsUploadedDate,
                        questionSentToClientDate: rfaFormDates.questionSentToClientDate,
                        responsesByClientDate: rfaFormDates.responsesByClientDate,
                        responsesFinalizedDate: rfaFormDates.responsesFinalizedDate,
                        upload_status: rfaFormDocumentProgress,
                        client_signature: rfaFormDocumentClientSignature.client_signature,
                        client_signature_date: rfaFormDocumentClientSignature.client_signature_date,
                        client_verification_signature: rfaFormDocumentClientSignature.client_verification_signature,
                        client_verification_signature_date: rfaFormDocumentClientSignature.client_verification_signature_date,
                        client_signature_data: rfaFormDocumentClientSignature.client_signature_data,
                        legalform_id: LegalFormRFADetails.id,
                        filename: LegalFormRFADetails.filename,
                        defendant_practice_details: legalFormsObject[index].defendant_practice_details,
                        response_deadline_startdate: LegalFormRFADetails.response_deadline_startdate,
                        response_deadline_enddate: LegalFormRFADetails.response_deadline_enddate,
                        plaintiff_name: LegalFormRFADetails.plaintiff_name,
                        defendant_name: LegalFormRFADetails.defendant_name,
                        serving_attorney_name: LegalFormRFADetails.serving_attorney_name,
                        serving_attorney_street: LegalFormRFADetails.serving_attorney_street,
                        serving_attorney_city: LegalFormRFADetails.serving_attorney_city,
                        serving_attorney_state: LegalFormRFADetails.serving_attorney_state,
                        serving_attorney_zip_code: LegalFormRFADetails.serving_attorney_zip_code,
                        serving_attorney_email: LegalFormRFADetails.serving_attorney_email,
                        serving_date: LegalFormRFADetails.serving_date,
                        QuestionsRespondedByClientLastSetofCount: rfaLastSetOfQuestions.QuestionsRespondedByClientLastSetofCount,
                        QuestionsSentToClientLastSetofCount: rfaLastSetOfQuestions.QuestionsSentToClientLastSetofCount,
                        attach_documents: ResRFAcount.attach_documents,
                        pdf_s3_file_key: ResRFAcount.pdf_s3_file_key
                    };
                }
            }
        }

        let casefrom;
        if(cases?.case_from){ casefrom = cases.case_from };
        if(cases && (cases.case_from == null)){casefrom = 'esquiretek'};
        if (cases?.integration_case_data) {
            cases.integration_case_data = JSON.parse(cases.integration_case_data);
        }

        const dataObject = Object.assign(plainCase, {
            frogs: FrogsArr,
            sprogs: SprogsArr,
            rfpd: RfpdArr,
            rfa: RfaArr,
            total_cost: extractamount.toFixed(2),
            medical_history: {
                documentCount: MedicalHistoryRecords.length,
                pagesCount,
                documentStatus,
                uploadedDocuments: MedicalHistoryRecords,
                summery_url,
                summery_documents
            },
            practice_name: practiceDetails.name,
            attorney_response_tracking: cases.attorney_response_tracking,
            case_from: casefrom,
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify(dataObject),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Cases.' }),
        };
    }
};

const caseDetailsCountSql = async (columns) => {

    const query = {};
    const response = {};
    query.where = {};
    const { Forms, Cases, LegalForms, Op } = await connectToDatabase();
    query.where.case_id = columns.case_id;
    query.where.document_type = columns.document_type;
    query.where.legalforms_id = columns.legalforms_id;
    query.where.practice_id = columns.practice_id;
    /* query.logging = console.log; */
    const legalformsCount = await Forms.count(query);

    query.where.client_response_status = { [Op.in]: ['SentToClient', 'ClientResponseAvailable'] }
    const QuestionsSentToClientCount = await Forms.count(query);
    query.where.client_response_status = 'ClientResponseAvailable';
    const QuestionsRespondedByClientCount = await Forms.count(query);
    delete query.where.client_response_status; // delete the object
    query.where.lawyer_response_status = 'Final';

    const legalFormsattachdocuments = await LegalForms.findOne({ where: { id: columns.legalforms_id, case_id: columns.case_id, practice_id: columns.practice_id, } })
    if (legalFormsattachdocuments.attach_documents) response.attach_documents = JSON.parse(legalFormsattachdocuments.attach_documents);
    if (legalFormsattachdocuments.pdf_s3_file_key) response.pdf_s3_file_key = legalFormsattachdocuments.pdf_s3_file_key;

    const QuestionsResponsesFinalizedCount = await Forms.count(query);
    response.legalformsCount = legalformsCount;
    response.QuestionsSentToClientCount = QuestionsSentToClientCount;
    response.QuestionsRespondedByClientCount = QuestionsRespondedByClientCount;
    response.QuestionsResponsesFinalizedCount = QuestionsResponsesFinalizedCount;

    return response;
};

const generateDocumentsObject = async (practice_id, case_id, legalformid, document_type) => {
    const s3BucketParams = { Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS, Expires: 60 * 60 * 1 };
    const { Forms, Op, LegalForms } = await connectToDatabase();
    const response = {};
    let generatedDocument = null;
    let finalDocument = null;
    let final_document_s3_key = null;
    let pos_document = null;
    const generatedDocumentObject = await LegalForms.findOne({
        where: {
            practice_id: practice_id,
            case_id: case_id,
            id: legalformid,
            document_type: document_type,
            [Op.or]: [{
                generated_document: {
                    [Op.ne]: null,
                },
            }, {
                final_document: {
                    [Op.ne]: null,
                },
            }, {
                pos_document: {
                    [Op.ne]: null,
                },
            }],
        },
        raw: true,
    });
    if (generatedDocumentObject) {
        generatedDocument = generatedDocumentObject.generated_document;
        finalDocument = generatedDocumentObject.final_document;

        if (generatedDocumentObject.generated_document_s3_key) {
            s3BucketParams.Key = generatedDocumentObject.generated_document_s3_key;
            generatedDocument = await s3.getSignedUrlPromise('getObject', s3BucketParams);
        }
        if (generatedDocumentObject.final_document_s3_key) {
            s3BucketParams.Key = generatedDocumentObject.final_document_s3_key;
            finalDocument = await s3.getSignedUrlPromise('getObject', s3BucketParams);
        }
        if (generatedDocumentObject.final_document_s3_key) {
            final_document_s3_key = generatedDocumentObject.final_document_s3_key;
        }
        if (generatedDocumentObject.pos_document) {
            s3BucketParams.Key = generatedDocumentObject.pos_document_s3_key;
            pos_document = await s3.getSignedUrlPromise('getObject', s3BucketParams);
        }
    }
    response.finalDocument = finalDocument;
    response.finalDocument_s3_file_key = final_document_s3_key;
    response.generatedDocument = generatedDocument;
    response.pos_document = pos_document;
    return response;
};

const legalFormsDetails = async (legalformid, document_type) => {
    const response = {};
    const { Forms, Op, LegalForms } = await connectToDatabase();
    const legalformdatas = await LegalForms.findOne({
        where: {
            document_type: document_type,
            id: legalformid,
        },
        raw: true
    });
    return legalformdatas;
};

const lastSetOfQuestions = async (cases_id, legalforms_ids, client_ids, document_types) => {
    const { Formotp, Forms, Op } = await connectToDatabase();
    try {
        const Formotps = await Formotp.findOne({
            where: {
                case_id: cases_id,
                legalforms_id: legalforms_ids,
                client_id: client_ids,
                document_type: document_types,
            },
            order: [['createdAt', 'DESC']],
            raw: true,
        });
        const response = {};
        if (Formotps) {
            const query = {};
            query.where = {};
            const query2 = {};
            query2.where = {};
            query.where.case_id = cases_id;

            query2.where.case_id = cases_id;
            query.where.legalforms_id = legalforms_ids;
            query2.where.legalforms_id = legalforms_ids;
            query.where.client_id = client_ids;
            query2.where.client_id = client_ids;
            query.where.document_type = document_types;
            query2.where.document_type = document_types;
            query.raw = true;
            query2.raw = true;
            /* query.logging=console.log;
            query2.logging=console.log; */

            if (Formotps.sending_type == 'selected_questions') {
                query.where.client_response_status = { [Op.in]: ['SentToClient', 'ClientResponseAvailable'] };
                query2.where.client_response_status = { [Op.like]: 'ClientResponseAvailable' };
                Formids = Formotps.question_ids;
                query.where.id = { [Op.in]: JSON.parse(Formids) };
                query2.where.id = { [Op.in]: JSON.parse(Formids) };
            } else if (Formotps.sending_type == 'all') {
                query.where.client_response_status = { [Op.in]: ['SentToClient', 'ClientResponseAvailable'] };
                query2.where.client_response_status = { [Op.like]: 'ClientResponseAvailable' };
            }
            /* console.log(query);
            console.log(query2); */
            const QuestionsSentToClientCount = await Forms.count(query);
            const QuestionResponsedQuesCount = await Forms.count(query2);
            response.QuestionsRespondedByClientLastSetofCount = QuestionResponsedQuesCount;
            response.QuestionsSentToClientLastSetofCount = QuestionsSentToClientCount;
        } else {
            response.QuestionsRespondedByClientLastSetofCount = 0;
            response.QuestionsSentToClientLastSetofCount = 0;
        }
        /* console.log(response); */
        return response;
    } catch (err) {
        console.log(err);
    }
};

const UpdateClientAndCase = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const practice_id = event.user.practice_id;
        const { Clients, Cases, Op } = await connectToDatabase();
        const clientData = {};
        if (input?.first_name)clientData.first_name = input.first_name;
        if (input?.last_name)clientData.last_name = input.last_name;
        if (input?.client_phone)clientData.client_phone = input.client_phone;

        if(input.middle_name) { clientData.middle_name = input.middle_name }else{ clientData.middle_name = null }
        const clientObject = Object.assign(clientData);
        const caseData = {
            case_title: input.case_title,
            case_number: input.case_number,
            case_plaintiff_name: input.case_plaintiff_name || null,
            case_defendant_name: input.case_defendant_name || null,
            county: input.county,
            state: input.state
        };
        if (input?.date_of_loss) caseData.date_of_loss = input.date_of_loss;
        const caseObject = Object.assign(caseData);
        if (input.client_id) {
            const userDetails = await Clients.update(clientObject, {
                where: {
                    id: input.client_id,
                    practice_id: practice_id
                }
            });
        }
        if (input.id) {
            const userDetails = await Cases.update(caseObject, {
                where: {
                    id: input.id,
                    client_id: input.client_id,
                    practice_id: practice_id
                }
            });
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'client and case details updated successfully',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update client and case details' }),
        };
    }
}


module.exports.create = middy(create).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
/* module.exports.getAll = middy(getAll).use(authMiddleware()).use(doNotWaitForEmptyEventLoop()); */
module.exports.getAll = getAll;
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getCaseDetails = middy(getCaseDetails).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.cloningCases = cloningCases;
module.exports.getCaseDetailsOptimze = getCaseDetailsOptimze;
module.exports.DuplicateCaseClientCreate = DuplicateCaseClientCreate;
module.exports.UpdateClientAndCase = UpdateClientAndCase;


