const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateCreateCases = function (data) {
    const rules = {
        client_id: 'required|min:4|max:64',
        start_date: 'date',
        case_title: 'required|min:4|max:200',
        case_number: 'required|min:1|max:64',
        status: 'min:3|max:64',
        state: 'required|min:2|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
exports.validateCreateLegalForms = function (data) {
    const rules = {
        practice_id: 'required|min:4|max:64',
        case_id: 'required|min:4|max:64',
        client_id: 'required|min:4|max:64',
        form_id: 'min:4|max:64',
        status: 'required|min:4|max:64',
        filename: 'required',
        document_type: 'required'
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateGetOneCase = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateUpdateCases = function (data) {
    const rules = {
        client_id: 'min:4|max:64',
        start_date: 'date',
        case_number: 'min:1|max:64',
        status: 'min:3|max:64',
        state: 'required|min:2|max:64',
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDeleteCase = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateReadCases = function (data) {
    const rules = {
        offset: 'numeric',
        limit: 'numeric',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateCreateParties = (data) => {
    const rules = {
        case_id: 'required',
        name: 'min:4|max:64|required',
        email: 'email|min:4|max:64',
        phone: 'required|numeric',
        address: 'min:4|max:64',
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
}

exports.validateCreatePartiesForms = (data) => {
    const rules = {
        case_id: 'required',
        party_id: 'required',
        legalforms_id: 'required',
        question_id: 'required',
        document_type: 'required',
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
}

exports.validateCreateClients = function (data) {
    const rules = {
        name: 'min:4|max:64',
        email: 'email|min:4|max:64',
        phone: 'required|numeric',
        address: 'min:4|max:64',
        hipaa_acceptance_status: 'boolean',
        hipaa_acceptance_date: 'date',
        fee_acceptance_status: 'boolean',
        fee_acceptance_date: 'date',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDuplicateCaseClientCreate = function (data) {
    const rules = {
        name: 'min:4|max:64',
        email: 'email|min:4|max:64',
        phone: 'required|numeric',
        address: 'min:4|max:64',
        dob: 'min:4|max:64',
        case_id: 'min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};