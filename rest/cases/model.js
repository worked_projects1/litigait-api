module.exports = (sequelize, type) => sequelize.define('Cases', {
        id: {
            type: type.STRING,
            primaryKey: true,
        },
        client_id: type.STRING,
        practice_id: type.STRING,
        case_title: type.STRING,
        case_number: type.STRING,
        case_from: type.STRING,     // To identify whether the case is created from filevine, mycase or clio.
        integration_case_id: type.STRING, // integrations - filevine, mycase, clio.
        integration_case_data: type.TEXT('long'), // To save the integration case data in JSON.
        claim_number: type.STRING,
        case_plaintiff_name: type.STRING,
        case_defendant_name: type.STRING,
        matter_id: type.STRING,
        status: type.STRING, // (new, active, closed)
        hipaa_acceptance_status: type.BOOLEAN,
        hipaa_acceptance_date: type.DATE,
        fee_acceptance_status: type.BOOLEAN,
        fee_acceptance_date: type.DATE,
        fee_terms: type.TEXT,
        date_of_loss: type.DATE,
        county: type.STRING,
        attorneys: type.TEXT,
        is_deleted: type.BOOLEAN,
        attorney_response_tracking:{
            type: type.BOOLEAN,
            defaultValue: true
        },
        is_archived: type.BOOLEAN,
        federal: type.BOOLEAN,
        federal_district: type.STRING,
        state: type.STRING,
        opposing_counsel: type.TEXT,
        opposing_counsel_info: type.BOOLEAN,
        propounder_opposing_counsel: type.TEXT('long'),
    },
    {
        indexes: [
            {
                name: 'practice_client_id',
                fields: ['practice_id','client_id']
            },
            {
                name: 'createdAt',
                fields: ['createdAt']
            },
            {
                name: 'is_deleted',
                fields: ['is_deleted']
            }
        ]
    }
    );
