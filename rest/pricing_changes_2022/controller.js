const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const { sendEmail } = require('../../utils/mailModule');
const { dateDiffInDays, getCurrentDate } = require('../../utils/timeStamp');
const { QueryTypes, where } = require('sequelize');



const subscriptionPlans = ["monthly", "yearly", "propounding_monthly_199", "propounding_yearly_2199", "responding_monthly_349", "responding_monthly_495", "responding_yearly_3490", "responding_yearly_5100"];
const monthlySubscriptionPlans = ["monthly", "propounding_monthly_199", "responding_monthly_349", "responding_monthly_495"];
const yearlySubscriptionPlans = ["yearly", "propounding_yearly_2199", "responding_yearly_3490", "responding_yearly_5100",];
const free_trial_eligible_plans = ["yearly", "responding_yearly_5100", "responding_yearly_3490"];

const monthly_plan_id = "('price_1IXq8xABxGGVMf06PHYnOP9n', 'price_1Ic43cEaYHv8KnTkI0U1KPNl','price_1Ic43cEaYHv8KnTkI0U1KPNl')";
const monthly_product_id = "('prod_JAATgQHtlC6Tbm', 'prod_JEX7aFVMTuVIuP','prod_JEX7aFVMTuVIuP')";

const yearly_plan_id = "('price_1IXq9WABxGGVMf06FMSyk4QE', 'price_1Ic44MEaYHv8KnTk0bLNYENm','price_1Ic44MEaYHv8KnTk0bLNYENm')";
const yearly_product_id = "('prod_JAAUTbmEF6ZzcN', 'prod_JEX8KCOo2AYgUg','prod_JEX8KCOo2AYgUg')";

const devUsers = ['m.shahidulkareem@rifluxyss.com','siva@miotiv.com','admin@test.com'];


const getAllYearlySubscriptions = async (event) => {
    try {

        const { sequelize, Practices, Op } = await connectToDatabase();

        let sqlQuery = "select Practices.name as practice_name , Practices.id as id, Plans.name as plan_name, Subscriptions.id as subscription_id, "+
        "Subscriptions.subscribed_on, Subscriptions.subscribed_valid_till, Subscriptions.is_subscription_updated, Subscriptions.is_subscription_ended from Subscriptions INNER JOIN Practices ON Subscriptions.practice_id = Practices.id " +
        "INNER JOIN Plans ON Subscriptions.plan_id = Plans.plan_id where Subscriptions.subscribed_valid_till <='2023-12-31 23:59:59' and Subscriptions.plan_id IN " +
        yearly_plan_id + " AND Subscriptions.stripe_product_id IN " + yearly_product_id +" AND Subscriptions.is_subscription_ended is not true order by Subscriptions.subscribed_valid_till asc";

            console.log(sqlQuery);
        const TableDataCount = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(TableDataCount),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(err),
        };
    }
}
const getYearlyRenewalPractice = async (event) => {
    try {
        const { sequelize, Practices, Op } = await connectToDatabase();
        const params = event.params || event.pathParameters;
        let practiceOj;
        let sqlQuery = "select Practices.name as practice_name , Practices.id as id, Plans.name as plan_name, Subscriptions.id as subscription_id, "+
        "Subscriptions.subscribed_on, Subscriptions.subscribed_valid_till from Subscriptions INNER JOIN Practices ON Subscriptions.practice_id = Practices.id " +
        "INNER JOIN Plans ON Subscriptions.plan_id = Plans.plan_id where Subscriptions.subscribed_valid_till <='2023-12-31 23:59:59' and Subscriptions.plan_id IN " +
        yearly_plan_id + " AND Subscriptions.stripe_product_id IN " + yearly_product_id +" AND Subscriptions.practice_id = '"+params.id+"' order by Subscriptions.subscribed_valid_till asc";

            console.log(sqlQuery);
        const TableDataCount = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });
        if(TableDataCount.length) practiceOj = TableDataCount[0];
        else practiceOj = {};
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(practiceOj),
        };
        
    } catch (err) {
        console.log(err);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'subscriptions creation failed .' }),
        };
    }
}
const getAllMonthlySubscriptions = async (event) => {
    try {

        const { sequelize, Practices, Op } = await connectToDatabase();

        let sqlQuery = "select Practices.name as practice_name ,Plans.name as plan_name, Subscriptions.id as subscription_id, "+
        "Subscriptions.subscribed_on, Subscriptions.subscribed_valid_till, Subscriptions.is_subscription_updated, Subscriptions.is_subscription_ended "+
        "from Subscriptions INNER JOIN Practices ON Subscriptions.practice_id = Practices.id " +
        "INNER JOIN Plans ON Subscriptions.plan_id = Plans.plan_id where Subscriptions.subscribed_valid_till <='2023-01-31 23:59:59' and Subscriptions.plan_id IN " +
        monthly_plan_id + " AND Subscriptions.stripe_product_id IN " + monthly_product_id + " AND Subscriptions.is_subscription_ended is not true order by Subscriptions.subscribed_valid_till asc";

        const TableDataCount = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(TableDataCount),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(err),
        };
    }
}
const monthlySubscriptionRenewal = async (event) => {
    try {
        const { sequelize, Subscriptions, Practices, Plans, Users, Settings, Op } = await connectToDatabase();
        const currentYear = new Date().getFullYear();
        const currentDate = new Date();

        const settingsObject = await Settings.findOne({ where: { key: 'global_settings' } });
        const plainSettingsObject = settingsObject.get({ plain: true });
        const settings = JSON.parse(plainSettingsObject.value);

        let practice_created_before = new Date(settings.old_pricing_till).getFullYear();
        let practice_created_after = new Date(settings.new_pricings_from).getFullYear();

        if (new Date().getTime() >= new Date(settings.new_pricings_from).getTime()) {
            console.log('Inside renewal subscription');
            console.log(getCurrentDate());
            const startOfTheDay = getCurrentDate() + ' 00:00:00';
            const endOfTheDay = getCurrentDate() + ' 23:59:59';

            let sqlQuery = "SELECT * FROM Subscriptions WHERE subscribed_valid_till >='" + startOfTheDay + "' AND " +
                "subscribed_valid_till <='" + endOfTheDay + "' AND plan_id IN " + monthly_plan_id + " AND plan_id IN " + monthly_plan_id +
                " AND is_subscription_ended IS NOT TRUE ORDER BY subscribed_valid_till ASC";

            console.log(sqlQuery);
            const TableDatas = await sequelize.query(sqlQuery, { type: QueryTypes.SELECT });

            for (let i = 0; i < TableDatas.length; i++) {
                let row = TableDatas[i];
                let old_subscription_obj = JSON.parse(row.stripe_subscription_data);
                console.log(old_subscription_obj?.status);
                if (old_subscription_obj?.status == 'active') {
                    let practice_id, user_email, usersObj;
                    const practiceObject = await Practices.findOne({ where: { id: row.practice_id }, logging: console.log });

                    const plansObj = await Plans.findOne({ where: { plan_type: 'responding_monthly_349' } });

                    if (old_subscription_obj?.metadata) {
                        if (old_subscription_obj?.metadata?.practice_id) practice_id = old_subscription_obj?.metadata?.practice_id;
                        else practice_id = practiceObject.id;

                        if (old_subscription_obj?.metadata?.user_email) {
                            user_email = old_subscription_obj?.metadata?.user_email;
                            usersObj = await Users.findOne({ where: { email: user_email }, logging: console.log, order: [['createdAt', 'ASC']] },);
                        } else {
                            usersObj = await Users.findOne({
                                where: {
                                    practice_id: practice_id, is_deleted: { [Op.not]: true },
                                    order: [['createdAt', 'ASC']]
                                }
                            });
                            user_email = usersObj.email;
                        }
                    }

                    const createObject = {
                        customer: practiceObject.stripe_customer_id,
                        items: [
                            { price: plansObj.plan_id },
                        ],
                        metadata: {
                            practice_id: practice_id,
                            user_email: user_email,
                            plan_id: plansObj.plan_id,
                            stripe_product_id: plansObj.stripe_product_id,
                        },
                    };
                    const deleted = await stripe.subscriptions.del(row.stripe_subscription_id);
                    const subscription = await stripe.subscriptions.create(createObject);
                    if (subscription && subscription.status && subscription.status === 'incomplete') {
                        await stripe.subscriptions.del(subscription.id);
                        let body = 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card';
                        await sendEmail('aravinthrifluxyss@gmail.com', `Important: AutoRenewal Subscription Failure - ${practiceObject.name}`, body, 'EsquireTek');
                    }
                    const updateColumn = {
                        subscribed_by: usersObj.id, subscribed_on: new Date(), plan_id: plansObj.plan_id,
                        stripe_subscription_id: subscription.id, stripe_subscription_data: JSON.stringify(subscription),
                        stripe_product_id: plansObj.stripe_product_id, plan_category: plansObj.plan_category,
                        subscribed_valid_till: new Date(parseInt(subscription.current_period_end) * 1000)
                    };
                    await Subscriptions.update(updateColumn, { where: { id: row.id } });

                    await sendEmail(
                        'aravinthrifluxyss@gmail.com',
                        `Important: AutoRenewal Subscription Success - ${practiceObject.name}`,
                        ` User Email : ${user_email}<br/>Practice Name : ${practiceObject.name}<br/>Environment : ${process.env.CODE_ENV}<br/>Subscription ID : ${row.id}`, 'EsquireTek'
                    );
                }
            }
        } else {
            console.log('**************** No renewal plans for today ******************');
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'subscriptions created successfully.' }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'subscriptions creation failed .' }),
        };
    }
}
const renewalYearlyPlan = async(event) =>{
    try {
        const { sequelize, Subscriptions, Settings, Practices, Plans, Users, Op } = await connectToDatabase();
        const currentYear = new Date().getFullYear();
        const currentDate = new Date();
        if(!devUsers.includes(event.user.email)) throw new HTTPError(400, `Invalid credentials.`);
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        if(!input?.practice_id) throw new HTTPError(400, `Practice id not found.`);
        const practiceObject = await Practices.findOne({ where: { id: input.practice_id } });
        if(!practiceObject) throw new HTTPError(400, `Invalid Practice id.`);
        const settingsObject = await Settings.findOne({where: { key: 'global_settings'}});
        const plainSettingsObject = settingsObject.get({plain: true});
        const settings = JSON.parse(plainSettingsObject.value);
            
        let practice_created_before = new Date(settings.old_pricing_till).getFullYear(); 
        let practice_created_after = new Date(settings.new_pricings_from).getFullYear();

        if (currentDate.getTime() >= new Date(settings.new_pricings_from).getTime()) {

            console.log('Inside renewal subscription');
            const startOfTheDay = getCurrentDate() + ' 00:00:00';
            const endOfTheDay = getCurrentDate() + ' 23:59:59';

            let sqlQuery = "SELECT * FROM Subscriptions WHERE plan_id IN " + yearly_plan_id +" AND practice_id = '"+input.practice_id+"'"+
                "AND is_subscription_ended IS NOT TRUE ORDER BY subscribed_valid_till ASC";
            console.log(sqlQuery);
            const TableDatas = await sequelize.query(sqlQuery, { type: QueryTypes.SELECT });
            console.log('***********');
            console.log(TableDatas);
            if(TableDatas.length == 0 ) throw new HTTPError(500, `Could not update this practice today.`);
            for (let i = 0; i < TableDatas.length; i++) {
                const row = TableDatas[i];

                const stripeObj = JSON.parse(row.stripe_subscription_data);
                const sub_status = stripeObj.status;
                const sub_cancel_at = stripeObj.cancel_at ? unixTimetoDateconvert(stripeObj.current_period_end) : undefined;
                const sub_current_period_end = unixTimetoDateconvert(stripeObj.current_period_end);
                const current_date = new Date();

                if ((sub_status && !['active', 'trialing'].includes(sub_status)) || (sub_current_period_end && current_date && sub_current_period_end <= current_date && sub_cancel_at && sub_cancel_at <= current_date)) {
                    let updateColumn = { stripe_product_id: null, stripe_subscription_id: null, subscribed_on: null, plan_id: null, subscribed_valid_till: null, };
                    await Subscriptions.update(updateColumn, { where: { practice_id: row.practice_id, id: row.id, } });
                }else{
                    let old_subscription_obj = JSON.parse(row.stripe_subscription_data);
                    let practice_id, user_email, usersObj;

                    const plansObj = await Plans.findOne({ where: { plan_type: 'responding_yearly_3490' } });

                    if (old_subscription_obj?.metadata) {
                        if (old_subscription_obj?.metadata?.practice_id) practice_id = old_subscription_obj?.metadata?.practice_id;
                        else practice_id = practiceObject.id;

                        if (old_subscription_obj?.metadata?.user_email) {
                            user_email = old_subscription_obj?.metadata?.user_email;
                            usersObj = await Users.findOne({ 
                                where: { email: user_email }, 
                                order: [['createdAt', 'ASC']] 
                            });
                        } else {
                            usersObj = await Users.findOne({
                                where: {
                                    practice_id: practice_id, is_deleted: { [Op.not]: true },
                                },
                                order: [['createdAt', 'ASC']]
                            });
                            user_email = usersObj.email;
                        }
                    }

                    const createObject = {
                        customer: practiceObject.stripe_customer_id,
                        items: [
                            { price: plansObj.plan_id },
                        ],
                        metadata: {
                            practice_id: practice_id,
                            user_email: user_email,
                            plan_id: plansObj.plan_id,
                            stripe_product_id: plansObj.stripe_product_id,
                        },
                    };
                    //const deleted = await stripe.subscriptions.del(row.stripe_subscription_id);
                    const subscription = await stripe.subscriptions.create(createObject);
                    if (subscription && subscription.status && subscription.status === 'incomplete') {
                        await stripe.subscriptions.del(subscription.id);
                        let body = 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card';
                        await sendEmail('aravinthrifluxyss@gmail.com', 'Important: AutoRenewal Subscription Failure', body, 'EsquireTek');
                    }

                    const updateColumn = {
                        subscribed_by: usersObj.id, subscribed_on: new Date(), plan_id: plansObj.plan_id,
                        stripe_subscription_id: subscription.id, stripe_subscription_data: JSON.stringify(subscription),
                        stripe_product_id: plansObj.stripe_product_id, plan_category: plansObj.plan_category,
                        subscribed_valid_till: new Date(parseInt(subscription.current_period_end) * 1000)
                    };
                    await Subscriptions.update(updateColumn, { where: { id: row.id } });

                    await sendEmail('aravinthrifluxyss@gmail.com', `Important: AutoRenewal Subscription Success - ${practiceObject.name}`,
                        ` User Email : ${user_email}<br/>Practice Name : ${practiceObject.name}<br/>Environment : ${process.env.CODE_ENV}<br/>Subscription ID : ${row.id}`, 'EsquireTek');
                }
            }
        } else {
            console.log('inside else');
            console.log('**************** No renewal plans for today ******************');
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({status: 'ok', message: 'subscriptions created successfully.'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'subscriptions creation failed .' }),
        };
    }

}


// const monthlySubscriptionStatusUpdate = async (event) => {
//     try {
//         const { Plans, Subscriptions, Op, Settings } = await connectToDatabase();

//         const getcurrentSubscription = await Subscriptions.findAll({
//             where: { plan_category: 'responding', stripe_subscription_data: { [Op.not]: null }, is_subscription_ended: true, },
//             logging: console.log, order: [["createdAt", "DESC"]], raw: true
//         });
// /*         console.log('length : '+getcurrentSubscription.length);
//         for (let i = 0; i < getcurrentSubscription.length; i++) {
//                 const json_data = JSON.parse(getcurrentSubscription[i].stripe_subscription_data);    
//                 console.log('Id : -> '+getcurrentSubscription[i].id);
//                 console.log('status : -> '+json_data.status);
//                 if(json_data.status!='canceled'){
//                     console.log('*****note*****');
//                 }
//         } */
//         /* console.log('getcurrentSubscription : -> ' + getcurrentSubscription.length); */
// for (let i = 0; i < getcurrentSubscription.length; i++) {
//     console.log('i : -> ' + i + 1);
//     const activeSubscription = await Subscriptions.findOne({ where: { id: getcurrentSubscription[i].id }, raw: true });
//     const json_data = JSON.parse(activeSubscription.stripe_subscription_data);
//     const subscription = await stripe.subscriptions.retrieve(json_data.id);
//     if (subscription?.id) {
//         const getActivePlan = await Plans.findOne({ where: { stripe_product_id: subscription.plan.product }, raw: true });
//         const updateColumn = {
//             subscribed_on: new Date(parseInt(subscription.current_period_start) * 1000),
//             plan_id: subscription.plan.id,
//             stripe_subscription_id: subscription.id,
//             subscribed_valid_till: new Date(parseInt(subscription.current_period_end) * 1000),
//             stripe_subscription_data: JSON.stringify(subscription),
//             stripe_product_id: subscription.plan.product,
//             plan_category: getActivePlan.plan_category,
//             is_subscription_updated: true
//         }
//         if (subscription.status != 'active' || subscription.cancel_at_period_end) {
//             updateColumn.is_subscription_ended = true;
//         }
//         await Subscriptions.update(updateColumn, { where: { id: activeSubscription.id } });
//     }
// }

//         return {
//             statusCode: 200,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': true
//             },
//             body: JSON.stringify({ status: 'ok', message: 'subscriptions success.' }),
//         };
//     } catch (err) {
//         console.log(err);
//         return {
//             statusCode: 500,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': true
//             },
//             body: JSON.stringify({ status: 'ok', message: 'subscriptions updates failed .' }),
//         };
//     }
// }

/* const manualSubscriptionForMonthly = async (event) =>{
    try {
        const { Plans, Subscriptions, Op, Settings } = await connectToDatabase();

        let createObject = {
            customer: 'cus_JJ1QeqJrciFEde',
            items: [
                { price: 'price_1MKFU5EaYHv8KnTk8Wc9HCWh' },
            ],
            metadata: {
                practice_id: '4607f557-778b-4d4c-ac68-64ce907685f9',
                user_email: 'james@hannlawfirm.com',
                plan_id: 'price_1MKFU5EaYHv8KnTk8Wc9HCWh',
                stripe_product_id: 'prod_N4OGeQbopE95Gj',
            },
        };

        const subscription = await stripe.subscriptions.create(createObject);
        console.log('subscription');
        const subscription_obj = JSON.stringify(subscription);
        let cx = await Subscriptions.update(
            { stripe_subscription_data: subscription_obj }, 
            { where: { practice_id: '4607f557-778b-4d4c-ac68-64ce907685f9', id: '37439312-eebe-46c3-aaef-704230e10646' } }
            );
            console.log(cx);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'subscriptions updated .' }),
        };    
    } catch (err) {
        console.log(err);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'subscriptions failed .' }),
        };
    }
} */

// const updateBrockmeierHistories = async() =>{
//     try {
//         const { Plans, Subscriptions, Op, Settings, SubscriptionHistory } = await connectToDatabase();
//         const subscriptionHistoryObj = await SubscriptionHistory.findAll({where:{practice_id:'7b2fef98-ad19-4a86-94f0-15b0127cde23'}, raw: true});
//         for (let i = 0; i < subscriptionHistoryObj.length; i++) {
//             const row = subscriptionHistoryObj[i];
//             const stripe_subscription_data = JSON.parse(row.stripe_subscription_data);
//             const getPlan = await Plans.findOne({where:{plan_id:stripe_subscription_data.plan.id}});
//             let update_columns = {
//                 plan_type:'',
//                 plan_id:'',
//                 stripe_subscription_id:'',
//                 price:'',   
//                 stripe_product_id:''
//             };
//             update_columns.plan_type = getPlan.plan_type;
//             update_columns.price = getPlan.price;
//             update_columns.plan_id = getPlan.plan_id;
//             update_columns.stripe_subscription_id = row.stripe_subscription_id;
//             update_columns.stripe_product_id = getPlan.stripe_product_id;
//             console.log(update_columns);
//             console.log('**************');
//             await SubscriptionHistory.update(update_columns,{where:{id:row.id}});
//         }
//         return {
//             statusCode: 200,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': true
//             },
//             body: JSON.stringify({ status: 'ok', message: 'subscriptions updated .' }),
//         };   
//     } catch (err) {
//         console.log(err);
//         return {
//             statusCode: 200,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': true
//             },
//             body: JSON.stringify({ status: 'ok', message: 'subscription updates failed .' }),
//         };
//     }
// }

const createSubscriptionHistoryManual = async(event)=>{
    try {
        let body = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    
        event.type = 'customer.subscription.updated';
        const propounding_plans = ['propounding_monthly_199','propounding_yearly_2199'];
        const monthly_plans = ['monthly','responding_monthly_495','responding_monthly_349','propounding_monthly_199'];
        const yearly_plans = ['yearly','responding_yearly_5100','responding_yearly_3490','propounding_yearly_2199'];
        const { Op, Users, Plans, Practices, SubscriptionHistoryWebhook, SubscriptionHistory, Subscriptions,
            DiscountCode, DiscountHistory } = await connectToDatabase();

        const customer_id = body.object.customer;
        const practicesobj = await Practices.findOne({ where: { stripe_customer_id: customer_id, is_deleted: { [Op.not]: true } }, raw: true });
        if(!practicesobj) throw new HTTPError(404, 'Invalid stripe customer id '+customer_id); 
        const usersobj = await Users.findOne({
            where: {
                practice_id: practicesobj.id, is_deleted: { [Op.not]: true }, role:{[Op.in]:['paralegal', 'lawyer']}, is_admin: true
            }, order: [['createdAt', 'ASC']], raw: true ,logging:console.log
        });

        let planObject;
        if (body.object.metadata.plan_id) {
            planObject = await Plans.findOne({ where: { plan_id: body.object.metadata.plan_id }, raw: true, logging: console.log });
        } else {
            planObject = await Plans.findOne({ where: { stripe_product_id: body.object.plan.product }, raw: true, logging: console.log });
        }

        const currentSubscriptionObj = await SubscriptionHistory.findOne({ where: { practice_id: practicesobj.id, plan_category: planObject.plan_category }, order: [['createdAt', 'DESC']], raw: true, logging:console.log });
        
        let subscriptionValidityStart = new Date(parseInt(body.object.current_period_start) * 1000);
        let subscriptionValidity = new Date(parseInt(body.object.current_period_end) * 1000);
        let latest_actvity, switchedPlan, esquiretek_activity_type, payment_type;
        let status = 'Success';
        if (event.type == 'customer.subscription.created') {
            payment_type = 'New';
        } else if (event.type == 'customer.subscription.updated') {
            payment_type = 'Renewal';
            if (currentSubscriptionObj?.plan_type && ((monthly_plans.includes(currentSubscriptionObj.plan_type) && monthly_plans.includes(planObject.plan_type)) ||
                (yearly_plans.includes(currentSubscriptionObj.plan_type) && yearly_plans.includes(planObject.plan_type)))) {
                payment_type = 'Renewal';
            } else {
                payment_type = 'New';
            }
        } else if (event.type == 'customer.subscription.deleted') {
            payment_type = 'Subscription Canceled';
            subscriptionValidity = new Date();
        }
        if (body.object.cancel_at_period_end == true && body.object.canceled_at && body.object.cancel_at && body.object.status == "active") {
            latest_actvity = 'downgrade';
            payment_type = 'Subscription Canceled';
            switchedPlan = 'subscription_cancel';
            esquiretek_activity_type = 'DOWNGRADE';
        } else {
            if (body.object.metadata.plan_id) {
                if (body.object.metadata.latest_actvity) {
                    latest_actvity = body.object.metadata.latest_actvity;
                }
            }
            switchedPlan = planObject.plan_type;
            const historyCount = await SubscriptionHistory.count({where: {practice_id: practicesobj.id}});
            if (historyCount <= 0) {
                esquiretek_activity_type = 'NEW_SUBSCRIPTION';
            } else {
                esquiretek_activity_type = 'UPGRADED';
            }
        }
        if (body.object.status != "active") {
            esquiretek_activity_type = undefined;
        }

        if (event.type == 'customer.subscription.created' && body.object.status != "active") {
            status = "Payment Failed";
            latest_actvity = 'Subscription Creation';
            payment_type = 'Failed';
            esquiretek_activity_type = 'UPGRADE';
        }
        if (event.type == 'customer.subscription.updated' && body.object.status != "active") {
            status = "Subscription Canceled";
            payment_type = 'Failed';
            esquiretek_activity_type = 'UPGRADE';
        }

        let cancel_at, canceled_at;
        if (body.object.cancel_at != null) cancel_at = new Date(parseInt(body.object.cancel_at) * 1000);
        if (body.object.canceled_at != null) canceled_at = new Date(parseInt(body.object.canceled_at) * 1000);
       
        let price = planObject.price;
        let discountCodeObj, discount_percentage, discount_price, discount_code;
        if ((event.type == 'customer.subscription.created' || event.type == 'customer.subscription.updated') &&
        body?.object?.discount?.coupon?.id && body?.object?.status == 'active'){
                discount_code = body.object.discount.coupon.name;
                discountCodeObj = await DiscountCode.findOne({where:{discount_code:discount_code}});
                discount_percentage = discountCodeObj.discount_percentage;
                const percentage = 1 - (discount_percentage/100);
                discount_price = price = (percentage * price).toFixed(2);
        }else if(event.type == 'customer.subscription.deleted'){
            price = currentSubscriptionObj.price;
        }
        console.log(usersobj.id);
        console.log(practicesobj.id);
        
        const subscriptionHistoryData = {
            id: uuid.v4(), order_date: new Date(), practice_id: practicesobj.id, user_id: usersobj.id,
            subscribed_by: usersobj.name, price: price, plan_type: planObject.plan_type, subscribed_on: new Date(), plan_id: planObject.plan_id,
            stripe_subscription_id: body.object.id, subscription_valid_start: subscriptionValidityStart, subscription_valid_till: subscriptionValidity,
            stripe_subscription_data: JSON.stringify(body.object), cancel_at: cancel_at, cancel_at_period_end: body.object.cancel_at_period_end,
            canceled_at: canceled_at, esquiretek_activity_type: esquiretek_activity_type, payment_type: payment_type, switched_plan: switchedPlan,
            stripe_latest_activity: latest_actvity, stripe_product_id: planObject.stripe_product_id, status: status, event_type: event.type,
            plan_category: planObject.plan_category
        };
        await SubscriptionHistory.create(subscriptionHistoryData);
        await SubscriptionHistoryWebhook.create(subscriptionHistoryData);

        // if(body?.object?.discount?.coupon?.id && body?.object?.status == 'active' && !body?.object?.cancel_at_period_end){
        //     let plan_type = planObject.plan_type.split('_').slice(0,planObject.plan_type.length-1);
        //     let discount_for = plan_type.slice(0,plan_type.length-1).join('_').toUpperCase();
        //     let subscriptionObj = await Subscriptions.findOne({
        //         where:{practice_id: practicesobj.id,plan_category:planObject.plan_category},
        //         order:[['createdAt','DESC']]
        //     });
        //     let discount_history = {
        //         id: uuid.v4(), practice_id: practicesobj.id, subscription_id: subscriptionObj.id, plan_category: planObject.plan_category,
        //         discount_for, promocode: discountCodeObj.name, discount_percentage, actual_price: planObject.price, discount_price,
        //     }
        //     console.log(discount_history);
        //     await DiscountHistory.create(discount_history);
        // }
        /* Subscribe Propounding Plans */
        // const checkPropoundingSubscription = await Subscriptions.findOne({where:{practice_id: practicesobj.id,plan_category:'propounding'},raw:true});
        // if(checkPropoundingSubscription && checkPropoundingSubscription.stripe_subscription_id.startsWith('pi_') && !practicesobj.is_propounding_canceled)
        // {
        //     let propoundingPlan;
        //     if(planObject?.plan_type && ['monthly','responding_monthly_349','responding_monthly_495'].includes(planObject.plan_type)) propoundingPlan = 'propounding_monthly_199';
        //     if(planObject?.plan_type && ['yearly','responding_yearly_5100','responding_yearly_3490'].includes(planObject.plan_type)) propoundingPlan = 'propounding_yearly_2199';
        //     const propoundingPlanObj = await Plans.findOne({ where: { plan_type: propoundingPlan, active: true } });

        //     let subscriptionCreateObj = {
        //         customer: practicesobj.stripe_customer_id,
        //         items: [
        //             { price: propoundingPlanObj.plan_id },
        //         ],
        //         metadata: {
        //             practice_id: practicesobj.id,
        //             user_email: usersobj.email,
        //             plan_id: propoundingPlanObj.plan_id,
        //             stripe_product_id: propoundingPlanObj.stripe_product_id,
        //         },
        //     }
        //     if (body?.object?.discount?.coupon?.name) {
        //         const propound_discountObj = await DiscountCode.findOne({ where: { discount_code: discount_code }, raw: true });
        //         if (propound_discountObj?.id) {
        //             const is_eligible_for = propound_discountObj.plan_type.split(',');
        //             if (is_eligible_for.includes(propoundingPlanObj.plan_type)) {
        //                 let stripe_couponcode_obj = JSON.parse(propound_discountObj.stripe_obj);
        //                 subscriptionCreateObj.coupon = stripe_couponcode_obj.id;
        //             }
        //         }
        //     }
        //     console.log(subscriptionCreateObj);
        //         const propoundingSubscription = await stripe.subscriptions.create(subscriptionCreateObj);
        //         if (propoundingSubscription?.status === 'incomplete') {
        //             await stripe.subscriptions.del(propoundingSubscription.id);
        //         }
        //         const subscriptionValidity = new Date(parseInt(propoundingSubscription.current_period_end) * 1000);
        //         const updateColumn = {
        //             subscribed_on: new Date(), plan_id: propoundingPlanObj.plan_id, stripe_subscription_id: propoundingSubscription.id,
        //             subscribed_valid_till: subscriptionValidity, stripe_subscription_data: JSON.stringify(propoundingSubscription),
        //             stripe_product_id: propoundingPlanObj.stripe_product_id, plan_category: propoundingPlanObj.plan_category
        //         };
        //         await Subscriptions.update(updateColumn, { where: {id: checkPropoundingSubscription.id}});
        // }

        return;
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}

const unixTimetoDateconvert = (timestamp) => {
    const unixTimestamp = timestamp;
    const date = new Date(unixTimestamp * 1000);
    return date;
};

const checkActiveCanceledSubscriptions = async(event) => {
    try {
        const { sequelize, Subscriptions, SubscriptionHistory,Practices, Plans, Users, Settings, Op } = await connectToDatabase();
        
        const subscriptionDetails = await Subscriptions.findAll({where:{plan_id:{[Op.not]:null},
        stripe_product_id:{[Op.not]:null},stripe_subscription_id:{[Op.not]:null}},logging:console.log});
        const updateColumns = { 
            stripe_product_id: null, stripe_subscription_id: null, 
            subscribed_on: null, plan_id: null, 
            subscribed_valid_till: null, 
        };
        for(let i =0; i < subscriptionDetails.length;i++){
            /* Compare Active and History */
            const activesub = subscriptionDetails[i];
            if (activesub.stripe_subscription_id.startsWith('sub_')) {
                let stripeObj = JSON.parse(activesub.stripe_subscription_data);
                let sub_status = stripeObj.status;
                let sub_cancel_at = stripeObj.cancel_at ? unixTimetoDateconvert(stripeObj.current_period_end) : undefined;
                let sub_current_period_end = unixTimetoDateconvert(stripeObj.current_period_end);
                let current_date = new Date();

                if ((sub_status && !['active', 'trialing'].includes(sub_status)) || (sub_current_period_end && current_date && sub_current_period_end <= current_date && sub_cancel_at && sub_cancel_at <= current_date)) {
                    console.log('practice id : '+activesub.practice_id+' i : '+i);
                    let updateColumn = { stripe_product_id: null, stripe_subscription_id: null, subscribed_on: null, plan_id: null, subscribed_valid_till: null, };
                    await Subscriptions.update(updateColumn, { where: { practice_id: activesub.practice_id, id: activesub.id, } });
                }
            }

/*   if (activesub && activesub.stripe_subscription_id && activesub.stripe_subscription_id.startsWith('sub_')) {
                const subscriptionObj = await Subscriptions.findOne({ where: { id: activesub.id } ,raw:true});
                const historyCount = await SubscriptionHistory.count({where:{practice_id:subscriptionObj.practice_id,
                    plan_id:subscriptionObj.plan_id,stripe_subscription_id:subscriptionObj.stripe_subscription_id}});
                
                    console.log(' i : '+ i +' count : '+historyCount+' practice_id : '+subscriptionObj.practice_id);
                    if(historyCount<0){
                    console.log(subscriptionObj.practice_id);
                }    
            } */
            /* Delete Plans */
            // console.log(i);
            // const activesub = subscriptionDetails[i];
            // if (activesub && activesub.stripe_subscription_id && activesub.stripe_subscription_id.startsWith('sub_')) {
            //     let subscription;
            //     const subscriptionObj = await Subscriptions.findOne({ where: { id: activesub.id } });
            //     try {
            //         subscription = await stripe.subscriptions.retrieve(activesub.stripe_subscription_id);    
            //     } catch (e) {
            //         console.log('********inside catch*********');
            //         console.log(activesub.id);
            //         await Subscriptions.update(updateColumns, { where: { id: activesub.id } });
            //         continue;
            //     }
            //     console.log('after catch');
                
            //     let getActivePlan = await Plans.findOne({ where: { stripe_product_id: subscription.plan.product }, raw: true });
            //     if(getActivePlan){
            //         const updateColumn = {
            //             stripe_subscription_data: JSON.stringify(subscription),
            //             subscribed_valid_till: new Date(parseInt(subscription.current_period_end) * 1000),
            //             subscribed_on: new Date(parseInt(subscription.current_period_start) * 1000),
            //             plan_id: subscription.plan.id,
            //             stripe_product_id: subscription.plan.product,
            //             plan_category: getActivePlan.plan_category
            //         }
            //         await Subscriptions.update(updateColumn, { where: { id: activesub.id } });
            //     }else{
            //         console.log('************* else update ****************');
            //         await Subscriptions.update(updateColumns, { where: { practice_id: subscriptionObj.practice_id, id: subscriptionObj.id, } });
            //         continue;
            //     }
            //     if (subscriptionObj.stripe_subscription_id.startsWith('sub_')) {
            //         let stripeObj = JSON.parse(subscriptionObj.stripe_subscription_data);
            //         let sub_status = stripeObj.status;
            //         let sub_cancel_at = stripeObj.cancel_at ? unixTimetoDateconvert(stripeObj.current_period_end) : undefined;
            //         let sub_current_period_end = unixTimetoDateconvert(stripeObj.current_period_end);
            //         let current_date = new Date();
        
            //         if ((sub_status && !['active', 'trialing'].includes(sub_status)) || (sub_current_period_end && current_date && sub_current_period_end <= current_date && sub_cancel_at && sub_cancel_at <= current_date)) {
            //             console.log('************* final update ****************');
            //             await Subscriptions.update(updateColumns, { where: { practice_id: subscriptionObj.practice_id, id: subscriptionObj.id, } });
            //         }else{
            //             // const historu = await SubscriptionHistory.count({where:{practice_id:subscriptionObj.practice_id}})
            //         }
            //     }
            // }

        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'subscriptions updated successfully.' }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'subscription updates failed .' }),
        };
    }
}
module.exports.getAllYearlySubscriptions = getAllYearlySubscriptions;
module.exports.getAllMonthlySubscriptions = getAllMonthlySubscriptions;
module.exports.monthlySubscriptionRenewal = monthlySubscriptionRenewal;
module.exports.renewalYearlyPlan = renewalYearlyPlan;
module.exports.getYearlyRenewalPractice = getYearlyRenewalPractice;
module.exports.createSubscriptionHistoryManual = createSubscriptionHistoryManual;
module.exports.checkActiveCanceledSubscriptions = checkActiveCanceledSubscriptions;
// module.exports.updateBrockmeierHistories = updateBrockmeierHistories;
/* module.exports.manualSubscriptionForMonthly = manualSubscriptionForMonthly; */  