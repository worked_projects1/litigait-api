const {HTTPError} = require('../../utils/httpResp');

const internalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const superAdminRole = ['superAdmin'];
const normalUserRole = ['superAdmin', 'manager', 'operator'];
const practiceRoles = ['lawyer', 'paralegal'];
const litigaitRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician', 'lawyer', 'paralegal']

exports.authorizeCreate = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetOne = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};
exports.authorizeGetAll = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdate = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroy = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeResetPassword = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeChangePassword = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeCheckSubscriptionValidation = function (user) {
    if (!normalUserRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdateSignature = function (user) {
    if (!practiceRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetAllUsersForDashboard = function (user) {
    if (!normalUserRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdateNameAndStateBarNumber = function (user) {
    if (!litigaitRoles.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};
