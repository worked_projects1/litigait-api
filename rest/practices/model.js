module.exports = (sequelize, type) => sequelize.define('Practices', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    name: type.STRING,
    address: type.STRING,
    street: type.STRING,
    city: type.STRING,
    state: type.STRING,
    zip_code: type.STRING,
    logoFile: type.STRING,
    stripe_customer_id: type.STRING,
    phone: type.STRING,
    fax: type.STRING,
    is_deleted: type.BOOLEAN,
    objections: type.BOOLEAN,
    one_time_activation_fee: type.BOOLEAN, // true - have to pay amount, false - payment completed or no need to pay
    is_yearly_free_trial_available : type.BOOLEAN, // 0 - no , 1 - yes
    is_propounding_canceled : type.BOOLEAN,
    global_attorney_response_tracking: type.BOOLEAN,
},
{
    indexes: [
        {
            name: 'is_deleted',
            fields: ['is_deleted']
        },
        {
            name: 'createdAt',
            fields: ['createdAt']
        },
        {
            name: 'state',
            fields: ['state']
        }
    ]
});
