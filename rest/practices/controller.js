const uuid = require('uuid');
const middy = require('@middy/core');
const moment = require('moment');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const AWS = require('aws-sdk');

AWS.config.update({region: process.env.REGION || 'us-east-1'});
const s3 = new AWS.S3({useAccelerateEndpoint: true});
/* const s3 = new AWS.S3(); */
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const authMiddleware = require('../../auth');
const {
    validateCreatePractices,
    validateGetOnePractice,
    validateUpdatePractices,
    validateDeletePractice,
    validateReadPractices,
} = require('./validation');

const { authorizeAdminGetFiles } = require('./authorize');
const { sendEmailwithCC, sendEmail } = require('../../utils/mailModule');
const { generateRandomString } = require('../../utils/randomStringGenerator');
const { QueryTypes } = require('sequelize');
const { dateDiffInDays } = require('../../utils/timeStamp');
const { customDateandTime } = require('../../utils/datetimeModule');

const create = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, {id: id || uuid.v4()});
        validateCreatePractices(dataObject);
        const {Practices, AdminObjections, CustomerObjections, Settings, Op} = await connectToDatabase();
        let current_date = new Date();
        let current_year = current_date.getFullYear();
        
        const settingsObject = await Settings.findOne({where: { key: 'global_settings'}});
        const plainSettingsObject = settingsObject.get({plain: true});
        const settings = JSON.parse(plainSettingsObject.value);
        
        let practice_created_before = new Date(settings.old_pricing_till).getFullYear(); 
        let practice_created_after = new Date(settings.new_pricings_from).getFullYear();

        const practices = await Practices.create(dataObject);
        const allAdminObjections = await AdminObjections.findAll({raw: true});
        const processedObjections = [];
        for (let i = 0; i < allAdminObjections.length; i += 1) {
            const objectionItem = allAdminObjections[i];
            objectionItem.practice_id = practices.id;
            delete objectionItem.created_by_admin_id;
            objectionItem.id = uuid.v4();
            processedObjections.push(objectionItem);
        }
        if (processedObjections.length) {
            await CustomerObjections.bulkCreate(processedObjections);
        }

        
            let createdDate = moment(practices.createdAt).format('MM-DD-YYYY HH:mm:ss');
            let template = `New Firm Account Created For ${practices.name}`
            await sendEmail(process.env.PRACTICE_EMAIL_RECEIPIENT,template,`
            Practice Name: ${practices.name}<br/>
            Plan Type: FREE TRIAL <br/>
            Created Date: ${createdDate}
            `);
        
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(practices),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the practices.'}),
        };
    }
};

const getOne = async (event) => {
    try {
        validateGetOnePractice(event.pathParameters);
        const { Practices, PracticesTemplates, Subscriptions, Plans, DiscountCode, Op } = await connectToDatabase();
        const practices = await Practices.findOne({ where: { id: event.pathParameters.id } });
        const practiceplain = practices.get({ plain: true });
        if (!practices) throw new HTTPError(404, `Practices with id: ${event.pathParameters.id} was not found`);
        const templateData = await PracticesTemplates.findOne({ where: { practice_id: event.pathParameters.id }, raw: true });
        let subscriptionDetails = [];
        const activeSubscriptionObj = await Subscriptions.findAll({ where: { practice_id: practiceplain.id }, raw: true, order: [['createdAt', 'DESC']] });
        for (let i = 0; i < activeSubscriptionObj.length; i++) {
            let subscriptionObj = activeSubscriptionObj[i];
            subscriptionObj.email = event.user.email;
            let details = await getSubscriptionDetails(subscriptionObj);
            subscriptionDetails.push(details);
        }

        practiceplain.subscription = subscriptionDetails;
        if (templateData) {
            practiceplain.status = templateData.status;
            practiceplain.custom_template = templateData.custom_template;
            practiceplain.custom_template_id = templateData.id;
        }
        practiceplain.propounding_price = {};
        practiceplain.responding_price = {};

        const subscriptionDeatils = await Subscriptions.findOne({
            where: {
                practice_id: event.pathParameters.id, plan_category: 'responding', plan_id: { [Op.not]: null }
            }, raw: true, logging: console.log
        });
        if (subscriptionDeatils && subscriptionDeatils.stripe_subscription_id) {
            let discountCodeDetails, is_eligible_for, type, actual_price, discount_price, discount_percentage, propounding_plan, actual_discount_price;

            let respondingPlan = await Plans.findOne({ where: { plan_id: subscriptionDeatils.plan_id },logging: console.log });
            if(!respondingPlan) throw new HTTPError(400, `Invalid subscriptions plan id`);
            const subscriptionStripeObj = JSON.parse(subscriptionDeatils.stripe_subscription_data);

            if (subscriptionStripeObj?.discount?.coupon?.id) {
                discountCodeDetails = await DiscountCode.findOne({
                    where: { discount_code: subscriptionStripeObj.discount.coupon.name }, raw: true, logging: console.log,
                });
                is_eligible_for = discountCodeDetails.plan_type.split(",");
                if (respondingPlan?.plan_type) {
                    discount_percentage = 1 - discountCodeDetails.discount_percentage / 100;
                    actual_discount_price = discount_price = (discount_percentage * respondingPlan.price).toFixed(2);

                    practiceplain.responding_price.type = respondingPlan?.plan_type;
                    practiceplain.responding_price.actual_price = respondingPlan?.price;
                    practiceplain.responding_price.discount_price = discount_price;
                    practiceplain.responding_price.actual_discount_price = actual_discount_price;
                    practiceplain.responding_price.discount_percentage = discount_percentage;
                }
            } else {
                practiceplain.responding_price.type = respondingPlan?.plan_type;
                practiceplain.responding_price.actual_price = respondingPlan?.price;
                practiceplain.responding_price.discount_price = respondingPlan?.price;
                practiceplain.responding_price.actual_discount_price = respondingPlan?.price;
                practiceplain.responding_price.discount_percentage = 0;
            }
            if (['responding_monthly_495', 'monthly', 'responding_monthly_349'].includes(respondingPlan.plan_type)) propounding_plan = 'propounding_monthly_199';
            if (['responding_yearly_5100', 'yearly', 'responding_yearly_3490'].includes(respondingPlan.plan_type)) propounding_plan = 'propounding_yearly_2199';

            const getPropoundingPlan = await Plans.findOne({ where: { plan_type: propounding_plan, active: { [Op.not]: false } } });
            const monthlyPropounding = await Plans.findOne({where:{plan_type:'propounding_monthly_199',active:true},raw:true});
            const yearlyPropounding = await Plans.findOne({where:{plan_type:'propounding_yearly_2199',active:true},raw:true});
            // if (!getPropoundingPlan) throw new HTTPError(400, `Invalid plan detail. please check`);

            if( monthlyPropounding?.id || yearlyPropounding?.id){
                
                const monthly_days = monthlyPropounding.validity;
                const yearly_days = yearlyPropounding.validity;
                const monthly_price = monthlyPropounding.price;
                const yearly_price = yearlyPropounding.price;
                const current_date = new Date();
                const responding_start_date = new Date(subscriptionDeatils.subscribed_on);
                let days_difference = dateDiffInDays(current_date, responding_start_date);
                let subscription_price = 0;

                if (is_eligible_for && getPropoundingPlan?.plan_type &&is_eligible_for.includes(getPropoundingPlan?.plan_type)) {
                    discount_percentage = discountCodeDetails.discount_percentage;
                    const percentage = 1 - (discountCodeDetails.discount_percentage / 100);
                    if (['propounding_monthly_199', 'propounding_yearly_2199'].includes(getPropoundingPlan.plan_type) && (days_difference == 0 || days_difference == 0)) {
                        let discount_amount  = (percentage * getPropoundingPlan.price).toFixed(2);
                        discount_price = discount_amount = actual_discount_price = discount_amount.toString();
                        actual_price = getPropoundingPlan.price;
                    } else if (getPropoundingPlan.plan_type == 'propounding_monthly_199' && days_difference > 0) {
                        days_difference = monthly_days - days_difference;
                        actual_price = Math.ceil((monthly_price / monthly_days) * days_difference);
                        discount_price = subscription_price = Math.ceil(actual_price * percentage);
                        actual_discount_price = (percentage * getPropoundingPlan.price).toFixed(2).toString();
                    } else if (getPropoundingPlan.plan_type == 'propounding_yearly_2199' && days_difference > 0) {
                        days_difference = yearly_days - days_difference;
                        actual_price = Math.ceil((yearly_price / yearly_days) * days_difference);
                        discount_price = subscription_price = Math.ceil(actual_price * percentage);
                        actual_discount_price = (percentage * getPropoundingPlan.price).toFixed(2).toString();
                    }
                    if (getPropoundingPlan?.plan_type) {
                        practiceplain.propounding_price.type = getPropoundingPlan.plan_type;
                        practiceplain.propounding_price.actual_price = actual_price;
                        practiceplain.propounding_price.discount_price = discount_price;
                        practiceplain.propounding_price.discount_percentage = discount_percentage;
                        practiceplain.propounding_price.days_difference = days_difference;
                        practiceplain.propounding_price.actual_discount_price = actual_discount_price;
                    };
                } else {
                    console.log('inside else');
                    if (['responding_monthly_495', 'monthly', 'responding_monthly_349', 'responding_yearly_5100', 'yearly', 'responding_yearly_3490'].includes(getPropoundingPlan.plan_type) && days_difference == 0) {
                        console.log('inside else first if');
                        actual_price = discount_price = actual_discount_price= getPropoundingPlan.price;
                        discount_percentage = 0;
                    } else if (['responding_monthly_495', 'monthly', 'responding_monthly_349'].includes(respondingPlan.plan_type)) {
                        console.log('inside else second if');
                        days_difference = monthly_days - days_difference;
                        discount_price = actual_price = actual_discount_price = Math.ceil((getPropoundingPlan.price / getPropoundingPlan.validity) * days_difference);
                        discount_percentage = 0;
                    } else if (['responding_yearly_5100', 'yearly', 'responding_yearly_3490'].includes(respondingPlan.plan_type)) {
                        console.log('inside else third if');
                        days_difference = yearly_days - days_difference;
                        console.log('getPropoundingPlan.price : '+getPropoundingPlan.price);
                        console.log('getPropoundingPlan.validity : '+getPropoundingPlan.validity);
                        console.log('days_difference : '+days_difference);
                        console.log(Math.ceil((getPropoundingPlan.price / getPropoundingPlan.validity) * days_difference));
                        discount_price = actual_price = actual_discount_price = Math.ceil((getPropoundingPlan.price / getPropoundingPlan.validity) * days_difference);
                        discount_percentage = 0;
                    }
                    if (getPropoundingPlan?.plan_type) {
                        practiceplain.propounding_price.type = getPropoundingPlan.plan_type;
                        practiceplain.propounding_price.actual_price = actual_price;
                        practiceplain.propounding_price.discount_price = discount_price;
                        practiceplain.propounding_price.discount_percentage = discount_percentage;
                        practiceplain.propounding_price.days_difference = days_difference;
                        practiceplain.propounding_price.actual_discount_price = actual_discount_price;
                    };
                }
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(practiceplain),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Practices.' }),
        };
    }
};
/* list all optimize start */
const getAll_new = async (event) => {
    try {
        const {sequelize, PracticesTemplates, Subscriptions} = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let sortQuery = '';
        let filterQuery = '';
        let searchQuery = '';
        let last_login_ts;
        let users_where = '';
        const query = event.headers;
        if (!query.offset && !query.limit) {
            let practicessRes = getallforClients(event);
            return practicessRes;
        }
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;

        let codeSnip = '';
        let codeSnip2 = '';
        let where = `WHERE is_deleted IS NOT true`;
        /**Filter**/
        if (query.filter != 'false') {
            query.filter = JSON.parse(query.filter);
            filterKey.type = query.filter.plan_type;
            if (filterKey.type != 'all' && filterKey.type != '' && filterKey.type) {
                filterQuery += ` AND plan_type LIKE '%${filterKey.type}%'`;
            }
        }
        /** This part only work's for DashBoard **/
/*         if( event?.query?.from == 'dashboard' && event?.query?.filter){
            last_login_ts = customDateandTime(event.query.filter);
            where += ' AND createdAt >= "' + last_login_ts.from_date + '" AND createdAt <= "' + last_login_ts.to_date + '"';
        }else if(!event?.query?.filter && event?.query?.from == 'dashboard' && event?.query?.filter_type == 'active' && event?.query?.from_date && event?.query?.to_date){
            last_login_ts = {};
            last_login_ts.from_date = event.query.from_date;
            last_login_ts.to_date = event.query.to_date;
        }

        if (event?.query?.from == 'dashboard' && event.query.filter_type == 'active' && last_login_ts?.from_date && last_login_ts?.to_date) {
            users_where += ' AND last_login_ts >= "' + last_login_ts.from_date + '" AND last_login_ts <= "' + last_login_ts.to_date + '"';
        } */

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` group by id ORDER BY createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` group by id ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND createdAt >= "'+event?.query?.from_date+'" AND createdAt <= "'+event?.query?.to_date+'" ';
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` name LIKE '%${searchKey}%' OR address LIKE '%${searchKey}%' OR street LIKE '%${searchKey}%' OR city LIKE '%${searchKey}%' OR state LIKE '%${searchKey}%' OR zip_code LIKE '%${searchKey}%' OR phone LIKE '%${searchKey}%' OR fax LIKE '%${searchKey}%' OR plan_name LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')';
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')';
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery;
            codeSnip2 = where + filterQuery;
        }
        
        if(!event?.query?.download){
            codeSnip+=` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let get_active_subscription_query = `select Practices.id as id, Practices.name as name, Practices.address as address,Practices.street as street, `+
        `Practices.city as city , Practices.state as state , Practices.zip_code as zip_code, Practices.logoFile as logofile, `+
        `Practices.phone as phone , Practices.fax as fax, Practices.createdAt as createdAt, Practices.is_deleted as is_deleted, `+
        `group_concat(Plans.name separator ', ') AS plan_name, group_concat(Plans.plan_type separator ', ') AS plan_type, Users.last_login_ts as last_login_ts, `+
        `count(Orders.id) as total_orders_count from Practices `+
        `inner join Subscriptions ON Practices.id = Subscriptions.practice_id `+
        `inner join Plans on Subscriptions.plan_id = Plans.plan_id `+
        `inner join Users ON Practices.id = Users.practice_id `+
        `left join Orders on Practices.id = Orders.practice_id `+
        `where Practices.is_deleted is not true and Subscriptions.plan_id is not null `+
        `group by Practices.id `;

        let get_tek_as_you_go_query = ` select Practices.id, Practices.name, Practices.address,Practices.street,Practices.city,Practices.state,Practices.zip_code,Practices.logoFile, `+
        `Practices.phone,Practices.fax ,Practices.createdAt, Practices.is_deleted as is_deleted, 'TEK AS-YOU-GO' as plan_name, `+
        `Users.last_login_ts as last_login_ts, count(Orders.id) as total_orders_count, 'pay_as_you_go' AS plan_type `+
        `from Practices left join Users ON Practices.id = Users.practice_id `+
        `left join Orders on Practices.id = Orders.practice_id `+
        `where Practices.is_deleted is not true and Practices.id not in (select practice_id from Subscriptions) group by Practices.id `;

        let get_canceled_subscription_query = ` select Practices.id, Practices.name, Practices.address,Practices.street,Practices.city,Practices.state,Practices.zip_code,Practices.logoFile, `+
        `Practices.phone,Practices.fax ,Practices.createdAt, Practices.is_deleted as is_deleted, 'Subscription Canceled' as plan_name , `+
        `Users.last_login_ts as last_login_ts , count(Orders.id) as total_orders_count, 'canceled_subscription' AS plan_type `+
        `from Practices left join Users ON Practices.id = Users.practice_id `+
        `left join Orders on Practices.id = Orders.practice_id `+
        `where Practices.is_deleted is not true and Practices.id in (select practice_id from Subscriptions) group by Practices.id `;

        let combine_query ;

        if (query?.filter == 'false' || !query?.filter?.plan_type || query?.filter?.plan_type == 'all' || query?.filter?.plan_type == '') {
            combine_query = get_active_subscription_query +'union'+ get_tek_as_you_go_query +'union'+ get_canceled_subscription_query;
        }else if (query?.filter != 'false' && query?.filter?.plan_type != '' && !['all','pay_as_you_go','canceled_subscription'].includes(query?.filter?.plan_type)) {
            combine_query = get_active_subscription_query;
        }else if (query?.filter != 'false' && query?.filter?.plan_type == 'pay_as_you_go') {
            combine_query = get_tek_as_you_go_query;
        }else if (query?.filter != 'false' && query?.filter?.plan_type == 'canceled_subscription') {
            combine_query = get_canceled_subscription_query;
        }


        let sqlQuery = 'select id, name, address,street,city,state,zip_code,logoFile,phone,fax , plan_name, plan_type, createdAt, is_deleted, last_login_ts, total_orders_count from ('+combine_query+') as practice_details ' + codeSnip;

        let sqlQueryCount = 'select id, name, address,street,city,state,zip_code,logoFile,phone,fax , plan_name, plan_type, createdAt, is_deleted, last_login_ts, total_orders_count from ('+combine_query+') as practice_details ' + codeSnip2;
        console.log(sqlQuery);
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });

        for (let i = 0; i < sqlQuery.length; i++) {
            if(serverData[i]?.total_orders_count){ 
                serverData[i].total_orders_count = serverData[i].total_orders_count.toString()
            };
            if(serverData[i]?.last_login_ts){ 
                serverData[i].last_login_ts = serverData[i].last_login_ts.toString()
            };
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the clientss.'}),
        };
    }
};

const getAll = async (event) => {
    try {
        const {sequelize, PracticesTemplates, Subscriptions} = await connectToDatabase();
        let searchKey = '';
        const sortKey = {};
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;
        if (!query.offset && !query.limit) {
            let practicessRes = getallforClients(event);
            return practicessRes;
        }
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;

        let codeSnip = '';
        let codeSnip2 = '';
        let where = `WHERE is_deleted IS NOT true`;

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' createdAt >= "'+event?.query?.from_date+'" AND createdAt <= "'+event?.query?.to_date+'" AND ';
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` name LIKE '%${searchKey}%' OR street LIKE '%${searchKey}%' OR phone LIKE '%${searchKey}%' OR address LIKE '%${searchKey}%' OR createdAt LIKE '%${searchKey}%' OR city LIKE '%${searchKey}%' OR state LIKE '%${searchKey}%' OR zip_code LIKE '%${searchKey}%' OR fax LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery ;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery ;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')' ;
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && sortQuery == '') {
            codeSnip = where ;
            codeSnip2 = where;
        }
        if(!event?.query?.download){
            codeSnip+=` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }


        let sqlQuery = 'select * from Practices ' + codeSnip;

        let sqlQueryCount = 'select * from Practices ' + codeSnip2;

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });
        for (let i = 0; i < serverData.length; i++) {
            const activeSubscriptionObj = await Subscriptions.findAll({where:{practice_id: serverData[i].id},raw : true, order: [['createdAt', 'DESC']]});
            const subscriptionDetails = [];
            for(let i=0;i<activeSubscriptionObj.length; i++)
            {    
                let subscriptionObj = activeSubscriptionObj[i];
                subscriptionObj.email = event.user.email;
                let details = await getSubscriptionDetails(subscriptionObj);    
                subscriptionDetails.push(details);
            }
            serverData[i].subscriptions = subscriptionDetails;
            const templateData = await PracticesTemplates.findAll({where: {practice_id: serverData[i].id}, raw: true});
            if (templateData.length != 0) {
                for (let j = 0; j < templateData.length; j += 1) {
                    if(templateData[j] && templateData[j].status) serverData[i].practice_template_status = templateData[j].status;
                    if(templateData[j] && templateData[j].custom_template) serverData[i].custom_template = templateData[j].custom_template;
                    if(templateData[j] && templateData[j].custom_template_id) serverData[i].custom_template_id = templateData[j].id;
                }
            }
        }

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the clientss.'}),
        };
    }
};

const getSubscriptionDetails = async(subscriptionObj) => {
    try {
        let activePlansObj = { plan_type:'', subscription_id:'', subscription_plan_status:'', cancel_at_period_end:'',
            current_period_start:'', current_period_end:'', plan_category:'responding' };

        const {Plans, Op, Practices} = await connectToDatabase();

        let planObj;
        if(subscriptionObj?.email == 'siva@ospitek.com') {
                planObj = await Plans.findOne({where:{plan_id: subscriptionObj.plan_id }});
        }else{
                planObj = await Plans.findOne({where:{plan_id: subscriptionObj.plan_id, active:true}});
        }
        activePlansObj.subscription_id = subscriptionObj.id;
        activePlansObj.plan_category = subscriptionObj.plan_category;
        if(subscriptionObj?.plan_id && subscriptionObj?.plan_id == 'free_trial') {
            activePlansObj.plan_type = 'free_trial';
            activePlansObj.subscription_plan_status = 'active_plan';
            activePlansObj.plan_category = 'active_plan';
        }else if(subscriptionObj?.plan_id && subscriptionObj?.plan_id !='free_trial'){
            activePlansObj.plan_type = planObj?.plan_type || null;
            activePlansObj.subscription_plan_status = 'active_plan';
        }else if(!subscriptionObj?.plan_id){ //consider as plan expired.
            activePlansObj.plan_type = '';
            activePlansObj.subscription_plan_status = 'plan_expired';
        }else{
            activePlansObj.plan_type = 'pay_as_you_go';
            activePlansObj.subscription_plan_status = 'active_plan';
        }
        if(subscriptionObj?.plan_id && subscriptionObj?.stripe_subscription_data) {
            if(subscriptionObj.stripe_subscription_id.startsWith('sub_')){
                const stripe_obj = JSON.parse(subscriptionObj.stripe_subscription_data);
                activePlansObj.cancel_at_period_end = stripe_obj.cancel_at_period_end;
                activePlansObj.current_period_start = stripe_obj.current_period_start;
                activePlansObj.current_period_end = stripe_obj.current_period_end;
            }else if(subscriptionObj.stripe_subscription_id.startsWith('pi_')){
                const practiceObj = await Practices.findOne({where:{id:subscriptionObj.practice_id},raw:true});
                if(practiceObj.is_propounding_canceled){
                    activePlansObj.cancel_at_period_end = true;
                    activePlansObj.current_period_start = (new Date(subscriptionObj.subscribed_on).getTime())/1000;
                    activePlansObj.current_period_end = (new Date(subscriptionObj.subscribed_valid_till).getTime())/1000;
                }
            
            }
        }else{
            activePlansObj.cancel_at_period_end = true;
        }
        return activePlansObj;
    } catch (err) {
        console.log(err);
    }
}

const getallforClients = async (event) => {
    try {
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.order = [
            ['createdAt', 'DESC'],
        ];
        validateReadPractices(query);
        const {Practices, Op, PracticesTemplates, Subscriptions, Users} = await connectToDatabase();
        query.where.is_deleted = {[Op.not]: true};
        query.raw = true;
        query.logging = console.log;
        query.attributes = ['id','name'];
        const practicess = await Practices.findAll(query);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(practicess),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the practicess.'}),
        };
    }
}

const update = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateUpdatePractices(input);
        const {Practices, PracticesTemplates} = await connectToDatabase();
        const practices = await Practices.findOne({where: {id: event.pathParameters.id}});
        if (!practices) throw new HTTPError(404, `Practices with id: ${event.pathParameters.id} was not found`);
        const updatedModel = Object.assign(practices, input);
        const updatePractice = await updatedModel.save();
        const practiceData = updatePractice.get({plain: true});
        if (input.custom_template && input.custom_template_s3_file_key) {
            const existingTemplate = await PracticesTemplates.findOne({
                where: {
                    practice_id: event.pathParameters.id
                }
            });
            if (existingTemplate) {
                existingTemplate.destroy();
                let dataObject = {};
                dataObject.id = uuid.v4();
                dataObject.custom_template = input.custom_template;
                dataObject.custom_template_s3_file_key = input.custom_template_s3_file_key;
                dataObject.status = 'Processing';
                dataObject.practice_id = event.pathParameters.id;
                await PracticesTemplates.create(dataObject);
                await sendEmailPracticeTemplateStatus(event);
            } else {
                /* await paymentSubscription(event); */
                let dataObject = {};
                dataObject.id = uuid.v4();
                dataObject.custom_template = input.custom_template;
                dataObject.custom_template_s3_file_key = input.custom_template_s3_file_key;
                dataObject.status = 'Processing';
                dataObject.practice_id = event.pathParameters.id;
                await PracticesTemplates.create(dataObject);
                await sendEmailPracticeTemplateStatus(event);
            }
        }
        const templateData = await PracticesTemplates.findOne({
            where: {practice_id: event.pathParameters.id},
            raw: true
        });
        if (templateData) {
            practiceData.status = templateData.status;
            practiceData.custom_template = templateData.custom_template;
            practiceData.custom_template_id = templateData.id;
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(practiceData),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Practices.'}),
        };
    }
};

const destroy = async (event) => {
    try {
        validateDeletePractice(event.pathParameters);
        const { Practices, PracticesTemplates, Clients, Cases, Forms, LegalForms, Orders, Formotp, Users } = await connectToDatabase();
        const practiceDetails = await Practices.findOne({where: {id: event.pathParameters.id}});
        if (!practiceDetails) throw new HTTPError(404, `Practices with id: ${event.pathParameters.id} was not found`);
        await Cases.destroy({where: {practice_id: practiceDetails.id}});
        // await Orders.destroy({ where: { practice_id: practiceDetails.id } });
        await Orders.update({is_deleted: true}, {where: {practice_id: practiceDetails.id}});
        await LegalForms.destroy({where: {practice_id: practiceDetails.id}});
        await Forms.destroy({where: {practice_id: practiceDetails.id}});
        const practicesTemplates = await PracticesTemplates.findOne({ where: {practice_id: practiceDetails.id}, raw: true});
        const clients = await Clients.findAll({ where: { practice_id: practiceDetails.id,}, raw: true});

        if (clients && clients.length) {
            for (let i = 0; i < clients.length; i += 1) {
                const clientDetails = clients[i];
                await Formotp.destroy({where: {client_id: clientDetails.id}});
            }
        }
        await Clients.destroy({where: {practice_id: practiceDetails.id}});
        await Users.destroy({where: {practice_id: practiceDetails.id}});
        if (practicesTemplates) {
            // const updatePracticeTemplates = await PracticesTemplates.findOne({where: {id: practicesTemplates.id}});
            // updatePracticeTemplates.is_deleted = true;
            // updatePracticeTemplates.save();
            await Practices.destroy({where: {id: practiceDetails.id}});
        }
        practiceDetails.is_deleted = true;
        await practiceDetails.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Practice and it\'s related data deleted successfully',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could destroy the Practices'}),
        };
    }
};

const getUploadURL = async (input) => {
    let fileExtention = '';
    if (input.file_name) {
        fileExtention = `.${input.file_name.split('.').pop()}`;
    }

    const s3Params = {
        Bucket: 'esquiretek-public-assets',
        Key: `${input.file_name}.${uuid.v4()}.${fileExtention}`,
        ContentType: input.content_type,
        ACL: 'public-read',
    };

    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
            public_url: `https://${s3Params.Bucket}.s3.amazonaws.com/${s3Params.Key}`,
        });
    });
};

const uploadFile = async (event) => {
    try {
        const input = JSON.parse(event.body);
        const uploadURLObject = await getUploadURL(input);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                uploadURL: uploadURLObject.uploadURL,
                s3_file_key: uploadURLObject.s3_file_key,
                public_url: uploadURLObject.public_url,
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not upload the legalForms.'}),
        };
    }
};

const getSecuredPublicUrlForPrivateFile = async (event) => {
    try {
        const input = event.body;
        AWS.config.update({region: process.env.S3_CLIENT_SIGNATURE_BUCKET_REGION});
        const publicUrl = await s3.getSignedUrlPromise('getObject', {
            Bucket: process.env.S3_BUCKET_FOR_PRIVATE_ASSETS,
            Expires: 60 * 60 * 1,
            Key: input.s3_file_key,
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                publicUrl,
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not upload the legalForms.'}),
        };
    }
};

const getSecuredPublicUrlForFormFile = async (event) => {
    try {
        authorizeAdminGetFiles(event.user);
        const input = event.body;
        AWS.config.update({region: process.env.S3_BUCKET_FOR_FORMS_REGION});
        const publicUrl = await s3.getSignedUrlPromise('getObject', {
            Bucket: process.env.S3_BUCKET_FOR_FORMS,
            Expires: 60 * 60 * 1,
            Key: input.s3_file_key,
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                publicUrl,
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not upload the legalForms.'}),
        };
    }
};

const getPrivateUploadURL = async (input) => {
    let fileExtention = '';
    let newfilename = '';
    if (input.file_name) {
        const filterarr = input.file_name.split('.');
        fileExtention = `.` + filterarr.pop();
        newfilename = filterarr.join("_");
        const randomCode = generateRandomString(8);
        newfilename = newfilename + '_' + randomCode + fileExtention;
    }

    const s3Params = {
        Bucket: process.env.S3_BUCKET_FOR_PRIVATE_ASSETS,
        Key: `${newfilename}`,
        ContentType: input.content_type,
        ACL: 'private',
    };

    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
            public_url: `https://${s3Params.Bucket}.s3.amazonaws.com/${s3Params.Key}`,
        });
    });
};

const uploadPrivateFile = async (event) => {
    try {
        const input = event.body;
        const uploadURLObject = await getPrivateUploadURL(input);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                uploadURL: uploadURLObject.uploadURL,
                s3_file_key: uploadURLObject.s3_file_key,
                // public_url: uploadURLObject.public_url,
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not upload the legalForms.'}),
        };
    }
};

const resetBillingData = async (event) => {
    try {
        authorizeAdminGetFiles(event.user);
        const {Practices} = await connectToDatabase();
        await Practices.update({
            stripe_customer_id: '',
        }, {
            where: {}, // <-- here
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'all practices payment data got reset',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not upload the legalForms.'}),
        };
    }
};

const objectionStatusUpdate = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        const {Practices} = await connectToDatabase();
        const practiceModel = await Practices.findOne({where: {id: event.user.practice_id}});
        if (!practiceModel) throw new HTTPError(404, `Practice Details with id: ${params.id} was not found`);
        const updatedModel = Object.assign(practiceModel, {
            objections: input.objections || false,
        });
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the AdminObjections.'}),
        };
    }
};

const sendEmailPracticeTemplateStatus = async (event) => {
    try {
        const {Practices, PracticesTemplates} = await connectToDatabase();
        const practiceDetails = await Practices.findOne({where: {id: event.user.practice_id}});
        const practiceTemplateDetails = await PracticesTemplates.findOne({where: {practice_id: event.user.practice_id}});
        const body = 'User Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;' + event.user.email + '<br/>' +
            'Practice Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;' + practiceDetails.name + '<br/>' +
            'Custom Template Key &nbsp;&nbsp;:&nbsp;' + practiceTemplateDetails.custom_template_s3_file_key + '<br/>';
        let to = [];
        if (process.env.CODE_ENV == "staging" || process.env.CODE_ENV == "local") {
            to = ['siva@miotiv.com'];
        } else {
            to = ['siva@miotiv.com', 'support@esquiretek.com'];
        }

        await sendEmail(to, `Custom Template Requested By Test`, body);
    } catch (err) {
        console.log(err);
    }
};


const getAllPracticeInfoForDashboard_old = async (event) => {
    try {
        const { sequelize, Subscriptions } = await connectToDatabase();
        let sortQuery = '';
        let searchQuery = '';
        let codeSnip = '';
        let codeSnip2 = '';

        const query = event.headers;

        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;

        let where = `WHERE Practices.is_deleted IS NOT true`;

        /** DashBoard **/
        let last_login_ts;
        if(event?.query?.filter){
            last_login_ts = customDateandTime(event.query.filter);
        }else if(!event?.query?.filter && event?.query?.filter_type == 'active' && event?.query?.from_date && event?.query?.to_date){
            last_login_ts = {};
            last_login_ts.from_date = event.query.from_date;
            last_login_ts.to_date = event.query.to_date;
        }

        if (event?.query?.filter) {
            let dates = customDateandTime(event.query.filter);
            where += ' AND Practices.createdAt >= "' + dates.from_date + '" AND Practices.createdAt <= "' + dates.to_date + '"';
        }
        if (event?.query?.from_date) {
            where += ' AND Practices.createdAt >= "' + event?.query?.from_date + '"';
        }
        if (event?.query?.to_date) {
            where += ' AND Practices.createdAt <= "' + event?.query?.to_date + '"';
        }

        if (event.query.filter_type == 'active' && last_login_ts?.from_date && last_login_ts?.to_date) {
            where += ' AND Users.is_deleted IS NOT true AND Users.last_login_ts >= "' + last_login_ts.from_date + '" AND Users.last_login_ts <= "' + last_login_ts.to_date + '"';
        }

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY Practices.createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            if (query?.sort?.column != 'all' && query?.sort?.column) {
                sortQuery += ` ORDER BY ${query?.sort?.column}`;
            }
            if (query?.sort?.type != 'all' && query?.sort?.type) {
                sortQuery += ` ${query?.sort?.type}`;
            }
        }

        /**Search**/
        if (query?.search != 'false' && query?.search) {
            searchQuery = ` Practices.name LIKE '%${query?.search}%' OR Practices.street LIKE '%${query?.search}%' OR Practices.phone LIKE '%${query?.search}%' OR Practices.address LIKE '%${query?.search}%' OR Practices.createdAt LIKE '%${query?.search}%' OR Practices.city LIKE '%${query?.search}%' OR Practices.state LIKE '%${query?.search}%' OR Practices.zip_code LIKE '%${query?.search}%' OR Practices.fax LIKE '%${query?.search}%'`;
        }

        if (searchQuery != '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery ;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery ;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')' ;
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && sortQuery == '') {
            codeSnip = where ;
            codeSnip2 = where;
        }

        if(!event?.query?.download){
            codeSnip+=` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let sqlQuery, sqlQueryCount = '';
        if (event.query.filter_type == 'active') {
            sqlQuery = 'SELECT Practices.* FROM Practices INNER JOIN Users ON Practices.id = Users.practice_id ' + codeSnip;
            sqlQueryCount = 'SELECT Practices.* FROM Practices INNER JOIN Users ON Practices.id = Users.practice_id ' + codeSnip2;
        } else{
            sqlQuery = 'select * from Practices ' + codeSnip;
            sqlQueryCount = 'select * from Practices ' + codeSnip2;
        }

        console.log(sqlQuery);
        const serverData = await sequelize.query(sqlQuery, { type: QueryTypes.SELECT });
        
        for (let i = 0; i < serverData.length; i++) {
            const activeSubscriptionObj = await Subscriptions.findAll({ where: { practice_id: serverData[i].id }, raw: true, order: [['createdAt', 'DESC']] });
            const subscriptionDetails = [];
            for (let j = 0; j < activeSubscriptionObj.length; j++) {
                let subscriptionObj = activeSubscriptionObj[j];
                subscriptionObj.email = event.user.email;
                let details = await getSubscriptionDetails(subscriptionObj);
                subscriptionDetails.push(details);
            }
            serverData[i].subscriptions = subscriptionDetails;
        }

        const TableDataCount = await sequelize.query(sqlQueryCount, { type: QueryTypes.SELECT });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the clientss.'}),
        };
    }
};
const getAllPracticeInfoForDashboard = async (event) => {
    try {
        const { sequelize, Subscriptions } = await connectToDatabase();
        let sortQuery = '';
        let searchQuery = '';
        let codeSnip = '';
        let codeSnip2 = '';

        const query = event.headers;

        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;

        let where = `WHERE Practices.is_deleted IS NOT true`;

        /** DashBoard **/
        let last_login_ts;
        if(event?.query?.filter){
            last_login_ts = customDateandTime(event.query.filter);
        }else if(!event?.query?.filter && event?.query?.filter_type == 'active' && event?.query?.from_date && event?.query?.to_date){
            last_login_ts = {};
            last_login_ts.from_date = event.query.from_date;
            last_login_ts.to_date = event.query.to_date;
        }

        if (event?.query?.filter) {
            let dates = customDateandTime(event.query.filter);
            where += ' AND Practices.createdAt >= "' + dates.from_date + '" AND Practices.createdAt <= "' + dates.to_date + '"';
        }
        if (event?.query?.from_date) {
            where += ' AND Practices.createdAt >= "' + event?.query?.from_date + '"';
        }
        if (event?.query?.to_date) {
            where += ' AND Practices.createdAt <= "' + event?.query?.to_date + '"';
        }

        if (event.query.filter_type == 'active' && last_login_ts?.from_date && last_login_ts?.to_date) {
            where += ' AND Users.last_login_ts >= "' + last_login_ts.from_date + '" AND Users.last_login_ts <= "' + last_login_ts.to_date + '"';
        }

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY Practices.createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            if (query?.sort?.column != 'all' && query?.sort?.column) {
                sortQuery += ` ORDER BY ${query?.sort?.column}`;
            }
            if (query?.sort?.type != 'all' && query?.sort?.type) {
                sortQuery += ` ${query?.sort?.type}`;
            }
        }

        /**Search**/
        if (query?.search != 'false' && query?.search) {
            searchQuery = ` Practices.name LIKE '%${query?.search}%' OR Practices.street LIKE '%${query?.search}%' OR Practices.phone LIKE '%${query?.search}%' OR Practices.address LIKE '%${query?.search}%' OR Practices.createdAt LIKE '%${query?.search}%' OR Practices.city LIKE '%${query?.search}%' OR Practices.state LIKE '%${query?.search}%' OR Practices.zip_code LIKE '%${query?.search}%' OR Practices.fax LIKE '%${query?.search}%'`;
        }

        if (searchQuery != '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery ;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery ;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')' ;
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && sortQuery == '') {
            codeSnip = where ;
            codeSnip2 = where;
        }

        if(!event?.query?.download){
            codeSnip+=` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let sqlQuery, sqlQueryCount = '';
        if (event.query.filter_type == 'active') {
            sqlQuery = 'SELECT Practices.* FROM Practices INNER JOIN Users ON Practices.id = Users.practice_id ' + codeSnip;
            sqlQueryCount = 'SELECT Practices.* FROM Practices INNER JOIN Users ON Practices.id = Users.practice_id ' + codeSnip2;
        } else{
            sqlQuery = 'select * from Practices ' + codeSnip;
            sqlQueryCount = 'select * from Practices ' + codeSnip2;
        }

        console.log(sqlQuery);
        const serverData = await sequelize.query(sqlQuery, { type: QueryTypes.SELECT });
        
        for (let i = 0; i < serverData.length; i++) {
            const activeSubscriptionObj = await Subscriptions.findAll({ where: { practice_id: serverData[i].id }, raw: true, order: [['createdAt', 'DESC']] });
            const subscriptionDetails = [];
            for (let j = 0; j < activeSubscriptionObj.length; j++) {
                let subscriptionObj = activeSubscriptionObj[j];
                subscriptionObj.email = event.user.email;
                let details = await getSubscriptionDetails(subscriptionObj);
                subscriptionDetails.push(details);
            }
            serverData[i].subscriptions = subscriptionDetails;
        }

        const TableDataCount = await sequelize.query(sqlQueryCount, { type: QueryTypes.SELECT });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the clientss.'}),
        };
    }
};

const getAllPracticeInfoForDashboard_new = async (event) => {
    try {
        const {sequelize, PracticesTemplates, Subscriptions} = await connectToDatabase();
        let searchKey = '';
        const filterKey = {};
        const sortKey = {};
        let sortQuery = '';
        let filterQuery = '';
        let searchQuery = '';
        let last_login_ts;
        let users_where = ' ';
        const query = event.headers;

        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;

        let codeSnip = '';
        let codeSnip2 = '';
        let where = `WHERE is_deleted IS NOT true`;
        /**Filter**/
        if (query.filter != 'false') {
            query.filter = JSON.parse(query.filter);
            filterKey.type = query.filter.plan_type;
            if (filterKey.type != 'all' && filterKey.type != '' && filterKey.type) {
                filterQuery += ` AND plan_type LIKE '%${filterKey.type}%'`;
            }
        }
        /** This part only work's for DashBoard **/
        if(event?.query?.filter){
            last_login_ts = customDateandTime(event.query.filter);
        }else if(!event?.query?.filter && event?.query?.filter_type == 'active' && event?.query?.from_date && event?.query?.to_date){
            last_login_ts = {};
            last_login_ts.from_date = event.query.from_date;
            last_login_ts.to_date = event.query.to_date;
        }

        if (event?.query?.filter) {
            let dates = customDateandTime(event.query.filter);
            where += ' AND createdAt >= "' + dates.from_date + '" AND createdAt <= "' + dates.to_date + '"';
        }
        if (event?.query?.from_date) {
            where += ' AND createdAt >= "' + event?.query?.from_date + '"';
        }
        if (event?.query?.to_date) {
            where += ' AND createdAt <= "' + event?.query?.to_date + '"';
        }


        if (event.query.filter_type == 'active' && last_login_ts?.from_date && last_login_ts?.to_date) {
            users_where += ' AND Users.last_login_ts >= "' + last_login_ts.from_date + '" AND Users.last_login_ts <= "' + last_login_ts.to_date + '" ';
        }

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` group by id ORDER BY createdAt DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` group by id ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' AND createdAt >= "'+event?.query?.from_date+'" AND createdAt <= "'+event?.query?.to_date+'" ';
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` name LIKE '%${searchKey}%' OR address LIKE '%${searchKey}%' OR street LIKE '%${searchKey}%' OR city LIKE '%${searchKey}%' OR state LIKE '%${searchKey}%' OR zip_code LIKE '%${searchKey}%' OR phone LIKE '%${searchKey}%' OR fax LIKE '%${searchKey}%' OR plan_name LIKE '%${searchKey}%'`;
        }

        if (searchQuery != '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery == '' && filterQuery != '' && sortQuery != '') {
            codeSnip = where + filterQuery + sortQuery;
            codeSnip2 = where + filterQuery + sortQuery;
        } else if (searchQuery == '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + sortQuery;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery != '') {
            codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery;
            codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
        } else if (searchQuery != '' && filterQuery == '' && sortQuery == '') {
            codeSnip = where + ' AND (' + searchQuery + ')';
            codeSnip2 = where + ' AND (' + searchQuery + ')';
        } else if (searchQuery != '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery + ' AND (' + searchQuery + ')';
            codeSnip2 = where + filterQuery + ' AND (' + searchQuery + ')';
        } else if (searchQuery == '' && filterQuery != '' && sortQuery == '') {
            codeSnip = where + filterQuery;
            codeSnip2 = where + filterQuery;
        }
        
        if(!event?.query?.download){
            codeSnip+=` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let get_active_subscription_query = `select Practices.id as id, Practices.name as name, Practices.address as address,Practices.street as street, `+
        `Practices.city as city , Practices.state as state , Practices.zip_code as zip_code, Practices.logoFile as logofile, `+
        `Practices.phone as phone , Practices.fax as fax, Practices.createdAt as createdAt, Practices.is_deleted as is_deleted, `+
        `group_concat(Plans.name separator ', ') AS plan_name, group_concat(Plans.plan_type separator ', ') AS plan_type, Users.last_login_ts as last_login_ts, `+
        `count(Orders.id) as total_orders_count from Practices `+
        `inner join Subscriptions ON Practices.id = Subscriptions.practice_id `+
        `inner join Plans on Subscriptions.plan_id = Plans.plan_id `+
        `inner join Users ON Practices.id = Users.practice_id `+
        `left join Orders on Practices.id = Orders.practice_id `+
        `where Practices.is_deleted is not true and Subscriptions.plan_id is not null${users_where}`+
        `group by Practices.id `;

        let get_tek_as_you_go_query = ` select Practices.id, Practices.name, Practices.address,Practices.street,Practices.city,Practices.state,Practices.zip_code,Practices.logoFile, `+
        `Practices.phone,Practices.fax ,Practices.createdAt, Practices.is_deleted as is_deleted, 'TEK AS-YOU-GO' as plan_name, `+
        `Users.last_login_ts as last_login_ts, count(Orders.id) as total_orders_count, 'pay_as_you_go' AS plan_type `+
        `from Practices left join Users ON Practices.id = Users.practice_id `+
        `left join Orders on Practices.id = Orders.practice_id `+
        `where Practices.is_deleted is not true${users_where}and Practices.id not in (select practice_id from Subscriptions) group by Practices.id `;

        let get_canceled_subscription_query = ` select Practices.id, Practices.name, Practices.address,Practices.street,Practices.city,Practices.state,Practices.zip_code,Practices.logoFile, `+
        `Practices.phone,Practices.fax ,Practices.createdAt, Practices.is_deleted as is_deleted, 'Subscription Canceled' as plan_name , `+
        `Users.last_login_ts as last_login_ts , count(Orders.id) as total_orders_count, 'canceled_subscription' AS plan_type `+
        `from Practices left join Users ON Practices.id = Users.practice_id `+
        `left join Orders on Practices.id = Orders.practice_id `+
        `where Practices.is_deleted is not true${users_where}and Practices.id in (select practice_id from Subscriptions) group by Practices.id `;

        let combine_query ;

        if (query?.filter == 'false' || !query?.filter?.plan_type || query?.filter?.plan_type == 'all' || query?.filter?.plan_type == '') {
            combine_query = get_active_subscription_query +'union'+ get_tek_as_you_go_query +'union'+ get_canceled_subscription_query;
        }else if (query?.filter != 'false' && query?.filter?.plan_type != '' && !['all','pay_as_you_go','canceled_subscription'].includes(query?.filter?.plan_type)) {
            combine_query = get_active_subscription_query;
        }else if (query?.filter != 'false' && query?.filter?.plan_type == 'pay_as_you_go') {
            combine_query = get_tek_as_you_go_query;
        }else if (query?.filter != 'false' && query?.filter?.plan_type == 'canceled_subscription') {
            combine_query = get_canceled_subscription_query;
        }


        let sqlQuery = 'select id, name, address,street,city,state,zip_code,logoFile,phone,fax , plan_name, plan_type, createdAt, is_deleted, last_login_ts, total_orders_count from ('+combine_query+') as practice_details ' + codeSnip;

        let sqlQueryCount = 'select id, name, address,street,city,state,zip_code,logoFile,phone,fax , plan_name, plan_type, createdAt, is_deleted, last_login_ts, total_orders_count from ('+combine_query+') as practice_details ' + codeSnip2;
        console.log(sqlQuery);
        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });

        for (let i = 0; i < sqlQuery.length; i++) {
            if(serverData[i]?.total_orders_count){ 
                serverData[i].total_orders_count = serverData[i].total_orders_count.toString()
            };
            if(serverData[i]?.last_login_ts){ 
                serverData[i].last_login_ts = serverData[i].last_login_ts.toString()
            };
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Practice details.'}),
        };
    }
};

module.exports.create = middy(create).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = getAll;
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.uploadFile = middy(uploadFile).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.uploadPrivateFile = uploadPrivateFile;
module.exports.getSecuredPublicUrlForPrivateFile = getSecuredPublicUrlForPrivateFile;
module.exports.getSecuredPublicUrlForFormFile = getSecuredPublicUrlForFormFile;
module.exports.resetBillingData = resetBillingData;
module.exports.objectionStatusUpdate = objectionStatusUpdate;
module.exports.sendEmailPracticeTemplateStatus = sendEmailPracticeTemplateStatus;
module.exports.getAllPracticeInfoForDashboard = getAllPracticeInfoForDashboard;


/* Subscription Example - Free trial

{
"id":"b03ce8d1-8b0d-43c2-b20b-7f2722c66d3e",
"practice_id":"520e9548-df55-471a-bbbd-baeb2f3983c0",
"subscribed_by":"88a35ac0-ed99-4404-80d3-58c64e20c4c6",
"subscribed_on":"2022-11-01 13:45:01",
"plan_id":"free_trial",
"stripe_subscription_id":"NULL",
"subscribed_valid_till":"NULL",
"stripe_subscription_data":"NULL",
"subscription_meta_data":"NULL",
"createdAt":"2022-11-01 13:45:01",
"updatedAt":"2022-11-01 13:45:01",
"stripe_product_id":"NULL"
}



*/