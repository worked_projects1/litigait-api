const uuid = require("uuid");
const connectToDatabase = require("../../db");
const {HTTPError} = require("../../utils/httpResp");

const create = async (event) => {
    try {
        let id;
        const input =
            typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const {PropoundResponder, Op} = await connectToDatabase();
        const practice_id = event.user.practice_id;
        if (!practice_id)
            throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
        const questions = JSON.stringify(input.questions);
        const dataObject = Object.assign(
            input,
            {id: id || uuid.v4()},
            {questions: questions},
            {responder_practice_id: practice_id}
        );
        await PropoundResponder.create(dataObject);

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                message: "Propound Responder details saved successfully.",
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "could not save Propound Responder details.",
            }),
        };
    }
};

const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const practice_id = event.user.practice_id;
        const {PropoundResponder, Op} = await connectToDatabase();
        if (!practice_id)
            throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
        const PropoundResponderObj = await PropoundResponder.findOne({
            where: {id: params.id, responder_practice_id: practice_id},
        });
        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(PropoundResponderObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "Could not get the Propound Responder Details.",
            }),
        };
    }
};

const getAll = async (event) => {
    try {
        const {PropoundResponder} = await connectToDatabase();
        const practice_id = event.user.practice_id;
        if (!practice_id)
            throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
        const PropoundResponderObj = await PropoundResponder.findAll({
            where: {responder_practice_id: practice_id},
        });

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(PropoundResponderObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "Could not get the Propound Responder Details.",
            }),
        };
    }
};

const update = async (event) => {
    try {
        const input =
            typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const {PropoundResponder, Op} = await connectToDatabase();
        const questions = JSON.stringify(input.questions);
        const practice_id = event.user.practice_id;
        if (!practice_id)
            throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
        const dataObject = Object.assign(input, {questions: questions});
        await PropoundResponder.update(dataObject, {
            where: {
                id: input.id,
                responder_practice_id: practice_id,
                propounder_case_id: input.responder_case_id,
            },
        });

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                status: "Ok",
                message: "Propound Responder Details Updated Successfully.",
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error:
                    err.message || "Could not update the Propound Responder Details.",
            }),
        };
    }
};

const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const {PropoundResponder, Op} = await connectToDatabase();
        const practice_id = event.user.practice_id;
        if (!practice_id)
            throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
        await PropoundResponder.destroy({where: {id: params.id}});

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                status: "Ok",
                message: "Propound Responder Details Removed Successfully.",
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error:
                    err.message || "Could not delete the Propound Responder Details.",
            }),
        };
    }
};

const getResponderDataByTokenId = async (event) => {
    try {
        const params = event.params;
        const {PropoundResponder, Cases, Op, Practices} = await connectToDatabase();
        const token_id = params.token_id;
        if (!token_id) throw new HTTPError(404, `Token id: ${token_id} was not found`);

        const propoundResponderResponse = {};
        const selected_forms = [];
        let responderIdArr = [];
        let caseDeatails = '';
        const PropoundResponderObj = await PropoundResponder.findAll({
            where: {token_id: token_id},
            attributes: ['id','propound_form_id','responder_email','responder_practice_name','responder_practice_id','responder_case_id',
                'responder_user_name','responder_state_bar_no','document_type','number_of_questions_sent','token_id','propounder_practice_id',
                'propounder_case_id','is_new_user','is_template_used_by_responder',],
            raw: true,
            logging: console.log    
        });

        let hasExistOtherCaseDetails = false;
        

        if(PropoundResponderObj.length >= 0) {

            const findPropoundResponderSameCaseInfoTemp = await PropoundResponder.findAll({where: {
                propounder_practice_id:PropoundResponderObj[0].propounder_practice_id,
                propounder_case_id:PropoundResponderObj[0].propounder_case_id,
                responder_practice_id:PropoundResponderObj[0].responder_practice_id,
                responder_case_id: {[Op.not]:null}
            },
            attributes: ['responder_case_id'],
            distinct: true,
            raw: true,
            logging: console.log
            });

             responderIdArr = findPropoundResponderSameCaseInfoTemp && findPropoundResponderSameCaseInfoTemp.length > 0 && [...new Set(findPropoundResponderSameCaseInfoTemp.map(e => e.responder_case_id))] || [];
            
            // if(responderIdArr.length < 1 ) throw new HTTPError( 500, "Other Case Deatails ID Exists.");

            

            for(let i=0 ; i  < PropoundResponderObj.length ; i++) {

                if( i == 0 ) Object.assign(propoundResponderResponse,PropoundResponderObj[i]);
                selected_forms.push(PropoundResponderObj[i].propound_form_id);
                if(PropoundResponderObj[i] && PropoundResponderObj[i].id)
                {
                    if(PropoundResponderObj[i].responder_practice_id) 
                    {
                        console.log('Inside Responder practice');
                        const findPropoundResponderSameCaseInfo = await PropoundResponder.findAll({where: {
                            propounder_practice_id:PropoundResponderObj[i].propounder_practice_id,
                            propounder_case_id:PropoundResponderObj[i].propounder_case_id,
                            responder_practice_id:PropoundResponderObj[i].responder_practice_id,
                        },
                        });

                        console.log(responderIdArr);
                        if( responderIdArr && responderIdArr.length ) 
                        {
                            console.log('Inside uPDATE  Responder');
                            const updateColumn = { responder_case_id : responderIdArr[0] };
                            const updateQuery = { 
                                propounder_practice_id:PropoundResponderObj[i].propounder_practice_id,
                                propounder_case_id:PropoundResponderObj[i].propounder_case_id,
                                responder_practice_id:PropoundResponderObj[i].responder_practice_id,
                                responder_case_id : null,
                                token_id: token_id
                            }
                            await PropoundResponder.update(updateColumn, {where: updateQuery, logging: console.log});
                            caseDeatails = await Cases.findOne({where: {id: responderIdArr[0], is_deleted: {[Op.not]: true}, is_archived: {[Op.not]: true}}, raw: true});
                        }
    
                    }
            }
        }
        }
        console.log(selected_forms);
        const response = Object.assign({}, { ...propoundResponderResponse} ,{selected_forms : selected_forms});
        
        const propounderPracticeObj = await Practices.findOne({where: {id:response.propounder_practice_id},raw: true});
        response.propounder_practice_name = propounderPracticeObj.name;
        if(caseDeatails && caseDeatails.id){
            response.case_id = caseDeatails.id;
            response.state = caseDeatails.state;
            response.client_id = caseDeatails.client_id;
            response.practice_id = caseDeatails.practice_id;
            response.case_title = caseDeatails.case_title;
            response.case_number = caseDeatails.case_number;
            response.claim_number = caseDeatails.claim_number;
            response.case_plaintiff_name = caseDeatails.case_plaintiff_name;
            response.case_defendant_name = caseDeatails.case_defendant_name;
            response.responder_case_id = responderIdArr[0];
        }


        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(response),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "Could not get the Propound Responder Details.",
            }),
        };
    }
};

const getRespondersTemplateDetailsByPracticeId = async (event) => {
    try {
        const {PropoundResponder, Op, PropoundForms} = await connectToDatabase();
        const practice_id = event.user.practice_id;
        const propoundResponderObj = await PropoundResponder.findAll({
            where: {
                responder_practice_id: practice_id,
                responder_case_id: null
            }, raw: true
        });
        for (let i = 0; i < propoundResponderObj.length; i++) {
            propoundResponderObj.questions[i] = JSON.parse(propoundResponderObj.questions[i]);
        }
        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(propoundResponderObj),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "Could not get the Responders template Details.",
            }),
        };
    }
};

const createReponderCaseDetails = async (event) => {
    try {
        const input =
            typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const {
            PropoundForms,
            PropoundResponder,
            Clients,
            Cases,
            Op,
            LegalForms,
            Forms,
        } = await connectToDatabase();
        /*{
      cases_details,
      propound_responder
      }*/
        /* 1.Create Client */
        let clientsObject = undefined;
        let responderCaseDetails = undefined;
        if (input && !input.client_id) {
            const clientColumns = Object.assign(
                {},
                {
                    id: uuid.v4(),
                    first_name: input.first_name,
                    last_name: input.last_name,
                    email: input.email,
                    phone: input.phone,
                    practice_id: event.user.practice_id,
                }
            );
            let name = '', address;
            if(input?.first_name){
                name = input?.first_name;
            }
            if (input.middle_name) { 
                name = name + ' ' + input.middle_name; 
            }
            if (input.last_name) { 
                name = name + ' ' + input.last_name; 
            }
            clientColumns.name = name;
            if(input.street){
            address = input.street + ', ' + input.city + ', ' + input.state + ' - ' + input.zip_code;
            clientColumns.address = address;
            }
            if(input.middle_name) { clientColumns.middle_name = input.middle_name }else{ clientColumns.middle_name = null };
            if(input.street){
             clientColumns.street = input.street || null;
             clientColumns.city = input.city || null;
             clientColumns.state = input.state || null;
             clientColumns.zip_code = input.zip_code || null;
            }
            if (input && input.dob) clientColumns.dob = input.dob;
            clientsObject = await Clients.create(clientColumns);
        } else {
            clientsObject = await Clients.findOne({
                where: {is_deleted: {[Op.not]: true}, id: input.client_id},
            });
        }
        if (!clientsObject) throw new HTTPError(404, `Client details not fount`);
        /* 2.Get case info from Propounder details */
        const propoundResponderDetails = await PropoundResponder.findAll({
            where: {
                token_id: input.token_id,
                propounder_case_id: input.propounder_case_id,
                propounder_practice_id: input.propounder_practice_id,
            },
        });
console.log(propoundResponderDetails);
        const propounderCaseDetails = await Cases.findOne({
            where: {
                id: input.propounder_case_id,
                practice_id: input.propounder_practice_id,
            },
        });
        /* 3.Create case details for responder */
        if(input && !input.case_id){
            let propounderCaseTitle = '';

            if (propounderCaseDetails && propounderCaseDetails.case_defendant_name && propounderCaseDetails.case_plaintiff_name ){
                propounderCaseTitle = `${propounderCaseDetails.case_defendant_name} V. ${propounderCaseDetails.case_plaintiff_name}`;
            }else{
                propounderCaseTitle = propounderCaseDetails.case_title;
            }
            const casesCloumn = Object.assign(
                {},
                {
                    id: uuid.v4(),
                    client_id: clientsObject.id, // Responder client ID
                    practice_id: clientsObject.practice_id, // Responder Practice ID
                    case_plaintiff_name:
                    propounderCaseDetails.case_defendant_name /* Propounder defendant is Responder's Plaintiff */,
                    case_defendant_name:
                    propounderCaseDetails.case_plaintiff_name /* Propounder Plaintiff is Responder's defendant */,
                    case_title: propounderCaseTitle,
                    date_of_loss: propounderCaseDetails.date_of_loss,
                    case_number: propounderCaseDetails.case_number,
                    matter_id: propounderCaseDetails.matter_id,
                    claim_number: propounderCaseDetails.claim_number,
                    federal: propounderCaseDetails.federal,
                    attorneys: event.user.id,
                }
            );
            if (propounderCaseDetails.federal) {
                casesCloumn.federal_district = propounderCaseDetails.federal_district;
            } else {
                casesCloumn.county = propounderCaseDetails.county;
                casesCloumn.state = propounderCaseDetails.state;
            }
            console.log(casesCloumn);
            console.log("**********");
            responderCaseDetails = await Cases.create(casesCloumn);
        }else{
            responderCaseDetails = await Cases.findOne({
                where: {
                    id: input.case_id, 
                    client_id: input.client_id , 
                    is_deleted: {[Op.not]: true}, 
                    is_archived: {[Op.not]: true},
                    state: input.state
                }
            });
        }
        /* 4.Create Legalform details for this case */
        for( j=0; j<propoundResponderDetails.length; j++){
            console.log(input.selected_forms[j]);

        const propoundFormsObj = await PropoundForms.findOne({
            where: {id: input.selected_forms[j]},
            raw: true,
        });
        const legalFormsColumn = Object.assign(
            {},
            {
                id: uuid.v4(),
                case_id: responderCaseDetails.id,
                client_id: responderCaseDetails.client_id,
                document_type: propoundFormsObj.document_type,
                filename: propoundFormsObj.file_name,
                practice_id: event.user.practice_id,
            }
        );
        console.log(legalFormsColumn);
        console.log("**********");
           const getlegalformsCount = await LegalForms.count({where: {
               document_type:propoundFormsObj.document_type,
               client_id:responderCaseDetails.client_id,
               case_id:responderCaseDetails.id,
               practice_id:event.user.practice_id,
           }}); 
        if(getlegalformsCount >=5){
            return {
                statusCode: 200,
                headers: {
                    "Content-Type": "text/plain",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": true,
                },
                body: JSON.stringify({status: 'ok', message:'forms count exist the limit.'}),
            };
        }
        const legalFormsObj = await LegalForms.create(legalFormsColumn);
        /* 5.Create form details against legalform_id ,case_id and client_id */
        const questionsArr = JSON.parse(propoundResponderDetails[j].questions);
        for (let i = 0; i < questionsArr.length; i++) {
            let questions = questionsArr[i];
            let formsColumn = Object.assign(questions, {
                id: uuid.v4(),
                case_id: legalFormsObj.case_id,
                practice_id: legalFormsObj.practice_id,
                client_id: legalFormsObj.client_id,
                question_options: JSON.stringify(questions.question_options),
                legalforms_id: legalFormsObj.id,
                document_type: legalFormsObj.document_type,
                lawyer_response_status: "NotStarted",
                client_response_status: "NotSetToClient",
            });
            console.log(formsColumn);
            await Forms.create(formsColumn);
        }
        propoundResponderDetails[j].is_template_used_by_responder = true;
        if( propoundResponderDetails[j] && !propoundResponderDetails[j].responder_case_id){
            propoundResponderDetails[j].responder_case_id = responderCaseDetails.id;
        }
        await propoundResponderDetails[j].save();
    };
        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(responderCaseDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "Could not create Responders case Details.",
            }),
        };
    }
};
module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.getResponderDataByTokenId = getResponderDataByTokenId;
module.exports.getRespondersTemplateDetailsByPracticeId = getRespondersTemplateDetailsByPracticeId;
module.exports.createReponderCaseDetails = createReponderCaseDetails;
