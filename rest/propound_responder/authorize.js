const {HTTPError} = require('../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const practiceRoles = ['lawyer', 'paralegal'];
const internalRoles = ['superAdmin', 'manager', 'operator'];
const superAdminRole = ['superAdmin'];
const normalUserRole = ['superAdmin', 'manager', 'operator'];

exports.authorizeCreate = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetOne = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};
exports.authorizeGetAll = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdate = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroy = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetRespondersTemplateDetailsByPracticeId = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeCreateReponderCaseDetails = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

