const uuid = require('uuid');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION || 'us-east-1'});
const s3 = new AWS.S3();

const create = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const dataObject = Object.assign(input, {
            id: uuid.v4() , 
            questions: JSON.stringify(input.questions) , 
            practice_id: event.user.practice_id
        });
        const { FrogsTemplates, Op } = await connectToDatabase();
        const frogsTemplateObj = await FrogsTemplates.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(frogsTemplateObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the frogs template.'}),
        };
    }
};
const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const { FrogsTemplates, Op } = await connectToDatabase();
        const frogsTemplateObj = await FrogsTemplates.findOne({where: {id: params.id}});
        if (!frogsTemplateObj) throw new HTTPError(404, `Frogs template with id: ${params.id} was not found`);
        if(frogsTemplateObj.questions) frogsTemplateObj.questions = JSON.parse(frogsTemplateObj.questions);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(frogsTemplateObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the frogs template.'}),
        };
    }
};
const getAll = async (event) => {
    try{
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        query.raw = true;
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query.attributes = ['id','practice_id','file_name','disc_type','state','is_deleted'];
        query.raw = true;
        query.logging = console.log;
        const { FrogsTemplates, Op} = await connectToDatabase();
        const frogsTemplateObj = await FrogsTemplates.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(frogsTemplateObj),
        };
    }catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the frogs template.'}),
        };
    }
}
const update = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { FrogsTemplates, Op } = await connectToDatabase();
        const frogsTemplateObj = await FrogsTemplates.findOne({where: {id: params.id}});
        if (!frogsTemplateObj) throw new HTTPError(404, `Frogs template with id: ${params.id} was not found`);
        frogsTemplateObj.questions = JSON.parse(input.questions);
        await frogsTemplateObj.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({status: 'ok' , message: 'Frogs template updated successfully.'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update frogs templates.'}),
        };
    }
}
const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const { FrogsTemplates, Op } = await connectToDatabase();
        const frogsTemplateObj = await FrogsTemplates.findOne({where: {id: params.id}});
        if (!frogsTemplateObj) throw new HTTPError(404, `Frogs template with id: ${params.id} was not found`);
        frogsTemplateObj.is_deleted = true;
        await frogsTemplateObj.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({status: 'ok' , message: 'Frogs template deleted successfully.'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not delete frogs templates.'}),
        };
    }
}

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;