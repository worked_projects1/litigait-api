module.exports = (sequelize, type) => sequelize.define('FrogsTemplates', { 
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    file_name: type.STRING,
    disc_type: type.STRING,
    questions: type.TEXT('long'),
    state: type.STRING,
    is_deleted: type.BOOLEAN
});