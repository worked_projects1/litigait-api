module.exports = (sequelize, type) => sequelize.define('ClientSignature', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    case_id: type.STRING,
    client_id: type.STRING,
    legalforms_id: type.STRING,
    document_type: type.STRING, // FROGS, SPROGS, RFPD, RFA
    client_signature: type.TEXT,
    client_signature_date: type.DATE,
    client_signature_s3_key: type.TEXT,
    client_verification_signature_s3_key: type.TEXT,
    client_verification_signature_date: type.DATE,
    client_signature_data: type.TEXT,
});
