const connectToDatabase = require('../../db');
const {generateColorCode} = require('../helpers/users.helper');

const updateFromsOtplawyerResponseStatusandPendingQuestions = async (event) => {
    try {
        const {Forms, OtherPartiesForms, Formotp, OtherPartiesFormsOtp, Op, sequelize} = await connectToDatabase();

        const getLeagalFormsData = await Formotp.findAll({
            where: {sending_type: 'all', scheduler_email: true},
            raw: true
        });

        for (let i = 0; i < getLeagalFormsData.length; i++) {
            const legalforms_id = getLeagalFormsData[i].legalforms_id;
            const client_id = getLeagalFormsData[i].client_id;
            const document_type = getLeagalFormsData[i].document_type;
            const case_id = getLeagalFormsData[i].case_id;

            const checkQuestionIDsExist = await Forms.findAll({
                where: {
                    legalforms_id, client_id, document_type, case_id, id: {[Op.in]: question_ids}
                },
            });
            if (checkQuestionIDsExist.length) {
                const fetchLatestclientResponseDate = await Forms.findAll({
                    where: {
                        legalforms_id, client_id, document_type, case_id
                    },
                    attributes: [[sequelize.fn('max', sequelize.col('last_updated_by_client')), 'last_updated_by_client']]
                });

                let latestClientResponseDate = undefined;
                let question_answeredStatus = false;
                const updateColumn = {};
                const updateQuery = {
                    legalforms_id, client_id, document_type, case_id,
                    id: getLeagalFormsData[i].id,
                    otp_code: getLeagalFormsData[i].otp_code,
                    otp_secret: getLeagalFormsData[i].otp_secret
                };
                if (fetchLatestclientResponseDate.length != 0 && fetchLatestclientResponseDate[0].last_updated_by_client) {
                    updateColumn.latest_client_response_date = fetchLatestclientResponseDate[0].last_updated_by_client;
                }

                const fetchLatestclientResponseCount = await Forms.count({
                    where: {
                        legalforms_id, client_id, document_type, case_id, client_response_status: 'SentToClient'
                    },
                });

                console.log(fetchLatestclientResponseCount);
                if (fetchLatestclientResponseCount == 0) {
                    updateColumn.is_client_answered_all_questions = true;
                }
                await Formotp.update(updateColumn, {where: updateQuery, logging: console.log});
            }

        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({message: "Forms OTP updated successfully."}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update Forms OTP updated.'}),
        };


    }
}
const updateFromsOtplawyerResponseStatusandPendingQuestionsforSelectedQuestions = async (event) => {
    try {
        const {Forms, OtherPartiesForms, Formotp, OtherPartiesFormsOtp, Op, sequelize} = await connectToDatabase();

        const getLeagalFormsData = await Formotp.findAll({
            where: {
                sending_type: 'selected_questions',
                scheduler_email: true
            }, logging: console.log, raw: true
        });

        for (let i = 0; i < getLeagalFormsData.length; i++) {
            const legalforms_id = getLeagalFormsData[i].legalforms_id;
            const client_id = getLeagalFormsData[i].client_id;
            const document_type = getLeagalFormsData[i].document_type;
            const case_id = getLeagalFormsData[i].case_id;
            const question_ids = JSON.parse(getLeagalFormsData[i].question_ids);

            const checkQuestionIDsExist = await Forms.findAll({
                where: {
                    legalforms_id, client_id, document_type, case_id, id: {[Op.in]: question_ids}
                },
            });
            if (checkQuestionIDsExist.length != 0) {
                const fetchLatestclientResponseDate = await Forms.findAll({
                    where: {
                        legalforms_id, client_id, document_type, case_id, id: {[Op.in]: question_ids}
                    },
                    attributes: [[sequelize.fn('max', sequelize.col('last_updated_by_client')), 'last_updated_by_client']],
                    logging: console.log
                });

                console.log(fetchLatestclientResponseDate[0].last_updated_by_client);

                const updateColumn = {};
                const updateQuery = {
                    legalforms_id, client_id, document_type, case_id,
                    id: getLeagalFormsData[i].id,
                    otp_code: getLeagalFormsData[i].otp_code,
                    otp_secret: getLeagalFormsData[i].otp_secret
                };
                if (fetchLatestclientResponseDate.length != 0 && fetchLatestclientResponseDate[0].last_updated_by_client) {
                    updateColumn.latest_client_response_date = fetchLatestclientResponseDate[0].last_updated_by_client;
                }

                const fetchLatestclientResponseCount = await Forms.count({
                    where: {
                        legalforms_id,
                        client_id,
                        document_type,
                        case_id,
                        client_response_status: 'SentToClient',
                        id: {[Op.in]: question_ids}
                    },
                });

                console.log(fetchLatestclientResponseCount);
                if (fetchLatestclientResponseCount == 0) {
                    updateColumn.is_client_answered_all_questions = true;
                }
                await Formotp.update(updateColumn, {where: updateQuery, logging: console.log});
            }
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({message: "Forms OTP updated successfully."}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update Forms OTP updated.'}),
        };
    }
}
const updateColorCode = async (event) => {
    try {
        const {Users, Op, sequelize} = await connectToDatabase();

        const usersObj = await Users.findAll({
            where: {
                practice_id: {[Op.ne]:null},
                is_deleted: {[Op.not]:true},
                color_code: {[Op.is]:null}
            },
            order: [
                ['practice_id', 'ASC'],
            ],
            logging: console.log,
             raw: true
        });
        console.log(usersObj.length);
        for (let i = 0; i < usersObj.length; i++) {
            let color_code = await generateColorCode(usersObj[i].practice_id, Users);
            console.log(' i : '+ i + ' Color code : '+color_code);
            await Users.update({color_code},{where:{id:usersObj[i].id, practice_id: usersObj[i].practice_id}});
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({message: "Color codes updated successfully."}),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update Color code for users.'}),
        };
    }
}
module.exports.updateFromsOtplawyerResponseStatusandPendingQuestions = updateFromsOtplawyerResponseStatusandPendingQuestions;
module.exports.updateFromsOtplawyerResponseStatusandPendingQuestionsforSelectedQuestions = updateFromsOtplawyerResponseStatusandPendingQuestionsforSelectedQuestions;
module.exports.updateColorCode = updateColorCode;