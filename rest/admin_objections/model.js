module.exports = (sequelize, type) => sequelize.define('AdminObjections', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    objection_title: type.STRING,
    objection_text: type.TEXT,
    created_by_admin_id: type.STRING,
    state: type.STRING,
});
