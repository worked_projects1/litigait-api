const uuid = require('uuid');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const {
    validateCreateAdminObjections,
    validateGetOneCustomerObjection,
    validateUpdateAdminObjections,
    validateDeleteCustomerObjection,
    validateReadAdminObjections,
} = require('./validation');

const {
    authorizeCreate,
    authorizeGetOne,
    authorizeGetAll,
    authorizeUpdate,
    authorizeDestroy,
} = require('./authorize');

const create = async (event) => {
    try {
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (event.user.id) {
            input.created_by_admin_id = event.user.id;
        }
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, {id: id || uuid.v4()});
        validateCreateAdminObjections(dataObject);
        authorizeCreate(event.user);
        const {AdminObjections} = await connectToDatabase();
        const adminObjectionss = await AdminObjections.create(dataObject);
        const adminObjectionssPlain = adminObjectionss.get({plain:true});
        await addObjectionToPractices(adminObjectionssPlain);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(adminObjectionss),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the adminObjectionss.'}),
        };
    }
};

const pushToAllPractices = async (event) => {
    const processedObjections = [];
    try {
        authorizeCreate(event.user);
        const {Practices, AdminObjections, CustomerObjections} = await connectToDatabase();
        const allPractices = await Practices.findAll({raw: true});
        const allAdminObjections = await AdminObjections.findAll({raw: true});

        for (let p = 0; p < allPractices.length; p += 1) {
            const practice = allPractices[p];
            const existingObjectionsCount = await CustomerObjections.count({where: {practice_id: practice.id}});
            if (existingObjectionsCount < allAdminObjections.length) {
                for (let i = 0; i < allAdminObjections.length; i += 1) {
                    const objectionItem = Object.assign({}, allAdminObjections[i]);
                    objectionItem.practice_id = practice.id;
                    delete objectionItem.created_by_admin_id;
                    objectionItem.id = `${uuid.v4()}${practice.id}`;
                    objectionItem.state = allAdminObjections[i].state;
                    processedObjections.push(objectionItem);
                }
            }
        }

        // console.log(JSON.stringify(processedObjections));

        if (processedObjections.length) {
            await CustomerObjections.bulkCreate(processedObjections);
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Objection data copied to all practices',
            }),
        };
    } catch (err) {
        // console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                error: err.message || 'Could not create the adminObjectionss.',
                processedObjections,
                err
            }),
        };
    }
};

const addObjectionToPractices = async (Objection) => {
    try {
        const newObjection = Objection;
        const processedObjections = [];

        const {Practices, Op, CustomerObjections} = await connectToDatabase();
        const allPractices = await Practices.findAll({
            where: {is_deleted: {[Op.not]: true}},
            raw: true,
            attributes: ['id']
        });
        for (let p = 0; p < allPractices.length; p += 1) {
            const practice = allPractices[p];
            const objectionItem = {};
            objectionItem.id = `${uuid.v4()}${practice.id}`;
            objectionItem.adminobjection_id = newObjection.id;
            objectionItem.practice_id = practice.id;
            objectionItem.objection_title = newObjection.objection_title;
            objectionItem.objection_text = newObjection.objection_text;
            objectionItem.objection_view = true;
            objectionItem.state = newObjection.state;
            processedObjections.push(objectionItem);
        }
        let res;
        if (processedObjections.length) {
            res = await CustomerObjections.bulkCreate(processedObjections);
        }
        return res;
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the AdminObjections.'}),
        };
    }
}
const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateGetOneCustomerObjection(params);
        //authorizeGetOne(event.user);
        const {AdminObjections} = await connectToDatabase();
        const adminObjectionss = await AdminObjections.findOne({where: {id: params.id}});
        if (!adminObjectionss) throw new HTTPError(404, `AdminObjections with id: ${params.id} was not found`);
        const plainCustomerObjection = adminObjectionss.get({plain: true});
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainCustomerObjection),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the AdminObjections.'}),
        };
    }
};

const getAll = async (event) => {
    try {
        let header = event.headers;
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.order = [
            ['objection_title', 'ASC'],
        ];
        validateReadAdminObjections(query);
        authorizeGetAll(event.user);
        const {AdminObjections} = await connectToDatabase();
        const adminObjectionsss = await AdminObjections.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(adminObjectionsss),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the adminObjectionsss.'}),
        };
    }
};

const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        validateUpdateAdminObjections(input);
        authorizeUpdate(event.user);
        const {AdminObjections, CustomerObjections} = await connectToDatabase();
        const adminObjectionss = await AdminObjections.findOne({where: {id: params.id}});
        if (!adminObjectionss) throw new HTTPError(404, `AdminObjections with id: ${params.id} was not found`);
        const updatedModel = Object.assign(adminObjectionss, input);
        await updatedModel.save();
        const updateColumn = {
            objection_title: input.objection_title,
            objection_text: input.objection_text,
            state: input.state,
        };
        await CustomerObjections.update(updateColumn, {where: {adminobjection_id: input.id}});

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the AdminObjections.'}),
        };
    }
};

const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateDeleteCustomerObjection(params);
        authorizeDestroy(event.user);
        const {AdminObjections} = await connectToDatabase();
        const adminObjectionsDetails = await AdminObjections.findOne({where: {id: params.id}});
        if (!adminObjectionsDetails) throw new HTTPError(404, `AdminObjections with id: ${params.id} was not found`);
        await adminObjectionsDetails.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(adminObjectionsDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could destroy fetch the AdminObjections.'}),
        };
    }
};

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.pushToAllPractices = pushToAllPractices;

