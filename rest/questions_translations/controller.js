const uuid = require('uuid');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const {
    validateCreate,
    validateGetOne,
    validateUpdate,
    validateDelete,
    validateRead,
} = require('./validation');

const create = async (event) => {
    try {
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, {id: id || uuid.v4()});
        // validateCreate(dataObject);
        const {QuestionsTranslations} = await connectToDatabase();
        const customerObjectionObject = await QuestionsTranslations.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionObject),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the customerObjectionObject.'}),
        };
    }
};


const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        // validateGetOne(params);
        const {QuestionsTranslations} = await connectToDatabase();
        const customerObjectionObject = await QuestionsTranslations.findOne({where: {id: params.id}});
        if (!customerObjectionObject) throw new HTTPError(404, `QuestionsTranslations with id: ${params.id} was not found`);
        const plainCustomerObjection = customerObjectionObject.get({plain: true});
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainCustomerObjection),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the QuestionsTranslations.'}),
        };
    }
};

const getAll = async (event) => {
    try {
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.order = [
            ['question_number', 'ASC'],
        ];
        validateRead(query);
        const {QuestionsTranslations} = await connectToDatabase();
        const customerObjectionObjects = await QuestionsTranslations.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionObjects),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the customerObjectionObjects.'}),
        };
    }
};

const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        // validateUpdate(input);
        const {QuestionsTranslations} = await connectToDatabase();
        const customerObjectionObject = await QuestionsTranslations.findOne({where: {id: params.id}});
        if (!customerObjectionObject) throw new HTTPError(404, `QuestionsTranslations with id: ${params.id} was not found`);
        const updatedModel = Object.assign(customerObjectionObject, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the QuestionsTranslations.'}),
        };
    }
};

const saveMultiple = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {QuestionsTranslations} = await connectToDatabase();
        for (let i = 0; i < input.length; i += 1) {
            const translationObject = input[i];
            translationObject.id = uuid.v4();
            const translatedObjectModel = await QuestionsTranslations.findOne({
                where: {
                    document_type: translationObject.document_type,
                    question_number: translationObject.question_number,
                    TargetLanguageCode: translationObject.TargetLanguageCode,
                },
            });
            if (translatedObjectModel) {
                translatedObjectModel.SourceLanguageText = translationObject.SourceLanguageText;
                translatedObjectModel.TargetLanguageText = translationObject.TargetLanguageText;
                await translatedObjectModel.save();
            } else {
                await QuestionsTranslations.create(translationObject);
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'All Questions translations updated successfully',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the QuestionsTranslations.'}),
        };
    }
};


const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        // validateDelete(params);
        const {QuestionsTranslations} = await connectToDatabase();
        const customerObjectionsDetails = await QuestionsTranslations.findOne({where: {id: params.id}});
        if (!customerObjectionsDetails) throw new HTTPError(404, `QuestionsTranslations with id: ${params.id} was not found`);
        await customerObjectionsDetails.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(customerObjectionsDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could destroy fetch the QuestionsTranslations.'}),
        };
    }
};

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.saveMultiple = saveMultiple;
