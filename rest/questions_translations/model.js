module.exports = (sequelize, type) => sequelize.define('QuestionsTranslations', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    question_number: type.STRING,
    SourceLanguageCode: type.STRING,
    TargetLanguageCode: type.STRING,
    SourceLanguageText: type.TEXT,
    TargetLanguageText: type.TEXT,
    document_type: type.STRING,
});
