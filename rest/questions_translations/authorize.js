const {HTTPError} = require('../../utils/httpResp');

const internalRoles = ['superAdmin', 'manager', 'operator'];
const superAdminRole = ['superAdmin'];
const normalUserRole = ['superAdmin', 'manager', 'operator'];

exports.authorizeCreate = function (user) {
    if (!superAdminRole.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetOne = function (user) {
    if (!superAdminRole.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};
exports.authorizeGetAll = function (user) {
    if (!superAdminRole.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdate = function (user) {
    if (!superAdminRole.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroy = function (user) {
    if (!superAdminRole.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeSaveMultiple = function (user) {
    if (!superAdminRole.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};
