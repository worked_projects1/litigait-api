module.exports = (sequelize, type) => sequelize.define('VersionHistory', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    version_date: type.DATE,
    version: type.INTEGER,
});
