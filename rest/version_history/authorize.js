
const litigaitRoles = ['superAdmin', 'lawyer', 'paralegal'];

exports.authorizeSetVersionHistory = function (user) {
    if (!litigaitRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};