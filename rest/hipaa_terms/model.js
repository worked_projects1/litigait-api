module.exports = (sequelize, type) => sequelize.define('HipaaTerms', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    terms_text: type.TEXT,
});
