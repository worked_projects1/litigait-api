const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');


const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const authMiddleware = require('../../auth');


const getHipaaTerms = async (event) => {
    try {
        const {HipaaTerms} = await connectToDatabase();
        const hipaatermsObject = await HipaaTerms.findOne({
            where: {
                practice_id: event.user.practice_id,
            }
        });
        let hipaaTermsJson = {
            practice_id: event.user.practice_id,
            terms_text: '',
        };
        if (hipaatermsObject) {
            hipaaTermsJson = hipaatermsObject.get({plain: true});
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(hipaaTermsJson),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Practices.'}),
        };
    }
};

const getHipaaTermsByClientId = async (event) => {
    try {
        const input = JSON.parse(event.body);
        const {HipaaTerms, Clients, Cases} = await connectToDatabase();
        const clientDetails = await Clients.findOne({
            where: {
                id: input.client_id,
            }
        });
        if (!clientDetails) throw new HTTPError(401, 'invalid Client id');
        const hipaatermsObject = await HipaaTerms.findOne({
            where: {
                practice_id: clientDetails.practice_id,
            }
        });
        let hipaaTermsJson = {
            practice_id: clientDetails.practice_id,
            terms_text: '',
        };

        if (hipaatermsObject) {
            hipaaTermsJson = hipaatermsObject.get({plain: true});
        }
        let hipaa_acceptance_status = clientDetails.hipaa_acceptance_status;
        if (input.case_id) {
            const caseDetails = await Cases.findOne({where: {id: input.case_id}});
            if (caseDetails) {
                hipaa_acceptance_status = caseDetails.hipaa_acceptance_status;
            }
        }
        hipaaTermsJson.hipaa_acceptance_status = hipaa_acceptance_status;
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(hipaaTermsJson),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Practices.'}),
        };
    }
};


const setHipaaTerms = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {HipaaTerms} = await connectToDatabase();
        const hipaatermsObject = await HipaaTerms.findOne({
            where: {
                practice_id: event.user.practice_id,
            }
        });
        if (hipaatermsObject) {
            hipaatermsObject.terms_text = input.terms_text;
            await hipaatermsObject.save();
        } else {
            const dataObject = Object.assign({
                practice_id: event.user.practice_id,
                terms_text: input.terms_text,
            }, {id: uuid.v4()});
            await HipaaTerms.create(dataObject);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Terms updated',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Practices.'}),
        };
    }
};

module.exports.getHipaaTerms = middy(getHipaaTerms).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.setHipaaTerms = middy(setHipaaTerms).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getHipaaTermsByClientId = getHipaaTermsByClientId;
module.exports.getHipaaTermsExpress = getHipaaTerms;
module.exports.setHipaaTermsExpress = setHipaaTerms;

