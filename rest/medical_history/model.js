module.exports = (sequelize, type) => sequelize.define('MedicalHistory', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    client_id: type.STRING,
    case_id: type.STRING,
    s3_file_key: type.TEXT,
    file_name: type.TEXT,
    comment: type.TEXT,
    pages: type.INTEGER,
    keys: type.BOOLEAN,
    textract_status: type.STRING,
    order_date: type.DATE,
    summery_doc_status: type.STRING
});
