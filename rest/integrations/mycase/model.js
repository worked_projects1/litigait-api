module.exports = (sequelize, type) => sequelize.define('Mycase', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    auth_code: type.TEXT('long'),
    access_token: type.TEXT('long'),
    // access_token_timestamp: type.DATE,
    refresh_token: type.TEXT('long'),
    // token_timestamp: type.DATE,
});