const {HTTPError} = require('../../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const practiceRoles = ['lawyer', 'paralegal'];
const internalRoles = ['superAdmin', 'manager', 'operator', 'QualityTechnician'];
const superAdminRole = ['superAdmin'];
const normalUserRole = ['superAdmin', 'manager', 'operator'];

exports.authorizeGetMycaseAuthToken = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeCreateClientandCase = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetMycaseData = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeMycaseFileUpload = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroyTokenData = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdateClientandCase = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdateCaseToMycase = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdateClientToMycase = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};