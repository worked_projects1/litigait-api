const axios = require("axios");
const connectToDatabase = require("../../../db");
const uuid = require("uuid");
const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.REGION || "us-east-1" });
const s3 = new AWS.S3();
const { HTTPError } = require("../../../utils/httpResp");
const { litifyAuthToken } = require("./access_token");
const moment = require("moment");


const getLitifyData = async (event) => {
    try{
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        if(!input.integration_case_id){ throw new HTTPError(400,'integration case id not found')};

        const { Clients, Cases, States, Op } = await connectToDatabase();
        const case_url = `https://esquiretek2.my.salesforce.com/services/data/v57.0/sobjects/litify_pm__Matter__c/${input.integration_case_id}`;
        
        const accessToken = await litifyAuthToken()
        const headers = {
            Authorization: "Bearer " + accessToken
        };
        let caseDetails, clientDetails, ResponseData = {}, clientData, states;
        
        caseDetails = await axios.request({ url: case_url, method: "get", headers}).then((res)=>res.data);
        if(!caseDetails){throw new HTTPError(400,'case details not found in Litify')};
        
        let stateCode;
        if(caseDetails?.litify_pm__Matter_State__c){
          let stateCodeArr = caseDetails.litify_pm__Matter_State__c.split(' ');
          stateCode = stateCodeArr[0];
          console.log(stateCode);
          states = await States.findOne({where: { state_code: stateCode }});
        }
        console.log(caseDetails);
       console.log(states && !caseDetails?.litify_pm__Matter_State);
        if(!(states || caseDetails?.litify_pm__Matter_State__c == null)){
          throw new HTTPError(400,'Unsupported state in EsquireTek.');
        }
        const litifyClientId = caseDetails.litify_pm__Client__c;
        
        const client_url = `https://esquiretek2.my.salesforce.com/services/data/v57.0/sobjects/Account/${litifyClientId}`;

        clientDetails = await axios.request({ url: client_url, method: "get", headers}).then((res)=>res.data);
          if(!clientDetails){throw new HTTPError(400,'client details not found in Litify')};
   
        const clientDatas = await Clients.findOne({where:{
          integration_client_id: clientDetails?.Id,
          practice_id: event.user.practice_id,
          is_deleted: {[Op.not]: true}, 
        }}); 

        if(!clientDatas && caseDetails && clientDetails && (states || caseDetails?.litify_pm__Matter_State__c == null)){
          const clientDataObj = Object.assign({},{
            id:uuid.v4(),
            practice_id: event.user.practice_id,
            phone: clientDetails?.litify_pm__Phone_Mobile__c || null,
            dob:clientDetails?.litify_pm__Date_of_birth__c || null,
            email:clientDetails?.litify_pm__Email__c || null,
            integration_client_id: clientDetails.Id,
            client_from: "litify"
        });
        let name='';
        if( clientDetails.Name &&!clientDetails?.litify_pm__First_Name__c && !clientDetails?.litify_pm__Last_Name__c){
          name += clientDetails.Name;
          let names = name.split(' ');
          clientDataObj.first_name = names[0];
          if(names.length > 1){
          names.shift();
          clientDataObj.last_name = names.join(' ');
          }
        };
        if(clientDetails?.litify_pm__First_Name__c){
          name += clientDetails.litify_pm__First_Name__c;
          clientDataObj.first_name = clientDetails.litify_pm__First_Name__c;
        }
        if(clientDetails?.litify_pm__Last_Name__c ){
          if(clientDetails.litify_pm__First_Name__c) name = name +' '
          name += clientDetails?.litify_pm__Last_Name__c;
          clientDataObj.last_name = clientDetails.litify_pm__Last_Name__c;
        }
        clientDataObj.name = name;
        clientData = await Clients.create(clientDataObj);
        if(!clientData){throw new HTTPError(400,'client data not found')};
        ResponseData.client = clientData;
        const integrationCaseData = Object.assign({}, clientDetails, caseDetails);
        const caseDataObj = Object.assign({},{
          id:uuid.v4(),
          practice_id: event.user.practice_id,
          client_id: clientData.id,
          integration_case_id: caseDetails.Id,
          case_title: caseDetails.litify_pm__Case_Title__c,
          county: caseDetails.litify_pm__Matter_City__c || null,
          state: stateCode || null,
          date_of_loss: caseDetails.litify_pm__Incident_date__c,
          integration_case_data: JSON.stringify(integrationCaseData),
          case_from: "litify"
        });
        const caseData = await Cases.create(caseDataObj);
        caseData.integration_case_data = JSON.parse(caseData.integration_case_data);
        ResponseData.case = caseData;
        };
        if(clientDatas && caseDetails && clientDetails && (states || caseDetails?.litify_pm__Matter_State__c == null)){
         console.log("with");
          let name = '';
          if( clientDetails?.Name &&!clientDetails?.litify_pm__First_Name__c && !clientDetails?.litify_pm__Last_Name__c){
            name += clientDetails.Name;
            let names = name.split(' ');
            clientDatas.first_name = names[0];
            if(names.length > 1){
            names.shift();
            clientDatas.last_name = names.join(' ');
            }
          };
          if(clientDetails?.litify_pm__First_Name__c){
            name += clientDetails.litify_pm__First_Name__c;
            clientDatas.first_name = clientDetails.litify_pm__First_Name__c;
          }
          if(clientDetails?.litify_pm__Last_Name__c ){
            if(clientDetails.litify_pm__First_Name__c) name = name +' '
            name += clientDetails?.litify_pm__Last_Name__c;
            clientDatas.last_name = clientDetails.litify_pm__Last_Name__c;
          }
          clientDatas.name = name;
          await clientDatas.save();
          ResponseData.client = clientDatas;
          
          const caseDatas = await Cases.findOne({where:{
            integration_case_id: caseDetails?.Id,
            practice_id: event.user.practice_id,
            client_id: clientDatas.id,
            is_deleted: {[Op.not]: true}, 
          }}); 
  
          if(caseDatas){
            const integrationCaseData = Object.assign({}, clientDetails, caseDetails);
            caseDatas.case_title = caseDetails.litify_pm__Case_Title__c;
            caseDatas.date_of_loss = caseDetails.litify_pm__Incident_date__c;
            caseDatas.state = stateCode || null;
            caseDatas.county = caseDetails.litify_pm__Matter_City__c || null;
            caseDatas.integrationCaseData = JSON.stringify(integrationCaseData);
            await caseDatas.save();
            caseDatas.integration_case_data = JSON.parse(caseDatas.integration_case_data);
            ResponseData.case = caseDatas;
          }
          if(!caseDatas){
            const integrationCaseData = Object.assign({}, clientDetails, caseDetails);
            const caseDataObj = Object.assign({},{
              id:uuid.v4(),
              practice_id: event.user.practice_id,
              client_id: clientDatas.id,
              integration_case_id: caseDetails.Id,
              case_title: caseDetails.litify_pm__Case_Title__c,
              county: caseDetails.litify_pm__Matter_City__c || null,
              state: stateCode,
              date_of_loss: caseDetails.litify_pm__Incident_date__c,
              integration_case_data: JSON.stringify(integrationCaseData),
              case_from: "litify"
            });
            const caseData = await Cases.create(caseDataObj);
            caseData.integration_case_data = JSON.parse(caseData.integration_case_data);
            ResponseData.case = caseData;
          }
        };
        
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({ResponseData}),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't fetch litify data  ",
      }),
    };
  }
}

const getLitifyDataC = async (event) => {
  try{
      const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
      if(!input.integration_case_id){ throw new HTTPError(400,'integration case id not found')};

      const { Clients, Cases, Op } = await connectToDatabase();
      const case_url = `https://esquiretek2.my.salesforce.com/services/data/v57.0/sobjects/Case/${input.integration_case_id}`;
      const accessToken = await litifyAuthToken()
      const headers = {
          Authorization: "Bearer " + accessToken
      };
      let caseDetails, clientDetails, ResponseData = {}, clientData;
      try{
        caseDetails = await axios.request({ url: case_url, method: "get", headers}).then((res)=>res.data);
      if(!caseDetails){throw new HTTPError(400,'case details not found in Litify')};
      }catch(e){
        throw new Error(e || 'An error occurred during the Litify request while fetching case details.');
      }
      const litifyClientId = caseDetails.AccountId;
      
      const client_url = `https://esquiretek2.my.salesforce.com/services/data/v57.0/sobjects/Account/${litifyClientId}`;
      try{
        clientDetails = await axios.request({ url: client_url, method: "get", headers}).then((res)=>res.data);
        if(!clientDetails){throw new HTTPError(400,'client details not found in Litify')};
      }catch(e){
        throw new Error(e || 'An error occurred during the Litify request while fetching client details.');
      }

      const clientDatas = await Clients.findOne({where:{
        integration_client_id: clientDetails?.Id,
        practice_id: event.user.practice_id,
        is_deleted: {[Op.not]: true}, 
      }});
      if(!clientDatas){
      const clientDataObj = Object.assign({},{
          id:uuid.v4(),
          practice_id: event.user.practice_id,
          first_name: clientDetails?.litify_pm__First_Name__c || null,
          last_name: clientDetails?.litify_pm__Last_Name__c || null,
          integration_client_id: clientDetails.Id,
          client_from: "litify"
      });
      let name='';
        if(clientDetails?.litify_pm__First_Name__c){
          name += clientDetails?.litify_pm__First_Name__c;
        }
        if(clientDetails?.litify_pm__Last_Name__c){
          if(clientDetails?.litify_pm__First_Name__c) name = name +' '
          name += clientDetails?.litify_pm__Last_Name__c;
        }
        clientDataObj.name = name;
      clientData = await Clients.create(clientDataObj);
      if(!clientData){throw new HTTPError(400,'client data not found')};
      ResponseData.client = clientData;

      const caseDataObj = Object.assign({},{
        id:uuid.v4(),
        practice_id: event.user.practice_id,
        client_id: clientData.id,
        integration_case_id: caseDetails.Id,
        case_number: caseDetails.CaseNumber,
        case_from: "litify"
      });
      const caseData = await Cases.create(caseDataObj);
      if(!caseData){throw new HTTPError(400,'case data not found')};
      ResponseData.case = caseData;
      }
      if(clientDatas){
        clientDatas.name = clientDetails.litify_pm__First_Name__c +' '+ clientDetails.litify_pm__Last_Name__c;
        clientDatas.first_name = clientDetails.litify_pm__First_Name__c;
        clientDatas.last_name = clientDetails.litify_pm__Last_Name__c;
        await clientDatas.save();
        ResponseData.client = clientDatas;
        
        const caseDatas = await Cases.findOne({where:{
          integration_case_id: caseDetails?.Id,
          practice_id: event.user.practice_id,
          client_id: clientDatas.id,
          is_deleted: {[Op.not]: true}, 
        }}); 
        
        if(caseDatas){
          caseDatas.case_number =  caseDetails.CaseNumber,
          await caseDatas.save();
          ResponseData.case = caseDatas;
        }
      };
     
  return {
    statusCode: 200,
    headers: {
      "Content-Type": "text/plain",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
    },
    body: JSON.stringify({ResponseData}),
  };
} catch (err) {
  console.log(err);
  return {
    statusCode: err.statusCode || 500,
    headers: {
      "Content-Type": "text/plain",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
    },
    body: JSON.stringify({
      error: err.message || "Couldn't fetch litify data  ",
    }),
  };
}
}

const updateClientToLitify = async(event) =>{
  try{ 
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    if(!input) throw new Error(400,'input data not found');
    const { Clients, Cases, Op } = await connectToDatabase();
    const { integration_client_id, first_name, last_name, phone, dob, email } = input;
    const url = `https://esquiretek2.my.salesforce.com/services/data/v57.0/sobjects/Account/${integration_client_id}`;    
    const accessToken = await litifyAuthToken();
    const headers = {
        Authorization: "Bearer " + accessToken,
        "Content-Type": "application/json"
    };
    let clientDetails;
    const client_url = `https://esquiretek2.my.salesforce.com/services/data/v57.0/sobjects/Account/${integration_client_id}`;
    try{
      clientDetails = await axios.request({ url: client_url, method: "get", headers}).then((res)=>res.data);
      if(!clientDetails){throw new HTTPError(400,'client details not found in Litify')};
    }catch(e){
      throw new Error(e || 'An error occurred during the Litify request while fetching client details.');
    }

    const clientData = await Clients.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_client_id: integration_client_id,
        is_deleted: {[Op.not]: true},
        client_from:'litify' 
      }
    });

    if(!clientData){throw new HTTPError(400,'client data not found')};
   
    clientData.first_name = first_name;
    clientData.last_name = last_name;
    clientData.phone = phone;
    clientData.dob = dob;
    clientData.email = email;
    await clientData.save();

    const dateOfBirth = dob && moment(dob).format('YYYY-MM-DD');
    let data = {
      litify_pm__Phone_Mobile__c: phone,
      litify_pm__Date_of_birth__c: dateOfBirth,
      litify_pm__Email__c: email
    };

    if( clientDetails?.Name &&!clientDetails?.litify_pm__First_Name__c && !clientDetails?.litify_pm__Last_Name__c){
      data.Name = first_name + ' ' + last_name;
    };
    if(clientDetails?.litify_pm__First_Name__c){
      data.litify_pm__First_Name__c = first_name;
    }
    if(clientDetails?.litify_pm__Last_Name__c){
      data.litify_pm__Last_Name__c = last_name;
    }

try{
    const litifyDetails = await axios.request({ url: url, method: "patch", headers, data }).then((res)=>res.data);
  }catch(e){
    throw new Error(e || 'An error occurred during the Litify request while fetching case details.');
  }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({message: "client details updated to litify successfully"}),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't fetch litify data  ",
      }),
    };
  }
}

const updateCaseToLitify = async(event) =>{
  try{ 
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { Clients, Cases, Op } = await connectToDatabase();
    const { integration_case_id, case_title, city, state, date_of_loss } = input;
    const url = `https://esquiretek2.my.salesforce.com/services/data/v57.0/sobjects/litify_pm__Matter__c/${integration_case_id}`;
    const accessToken = await litifyAuthToken()
    
    const headers = {
        Authorization: "Bearer " + accessToken,
        "Content-Type": "application/json"
    };

    const caseData = await Cases.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_case_id: integration_case_id,
        is_deleted: {[Op.not]: true},
        case_from:'litify' 
      }
    });

    if (!caseData) { throw new HTTPError(400, 'case not found') };

    let states = state && state == "CA"?"CA California": state =="TN"?"TN Tennessee": state =="TX"?"TX Texas" : state =="FL"?"FL Florida" : state =="NV"?"NV Nevada" : state =="IN"?"IN Indiana" : state =="MN"?"MN Minnesota" : state =="GA"?"GA Georgia" : state =="IA"?"IA Iowa" : state =="AZ"?"AZ Arizona" : state =="WA"?"WA Washington" : state =="MI"?"MI Michigan" : null;

    caseData.case_title = case_title;
    caseData.case_title = date_of_loss;
    await caseData.save();

    const data = {
      litify_pm__Case_Title__c: case_title,
      litify_pm__Matter_City__c: city,
      litify_pm__Matter_State__c: states,
      litify_pm__Incident_date__c: date_of_loss
    };

    const litifyDetails = await axios.request({ url: url, method: "patch", headers, data });

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({message: "case details updated to litify successfully"}),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't fetch litify data  ",
      }),
    };
  }
}

const commonUpdateLitify = async(event) =>{
  try{ 
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    if(!input) throw new Error(400,'input data not found');
    const { Clients, Cases, Op } = await connectToDatabase();
    const { integration_client_id, integration_case_id, client_phone,  state, county } = input;
    const client_url = `https://esquiretek2.my.salesforce.com/services/data/v57.0/sobjects/Account/${integration_client_id}`;
    const case_url = `https://esquiretek2.my.salesforce.com/services/data/v57.0/sobjects/litify_pm__Matter__c/${integration_case_id}`;
   
    const accessToken = await litifyAuthToken();
    const headers = {
        Authorization: "Bearer " + accessToken,
        "Content-Type": "application/json"
    };

    const clientData = await Clients.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_client_id: integration_client_id,
        is_deleted: {[Op.not]: true},
        client_from:'litify' 
      }
    });

    if(!clientData){throw new HTTPError(400,'client data not found')};

    const caseData = await Cases.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_case_id: integration_case_id,
        is_deleted: {[Op.not]: true},
        case_from:'litify' 
      }
    });

    if (!caseData) { throw new HTTPError(400, 'case not found') };
    let states = state && state == "CA"?"CA California": state =="TN"?"TN Tennessee": state =="TX"?"TX Texas" : state =="FL"?"FL Florida" : state =="NV"?"NV Nevada" : state =="IN"?"IN Indiana" : state =="MN"?"MN Minnesota" : state =="GA"?"GA Georgia" : state =="IA"?"IA Iowa" : state =="AZ"?"AZ Arizona" : state =="WA"?"WA Washington" : state =="MI"?"MI Michigan" : null;
    
    const client_data = {
      litify_pm__Phone_Mobile__c: client_phone,
    };

    const case_data = {
      litify_pm__Matter_State__c: states,
      litify_pm__Matter_City__c: county,
    };

    const litifyClientDetails = await axios.request({ url: client_url, method: "patch", headers, data:client_data }).then((res)=>res.data);
    const litifyCaseDetails = await axios.request({ url: case_url, method: "patch", headers, data:case_data }).then((res)=>res.data);

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({message: "client details updated to litify successfully"}),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Couldn't fetch litify data  ",
      }),
    };
  }
}

module.exports.getLitifyData = getLitifyData;
module.exports.updateCaseToLitify = updateCaseToLitify;
module.exports.updateClientToLitify = updateClientToLitify;
module.exports.commonUpdateLitify = commonUpdateLitify;