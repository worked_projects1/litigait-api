const axios = require("axios");
const decode = require("urldecode");
const connectToDatabase = require("../../../db");
const encode = require('encodeurl');
const uuid = require("uuid");
const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.REGION || "us-east-1" });
const s3 = new AWS.S3();
const { QueryTypes } = require("sequelize");
const timeZone = require("../../../utils/timeStamp");
const { HTTPError } = require("../../../utils/httpResp");
const request = axios.create({
  baseURL: process.env.CLIO_URL,
});
const { authorizeGetClioAuthToken,
  authorizeGetClioData,
  authorizeCreateClientandCase,
  authorizeUpdateClientandCase,
  authorizeDestroyTokenData,
  authorizeUpdateCaseToClio,
  authorizeUpdateClientToClio
} = require("./authorize");

const getClioAuthToken = async (event) => {
  try {
    authorizeGetClioAuthToken(event.user);
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    if (input && !input.code) throw new HTTPError(400, `Code not found.`);
    // const code = decode(input.code);
    const code = input.code;
    let client_id, client_secret, url, redirect_uri;
    const { Clio } = await connectToDatabase();

    client_id = process.env.CLIO_CLIENTID;
    client_secret = process.env.CLIO_CLIENTSECRET;
    url = process.env.CLIO_URL + 'oauth/token';
    redirect_uri = process.env.CLIO_REDIRECTURL;

    let headers = {
      Accept: "*/*",
    };
    let params = { code, grant_type: "authorization_code", redirect_uri, client_id, client_secret };

    const result = await axios.request({ url: url, params, method: "post", headers });

    const dataObject = Object.assign(result.data, {
      id: uuid.v4(),
      practice_id: event.user.practice_id,
      auth_code: code,
      access_token: result.data.access_token,
      access_token_timestamp: new Date(),
      refresh_token: result.data.refresh_token,
    });

    await Clio.destroy({ where: { practice_id: event.user.practice_id } });
    await Clio.create(dataObject);

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "clio token details successfully fetched",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Can not fetch clio token details",
      }),
    };
  }
};

const getClioData = async (event) => {
  try {
    authorizeGetClioData(event.user);
    const { Clio } = await connectToDatabase();
    const params = event.query || event.pathParameters;
    const pageToken = params.page_token;
    if (!params.type) {
      throw new HTTPError(400, `client or case type not found`);
    }
    validateToken(event);
    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    let clientDetails, dataObject, caseDetails;

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const headers = {
        ContentType: "application/json",
        Authorization: "Bearer " + tokenData.access_token,
    };

    if (params.type == "Client") {
      let fields = "fields=id,name,first_name,middle_name,last_name,date_of_birth,type,primary_address{street,city,province,postal_code,country,name,id},primary_email_address,primary_phone_number,is_client&limit=25"
      if (pageToken) {
        fields += `&page_token=${pageToken}`;
      }

      let url = `${process.env.CLIO_URL}api/v4/contacts.json?${fields}`;
      clientDetails = await axios.request({ url: url, method: "get", headers});
      const clientDataCount = clientDetails?.data?.meta?.records;

      if (clientDetails?.data?.meta?.paging?.next) {
        const clientPageToken = clientDetails.data.meta.paging.next.split("page_token=");
        clientDetails.data.page_token = clientPageToken[1];
        dataObject = Object.assign(
          {},
          {
            clientDetails: clientDetails.data,
            totalCount: clientDataCount,
            token: clientPageToken[1],
            hasMore: true,
          }
        );
      } else {
        dataObject = Object.assign(
          {},
          {
            clientDetails: clientDetails.data,
            totalCount: clientDataCount,
            hasMore: false,
          }
        );
      }
    }

    if (params.type == "Cases") {
      let fields = "fields=id,close_date,description,display_number,client{first_name,id,last_name,middle_name,name,primary_email_address,type},status,originating_attorney{enabled,first_name,id,last_name,name,email},responsible_attorney{enabled,first_name,id,last_name,name,email},&limit=25";

      if (pageToken) {
        fields += `&page_token=${pageToken}`;
      }
      let url = `${process.env.CLIO_URL}api/v4/matters.json?${fields}`;
      caseDetails = await axios.request({ url: url, method: "get", headers});
      const caseDataCount = caseDetails?.data?.meta?.records;
      if (caseDetails?.data?.meta?.paging?.next) {
        const casePageToken = caseDetails.data.meta.paging.next.split("page_token=");
        caseDetails.data.page_token = casePageToken[1];
        dataObject = Object.assign(
          {},
          {
            caseDetails: caseDetails.data,
            totalCount: caseDataCount,
            token: casePageToken[1],
            hasMore: true,
          }
        );
      } else {
        dataObject = Object.assign(
          {},
          {
            caseDetails: caseDetails.data,
            totalCount: caseDataCount,
            hasMore: false,
          }
        );
      }
    }
    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify(dataObject),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Can not fetch clio details",
      }),
    };
  }
};

const destroyTokenData = async (event) => {
  try {
    authorizeDestroyTokenData(event.user);
    const params = event.params || event.pathParameters;

    const practice_id = event.user.practice_id;

    const { Clio } = await connectToDatabase();

    const clioDetails = await Clio.findOne({ where: { practice_id, id: params.id } });

    if (!clioDetails) {
      throw new HTTPError(400, "Details not found for this id");
    }

    await Clio.destroy({ where: { practice_id, id: params.id } });

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "Clio details deleted successfully",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not delete Clio details",
      }),
    };
  }
};

const validateToken = async (event) => {
  try {
    const { Clio, Op, sequelize } = await connectToDatabase();

    let sqlQuery = `SELECT id, practice_id, access_token_timestamp,` + ` DATE_ADD(access_token_timestamp, INTERVAL 30 DAY) AS access_token_valid, now() FROM Clios ` + `where DATE_ADD(access_token_timestamp, INTERVAL 30 DAY) <= now() AND practice_id ='${event.user.practice_id}'`;

    const response = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT,
    });

    if (response.length != 0) {
      const ClioObj = await Clio.findOne({
        where: { practice_id: event.user.practice_id }
      });
      const client_id = process.env.CLIO_CLIENTID;
      const client_secret = process.env.CLIO_CLIENTSECRET;
      let url = process.env.CLIO_URL + 'oauth/token';
      let redirect_uri = process.env.CLIO_REDIRECTURL;
      let refresh_token = ClioObj.refresh_token;


      let headers = {
        Accept: "*/*",
      };
      let params = { refresh_token, "Content-Type": "application/x-www-form-urlencoded", grant_type: "refresh_token", redirect_uri, client_id, client_secret };

      const result = await axios.request({ url: url, params, method: "post", headers });
      if (result?.data && result.data?.access_token) {
        await Clio.destroy({ where: { practice_id: event.user.practice_id } });
        const dataObject = Object.assign(result.data, {
          id: uuid.v4(),
          practice_id: event.user.practice_id,
          auth_code: ClioObj.auth_code,
          access_token: result.data.access_token,
          access_token_timestamp: new Date(),
          refresh_token: ClioObj.refresh_token,
        });
        await Clio.create(dataObject);
      }
    }
  } catch (err) {
    console.log(err);
  }
}

const createClientandCase = async (event) => {
  try {
    authorizeCreateClientandCase(event.user);
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { Clients, Cases, Users, Op } = await connectToDatabase();
    let clientId;
    for (let i = 0; i < input.length; i++) {
      const clientDetails = input[i];

      if (clientDetails && !clientDetails.client_id) throw new HTTPError(400, `client id not found.`);
      if (clientDetails && !clientDetails.name) throw new HTTPError(400, `client name not found.`);

      const clientData = await Clients.findOne({
        where: {
          integration_client_id: clientDetails.client_id,
          is_deleted: { [Op.not]: true },
          practice_id: event.user.practice_id,
          client_from: "clio"
        },
      });

      if (event.user.practice_id) {
        clientDetails.practice_id = event.user.practice_id;
      }
      if (!clientData) {
        const ClientDataObject = Object.assign(clientDetails, {
          id: uuid.v4(),
          clio_client_created: new Date(),
          client_from: 'clio',
          integration_client_id: clientDetails.client_id,
          name: clientDetails.name,
          email: clientDetails?.email || null,
          phone: clientDetails?.phone || null,
          dob: clientDetails?.dob || null,
          address: clientDetails?.address || null,
          clio_phone_reference_id: clientDetails?.clio_phone_reference_id || null,
          clio_email_reference_id: clientDetails?.clio_email_reference_id || null
        });

        const clientsObject = await Clients.create(ClientDataObject);
        clientId = await clientsObject.id;
      } else { clientId = await clientData.id; }

      if (input[i].cases) {
        const cases = input[i].cases;
        const attorney = [];
        const attorneyEmail = [];
        const casesDetails = cases[0];
        casesDetails.client_id = clientId;
        casesDetails.practice_id = event.user.practice_id;
        if (casesDetails?.originating_attorney?.email) { attorneyEmail.push(casesDetails.originating_attorney.email) };
        if (casesDetails?.responsible_attorney?.email) { attorneyEmail.push(casesDetails.responsible_attorney.email) };
        if (attorneyEmail.length != 0) {
          const userData = await Users.findAll({
            where: {
              practice_id: event.user.practice_id,
              email: { [Op.in]: attorney },
              is_deleted: { [Op.not]: true },
            }, raw: true
          });

          for (let j = 0; j < userData.length; j++) {
            attorney.push(userData.id);
          }
        }
        const attorneys = attorney.toString();
        const caseData = await Cases.findOne({
          where: {
            integration_case_id: casesDetails.id,
            client_id: clientId,
            is_deleted: { [Op.not]: true },
            case_form: "clio"
          },
        });
        if (!caseData) {
          const caseDataObject = Object.assign(casesDetails, {
            id: uuid.v4(),
            matter_id: clientDetails.case_id,
            integration_case_id: clientDetails.case_id,
            integration_case_data: JSON.stringify(clientDetails),
            case_form: "clio",
            case_title: clientDetails?.case_title || null,
          });

          if (attorneys) {
            caseDataObject.attorneys = attorneys;
          }

          const casesObject = await Cases.create(caseDataObject);
        }
      }
    }
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: 'ok',
        message: 'Successfully Added Clients And Cases',
      }),
    };
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not create Clients and Cases.' }),
    };
  }
}

const UpdateClientandCase = async (event) => {
  try {
    authorizeUpdateClientandCase(event.user);
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    const { Users, Clients, Cases, Clio, Op } = await connectToDatabase();

    let clientId;

    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    for (let i = 0; i < input.length; i++) {
      let clientDetails = input[i];
      if (!clientDetails.client_id) throw new HTTPError(400, `client id not found.`);
      if (!clientDetails.name) throw new HTTPError(400, `client name not found.`);

      const clientData = await Clients.findOne({
        where: {
          integration_client_id: clientDetails.client_id,
          is_deleted: { [Op.not]: true },
          practice_id: event.user.practice_id,
          client_from: "clio"
        }
      });

      if (clientData) {
        clientData.name = clientDetails?.name || null;
        clientData.email = clientDetails?.email || null;
        clientData.phone = clientDetails?.phone || null;
        clientData.address = clientDetails?.address || null;
        clientData.dob = clientDetails?.dob || null;
        clientData.clio_phone_reference_id = clientDetails?.clio_phone_reference_id || null;
        clientData.clio_email_reference_id = clientDetails?.clio_email_reference_id || null;
      }
      await clientData.save();
      clientId = clientData.id;
      if (input[i].cases) {
        const cases = input[i].cases;
        const casesDetails = cases[0];
        if (!casesDetails.id) { throw new HTTPError(400, `case id not found`); }
        const caseData = await Cases.findOne({
          where: {
            integration_case_id: input[i].case_id,
            client_id: clientId,
            is_deleted: { [Op.not]: true },
            case_from: "clio"
          },
        });
        if (caseData) {
          let attorney = [];
          const attorneyEmail = [];
          casesDetails.client_id = clientId;
          casesDetails.practice_id = event.user.practice_id;
          if (casesDetails?.originating_attorney?.email) { attorneyEmail.push(casesDetails.originating_attorney.email) };
          if (casesDetails?.responsible_attorney?.email) { attorneyEmail.push(casesDetails.responsible_attorney.email) };

          if (caseData.attorneys) {
            attorney = (caseData.attorneys).split(',');
          }

          if (attorneyEmail.length != 0) {
            const userData = await Users.findAll({
              where: {
                practice_id: event.user.practice_id,
                email: { [Op.in]: attorney },
                is_deleted: { [Op.not]: true },
              }, raw: true
            });

            for (let j = 0; j < userData.length; j++) {
              if (userData[j]?.id && (!attorney.includes(userData[j].id))) {
                attorney.push(userData[j].id);
              }
            }
          }
          const attorneys = attorney.toString();
          caseData.case_title = input[i]?.case_title || null;
          caseData.attorneys = attorneys || null;
          await caseData.save();
        }
      }
    }
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: 'ok',
        message: 'Clients and Cases are successfully updated ',
      }),
    };
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not update clients and cases.' }),
    };
  }
}

const updateCaseToClio = async (event) => {
  try {
    authorizeUpdateCaseToClio(event.user);
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    const { Cases, Clio, Op } = await connectToDatabase();

    await validateToken(event);

    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const { integration_case_id, case_title, case_number, date_of_loss } = input;

    const caseData = await Cases.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_case_id: integration_case_id,
      }
    });

    if (!caseData) { throw new HTTPError(400, 'case not found') };

    caseData.case_title = case_title;
    caseData.case_number = case_number;
    caseData.date_of_loss = date_of_loss;
    await caseData.save();

    const headers = {
      ContentType: "application/json",
      Authorization: "Bearer " + tokenData.access_token,
    };

    let data = {
        description: case_title,
    };

    let url = `${process.env.CLIO_URL}api/v4/matters/${integration_case_id}.json`;

    const clioDetails = await axios.request({ url: url, method: "patch", headers, data })

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "client details updated to clio successfully",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not update case details",
      }),
    };
  }
}

const updateClientToClio = async (event) => {
  try {
    authorizeUpdateClientToClio(event.user);
    const input =
      typeof event.body === "string" ? JSON.parse(event.body) : event.body;

    const { Clients, Clio, Op } = await connectToDatabase();

    await validateToken(event);

    const tokenData = await Clio.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (tokenData && !tokenData?.access_token) throw new HTTPError(400, 'Access token not found');

    const { integration_client_id, name, dob, email, phone, address } = input;
    if (!integration_client_id) { throw new HTTPError(400, 'Clio Integration client id not provided') };
    const clientData = await Clients.findOne({
      where: {
        practice_id: event.user.practice_id,
        integration_client_id: integration_client_id,
        client_from: "clio",
        is_deleted: { [Op.not]: true }
      }
    });

    if (!clientData) { throw new HTTPError(400, 'client not found') };

    const headers = {
      ContentType: "application/json",
      Authorization: "Bearer " + tokenData.access_token,
    };

    let url = `${process.env.CLIO_URL}api/v4/contacts/${integration_client_id}.json`;

    let data = {
      name: name,
      phone_numbers: {
        number: phone,
      }
    };

    clientData.name = name;
    clientData.phone = phone;
    if (address) { clientData.address = address }
    if (dob) {
      clientData.dob = dob;
      data.date_of_birth = dob;
    }
    if (email) {
      clientData.email = email;
      data.email_addresses = {
        address: email
      };
    }
    if (clientData.clio_phone_reference_id) { data.phone_numbers.id = clientData.clio_phone_reference_id };
    if (clientData.clio_email_reference_id) { data.email_addresses.address = clientData.clio_email_reference_id };

    await clientData.save();

    const clioDetails = await axios.request({ url: url, method: "patch", headers, data })

    return {
      statusCode: 200,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        status: "ok",
        message: "client details updated to clio successfully",
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      },
      body: JSON.stringify({
        error: err.message || "Could not update client details",
      }),
    };
  }
}

const documentUploadToClio = async() =>{
  try{

  }
  catch(err){
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Origin": true,
      },
      body: JSON.stringify({
        error: err.message || "Document upload to clio failed"
      })
    };
  }
}
module.exports.getClioAuthToken = getClioAuthToken;
module.exports.getClioData = getClioData;
module.exports.destroyTokenData = destroyTokenData;
module.exports.createClientandCase = createClientandCase;
module.exports.UpdateClientandCase = UpdateClientandCase;
module.exports.updateCaseToClio = updateCaseToClio;
module.exports.updateClientToClio = updateClientToClio;
module.exports.documentUploadToClio = documentUploadToClio;