const uuid = require('uuid');
const axios = require("axios");
const connectToDatabase = require('../../../db');
const{ decrypted } = require('../../helpers/filevine.helper');
const {
    validateCreateClients,
    validateCreateCases
} = require('./validation');
const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.REGION || "us-east-1" });
const s3 = new AWS.S3();
const { HTTPError } = require("../../../utils/httpResp");
const { add } = require('lodash');
const createClientandCase = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { Clients, Cases, Op } = await connectToDatabase();
        
        for (let i = 0; i < input.length; i++) {
            if (input[i].dob == "") { input[i].dob = null; }
            const clientDetails = input[i];
            const clientData = await Clients.findOne({
                where: {
                    integration_client_id: input[i].personId.native,
                    client_from: "filevine",
                    is_deleted: { [Op.not]: true },
                    practice_id: event.user.practice_id
                }
            });
            if (event.user.practice_id) {
                clientDetails.practice_id = event.user.practice_id;
            }
            let clientId;
            if (!clientData && clientDetails) {
                const dataObject = Object.assign(clientDetails, {
                    id: uuid.v4(),
                    client_from: 'filevine',
                    integration_client_id: input[i].personId.native
                });

             let name = '' ;
            if(clientDetails.firstName){
                name = clientDetails.firstName;
                dataObject.first_name;
            }
            if (clientDetails.middleName) {
                name = name + ' ' + clientDetails.middleName;
                dataObject.middle_name;
            }
            if (clientDetails.lastName) {
                name = name + ' ' + clientDetails.lastName;
                dataObject.last_name;
            }
            dataObject.name = name;
            
        if(clientDetails.addresses[i-i]){
            
            let address = '';
            if(clientDetails.addresses[i-i].line1 ){
                address = clientDetails.addresses[i-i].line1 + ', ';
            }
            if(clientDetails.addresses[i-i].city){
                address = address + clientDetails.addresses[i-i].city + ', ';
            }
            if(clientDetails.addresses[i-i].state){
                address = address + clientDetails.addresses[i-i].state + ' ';
            }
            if(clientDetails.addresses[i-i].postalCode){
                address = address + clientDetails.addresses[i-i].postalCode;
            }
        
            if(clientDetails.addresses.length > 0){
                dataObject.street = clientDetails.addresses[i-i].line1 || null;
                dataObject.city = clientDetails.addresses[i-i].city || null;
                dataObject.state = clientDetails.addresses[i-i].state || null;
                dataObject.zip_code = clientDetails.addresses[i-i].postalCode || null;
                dataObject.address = address || null;
            }
        };
                // validateCreateClients(dataObject);
                const clientsObject = await Clients.create(dataObject);
                clientId = clientsObject.id;
            } else { clientId = clientData.id }
            if (input[i].cases) {
                const cases = input[i].cases;
                const casesDetails = cases[i-i];
                casesDetails.client_id = clientId;
                casesDetails.practice_id = event.user.practice_id;
                if (casesDetails && !casesDetails.projectId.native) throw new HTTPError(400, `case id not found.`);
                const caseData = await Cases.findOne({
                    where: {
                        integration_case_id: casesDetails.projectId.native,
                        client_id: clientId,
                        case_from: "filevine",
                        is_deleted: { [Op.not]: true }
                    }
                });
                if (!caseData) {
                    const casesDataObject = Object.assign(casesDetails, {
                        id: uuid.v4(),
                        integration_case_id: casesDetails.projectId.native,
                        integration_case_data: JSON.stringify(input[i]),
                        case_from: "filevine",
                        case_title: casesDetails.projectName,
                        case_number: casesDetails.projectId.native,
                        date_of_loss: casesDetails.incidentDate
                    });
                    // validateCreateCases(casesDataObject);
                    const casesObject = await Cases.create(casesDataObject);
                }
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Successfully Created Clients And Cases',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create Clients and Cases.' }),
        };
    }
}

const updateClientandCase = async (event) => {
    try{
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { Clients, Cases, Op } = await connectToDatabase();
        for (let i = 0; i < input.length; i++) {
            const clientDetails = input[i];
            const clientData = await Clients.findOne({
                where: {
                    integration_client_id: input[i].personId.native,
                    client_from: "filevine",
                    is_deleted: { [Op.not]: true },
                    practice_id: event.user.practice_id
                }
            });
            if(clientDetails && clientData){
            let name = '';
            if(clientDetails?.first_name){
                name = clientDetails?.first_name;
            }
            if (clientDetails.middle_name) {
                name = name + ' ' + clientDetails.middle_name;
            }
            if (clientDetails.last_name) {
                name = name + ' ' + input.last_name;
            }
            
            clientData.name = clientDetails.name;
            clientData.first_name = clientDetails.firstName;
            clientData.middle_name = clientDetails.middleName || null;
            clientData.last_name = clientDetails.lastName;
            clientData.name = clientDetails.name;
            clientData.email = clientDetails.email || null;
            clientData.phone = clientDetails.phone || null;
            
            if(clientDetails.addresses[i-i]){
            let address = '';
            if(clientDetails.addresses[i-i].line1 ){
                address = clientDetails.addresses[i-i].line1 + ', ';
            }
            if(clientDetails.addresses[i-i].city){
                address = address + clientDetails.addresses[i-i].city + ', ';
            }
            if(clientDetails.addresses[i-i].state && clientDetails.addresses[i-i].postalCode){
                address = address + clientDetails.addresses[i-i].state + ' - '+ clientDetails.addresses[i-i].postalCode;
            }
            if(clientDetails.addresses[i-i].state && !clientDetails.addresses[i-i].postalCode ){
                address = address + clientDetails.addresses[i-i].state
            }
            if(!clientDetails.addresses[i-i].state && clientDetails.addresses[i-i].postalCode ){
                address = address + clientDetails.addresses[i-i].postalCode
            }

            clientData.dob = clientDetails.dob || null;
            clientData.street = clientDetails.addresses[i-i].line1 || null;
            clientData.city = clientDetails.addresses[i-i].city || null;
            clientData.state = clientDetails.addresses[i-i].state || null;
            clientData.zip_code = clientDetails.addresses[i-i].postalCode || null;
            clientData.address = address || null;
        }
            await clientData.save();
        }
            if (input[i].cases) {
                const cases = input[i].cases;
                const casesDetails = cases[i-i];
                const caseData = await Cases.findOne({
                    where: {
                        integration_case_id: casesDetails.projectId.native,
                        client_id: clientData.id,
                        practice_id: event.user.practice_id,
                        case_from: "filevine",
                        is_deleted: { [Op.not]: true }
                    }
                });

                if(caseData){
                    caseData.integration_case_data = JSON.stringify(input[i]);
                    caseData.case_title = casesDetails.projectName;
                    await caseData.save();
                }
            }
        }
        
        
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Successfully Updated Clients And Cases',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not Update Clients and Cases.' }),
        };
    }
}

const fileVinekeydetails = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const practice_id = event.user.practice_id;
        const { Users, Filevine } = await connectToDatabase();

        const fileVinedetails = await Filevine.findOne({ where:{practice_id:event.user.practice_id} });

        const filevineData = {
            practice_id: event.user.practice_id,
            user_id: event.user.id,
            filevine_user_id: input.filevine_user_id,
            filevine_org_id: input.filevine_org_id,
            filevine_key: input.filevine_key,
            filevine_secret: input.filevine_secret,
            filevine_refresh_token: input.filevine_refresh_token,
            filevine_hash: input.filevine_hash,
            filevine_timeStamp: input.filevine_timeStamp
        }
        if (!fileVinedetails) {
            const dataObject = Object.assign(filevineData, { id: uuid.v4() });

            const userDetails = await Filevine.create(dataObject);
        } else {
            const userDetails = await Filevine.update(filevineData, { where: { id: fileVinedetails.id } });
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Filevine details saved',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not save filevine details' }),
        };
    }
}

const destroyTokenData = async (event) => {
    try {
        const params = event.params || event.pathParameters;

        const practice_id = event.user.practice_id;

        const { Filevine } = await connectToDatabase();

        const fileVinedetails = await Filevine.findOne({ where: {practice_id, id: params.id} });

        if (!fileVinedetails.id) { throw new HTTPError(400, "Details not found for this id") };

        await Filevine.destroy({ where: { practice_id, id: params.id } });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Filevine details deleted successfully',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not delete filevine details' }),
        };
    }
}

const fileUpload = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        if (!input || !input.s3_file_key) throw new HTTPError(404, `S3 file key not found.`);
        if (!input.url) throw new HTTPError(404, `upload url not found.`);
        const s3_file_key = input.s3_file_key;
        const uploadurl = input.url;
        const contentType = input.contentType;
        const filedata = await readDiscoveryFile(s3_file_key);


        await axios.put(uploadurl, filedata, {
            headers: {
                Accept: 'application/json',
                'Content-Type': contentType,
            },
        }).then((response) => console.log(response)).catch(err => {
            console.log(err);
        });

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                status: "ok",
                message: "Files successfully uploaded",
            }),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({ error: err.message || "Could upload file" }),
        };
    }
}

const readDiscoveryFile = (filename) => {
    return new Promise(function (resolve, reject) {
        const s3BucketParams = {
            Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
            Key: filename,
        };
        s3.getObject(s3BucketParams, function (err, data) {
            if (err) {
                console.log(err);
                reject(err.message);
            } else {
                //var data = Buffer.from(data.Body).toString('utf8');
                resolve(data.Body);
            }
        });
    });
};

const fileSizeUsingS3Key = async (event) => {
    try {
        const input_body = typeof event.body === "string" ? JSON.parse(event.body) : event.body;

        let s3_file_key = input_body.s3_file_key;

        const s3BucketParams = {
            Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS,
            Key: s3_file_key,
        };
        let response = await s3.getObject(s3BucketParams).promise();

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                status: "ok",
                file_size: response.ContentLength
            }),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({ error: err.message || "Could not find file" }),
        };
    }
}

const updateClientToFilevine = async(event) => {
    try{
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { Clients, Cases, Users, Filevine, Op } = await connectToDatabase();
        const { integration_client_id, dob, email, phone, first_name, middle_name, last_name, street, city, state, zip_code, access_token, refresh_token, filevine_org_id, filevine_user_id } = input;

        if(!access_token){ throw new HTTPError(400,'access_token not found in input')};
        if(!refresh_token){ throw new HTTPError(400,'access_token not found in input')};
        const accessToken = decrypted(access_token, event.user.id);
        const refreshToken = decrypted(refresh_token, event.user.id);

        const clientData = await Clients.findOne({ where:{
            integration_client_id: integration_client_id,
            client_from: 'filevine',
            practice_id: event.user.practice_id,
            is_deleted: { [Op.not]: true }
        }});

        if(!clientData){ throw new HTTPError(400,'client not found')};

        const userData = await Users.findOne({where: { id: event.user.id,is_deleted: { [Op.not]: true } }});
        if(!userData){ throw new HTTPError(400,'user not found')};

        const caseData = await Cases.findOne({where: { 
            client_id: clientData.id, 
            practice_id: event.user.practice_id,
            case_from: 'filevine',
            is_deleted: { [Op.not]: true }
        }});

        if(!caseData){ throw new HTTPError(400,'case not found for this client')};
        const filevineCaseData =  JSON.parse(caseData?.integration_case_data);
        let name = '', address;
        if(first_name){
            name = first_name;
        }
        if (middle_name) {
            name = name + ' ' + middle_name;
        }
        if (last_name) {
            name = name + ' ' + last_name;
        }
        clientData.name = name;
        clientData.first_name = first_name;
        clientData.middle_name = middle_name || null;
        clientData.last_name = last_name;
        clientData.phone = phone;

    let data = {
      firstName: first_name,
      middleName: middle_name || null,
      lastName: last_name,
      phones:[{
        phoneId: filevineCaseData?.phones.phoneId,
        number: phone
      }]
    };
    if(phone){
        for(let i = 1; i < filevineCaseData.phones.length; i++){
            data.phones.push(filevineCaseData.phones[i]);
        }
    }
    if (dob) {
        clientData.dob = dob;
        data.birthdate = dob;
      }

    if(street){
        address = street + ', ' + city + ', ' + state + ', ' + zip_code;
        clientData.address = address;
        clientData.street = street;
        clientData.city = city;
        clientData.state = state;
        clientData.zip_code = zip_code;

        data.addresses = [{
            addressId: filevineCaseData.addresses.addressId,
            line1: street,
            line2: null,
            city,
            state,
            postalCode: zip_code
        }];
        for(let j = 1; j < filevineCaseData.addresses.length; j++){
            data.addresses.push(filevineCaseData.addresses[j]);
        }
    }
    
    if (email) {
      clientData.email = email;
      data.emails = [{
        emailId: filevineCaseData.emails.emailId,
        address: email
      }];
      for(let k = 1; k < filevineCaseData.emails.length; k++){
        data.emails.push(filevineCaseData.emails[k]);
    }
    }

    const url = `${process.env.FILEVINE_URL}/contacts/${integration_client_id}`;
    const headers = {
        ContentType: "application/json",
        Authorization: "Bearer " + accessToken,
        "x-fv-orgid": filevine_org_id,
        "x-fv-userid": filevine_user_id,
        "x-fv-sessionid": refreshToken
        };

    await clientData.save();
    const FilevineDetails = await axios.request({ url: url, method: "patch", headers, data });
    let caseIntegrationData = {};
    caseIntegrationData = FilevineDetails.data;
    caseIntegrationData.cases = filevineCaseData.cases;
    caseData.integration_case_data = JSON.stringify(caseIntegrationData);
    await caseData.save();
        return {
            statusCode: 200,
            headers: {
              "Content-Type": "text/plain",
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
              status: "ok",
              message: "client details updated to filevine successfully",
              response: FilevineDetails.data
            }),
          };
        } catch (err) {
          console.log(err);
          return {
            statusCode: err.statusCode || 500,
            headers: {
              "Content-Type": "text/plain",
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
              error: err.message || "Could not update client details",
            }),
          };
        }
};

const updateCaseToFilevine = async(event) => {
    try{
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { Cases, Users, Filevine, Op } = await connectToDatabase();
        const { integration_case_id, case_title, case_number, date_of_loss, access_token, refresh_token, filevine_org_id, filevine_user_id } = input;

        if(!access_token){ throw new HTTPError(400,'access_token not found in input')};
        if(!refresh_token){ throw new HTTPError(400,'access_token not found in input')};
        const accessToken = decrypted(access_token, event.user.id);
        const refreshToken = decrypted(refresh_token, event.user.id);

        const userData = await Users.findOne({where: {id: event.user.id,is_deleted: { [Op.not]: true } }});
        if(!userData){ throw new HTTPError(400,'user not found')};

        const caseData = await Cases.findOne({where: { 
            integration_case_id: integration_case_id, 
            practice_id: event.user.practice_id,
            case_from: 'filevine',
            is_deleted: { [Op.not]: true }
        }});

          if (!caseData) { throw new HTTPError(400, 'case not found') };
          caseData.case_title = case_title;
          caseData.case_number = case_number;
          caseData.date_of_loss = date_of_loss;
          await caseData.save();

        let url = `${process.env.FILEVINE_URL}/projects/${integration_case_id}`;

        const headers = {
            ContentType: "application/json",
            Authorization: "Bearer " + accessToken,
            "x-fv-orgid": filevine_org_id,
            "x-fv-userid": filevine_user_id,
            "x-fv-sessionid": refreshToken
            };
        
        const data = {
            projectName: case_title
        } 

        const filevineDetails = await axios.request({ url: url, method: "patch", headers, data })

        return {
            statusCode: 200,
            headers: {
              "Content-Type": "text/plain",
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
              status: "ok",
              message: "case details updated to filevine successfully",
             Response: filevineDetails.data
            }),
          };
        } catch (err) {
          console.log(err);
          return {
            statusCode: err.statusCode || 500,
            headers: {
              "Content-Type": "text/plain",
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
              error: err.message || "Could not update case details",
            }),
          };
        }
}

const commonUpdateFilevine = async(event) => {
    try{
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { Clients, Cases, Users, Op } = await connectToDatabase();
        const { integration_client_id, client_phone, access_token, refresh_token, filevine_org_id, filevine_user_id } = input;

        if(!access_token){ throw new HTTPError(400,'access_token not found in input')};
        if(!refresh_token){ throw new HTTPError(400,'access_token not found in input')};
        const accessToken = decrypted(access_token, event.user.id);
        const refreshToken = decrypted(refresh_token, event.user.id);

        const clientData = await Clients.findOne({ where:{
            integration_client_id: integration_client_id,
            client_from: 'filevine',
            practice_id: event.user.practice_id,
            is_deleted: { [Op.not]: true }
        }});

        if(!clientData){ throw new HTTPError(400,'client not found')};

        const userData = await Users.findOne({where: { id: event.user.id,is_deleted: { [Op.not]: true } }});
        if(!userData){ throw new HTTPError(400,'user not found')};
        

        const caseData = await Cases.findOne({where: { 
            client_id: clientData.id, 
            practice_id: event.user.practice_id,
            case_from: 'filevine',
            is_deleted: { [Op.not]: true }
        }});

        if(!caseData){ throw new HTTPError(400,'case not found for this client')};
        const filevineCaseData =  JSON.parse(caseData?.integration_case_data);
        
    let data = {
      phones:[{
        phoneId: filevineCaseData?.phones.phoneId,
        number: client_phone
      }]
    };
    if(client_phone){
        for(let i = 1; i < filevineCaseData.phones.length; i++){
            data.phones.push(filevineCaseData.phones[i]);
        }
    }

    const url = `${process.env.FILEVINE_URL}/contacts/${integration_client_id}`;
    const headers = {
        ContentType: "application/json",
        Authorization: "Bearer " + accessToken,
        "x-fv-orgid": filevine_org_id,
        "x-fv-userid": filevine_user_id,
        "x-fv-sessionid": refreshToken
        };
        
    const FilevineDetails = await axios.request({ url: url, method: "patch", headers, data });

        return {
            statusCode: 200,
            headers: {
              "Content-Type": "text/plain",
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
              status: "ok",
              message: "client details updated to filevine successfully",
              response: FilevineDetails.data
            }),
          };
        } catch (err) {
          console.log(err);
          return {
            statusCode: err.statusCode || 500,
            headers: {
              "Content-Type": "text/plain",
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
              error: err.message || "Could not update client details",
            }),
          };
        }
};

module.exports.createClientandCase = createClientandCase;
module.exports.fileVinekeydetails = fileVinekeydetails;
module.exports.destroyTokenData = destroyTokenData;
module.exports.fileUpload = fileUpload;
module.exports.fileSizeUsingS3Key = fileSizeUsingS3Key;
module.exports.updateClientandCase = updateClientandCase;
module.exports.updateClientToFilevine = updateClientToFilevine;
module.exports.updateCaseToFilevine = updateCaseToFilevine;
module.exports.commonUpdateFilevine = commonUpdateFilevine;