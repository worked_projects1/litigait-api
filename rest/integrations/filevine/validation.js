const Validator = require('validatorjs');
const {HTTPError} = require('../../../utils/httpResp');

exports.validateCreateClients = function (data) {
    const rules = {
        practice_id: 'required',
        name: 'min:4|max:64',
        email: 'email|min:4|max:64',
        phone: 'required|numeric',
        address: 'min:4|max:64',
        hipaa_acceptance_status: 'boolean',
        hipaa_acceptance_date: 'date',
        fee_acceptance_status: 'boolean',
        fee_acceptance_date: 'date',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
exports.validateCreateCases = function (data) {
    const rules = {
        client_id: 'required|min:4|max:64',
        practice_id: 'required',
        start_date: 'date',
        case_title: 'required|min:4|max:200',
        case_number: 'required|min:1|max:64',
        status: 'min:3|max:64',
        state: 'required|min:2|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};