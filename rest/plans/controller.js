const uuid = require('uuid');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const {sendEmail} = require('../../utils/mailModule');

const {
    validateCreate,
    validateGetOne,
    validateUpdate,
    validateDelete,
    validateRead,
} = require('./validation');

const {
    authorizeCreate,
    authorizeGetOne,
    authorizeGetAll,
    authorizeUpdate,
    authorizeDestroy,
} = require('./authorize');

const create = async (event) => {
    try {
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (event.user.id) {
            input.created_by_admin_id = event.user.id;
        }
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, {id: id || uuid.v4()});
        // validateCreate(dataObject);
        // authorizeCreate(event.user);
        const {Plans, Users, Op} = await connectToDatabase();
        if (dataObject.price && input.plan_type != 'pay_as_you_go' && input.plan_type != 'free_trial') {
            const product = await stripe.products.create({
                name: dataObject.name,
            });
            const price = await stripe.prices.create({
                unit_amount: parseFloat(dataObject.price) * 100,
                currency: 'usd',
                recurring: {interval: 'day', interval_count: parseInt(input.validity)},
                product: product.id,
            });
            dataObject.stripe_product_id = product.id;
            dataObject.stripe_price_id = price.id;
            dataObject.plan_id = price.id;
        }
        if (input.plan_type === 'pay_as_you_go') {
            dataObject.plan_id = 'pay_as_you_go';
        }
        if (input.plan_type === 'free_trial') {
            dataObject.plan_id = 'free_trial';
        }
        const planModelObject = await Plans.create(dataObject);
        const plainplanModel = planModelObject.get({plain: true});
        const UsersModel = await Users.findOne({
            where: {
                id: plainplanModel.created_by_admin_id,
                is_deleted: {[Op.not]: true}
            }, raw: true
        });
        if (UsersModel) plainplanModel.created_by_admin_name = UsersModel.name;
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainplanModel),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the planModelObject.'}),
        };
    }
};


const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        /* validateGetOne(params);
        authorizeGetOne(event.user); */
        const {Plans} = await connectToDatabase();
        const planModelObject = await Plans.findOne({where: {id: params.id}});
        if (!planModelObject) throw new HTTPError(404, `Plans with id: ${params.id} was not found`);
        const plainCustomerObjection = planModelObject.get({plain: true});
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainCustomerObjection),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Plans.'}),
        };
    }
};

const getAll = async (event) => {
    try {
        const {Plans, Users, Op, Practices, Settings} = await connectToDatabase();
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.order = [
            ['order', 'ASC'],
        ];
        query.raw = true;
        const userInfo = event.user;
        let currentYear = new Date().getFullYear();
        // if (!query.page || query.page == 'homePage' || !query.page == 'settings' || !query.page == 'adminSettings') {
        //     query.where.plan_id = {[Op.notIn]: ['pay_as_you_go', 'free_trial']};
        // }

        console.log(query.page);
        // if(query.page == 'settings'){
        //     if (currentYear == 2022) query.where.plan_id = {[Op.notIn]: ['responding_monthly_349','responding_yearly_3490']};
        // }

        if(userInfo?.practice_id){
            const practiceObj = await Practices.findOne({where:{id:userInfo.practice_id},raw:true});
            const settingsObject = await Settings.findOne({where: { key: 'global_settings'}});
            const plainSettingsObject = settingsObject.get({plain: true});
            const settings = JSON.parse(plainSettingsObject.value);
            
            let practice_created_year = new Date(practiceObj.createdAt).getFullYear();
            console.log(typeof practice_created_year);
            console.log(practice_created_year);
            let practice_created_before = 2022; 
            let practice_created_after = 2023;
            if (practice_created_year <= practice_created_before) {
                query.where = {
                    [Op.or]: [
                        { practice_created_pre_post_year: { [Op.lte]: practice_created_before } }, { plan_category: 'propounding' },
                        { plan_id: { [Op.in]: ['pay_as_you_go', 'free_trial'] } }
                    ]
                }
            } else {
                query.where = {
                    [Op.or]: [
                        { practice_created_pre_post_year: { [Op.gte]: practice_created_after } }, { plan_category: 'propounding' },
                        { plan_id: { [Op.in]: ['pay_as_you_go', 'free_trial'] } }
                    ]
                }

            }
        }

        query.where.active = true;
        query.logging = console.log;


        const planModelObjects = await Plans.findAll(query);
        for (let i = 0; i < planModelObjects.length; i += 1) {
            const UsersModel = await Users.findOne({
                where: {
                    id: planModelObjects[i].created_by_admin_id,
                    is_deleted: {[Op.not]: true}
                }, raw: true
            });
            planModelObjects[i].created_by_admin_name = UsersModel.name;
            if (planModelObjects[i].plan_type === 'pay_as_you_go' && !planModelObjects[i].plan_id) {
                planModelObjects[i].plan_id = 'pay_as_you_go';
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(planModelObjects),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the planModelObjects.'}),
        };
    }
};

const getAllSignup = async (event) => {
    try {
        const { Plans, Users, Op, Practices, Settings } = await connectToDatabase();
        const query = event.queryStringParameters || event.query || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.order = [
            ['order', 'ASC'],
        ];
        query.raw = true;
        if (!query.page || query.page == 'homePage' || !query.page == 'settings' || !query.page == 'adminSettings') {
            query.where.plan_type = {[Op.notIn]: ['pay_as_you_go', 'free_trial','propounding_yearly_2199','propounding_monthly_199','responding_monthly_349','responding_yearly_3490']};
            let currentYear = new Date().getFullYear();
            const settingsObject = await Settings.findOne({where: { key: 'global_settings'}});
            const plainSettingsObject = settingsObject.get({plain: true});
            const settings = JSON.parse(plainSettingsObject.value);
            
            let practice_created_before = new Date(settings.old_pricing_till).getFullYear(); 
            console.log('settings.old_pricing_till : '+settings.old_pricing_till);
            console.log('practice_created_before : '+practice_created_before);
            let practice_created_after = new Date(settings.new_pricings_from).getFullYear();
            if (new Date().getTime() >= new Date(settings.new_pricings_from).getTime() ) {
                query.where.practice_created_pre_post_year = {[Op.gte]:practice_created_after}
            }else{
                query.where.practice_created_pre_post_year = {[Op.lte]:practice_created_before}
            }
        }
        query.where.active = true;
        query.logging = console.log;

        const planModelObjects = await Plans.findAll(query);
        for (let i = 0; i < planModelObjects.length; i += 1) {
            const UsersModel = await Users.findOne({
                where: {
                    id: planModelObjects[i].created_by_admin_id,
                    is_deleted: {[Op.not]: true}
                }, raw: true
            });
            planModelObjects[i].created_by_admin_name = UsersModel.name;
            if (planModelObjects[i].plan_type === 'pay_as_you_go' && !planModelObjects[i].plan_id) {
                planModelObjects[i].plan_id = 'pay_as_you_go';
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(planModelObjects),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the planModelObjects.'}),
        };
    }
};

const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        validateUpdate(input);
        authorizeUpdate(event.user);
        const {Plans} = await connectToDatabase();
        const planModelObject = await Plans.findOne({where: {id: params.id}});
        if (!planModelObject) throw new HTTPError(404, `Plans with id: ${params.id} was not found`);
        if (planModelObject.plan_type !== 'pay_as_you_go' && planModelObject.plan_type !== 'free_tier') {
            if (input.price && input.price !== planModelObject.price) {
                const price = await stripe.prices.create({
                    unit_amount: parseFloat(input.price) * 100,
                    currency: 'usd',
                    recurring: {interval: 'day', interval_count: parseInt(input.validity || planModelObject.validity)},
                    product: planModelObject.stripe_product_id,
                });
                input.stripe_price_id = price.id;
                input.plan_id = price.id;
            }
            if (input.name && input.name !== planModelObject.name) {
                await stripe.products.update(
                    planModelObject.stripe_product_id,
                    {name: input.name}
                );
            }
        }

        const updatedModel = Object.assign(planModelObject, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Plans.'}),
        };
    }
};

const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateDelete(params);
        authorizeDestroy(event.user);
        const {Plans} = await connectToDatabase();
        const adminObjectionsDetails = await Plans.findOne({where: {id: params.id}});
        if (!adminObjectionsDetails) throw new HTTPError(404, `Plans with id: ${params.id} was not found`);
        await adminObjectionsDetails.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(adminObjectionsDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could destroy fetch the Plans.'}),
        };
    }
};

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.getAllSignup = getAllSignup;