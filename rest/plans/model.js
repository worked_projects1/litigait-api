module.exports = (sequelize, type) => sequelize.define('Plans', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    plan_id: type.STRING,
    stripe_product_id: type.STRING,
    stripe_price_id: type.STRING,
    plan_type: type.STRING,
    name: type.STRING,
    price: type.FLOAT,
    description: type.TEXT,
    validity: type.INTEGER,
    features_included: type.TEXT,
    custom_pricing_text: type.TEXT, // JSON Data
    created_by_admin_id: type.STRING,
    active: type.BOOLEAN,
    order: type.INTEGER,
    plan_category: type.STRING,
    practice_created_pre_post_year :{
        type:type.INTEGER,
        comment: '2022-Pre. 2023-Post.'
    }
});
/* 
    Plans must be displayed based on the practice's creation year. If the practise was created on or before 2022, 
    the monthly and yearly plans are different. if the practise was created after 2023 and has a new monthly and yearly plan.
*/