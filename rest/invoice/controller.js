const uuid = require('uuid');
const AWS = require('aws-sdk');

AWS.config.update({region: process.env.REGION || 'us-east-1'});
const s3 = new AWS.S3();

const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const {sendEmail} = require('../../utils/mailModule');

const sendInvoice = async (event) => {
    try {
        const users = event.user;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (!users.email) throw new HTTPError(400, 'Email ID was not found for this User');
        if (!input.case_id) throw new HTTPError(400, 'Case ID was not found');
        const {Practices, Users, Clients, Cases} = await connectToDatabase();
        const caseDetails = await Cases.findOne({where: {id: input.case_id}});
        const s3BucketParams = {Bucket: process.env.S3_BUCKET_FOR_INVOICES};
        s3BucketParams.Key = input.invoice_s3_key;
        const pdfLink = await s3.getSignedUrlPromise('getObject', s3BucketParams);
        const attachments = [
            {
                filename: input.invoice_name,
                href: pdfLink, // URL of document save in the cloud.
                contentType: 'application/pdf',
            },
        ];
        let email = users.email;
        let template = 'Hi';
        if(users?.name) template = template + ' '+users.name; 
        const body = template+','+ '<br><br>' + 'We have attached the invoice of medical history order for the case - ' + caseDetails.case_title + '<br><br>' +
            'Notification from EsquireTek.';
        await sendEmail(email, 'Invoice For Medical History Order', body, 'EsquireTek', attachments);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({message: "Invoice send successfully."}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not send email.'}),
        };

    }
}
module.exports.sendInvoice = sendInvoice;