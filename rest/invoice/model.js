module.exports = (sequelize, type) => sequelize.define('Invoices', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    s3_file_key: type.STRING,
    medical_summery_id: type.STRING,
    user_id: type.STRING,
    practice_id: type.STRING,
    case_id: type.STRING,
    client_id: type.STRING,
    order_id: type.STRING,
    invoice_no: {
        type: type.INTEGER,
        autoIncrement: true,
        allowNull: false,
        unique: true,
    },
    invoice_date: type.DATE,
    invoice_type: type.STRING, //Refund , Paid
    invoice_name: type.STRING, //Case Title + invoice no
    qty: type.STRING,
    description: type.STRING, // Case Title
    unitprice: type.STRING,
    total: type.STRING,
    subtotal: type.STRING,
    salestax: type.STRING,
    shipping_and_handling: type.STRING,
    total_due: type.STRING,
    is_deleted: type.BOOLEAN,
}); 