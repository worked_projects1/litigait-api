module.exports = (sequelize, type) => sequelize.define('Subscriptions', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    subscribed_by: type.STRING,
    subscribed_on: type.DATE,
    plan_id: type.STRING,
    stripe_subscription_id: type.STRING,
    subscribed_valid_till: type.DATE,
    stripe_subscription_data: type.TEXT,
    subscription_meta_data: type.TEXT,
    stripe_product_id: type.STRING,
    plan_category: type.STRING,
    is_subscription_updated: type.BOOLEAN,
    is_subscription_ended: type.BOOLEAN,
});

/* 

ALTER TABLE `litigait`.`Subscriptions` 
ADD COLUMN `is_subscription_updated` TINYINT(1) NULL DEFAULT NULL AFTER `plan_category`,
ADD COLUMN `is_subscription_ended` TINYINT(1) NULL DEFAULT NULL AFTER `is_subscription_updated`;

*/