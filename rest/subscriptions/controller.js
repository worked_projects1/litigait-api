const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const { sendEmail } = require('../../utils/mailModule');
const { validateCreate, validateGetOne, validateUpdate, validateDelete, validateRead } = require('./validation');
const { dateDiffInDays, unixTimeStamptoDateTime, unixTimeStamptoDate } = require('../../utils/timeStamp');
const { authorizeAdminGetAll } = require('./authorize');
const { QueryTypes } = require('sequelize');
const { customDateandTime } = require('../../utils/datetimeModule');

const subscriptionPlans = [ "monthly", "yearly","propounding_monthly_199","propounding_yearly_2199","responding_monthly_349","responding_monthly_495","responding_yearly_3490","responding_yearly_5100"];
const monthlySubscriptionPlans = [ "monthly", "propounding_monthly_199", "responding_monthly_349", "responding_monthly_495"];
const yearlySubscriptionPlans = [ "yearly", "propounding_yearly_2199", "responding_yearly_3490", "responding_yearly_5100",];
const free_trial_eligible_plans = [ "yearly", "responding_yearly_5100", "responding_yearly_3490"];

const create = async (event) => {
    try {
        const { Practices, Plans, Subscriptions,  DiscountCode, DiscountHistory,Settings, Users, Op } = await connectToDatabase();
        const page = event.body.page;

        const emailText = event.user.email;
        let userName = '';
        let id, discountObj, planType, planDuration;
        let subscription_is_eligible_to_couponcode = false;

        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (page == 'register' && input.subscription_id) {
            await Subscriptions.destroy({ where: { id: input.subscription_id, practice_id: event.user.practice_id } });
        }
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4(), practice_id: event.user.practice_id });

        if (!input.plan_id) throw new HTTPError(400, 'selected plan is empty.');
        const plansObj = await Plans.findOne({ where: { plan_id: input.plan_id, active: true }, raw: true });
        if (!plansObj) throw new HTTPError(400, 'please check selected plan details.');
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        planType = plansObj.plan_type;        
        const usersObj = await Users.findOne({ where: { id: event.user.id,is_deleted:{[Op.not]:true} } });
        if (input.plan_id === 'free_trial') {
            Object.assign(dataObject, { practice_id: event.user.practice_id, subscribed_by: event.user.id, subscribed_on: new Date() });
            await Subscriptions.create(dataObject);
        } else {
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });
            if (paymentMethods.data.length == 0) throw new HTTPError(400, 'This customer has no attached payment source or default payment method.');
            
            event.body.paymentMethod_id = paymentMethods.data[0];

            const getAllActiveSubscriptions = await Subscriptions.findAll({
                where: {
                    practice_id: event.user.practice_id,
                    plan_category: input.plan_category,
                    plan_id: { [Op.not]: [null, 'free_trail'] },
                },
                raw: true,
            });
            if (getAllActiveSubscriptions.length != 0) throw new HTTPError(`This practice already have subscriptions.`);

            if (input.discount_code) {
                discountObj = await DiscountCode.findOne({ where: { discount_code: input.discount_code, is_deleted: { [Op.not]: true } }, raw: true });
                if (!discountObj?.id) throw new HTTPError(400, `Invalid discount code ${input.discount_code}.`);
                const is_eligible_for = discountObj.plan_type.split(',');
                if (is_eligible_for.includes(plansObj.plan_type)) subscription_is_eligible_to_couponcode = true;
            }
            /* One Time Fees charges Here */
            if (practiceObject.one_time_activation_fee) {
                event.body.plan_type = plansObj.plan_type;
                let response = await oneTimeActivationCharge(event);
                if (!response?.status) throw new HTTPError(400, response.message);
            }

            const settingsObject = await Settings.findOne({where: { key: 'global_settings'}});
            const plainSettingsObject = settingsObject.get({plain: true});
            const settings = JSON.parse(plainSettingsObject.value);
            
            let practice_created_before =new Date(settings.old_pricing_till).getFullYear(); 
            let practice_created_after =new Date(settings.new_pricings_from).getFullYear();

            let practiceCreatedYear = new Date(practiceObject.createdAt).getFullYear();
            

            let createObject = {
                customer: practiceObject.stripe_customer_id,
                items: [
                    { price: dataObject.plan_id },
                ],
                metadata: {
                    practice_id: event.user.practice_id,
                    user_email: event.user.email,
                    plan_id: dataObject.plan_id,
                    stripe_product_id: plansObj.stripe_product_id,
                },
            };

            /* Check discount */

            if (input.discount_code && new Date(practiceObject.createdAt).getTime() >= new Date(settings.new_pricings_from).getTime() && subscription_is_eligible_to_couponcode) {
                let stripe_couponcode_obj = JSON.parse(discountObj.stripe_obj);
                createObject.coupon = stripe_couponcode_obj.id;
            }
            /* Check Yearly free trial  */
            /*if(practiceObject.is_yearly_free_trial_available && free_trial_eligible_plans.includes(plansObj.plan_type)){
                createObject.trial_period_days = 60;
            } */
            const subscription = await stripe.subscriptions.create(createObject);
            console.log(subscription);
            if (subscription && subscription.status && subscription.status === 'incomplete') {
                await stripe.subscriptions.del(subscription.id);
                await Subscriptions.create({
                    id: dataObject.id, practice_id: dataObject.practice_id,
                    subscribed_by: event.user.id, subscribed_on: new Date(),
                    stripe_subscription_data: JSON.stringify(subscription),
                    plan_category: plansObj.plan_category
                });

                throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
            }
            if (practiceObject.is_yearly_free_trial_available && free_trial_eligible_plans.includes(plansObj.plan_type)) {
                practiceObject.is_yearly_free_trial_available = false;
                await practiceObject.save();
            }

            const subscriptionValidity = new Date(parseInt(subscription.current_period_end) * 1000);
            await Subscriptions.destroy({ where: { practice_id: dataObject.practice_id, plan_id: { [Op.or]: [null, 'free_trial'] } }, logging: console.log });
            const subscriptionObj = await Subscriptions.create({
                id: dataObject.id, practice_id: dataObject.practice_id,
                subscribed_by: event.user.id, subscribed_on: new Date(), plan_id: dataObject.plan_id, stripe_subscription_id: subscription.id,
                stripe_subscription_data: JSON.stringify(subscription), subscription_meta_data: '', stripe_product_id: plansObj.stripe_product_id,
                subscribed_valid_till: subscriptionValidity, plan_category: plansObj.plan_category
            });
            /* This if condition only work's in local level. */
            if(subscription?.discount?.coupon?.id && subscription.status == 'active' && process.env.NODE_ENV === 'test' ){
                console.log('inside discount history');
                let discountCodeObj,discount_percentage,discounted_price, discount_amount;

                /* console.log(JSON.stringify(invoice)); */

                const discount_code = subscription.discount.coupon.name;
                discountCodeObj = await DiscountCode.findOne({where:{discount_code:discount_code}});
                discount_percentage = discountCodeObj.discount_percentage;
                const percentage = 1 - (discount_percentage/100);
                discounted_price = (percentage * plansObj.price).toFixed(2);

                let plan_type = plansObj.plan_type.split('_').slice(0,plansObj.plan_type.length-1);
                let discount_for = plan_type.slice(0,plan_type.length-1).join('_').toLowerCase();
                discount_amount = plansObj.price - parseFloat(discounted_price);

                let discount_history = {
                    id: uuid.v4(), practice_id: dataObject.practice_id, plan_category: plansObj.plan_category,
                    discount_for, promocode: discountCodeObj.name, discount_percentage,   plan_type: plansObj.plan_type, discounted_price, discount_amount, base_price: plansObj.price
                }
                console.log(discount_history);
                await DiscountHistory.create(discount_history);
            }

            let bodyTxt = '';
            let subject = '';
            let firstPoint = '';
    
            if (monthlySubscriptionPlans.includes(planType)) {
                planDuration = "month";
            }
            if (yearlySubscriptionPlans.includes(planType)) {
                planDuration = "year";
            }
            if (page == 'register') {
                subject = 'Welcome To EsquireTek';
                firstPoint = `<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thank you for signing up for the ${plansObj.name}. We are extremely excited for you to join our community and use our platform to your firm’s benefit. Please note that your subscription comes with the following benefits.</p>`;
            } else if (page == 'settings') {
                subject = 'Your EsquireTek Subscription Benefits';
                firstPoint = `<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thank you for signing up for the ${plansObj.name}.
                        Please note that your subscription comes with the following benefits.</p>`;
            }
            FROM_NAME = 'EsquireTek';
            bodyTxt = `${firstPoint
    
                }<div style="width:auto;">` +
                '<ol style="margin:0px;padding:0px;padding-left:33px">' +
                '<li>' +
                '10% off all medical summary orders – Many firms have used our overseas medical summary team to get summaries of extensive medical records and save their firm time and money. ' +
                'You will now get those same high quality services at a 10% discount!' +
                '</li>' +
                '<li>' +
                'A dedicated account manager – shortly your account executive will reach out to you to introduce themselves and let you know that you have a direct point of contact should you have any questions or issues. ' +
                'We want to make sure you are getting the most out of your subscription and your AE will help you do that.' +
                '</li>' +
                '<li>' +
                'Unlimited Discovery and Unlimited TekSign – You may or may not have been using all of the features available to you but they are now unlimited and we highly recommend you try out our shell service, the completed discovery service, and TekSign. ' +
                'TekSign allows clients to sign verification via their cellphone!' +
                '</li>' +
                '</ol>' +
                '</div>' +
    
                '<p style="marigin-bottom:5px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thanks so much for trusting us and we hope we can help build and scale your practice for the future! </p>' +
                '<p style="marigin-top:0px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your credit card will be billed every ' + planDuration + ' per our terms of service. If you would like to cancel at anytime please email us at support@esquiretek.com.</p>';
    
                let template;
                if(usersObj?.name){ template = 'Dear' + ' ' + usersObj.name}else{ template = 'Hi' };
            await sendEmail(emailText, subject, `${template}, <br/> ${bodyTxt} <br/>`, 'EsquireTek');
            }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'subscriptions created successfully.'
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the subscription.' }),
        };
    }
}

const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        const { Subscriptions, Settings, Plans, Users, Practices, SubscriptionHistory, DiscountCode, DiscountHistory, Op } = await connectToDatabase();

        let id, discountObj;
        let is_eligible_to_couponcode = false;

        const practiceModelObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        const plansObj = await Plans.findOne({ where: { plan_id: input.plan_id, active: true }, logging:console.log,raw: true });
        if (!plansObj) throw new HTTPError(400, 'please check selected plan details.');

        let practiceCreatedYear = new Date(practiceModelObject.createdAt).getFullYear();
        const settingsObject = await Settings.findOne({where: { key: 'global_settings'}});
        const plainSettingsObject = settingsObject.get({plain: true});
        const settings = JSON.parse(plainSettingsObject.value);
        
        let practice_created_before = new Date(settings.old_pricing_till).getFullYear(); 
        let practice_created_after = new Date(settings.new_pricings_from).getFullYear();


        const subscriptionObj = await Subscriptions.findOne({ where: { id: params.id }, order: [['createdAt', 'DESC']] });
        if (!subscriptionObj) throw new HTTPError(404, `Subscriptions with id: ${params.id} was not found`);


        const paymentMethods = await stripe.paymentMethods.list({ customer: practiceModelObject.stripe_customer_id, type: 'card' });

        if (paymentMethods.data.length == 0) {
            throw new HTTPError(400, 'This customer has no attached payment source or default payment method.');
        }
        event.body.paymentMethod_id = paymentMethods.data[0];

        const getAllActiveSubscriptions = await Subscriptions.findAll({ where: { practice_id: event.user.practice_id, plan_category: input.plan_category, plan_id: { [Op.not]: null } }, raw: true });
        if (getAllActiveSubscriptions.length != 0) {
            for (let x = 0; x < getAllActiveSubscriptions.length; x++) {
                getAllActiveSubscriptions[x].email = event.user.email;
                getAllActiveSubscriptions[x].delete_this_subscription = true;
                let response = await validateSubscriptionPlans(getAllActiveSubscriptions[x]);
                if (!response?.status) throw new HTTPError(400, response.message);
            }
        }
        const afterVerifyActiveSubscriptions = await Subscriptions.findAll({
            where: {
                practice_id: event.user.practice_id, plan_category: input.plan_category, plan_id: { [Op.not]: null },
                id: { [Op.not]: params.id },
            }, raw: true,
        });
        if (afterVerifyActiveSubscriptions.length > 1) throw new HTTPError(400, `Cannot update subscription plan. please check with admin.`);

        let subscriptionCreateObj = {
            customer: practiceModelObject.stripe_customer_id,
            items: [
                { price: input.plan_id },
            ],
            metadata: {
                practice_id: event.user.practice_id,
                user_email: event.user.email,
                plan_id: input.plan_id,
                stripe_product_id: plansObj.stripe_product_id,
            },
        }

        if (input.discount_code) {
            discountObj = await DiscountCode.findOne({ where: { discount_code: input.discount_code, is_deleted: { [Op.not]: true } }, raw: true });
            if (!discountObj?.id) throw new HTTPError(400, `Invalid discount code ${input.discount_code}.`);
            const is_eligible_for = discountObj.plan_type.split(',');
            if (is_eligible_for.includes(plansObj.plan_type)) is_eligible_to_couponcode = true;
        }

        /* One Time Fees charges Here */
        if (practiceModelObject.one_time_activation_fee) {
            let response = await oneTimeActivationCharge(event);
            if (!response?.status) throw new HTTPError(400, response.message);
        }
        /* One time activation end Here */

        /* Check discount */
        if (input.discount_code && new Date(practiceModelObject.createdAt).getTime() >= new Date(settings.new_pricings_from).getTime() && is_eligible_to_couponcode) {
            let stripe_couponcode_obj = JSON.parse(discountObj.stripe_obj);
            subscriptionCreateObj.coupon = stripe_couponcode_obj.id;
        }

        /* Check Yearly free trial  */
/*         if(practiceModelObject.is_yearly_free_trial_available && free_trial_eligible_plans.includes(plansObj.plan_type)){
            subscriptionCreateObj.trial_period_days = 60;
        } */

        if (subscriptionObj?.stripe_subscription_id) await stripe.subscriptions.del(subscriptionObj.stripe_subscription_id);
        const subscription = await stripe.subscriptions.create(subscriptionCreateObj);

        if (subscription?.status === 'incomplete') {
            await stripe.subscriptions.del(subscription.id);
            /* Null the previous Subscriptions */
            const updateColumn = { stripe_product_id: null, stripe_subscription_id: null, subscribed_on: null, plan_id: null, subscribed_valid_till: null, };
            await Subscriptions.update(updateColumn, { where: { practice_id: event.user.practice_id, id: params.id, } });
            throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
        }
        await Subscriptions.destroy({ where: { id: params.id } });
        const subscriptionValidity = new Date(parseInt(subscription.current_period_end) * 1000);
        const stripeUpdateColumn = {
            id: uuid.v4(), practice_id: event.user.practice_id, subscribed_by: event.user.id,
            subscribed_on: new Date(), plan_id: input.plan_id, stripe_subscription_id: subscription.id,
            subscribed_valid_till: subscriptionValidity, stripe_subscription_data: JSON.stringify(subscription),
            subscription_meta_data: '', stripe_product_id: plansObj.stripe_product_id, plan_category: plansObj.plan_category
        };
        const subscriptionupdateObj = await Subscriptions.create(stripeUpdateColumn);

        if (practiceModelObject.is_yearly_free_trial_available && free_trial_eligible_plans.includes(plansObj.plan_type)) {
            practiceModelObject.is_yearly_free_trial_available = false;
            await practiceModelObject.save();
        }

        /* Remove Exsiting subscription plan */
        if(subscription?.discount?.coupon?.id && subscription.status == 'active' && process.env.NODE_ENV === 'test' ){
            console.log('inside discount history');
            const discount_code = subscription.discount.coupon.name;
            const discountCodeObj = await DiscountCode.findOne({where:{discount_code:discount_code}});
            const discount_percentage = discountCodeObj.discount_percentage;
            const percentage = 1 - (discount_percentage/100);
            let discounted_price = (percentage * plansObj.price).toFixed(2);
            

            let plan_type = plansObj.plan_type.split('_').slice(0,plansObj.plan_type.length-1);
            let discount_for = plan_type.slice(0,plan_type.length-1).join('_').toLowerCase();
            /* const invoiceObj = await stripe.invoices.retrieve(subscription?.latest_invoice);
            let discounted_price = invoiceObj?.total_discount_amounts[0]?.amount;
             discounted_price = discounted_price / 100;
            let discount_amount = invoiceObj?.total / 100; */
            discount_amount = plansObj.price - parseFloat(discounted_price);

            let discount_history = {
                id: uuid.v4(), practice_id: subscriptionupdateObj.practice_id, plan_category:plansObj.plan_category,
                discount_for, promocode: discountCodeObj.name, discount_percentage, base_price: plansObj.price, discount_amount, discounted_price, plan_type: plansObj.plan_type
            }
            console.log(discount_history);
            await DiscountHistory.create(discount_history);
        }

        /* Propounding Subscription Start's here. */
        const checkPropoundingSubscription = await Subscriptions.findOne({
            where:{practice_id : subscriptionupdateObj.practice_id, plan_category: 'propounding' },
        });

        if(subscriptionupdateObj?.id && checkPropoundingSubscription ?.id){
        
            const propoundingStripeObj = JSON.parse(checkPropoundingSubscription.stripe_subscription_data);

            let propoundingPlan;
            const respondingPlan = await Plans.findOne({where:{plan_id : subscriptionupdateObj.plan_id},raw:true});
            if(respondingPlan?.plan_type && ['monthly','responding_monthly_349','responding_monthly_495'].includes(respondingPlan.plan_type)) propoundingPlan = 'propounding_monthly_199';
            if(respondingPlan?.plan_type && ['yearly','responding_yearly_5100','responding_yearly_3490'].includes(respondingPlan.plan_type)) propoundingPlan = 'propounding_yearly_2199';
            if(checkPropoundingSubscription?.id && !practiceModelObject.is_propounding_canceled && !propoundingStripeObj?.cancel_at_period_end ){
                /*
                    * cancel existing propounding plan. subscripe new propounding plan based on responding plan type
                        e.g : like the user subscripe monthly responding , monthly propounding plan .
                    * user cancel the responding subscription aw well as cancel the propounding subscription.
                    * check propounding product id  start's with 'sub_' its subscriped monthly or yearly plan.
                    * if product id statr's with 'pi_'  its subscriped only for days.
                */  
               console.log('propoundingPlan : '+propoundingPlan);
                if(checkPropoundingSubscription.stripe_subscription_id.startsWith('sub_')){
                    await stripe.subscriptions.del(checkPropoundingSubscription.stripe_subscription_id);
                }
                const getPropoundingPlan = await Plans.findOne({where:{plan_type: propoundingPlan},raw: true});
                subscriptionCreateObj.items[0] = { price: getPropoundingPlan.plan_id };
                subscriptionCreateObj.metadata.plan_id =  getPropoundingPlan.plan_id;
                subscriptionCreateObj.metadata.stripe_product_id =  getPropoundingPlan.stripe_product_id;
                if (input.discount_code) {
                    const propound_discountObj = await DiscountCode.findOne({ where: { discount_code: input.discount_code, is_deleted: { [Op.not]: true } }, raw: true });
                    if (!propound_discountObj?.id) throw new HTTPError(400, `Invalid discount code ${input.discount_code}.`);
                    const is_eligible_for = propound_discountObj.plan_type.split(',');
                    if (!is_eligible_for.includes(getPropoundingPlan.plan_type)){
                        delete subscriptionCreateObj.coupon;
                    }
                }

                const propoundingSubscription = await stripe.subscriptions.create(subscriptionCreateObj);
        
                if (propoundingSubscription?.status === 'incomplete') {
                    const updateColumn = { stripe_product_id: null, stripe_subscription_id: null, subscribed_on: null, plan_id: null, subscribed_valid_till: null, };
                    await Subscriptions.update(updateColumn, { where: { practice_id: checkPropoundingSubscription.practice_id, id: checkPropoundingSubscription.id, plan_category: 'propounding' } });
                    await stripe.subscriptions.del(propoundingSubscription.id);
                    throw new HTTPError(400, 'Could not subscripe propounding plan.Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
                }
                
                const subscriptionValidity = new Date(parseInt(propoundingSubscription.current_period_end) * 1000);

                const stripeUpdateColumn = {
                    id: uuid.v4(), practice_id: event.user.practice_id, subscribed_by: event.user.id, subscribed_on: new Date(),
                    plan_id: getPropoundingPlan.plan_id, stripe_subscription_id: propoundingSubscription.id, subscribed_valid_till: subscriptionValidity,
                    stripe_subscription_data: JSON.stringify(propoundingSubscription), subscription_meta_data: '',
                    stripe_product_id: getPropoundingPlan.stripe_product_id, plan_category: getPropoundingPlan.plan_category
                };
                await checkPropoundingSubscription.destroy();
                const subscriptionPropoundingupdateObj = await Subscriptions.create(stripeUpdateColumn);
                if(checkPropoundingSubscription?.id && practiceModelObject.is_propounding_canceled){
                    await Practices.update({is_propounding_canceled:null}, { where: { id: checkPropoundingSubscription.practice_id} });
                }
                /* Below the if condition will work only in local level. */
                if(propoundingSubscription?.discount?.coupon?.id && propoundingSubscription.status == 'active' && process.env.NODE_ENV === 'test' ){

                    const discount_code = propoundingSubscription.discount.coupon.name;
                    const discountCodeObj = await DiscountCode.findOne({where:{discount_code:discount_code}});
                    const discount_percentage = discountCodeObj.discount_percentage;
                    const percentage = 1 - (discount_percentage/100);
                    const discounted_price = (percentage * getPropoundingPlan.price).toFixed(2);
                    const discount_amount = getPropoundingPlan.price - parseFloat(discounted_price);
                    
                    let plan_type = getPropoundingPlan.plan_type.split('_').slice(0,getPropoundingPlan.plan_type.length-1);
                    let discount_for = plan_type.slice(0,plan_type.length-1).join('_').toLowerCase();
                    
                    /* const invoiceObj = await stripe.invoices.retrieve(subscription?.latest_invoice);
                    let discounted_price = invoiceObj?.total_discount_amounts[0]?.amount;
                     discounted_price = discounted_price / 100;
                    let discount_amount = invoiceObj?.total / 100; */

                    let discount_history = {
                        id: uuid.v4(), practice_id: subscriptionPropoundingupdateObj.practice_id, plan_category:getPropoundingPlan.plan_category,
                        discount_for, promocode: discountCodeObj.name, discount_percentage, base_price: getPropoundingPlan.price, discounted_price, discount_amount, plan_type: getPropoundingPlan.plan_type
                    }
                    await DiscountHistory.create(discount_history);
                }

            }
        }
        /* if (checkPropoundingSubscription?.id && practiceModelObject.is_propounding_canceled && propoundingStripeObj?.cancel_at_period_end && checkPropoundingSubscription?.stripe_product_id && checkPropoundingSubscription?.stripe_subscription_id) {
            await Subscriptions.destroy({ where: { id: checkPropoundingSubscription.id, plan_category: 'propounding' } });
        } */

        let bodyTxt = '';
        let subject = '';
        let firstPoint = '';
        const usersObj = await Users.findOne({ where: { id: event.user.id,is_deleted:{[Op.not]:true} } });
        if (monthlySubscriptionPlans.includes(plansObj.plan_type)) {
            planDuration = "month";
        }
        if (yearlySubscriptionPlans.includes(plansObj.plan_type)) {
            planDuration = "year";
        }
        subject = 'Your EsquireTek Subscription Benefits';
        firstPoint = `<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thank you for signing up for the ${plansObj.name}. Please note that your subscription comes with the following benefits.</p>`;
        FROM_NAME = 'EsquireTek';
        bodyTxt = `${firstPoint}<div style="width:auto;">` +
            '<ol style="margin:0px;padding:0px;padding-left:33px">' +
            '<li>' +
            '10% off all medical summary orders – Many firms have used our overseas medical summary team to get summaries of extensive medical records and save their firm time and money. ' +
            'You will now get those same high quality services at a 10% discount!' +
            '</li>' +
            '<li>' +
            'A dedicated account manager – shortly your account executive will reach out to you to introduce themselves and let you know that you have a direct point of contact should you have any questions or issues. ' +
            'We want to make sure you are getting the most out of your subscription and your AE will help you do that.' +
            '</li>' +
            '<li>' +
            'Unlimited Discovery and Unlimited TekSign – You may or may not have been using all of the features available to you but they are now unlimited and we highly recommend you try out our shell service, the completed discovery service, and TekSign. ' +
            'TekSign allows clients to sign verification via their cellphone!' +
            '</li>' +
            '</ol>' +
            '</div>' +

            '<p style="marigin-bottom:5px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thanks so much for trusting us and we hope we can help build and scale your practice for the future! </p>' +
            '<p style="marigin-top:0px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your credit card will be billed every ' + planDuration + ' per our terms of service. If you would like to cancel at anytime please email us at support@esquiretek.com.</p>';
            let template;
                if(usersObj?.name){ template = 'Dear' + ' ' + usersObj.name}else{ template = 'Hi' };
        await sendEmail(event.user.email, subject, `${template}, <br/> ${bodyTxt} <br/>`, 'EsquireTek');
        
/* subscriptionupdateObj */
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Subscriptions updated successfully.',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Subscriptions.' }),
        };
    }
};

const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateGetOne(params);
        const { Subscriptions } = await connectToDatabase();
        const subscriptionModelObject = await Subscriptions.findOne({ where: { id: params.id }, order: [['createdAt', 'DESC']] });
        if (!subscriptionModelObject) throw new HTTPError(404, `Subscriptions with id: ${params.id} was not found`);
        const plainCustomerObjection = subscriptionModelObject.get({ plain: true });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainCustomerObjection),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Subscriptions.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        let header = event.headers;
        const query = event.queryStringParameters || {};
        if (header.offset) query.offset = parseInt(header.offset, 10);
        if (header.limit) query.limit = parseInt(header.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }

        query.where.practice_id = event.user.practice_id;
        query.order = [
            ['plan_category', 'DESC'],
        ];
        validateRead(query);
        query.raw = true;
        const { Subscriptions, Plans } = await connectToDatabase();
        let subscriptionModelObjects = await Subscriptions.findAll(query);
        if (!subscriptionModelObjects.length) {
            subscriptionModelObjects = [];
        }
        if (!subscriptionModelObjects.length) {
            const activePlan = await Plans.findOne({ where: { plan_type: 'pay_as_you_go' }, raw: true });
            subscriptionModelObjects = [{ id: 'pay_as_you_go', plan_id: 'pay_as_you_go', planDetails: activePlan, }];
        } else {
            for (let i = 0; i < subscriptionModelObjects.length; i += 1) {
                if (subscriptionModelObjects[i].plan_id && subscriptionModelObjects[i].plan_id !== 'free_trial' && subscriptionModelObjects[i].stripe_subscription_id.startsWith('sub_')) {
                    let checkRespondingPlanExist = subscriptionModelObjects[i];
                    checkRespondingPlanExist.email = event.user.email;
                    checkRespondingPlanExist.delete_this_subscription = false;
                    let response = await validateSubscriptionPlans(checkRespondingPlanExist);
                    if (!response?.status) throw new HTTPError(400, response.message);
                    if (!response?.id) {
                        const activePlan = await Plans.findOne({
                            where: { stripe_product_id: checkRespondingPlanExist.stripe_product_id, },
                            raw: true
                        });
                        subscriptionModelObjects[i].planDetails = activePlan;
                        subscriptionModelObjects[i].stripe_subscription_data = JSON.parse(subscriptionModelObjects[i].stripe_subscription_data);
                    } else {
                        subscriptionModelObjects.splice(i, 1);
                    }
                } else {
                    subscriptionModelObjects[i].stripe_subscription_data = JSON.parse(subscriptionModelObjects[i].stripe_subscription_data);
                    const freeTrialPlan = await Plans.findOne({ where: { plan_id: subscriptionModelObjects[i].plan_id, }, raw: true });
                    subscriptionModelObjects[i].planDetails = freeTrialPlan;
                }
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                data: subscriptionModelObjects,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the subscriptionModelObjects.' }),
        };
    }
};

const getAllHistory = async (event) => {
    try {
        const query = event.headers;
        const practice_id = event.user.practice_id;
        let searchKey = "";
        const sortKey = {};
        let sortQuery = "";
        let searchQuery = "";
        const { sequelize, Plans, Users, Op, SubscriptionHistory } =
            await connectToDatabase();
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        query.raw = true;
        if (!query.where) {
            query.where = {};
        }
        let codeSnip = "";
        let codeSnip2 = "";

        if (practice_id) {
            const practice_data = await Users.findOne({
                where: {
                    practice_id: practice_id,
                    is_deleted: { [Op.not]: true },
                    role: { [Op.not]: null },
                },
            });
            if (!practice_data) {
                throw new HTTPError(400, `invalid Practice ${event.user.practice_id} `);
            } else {
                query.where.practice_id = event.user.practice_id;
            }
        }

        let where = `WHERE Subscriptions.practice_id = '${practice_id}' `;
        /* let where = `WHERE SubscriptionHistories.practice_id = '${practice_id}' `; */

        /**Sort**/
        if (query.sort == "false") {
            sortQuery += ` ORDER BY SubscriptionHistories.createdAt DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != "all" && sortKey.column != "" && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != "all" && sortKey.type != "" && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
            searchQuery = ` Subscriptions.subscribed_on LIKE '%${searchKey}%' OR SubscriptionHistories.subscribed_by LIKE '%${searchKey}%' OR SubscriptionHistories.price LIKE '%${searchKey}%' OR SubscriptionHistories.subscription_valid_till LIKE '%${searchKey}%' OR SubscriptionHistories.payment_type LIKE '%${searchKey}%' OR SubscriptionHistories.status LIKE '%${searchKey}%' OR SubscriptionHistories.plan_type LIKE '%${searchKey}%' `;
        }

        /* if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
            searchQuery = ` SubscriptionHistories.subscribed_on LIKE '%${searchKey}%' OR SubscriptionHistories.subscribed_by LIKE '%${searchKey}%' OR SubscriptionHistories.price LIKE '%${searchKey}%' OR SubscriptionHistories.subscription_valid_till LIKE '%${searchKey}%' OR SubscriptionHistories.payment_type LIKE '%${searchKey}%' OR SubscriptionHistories.status LIKE '%${searchKey}%' OR SubscriptionHistories.plan_type LIKE '%${searchKey}%' `;
        } */


        if (searchQuery != "" && sortQuery != "") {
            codeSnip = where +" AND (" +searchQuery +")" +sortQuery;
            codeSnip2 = where + " AND (" + searchQuery + ")" + sortQuery;
        } else if (searchQuery == "" && sortQuery != "") {
            codeSnip = where + sortQuery ;
            codeSnip2 = where + sortQuery;
        } else if (searchQuery != "" && sortQuery == "") {
            codeSnip = where +" AND (" +searchQuery + ")";
            codeSnip2 = where + " AND (" + searchQuery + ")";
        } else if (searchQuery == "" && sortQuery == "") {
            codeSnip = where ;
            codeSnip2 = where;
        }

        if(!event?.query?.download){
            codeSnip+=` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let sqlQueryWithPagination = "SELECT Subscriptions.id,Subscriptions.subscribed_on as order_date,SubscriptionHistories.practice_id,SubscriptionHistories.user_id,SubscriptionHistories.subscribed_by,SubscriptionHistories.price,SubscriptionHistories.subscribed_on,SubscriptionHistories.plan_id,SubscriptionHistories.plan_type,SubscriptionHistories.stripe_subscription_id,SubscriptionHistories.subscription_valid_start,SubscriptionHistories.subscription_valid_till,SubscriptionHistories.cancel_at,SubscriptionHistories.cancel_at_period_end,SubscriptionHistories.canceled_at,SubscriptionHistories.esquiretek_activity_type,SubscriptionHistories.stripe_subscription_data,SubscriptionHistories.stripe_product_id,SubscriptionHistories.status,SubscriptionHistories.payment_type,SubscriptionHistories.switched_plan,SubscriptionHistories.stripe_latest_activity,SubscriptionHistories.is_reminder_send,SubscriptionHistories.createdAt,SubscriptionHistories.updatedAt " + "FROM SubscriptionHistories " + "INNER JOIN Practices ON SubscriptionHistories.practice_id = Practices.id INNER JOIN Subscriptions ON SubscriptionHistories.practice_id = Subscriptions.practice_id and SubscriptionHistories.plan_category = Subscriptions.plan_category " + codeSnip;

        let sqlQueryForAllData = "SELECT SubscriptionHistories.id,SubscriptionHistories.subscribed_on as order_date,SubscriptionHistories.practice_id,SubscriptionHistories.user_id,SubscriptionHistories.subscribed_by,SubscriptionHistories.price,SubscriptionHistories.subscribed_on,SubscriptionHistories.plan_id,SubscriptionHistories.plan_type,SubscriptionHistories.stripe_subscription_id,SubscriptionHistories.subscription_valid_start,SubscriptionHistories.subscription_valid_till,SubscriptionHistories.cancel_at,SubscriptionHistories.cancel_at_period_end,SubscriptionHistories.canceled_at,SubscriptionHistories.esquiretek_activity_type,SubscriptionHistories.stripe_subscription_data,SubscriptionHistories.stripe_product_id,SubscriptionHistories.status,SubscriptionHistories.payment_type,SubscriptionHistories.switched_plan,SubscriptionHistories.stripe_latest_activity,SubscriptionHistories.is_reminder_send,SubscriptionHistories.createdAt,SubscriptionHistories.updatedAt " + "FROM SubscriptionHistories " + "INNER JOIN Practices ON SubscriptionHistories.practice_id = Practices.id INNER JOIN Subscriptions ON SubscriptionHistories.practice_id = Subscriptions.practice_id and SubscriptionHistories.plan_category = Subscriptions.plan_category " + codeSnip2;

/*         let sqlQueryWithPagination = "SELECT SubscriptionHistories.id,SubscriptionHistories.subscribed_on as order_date,SubscriptionHistories.practice_id,SubscriptionHistories.user_id,SubscriptionHistories.subscribed_by,SubscriptionHistories.price,SubscriptionHistories.subscribed_on,SubscriptionHistories.plan_id,SubscriptionHistories.plan_type,SubscriptionHistories.stripe_subscription_id,SubscriptionHistories.subscription_valid_start,SubscriptionHistories.subscription_valid_till,SubscriptionHistories.cancel_at,SubscriptionHistories.cancel_at_period_end,SubscriptionHistories.canceled_at,SubscriptionHistories.esquiretek_activity_type,SubscriptionHistories.stripe_subscription_data,SubscriptionHistories.stripe_product_id,SubscriptionHistories.status,SubscriptionHistories.payment_type,SubscriptionHistories.switched_plan,SubscriptionHistories.stripe_latest_activity,SubscriptionHistories.is_reminder_send,SubscriptionHistories.createdAt,SubscriptionHistories.updatedAt " + "FROM SubscriptionHistories " + "INNER JOIN Practices ON SubscriptionHistories.practice_id = Practices.id " + codeSnip2;

        let sqlQueryForAllData = "SELECT SubscriptionHistories.id,SubscriptionHistories.subscribed_on as order_date,SubscriptionHistories.practice_id,SubscriptionHistories.user_id,SubscriptionHistories.subscribed_by,SubscriptionHistories.price,SubscriptionHistories.subscribed_on,SubscriptionHistories.plan_id,SubscriptionHistories.plan_type,SubscriptionHistories.stripe_subscription_id,SubscriptionHistories.subscription_valid_start,SubscriptionHistories.subscription_valid_till,SubscriptionHistories.cancel_at,SubscriptionHistories.cancel_at_period_end,SubscriptionHistories.canceled_at,SubscriptionHistories.esquiretek_activity_type,SubscriptionHistories.stripe_subscription_data,SubscriptionHistories.stripe_product_id,SubscriptionHistories.status,SubscriptionHistories.payment_type,SubscriptionHistories.switched_plan,SubscriptionHistories.stripe_latest_activity,SubscriptionHistories.is_reminder_send,SubscriptionHistories.createdAt,SubscriptionHistories.updatedAt " + "FROM SubscriptionHistories " + "INNER JOIN Practices ON SubscriptionHistories.practice_id = Practices.id " + codeSnip2; */

        console.log(sqlQueryForAllData);
        const dataWithPagination = await sequelize.query(sqlQueryWithPagination, {
            type: QueryTypes.SELECT,
        });

        for (let i = 0; i < dataWithPagination.length; i++) {
            if (dataWithPagination[i].stripe_subscription_data) {
                const activePlan = await Plans.findOne({where:{
                    stripe_product_id: dataWithPagination[i].stripe_product_id,}
                });
                dataWithPagination[i].planDetails = activePlan;
                dataWithPagination[i].stripe_subscription_data = JSON.parse(dataWithPagination[i].stripe_subscription_data);
            }
            if (dataWithPagination[i].plan_type) {

                let monthly_plan = ['monthly', 'responding_monthly_349', 'responding_monthly_495'];
                let yearly_plan = ['yearly', 'responding_yearly_3490', 'responding_yearly_5100'];

                if (dataWithPagination[i].plan_type == 'free_trial') {
                    dataWithPagination[i].plan_type = 'FREE TRIAL';
                } else if (dataWithPagination[i].plan_type == 'pay_as_you_go') {
                    dataWithPagination[i].plan_type = 'TEK AS-YOU-GO';
                } else if (dataWithPagination[i].plan_type == 'propounding_monthly_199') {
                    dataWithPagination[i].plan_type = 'PROPOUNDING MONTHLY';
                } else if (dataWithPagination[i].plan_type == 'propounding_yearly_2199') {
                    dataWithPagination[i].plan_type = 'PROPOUNDING YEARLY';
                } else if (monthly_plan.includes(dataWithPagination[i].plan_type)) {
                    dataWithPagination[i].plan_type = 'MONTHLY';
                } else if (yearly_plan.includes(dataWithPagination[i].plan_type)) {
                    dataWithPagination[i].plan_type = 'YEARLY';
                }
            };

            if(!dataWithPagination[i]?.subscribed_by){
                const users_obj = await Users.findOne({where:{id:dataWithPagination[i].user_id},raw:true});
                if(users_obj?.name){
                    await SubscriptionHistory.update({subscribed_by:users_obj.name},{where:{practice_id:users_obj.practice_id,id:dataWithPagination[i].id}});
                    dataWithPagination[i].subscribed_by = users_obj.name;
                }
            }
        }

        const getAllData = await sequelize.query(sqlQueryForAllData, {
            type: QueryTypes.SELECT,
        });

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": getAllData.length,
            },
            body: JSON.stringify(dataWithPagination),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the subscription.' }),
        };
    }
};

const adminGetAllHistory = async (event) => {
    try {
        const query = event.headers;
        let searchKey = "";
        const sortKey = {};
        let sortQuery = "";
        let searchQuery = "";
        const { sequelize, Plans, Users, SubscriptionHistory, Op } =
            await connectToDatabase();
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        query.raw = true;
        if (!query.where) {
            query.where = {};
        }
        let codeSnip = "";
        let codeSnip2 = "";

        let where = `WHERE `;

        /**Sort**/
        if (query.sort == "false") {
            sortQuery += ` ORDER BY SubscriptionHistories.createdAt DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != "all" && sortKey.column != "" && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != "all" && sortKey.type != "" && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }
        if (event?.query?.from_date && event?.query?.to_date) {
            where += ' SubscriptionHistories.createdAt >= "'+event?.query?.from_date+'" AND SubscriptionHistories.createdAt <= "'+event?.query?.to_date+'"';
        }
        /**Search**/
        if (searchKey != "false" && searchKey.length >= 1 && searchKey != "") {
            searchQuery = ` SubscriptionHistories.order_date LIKE '%${searchKey}%' OR SubscriptionHistories.subscribed_by LIKE '%${searchKey}%' OR SubscriptionHistories.price LIKE '%${searchKey}%' OR SubscriptionHistories.subscription_valid_till LIKE '%${searchKey}%' OR SubscriptionHistories.payment_type LIKE '%${searchKey}%' OR SubscriptionHistories.status LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%' `;
        }


        if (searchQuery != "" && sortQuery != "") {
            codeSnip = where + " (" + searchQuery + ")" + sortQuery ;
            codeSnip2 = where + " (" + searchQuery + ")" + sortQuery;
        } else if (searchQuery == "" && sortQuery != "") {
            codeSnip = event?.query?.from_date && event?.query?.to_date ? where + sortQuery : sortQuery;
            codeSnip2 = event?.query?.from_date && event?.query?.to_date ? where + sortQuery : sortQuery;
        } else if (searchQuery != "" && sortQuery == "") {
            codeSnip = where + " (" + searchQuery + ")" ;
            codeSnip2 = where + " (" + searchQuery + ")";
        }

        if(!event?.query?.download){
            codeSnip+=` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let sqlQueryWithPagination = "SELECT SubscriptionHistories.id,Practices.name AS practice_name,SubscriptionHistories.order_date,SubscriptionHistories.practice_id,SubscriptionHistories.user_id,SubscriptionHistories.subscribed_by,SubscriptionHistories.price,SubscriptionHistories.subscribed_on,SubscriptionHistories.plan_id,SubscriptionHistories.plan_type,SubscriptionHistories.stripe_subscription_id,SubscriptionHistories.subscription_valid_start,SubscriptionHistories.subscription_valid_till,SubscriptionHistories.cancel_at,SubscriptionHistories.cancel_at_period_end,SubscriptionHistories.canceled_at,SubscriptionHistories.esquiretek_activity_type,SubscriptionHistories.stripe_subscription_data,SubscriptionHistories.stripe_product_id,SubscriptionHistories.status,SubscriptionHistories.payment_type,SubscriptionHistories.switched_plan,SubscriptionHistories.stripe_latest_activity,SubscriptionHistories.is_reminder_send,SubscriptionHistories.createdAt,SubscriptionHistories.updatedAt " + "FROM SubscriptionHistories " + "INNER JOIN Practices ON SubscriptionHistories.practice_id = Practices.id INNER JOIN Subscriptions ON SubscriptionHistories.practice_id = Subscriptions.practice_id and SubscriptionHistories.plan_category = Subscriptions.plan_category " + codeSnip;

        let sqlQueryForAllData = "SELECT SubscriptionHistories.id,Practices.name AS practice_name,SubscriptionHistories.order_date,SubscriptionHistories.practice_id,SubscriptionHistories.user_id,SubscriptionHistories.subscribed_by,SubscriptionHistories.price,SubscriptionHistories.subscribed_on,SubscriptionHistories.plan_id,SubscriptionHistories.plan_type,SubscriptionHistories.stripe_subscription_id,SubscriptionHistories.subscription_valid_start,SubscriptionHistories.subscription_valid_till,SubscriptionHistories.cancel_at,SubscriptionHistories.cancel_at_period_end,SubscriptionHistories.canceled_at,SubscriptionHistories.esquiretek_activity_type,SubscriptionHistories.stripe_subscription_data,SubscriptionHistories.stripe_product_id,SubscriptionHistories.status,SubscriptionHistories.payment_type,SubscriptionHistories.switched_plan,SubscriptionHistories.stripe_latest_activity,SubscriptionHistories.is_reminder_send,SubscriptionHistories.createdAt,SubscriptionHistories.updatedAt " + "FROM SubscriptionHistories " + "INNER JOIN Practices ON SubscriptionHistories.practice_id = Practices.id INNER JOIN Subscriptions ON SubscriptionHistories.practice_id = Subscriptions.practice_id and SubscriptionHistories.plan_category = Subscriptions.plan_category " + codeSnip2;

/*         let sqlQueryWithPagination = "SELECT SubscriptionHistories.id,Practices.name AS practice_name,SubscriptionHistories.order_date,SubscriptionHistories.practice_id,SubscriptionHistories.user_id,SubscriptionHistories.subscribed_by,SubscriptionHistories.price,SubscriptionHistories.subscribed_on,SubscriptionHistories.plan_id,SubscriptionHistories.plan_type,SubscriptionHistories.stripe_subscription_id,SubscriptionHistories.subscription_valid_start,SubscriptionHistories.subscription_valid_till,SubscriptionHistories.cancel_at,SubscriptionHistories.cancel_at_period_end,SubscriptionHistories.canceled_at,SubscriptionHistories.esquiretek_activity_type,SubscriptionHistories.stripe_subscription_data,SubscriptionHistories.stripe_product_id,SubscriptionHistories.status,SubscriptionHistories.payment_type,SubscriptionHistories.switched_plan,SubscriptionHistories.stripe_latest_activity,SubscriptionHistories.is_reminder_send,SubscriptionHistories.createdAt,SubscriptionHistories.updatedAt " + "FROM SubscriptionHistories " + "INNER JOIN Practices ON SubscriptionHistories.practice_id = Practices.id " + codeSnip;

        let sqlQueryForAllData = "SELECT SubscriptionHistories.id,Practices.name AS practice_name,SubscriptionHistories.order_date,SubscriptionHistories.practice_id,SubscriptionHistories.user_id,SubscriptionHistories.subscribed_by,SubscriptionHistories.price,SubscriptionHistories.subscribed_on,SubscriptionHistories.plan_id,SubscriptionHistories.plan_type,SubscriptionHistories.stripe_subscription_id,SubscriptionHistories.subscription_valid_start,SubscriptionHistories.subscription_valid_till,SubscriptionHistories.cancel_at,SubscriptionHistories.cancel_at_period_end,SubscriptionHistories.canceled_at,SubscriptionHistories.esquiretek_activity_type,SubscriptionHistories.stripe_subscription_data,SubscriptionHistories.stripe_product_id,SubscriptionHistories.status,SubscriptionHistories.payment_type,SubscriptionHistories.switched_plan,SubscriptionHistories.stripe_latest_activity,SubscriptionHistories.is_reminder_send,SubscriptionHistories.createdAt,SubscriptionHistories.updatedAt " + "FROM SubscriptionHistories " + "INNER JOIN Practices ON SubscriptionHistories.practice_id = Practices.id " + codeSnip2; */

        const dataWithPagination = await sequelize.query(sqlQueryWithPagination, {
            type: QueryTypes.SELECT,
        });

        for (let i = 0; i < dataWithPagination.length; i++) {
            if (dataWithPagination[i].stripe_subscription_data) {
                const activePlan = await Plans.findOne({ where:{stripe_product_id: dataWithPagination[i].stripe_product_id,}
                });
                dataWithPagination[i].planDetails = activePlan;
                dataWithPagination[i].stripe_subscription_data = JSON.parse(dataWithPagination[i].stripe_subscription_data);
            }
            if (dataWithPagination[i].plan_type) {

                let monthly_plan = ['monthly', 'responding_monthly_349', 'responding_monthly_495'];
                let yearly_plan = ['yearly', 'responding_yearly_3490', 'responding_yearly_5100'];

                if (dataWithPagination[i].plan_type == 'free_trial') {
                    dataWithPagination[i].plan_type = 'FREE TRIAL';
                } else if (dataWithPagination[i].plan_type == 'pay_as_you_go') {
                    dataWithPagination[i].plan_type = 'TEK AS-YOU-GO';
                } else if (dataWithPagination[i].plan_type == 'propounding_monthly_199') {
                    dataWithPagination[i].plan_type = 'PROPOUNDING MONTHLY';
                } else if (dataWithPagination[i].plan_type == 'propounding_yearly_2199') {
                    dataWithPagination[i].plan_type = 'PROPOUNDING YEARLY';
                } else if (monthly_plan.includes(dataWithPagination[i].plan_type)) {
                    dataWithPagination[i].plan_type = 'MONTHLY';
                } else if (yearly_plan.includes(dataWithPagination[i].plan_type)) {
                    dataWithPagination[i].plan_type = 'YEARLY';
                }
            };
            if(!dataWithPagination[i]?.subscribed_by){
                const users_obj = await Users.findOne({where:{id:dataWithPagination[i].user_id},raw:true});
                if(users_obj?.name){
                    await SubscriptionHistory.update({subscribed_by:users_obj.name},{where:{practice_id:users_obj.practice_id,id:dataWithPagination[i].id}});
                    dataWithPagination[i].subscribed_by = users_obj.name;
                }
            }
        }

        const getAllData = await sequelize.query(sqlQueryForAllData, {
            type: QueryTypes.SELECT,
        });

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": getAllData.length,
            },
            body: JSON.stringify(dataWithPagination),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the subscription.' }),
        };
    }
};

const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        const { Subscriptions } = await connectToDatabase();
        const subscriptionModelObject = await Subscriptions.findOne({ where: { id: params.id }, order: [['createdAt', 'DESC']] });
        if (!subscriptionModelObject) throw new HTTPError(404, `Subscriptions with id: ${params.id} was not found`);
        await stripe.subscriptions.del(
            subscriptionModelObject.stripe_subscription_id
        );
        await subscriptionModelObject.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Successfully removed the subscription',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy fetch the Subscriptions.' }),
        };
    }
};

const validateSubscriptionPlans = async (subscriptionObj) => {
    try {
        console.log('inside validation');
        const { Subscriptions, Op } = await connectToDatabase();
        let plan_id = subscriptionObj?.plan_id;
        let stripeObj = subscriptionObj?.stripe_subscription_data ? JSON.parse(subscriptionObj.stripe_subscription_data) : undefined;
        let sub_status = stripeObj?.status; // status 'trailing' for free trail plans;
        let sub_cancel_at = stripeObj?.cancel_at ? unixTimetoDateconvert(stripeObj.current_period_end) : undefined;
        let sub_current_period_end = stripeObj?.current_period_end ? unixTimetoDateconvert(stripeObj.current_period_end) : undefined;
        let current_date = new Date();
        let delete_this_subscription = subscriptionObj?.delete_this_subscription;
        
        let response = { status: true, message: 'success' };
        if (plan_id == 'free_trial') {
            await Subscriptions.destroy({ where: { id: subscriptionObj.id } });
        }
        else if ((sub_status && !['active','trialing'].includes(sub_status)) || (sub_current_period_end &&
                current_date && sub_cancel_at && sub_current_period_end <= current_date && sub_cancel_at <= current_date)) {
            if (delete_this_subscription) {
                await Subscriptions.destroy({ where: { id: subscriptionObj.id } });
            } else {
                let updateColumn = {
                    stripe_product_id: null, stripe_subscription_id: null, subscribed_on: null,
                    plan_id: null, subscribed_valid_till: null,
                };

                await Subscriptions.update(updateColumn, { where: { practice_id: subscriptionObj.practice_id, id: subscriptionObj.id, }, });
                response.id = subscriptionObj.id;
            }
        }
        return response;
    } catch (err) {
        console.log(err.message);
        return { status: false, message: 'Subscription validation error.' };
    }
};

const unixTimetoDateconvert = (timestamp) => {
    const unixTimestamp = timestamp;
    const date = new Date(unixTimestamp * 1000);
    return date;
};

const oneTimeActivationCharge = async (event) => {
    try {

        const { Practices, SubscriptionHistory, DiscountCode, Plans, DiscountHistory, Op } = await connectToDatabase();
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!input.paymentMethod_id?.id) throw new HTTPError(400, 'Payment method id not found.');

        let discount_history = {
            id: uuid.v4(), practice_id: event.user.practice_id, subscription_id: '',plan_category:'responding',
            discount_for: 'activationFee', promocode: input.discount_code, discount_percentage: '', base_price: 249, discounted_price: '',discount_amount:''
        }

        let discounted_price = 249;
        let is_eligible_for;
        if (input.discount_code) {
            const discountObj = await DiscountCode.findOne({ where: { discount_code: input.discount_code, is_deleted: { [Op.not]: true } }, raw: true });
            if (!discountObj) throw new HTTPError(400, `Invalid discount code ${input.discount_code}.`);
            is_eligible_for = discountObj.plan_type.split(',');
            if (is_eligible_for.includes('activation_fee')) {
                discount_history.discount_percentage = discountObj.discount_percentage;
                const percentage = 1 - (discountObj.discount_percentage / 100);
                discounted_price = (percentage * discounted_price).toFixed(2);
                discounted_price = Math.ceil(discounted_price).toFixed(2);
                discount_history.discounted_price = discounted_price;
                discount_history.discount_amount =  (249 - discounted_price).toFixed(2);
            }
        }

        if (discounted_price != 0) {
            let onetime_obj = {
                amount: parseInt(discounted_price * 100), currency: 'usd', customer: practiceObject.stripe_customer_id,
                payment_method: input.paymentMethod_id.id, off_session: true, confirm: true,
            }
            const paymentIntent = await stripe.paymentIntents.create(onetime_obj);


            if (paymentIntent && paymentIntent.status && paymentIntent.status === 'incomplete') {
                const paymentIntent = await stripe.paymentIntents.cancel(paymentIntent.id);
                await SubscriptionHistory.create({
                    id: uuid.v4(), order_date: new Date(), practice_id: event.user.practice_id,
                    user_id: event.user.id, subscribed_by: event.user.name, price: discounted_price, subscribed_on: new Date(),
                    plan_type: 'activationFee', payment_type: 'New', status: 'Failure', plan_category: 'responding'
                });
                throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
            }
        }

        const subscriptionHistoryData = {
            id: uuid.v4(), order_date: new Date(), practice_id: event.user.practice_id, user_id: event.user.id,
            subscribed_by: event.user.name, price: discounted_price, subscribed_on: new Date(), plan_type: 'activationFee', payment_type: 'New',
            status: 'Success', plan_category: 'responding'
        };
        const subscriptionHistoryObj = await SubscriptionHistory.create(subscriptionHistoryData);
        if (input.discount_code && is_eligible_for.includes('activation_fee')) {
            await DiscountHistory.create(discount_history);
        }
        if (subscriptionHistoryObj) {
            practiceObject.one_time_activation_fee = false;
            practiceObject.discount_amount = 0;
            await practiceObject.save();
        }
        return { status: true, message: 'success' };
    } catch (err) {
        return { status: false, message: err };
    }
};

const createPropoundingSubscription = async (event) => {
    try {
        const { Practices, Plans, Subscriptions, Settings, SubscriptionHistory, DiscountCode, Users, DiscountHistory, Op } = await connectToDatabase();
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const monthlyPropounding = await Plans.findOne({where:{plan_type:'propounding_monthly_199'},raw:true});
        const yearlyPropounding = await Plans.findOne({where:{plan_type:'propounding_yearly_2199'},raw:true});

        const monthly_days = monthlyPropounding.validity;
        const yearly_days = yearlyPropounding.validity;
        const monthly_price = monthlyPropounding.price;
        const yearly_price = yearlyPropounding.price;
        
        const plansObj = await Plans.findOne({ where: { plan_id: input.plan_id } });
        const practiceObj = await Practices.findOne({ where: { id: event.user.practice_id } });
        const usersobj = await Users.findOne({
            where: {
                practice_id: practiceObj.id, is_deleted: { [Op.not]: true }, role: 'lawyer', is_admin: true
            }, order: [['createdAt', 'ASC']], raw: true
        });

        if (!plansObj) throw new HTTPError(400, 'please check selected plan details.');
        const respondingObj = await Subscriptions.findOne({ where: { practice_id: event.user.practice_id, plan_category: 'responding' }, order: [['createdAt', 'DESC']], logging: console.log, raw: true });
        if (!respondingObj) throw new HTTPError(400, 'Responding plans not found please check.');
        
        const practiceCreatedYear = new Date(practiceObj.createdAt).getFullYear();
        const settingsObject = await Settings.findOne({where: { key: 'global_settings'}});
        const plainSettingsObject = settingsObject.get({plain: true});
        const settings = JSON.parse(plainSettingsObject.value);
        let practice_created_before = new Date(settings.old_pricing_till).getFullYear(); 
        let practice_created_after = new Date(settings.new_pricings_from).getFullYear();
        
        const current_date = new Date();
        const responding_start_date = new Date(respondingObj.subscribed_on);
        let days_difference = dateDiffInDays(current_date, responding_start_date);
        let subscription_price = 0;
        let propounding_trail_days = 0;
        let trail_end_date = 0;
        let trail_start_date = 0;
        let subscriptionCreateObj;

        const paymentMethods = await stripe.paymentMethods.list({ customer: practiceObj.stripe_customer_id, type: 'card', });
        if (paymentMethods.data.length == 0) {
            throw new HTTPError(400, 'This customer has no attached payment source or default payment method.');
        }
        event.body.paymentMethod_id = paymentMethods.data[0];

        let createObject = {
            customer: practiceObj.stripe_customer_id,
            items: [
                { price: input.plan_id },
            ],
            metadata: {
                practice_id: event.user.practice_id, user_email: event.user.email,
                plan_id: input.plan_id, stripe_product_id: plansObj.stripe_product_id,
            },
        };
        let is_eligible_to_couponcode = false;
        let type, base_price, discounted_price , discount_percentage , discountObj;
        let percentage = 1; //default percentage;
        if (input.discount_code) {
            discountObj = await DiscountCode.findOne({ where: { discount_code: input.discount_code, is_deleted: { [Op.not]: true } }, raw: true });
            if (!discountObj?.id) throw new HTTPError(400, `Invalid discount code ${input.discount_code}.`);
            const is_eligible_for = discountObj.plan_type.split(',');
            if (is_eligible_for.includes(plansObj.plan_type)) is_eligible_to_couponcode = true;
        }
        if (input.discount_code && is_eligible_to_couponcode) {
            let stripe_couponcode_obj = JSON.parse(discountObj.stripe_obj);
            createObject.coupon = stripe_couponcode_obj.id;
            discount_percentage = discountObj.discount_percentage;
            percentage = 1 - (discountObj.discount_percentage/100);
        }
        let respondingStripeObj = JSON.parse(respondingObj.stripe_subscription_data);
        if(respondingStripeObj?.trial_end){
            trail_end_date = new Date(respondingStripeObj.trial_end * 1000);
            console.log(current_date);
            console.log(trail_end_date);
            propounding_trail_days = dateDiffInDays(trail_end_date, current_date);
        }
        if ((respondingStripeObj.status == 'trialing' && plansObj.plan_type == 'propounding_yearly_2199' && propounding_trail_days > 0) || 
            (days_difference == 0 && (plansObj.plan_type == 'propounding_monthly_199' || plansObj.plan_type == 'propounding_yearly_2199'))
            ) {
            discounted_price = (percentage * plansObj.price).toFixed(2).toString();
            base_price = plansObj.price;
            
/*             if(plansObj.plan_type == 'propounding_yearly_2199' && respondingStripeObj.status == 'trialing'){
                createObject.trial_period_days = propounding_trail_days;
            } */
            subscriptionCreateObj = await stripe.subscriptions.create(createObject);
            console.log(subscriptionCreateObj);
            if (subscriptionCreateObj && subscriptionCreateObj.status && subscriptionCreateObj.status === 'incomplete') {
                await stripe.subscriptions.del(subscriptionCreateObj.id);
                throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
            }
            if (respondingStripeObj?.cancel_at_period_end){
                subscriptionCreateObj  = await stripe.subscriptions.update(subscriptionCreateObj.id, {cancel_at_period_end: true});
            }
            
        } else if (plansObj.plan_type == 'propounding_monthly_199' && days_difference > 0) {
            days_difference = monthly_days - days_difference;
            base_price = Math.ceil((monthly_price / monthly_days) * days_difference);
            discounted_price = subscription_price = Math.ceil( base_price * percentage);
        } else if (plansObj.plan_type == 'propounding_yearly_2199' && days_difference > 0) {
            days_difference = yearly_days - days_difference;
            base_price = Math.ceil((yearly_price / yearly_days) * days_difference);
            discounted_price = subscription_price = Math.ceil( base_price * percentage);
        }

        /* If practice is not eligible to coupon code the default percentage is 1;*/
        if ( !subscriptionCreateObj?.id ) {
            let propounding_subscription_obj = {
                amount: parseInt(subscription_price * 100), currency: 'usd', customer: practiceObj.stripe_customer_id,
                payment_method: input.paymentMethod_id.id, off_session: true, confirm: true,
            }
            if (subscription_price != 0) {
                subscriptionCreateObj = await stripe.paymentIntents.create(propounding_subscription_obj);   
            }else if (subscription_price == 0){
                subscriptionCreateObj = Object.assign(propounding_subscription_obj,{id :'pi_one_hundred_per_cent_discount',status:'succeeded'});
            }
            if (subscriptionCreateObj && subscriptionCreateObj.status && subscriptionCreateObj.status === 'incomplete') {
                const subscriptionHistoryData = {
                    id: uuid.v4(), order_date: new Date(), practice_id: event.user.practice_id, user_id: usersobj.id, 
                    subscribed_by: usersobj.name, price: parseInt(subscription_price),
                    plan_type: plansObj.plan_type, subscribed_on: new Date(), plan_id: plansObj.plan_id, 
                    stripe_subscription_id: subscriptionCreateObj.id, subscription_valid_start: new Date(), 
                    subscription_valid_till: respondingObj.subscribed_valid_till, stripe_subscription_data: JSON.stringify(subscriptionCreateObj), 
                    esquiretek_activity_type: 'NEW_SUBSCRIPTION', payment_type: 'New', stripe_product_id: plansObj.stripe_product_id,
                    status: 'Payment failure', switched_plan: plansObj.plan_type, latest_actvity :'Subscription Creation',
                    payment_type : 'failure', esquiretek_activity_type : 'UPGRADE', plan_category: 'propounding'
                };
                await SubscriptionHistory.create(subscriptionHistoryData);
                const paymentIntent = await stripe.paymentIntents.cancel(paymentIntent.id);
                throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
            }
        }
        let subscriptionObj;
        console.log('subscriptionCreateObj?.id : '+subscriptionCreateObj?.id);
        if (subscriptionCreateObj?.id) {
            console.log('inside create');
            await Subscriptions.destroy({ where: { practice_id: event.user.practice_id, plan_id: null, stripe_subscription_id: null, plan_category: plansObj.plan_category } });
            subscriptionObj = await Subscriptions.create({
                id: uuid.v4(), 
                practice_id: event.user.practice_id, 
                subscribed_by: event.user.id,
                subscribed_on: new Date(), 
                plan_id: input.plan_id, 
                stripe_subscription_id: subscriptionCreateObj.id,
                stripe_subscription_data: JSON.stringify(subscriptionCreateObj), 
                subscription_meta_data: '',
                stripe_product_id: plansObj.stripe_product_id, 
                subscribed_valid_till: respondingObj.subscribed_valid_till,
                plan_category: plansObj.plan_category
            });
            if(respondingStripeObj?.cancel_at_period_end) practiceObj.is_propounding_canceled = true;
            else practiceObj.is_propounding_canceled = false;

            await practiceObj.save();

            const plainSubscriptionText = subscriptionObj.get({ plain: true });

            if (subscriptionCreateObj?.id.startsWith('pi_')) {
                const subscriptionHistoryData = {
                    id: uuid.v4(), order_date: new Date(), practice_id: event.user.practice_id, user_id: usersobj.id, 
                    subscribed_by: usersobj.name, price: parseInt(subscription_price),
                    plan_type: plansObj.plan_type, subscribed_on: new Date(), plan_id: plansObj.plan_id, 
                    stripe_subscription_id: subscriptionCreateObj.id, subscription_valid_start: new Date(), 
                    subscription_valid_till: respondingObj.subscribed_valid_till, stripe_subscription_data: JSON.stringify(subscriptionCreateObj), 
                    esquiretek_activity_type: 'NEW_SUBSCRIPTION', payment_type: 'New', stripe_product_id: plansObj.stripe_product_id,
                    status: 'Success', switched_plan: plansObj.plan_type, latest_actvity :'Subscription Creation',
                    payment_type : 'New', esquiretek_activity_type : 'UPGRADE', plan_category: 'propounding'
                };

                let subscriptionHistoryObj = await SubscriptionHistory.create(subscriptionHistoryData);
                subscriptionHistoryObj = subscriptionHistoryObj.get({plain:true});
                if (subscriptionHistoryObj?.id && input.discount_code && is_eligible_to_couponcode) {
                   console.log('inside if');
                    let plan_type = plansObj.plan_type.split('_').slice(0,plansObj.plan_type.length-1);
                    let discount_for = plan_type.slice(0,plan_type.length-1).join('_').toLowerCase();

                    let discount_amount = parseInt(base_price) - parseInt(discounted_price);
                    let discount_history = {
                        id: uuid.v4(), practice_id: event.user.practice_id, plan_category:'propounding',
                        discount_for, promocode: input.discount_code, discount_percentage, base_price, discounted_price, discount_amount, plan_type: plansObj.plan_type
                    };
                    console.log(discount_history);
                    await DiscountHistory.create(discount_history);
                }
            }
            // let emailBody = 'Hi '+practiceObj.name+','+'<br><br>'+'Thank you for signing up for propounding feature.'+'<br><br>'+
            // 'Your credit card will be billed every month per our terms of service. If you would like to cancel at anytime please email us at support@esquiretek.com.';
            let template = 'Hi';
            if(event?.user?.name) template = template+' '+event.user.name;
            let emailBody = template+',<br><br>'+
            practiceObj.name.charAt(0).toUpperCase() + practiceObj.name.slice(1) + ' now has access to our propounding features. Thanks for signing up! We are excited to help speed up your propounding process.' + '<br><br>' +
                'If you have questions, we are here to help. Email us any time at support@esquiretek.com.<br><br>The EsquireTek Team.';

            let subject = 'Propounding Features Unlocked';
            await sendEmail(event.user.email, subject, emailBody, 'EsquireTek');
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                subscriptionObj
                // status: 'ok',
                // message: 'subscriptions created successfully.'
            }),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create propounding subscription.' }),
        };
    }
};

const cancelPropoundingSubscription = async (event) => {
    try {
        const { Practices, Op, Subscriptions, Users } = await connectToDatabase();
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const practiceObj = await Practices.findOne({ where: { id: event.user.practice_id } });
        const usersObj = await Users.findOne({ 
            where: { practice_id: event.user.practice_id, is_deleted: { [Op.not]: true}},
            order:[['createdAt','ASC']],
            logging:console.log 
        });    
        const checkPropoundingSubscription = await Subscriptions.findOne({
            where:{practice_id : event.user.practice_id, plan_category: 'propounding' },
            raw:true
        });
        let cancelObj, endDate;
        if(checkPropoundingSubscription.stripe_subscription_id.startsWith('sub_')){
            console.log('inside if');
            cancelObj = await stripe.subscriptions.update(checkPropoundingSubscription.stripe_subscription_id, {cancel_at_period_end: true});
            endDate = unixTimeStamptoDate(cancelObj.cancel_at);
        }else{
            cancelObj = new Date(checkPropoundingSubscription.subscribed_valid_till).getTime();
            endDate = unixTimeStamptoDate(cancelObj);
        }
        practiceObj.is_propounding_canceled = true;
        await practiceObj.save();
        let template ='Hi' ;
                if(usersObj?.name){ template = template+ ' ' + usersObj.name};
        let subject = `Your Propounding feature will be active till ${endDate}`;
        let message_body = template+',<br><br>'+
        'We have canceled the auto-renewal for propounding feature based on your request. You will be able to use propounding feature till the end of the current subscription period '+endDate+'.<br><br>'+
        'Thank you for trying out the propounding feature. We are always looking to improve EsquireTek if you have any feedback for us on how to improve our platform, or if this cancellation was in error, please email (support@esquiretek.com) with any comments or concerns.';
        await sendEmail(usersObj.email, subject, message_body, 'EsquireTek');
             
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ status: 'ok', message: 'Propounding subscriptions created successfully.'}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create propounding subscription.' }),
        };
    }
};

const getAllActivationFeeAndFeeWaived = async (event) => {
    try {
        const query = event.headers;

        let sortQuery = '';
        let searchQuery = '';
        let codeSnip = '';
        let codeSnip2 = '';

        const { sequelize, Plans } = await connectToDatabase();
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let filter = event?.query?.filter;

        let where = `WHERE SubscriptionHistories.payment_type != 'Subscription Canceled'`;
        /* Dashboard */
        if (filter) {
            let dates = customDateandTime(event.query.filter);
            where += ' AND SubscriptionHistories.createdAt >= "' + dates.from_date + '" AND SubscriptionHistories.createdAt <= "' + dates.to_date + '"';
        }
        if (event?.query?.from_date) {
            from_date = event.query.from_date;
            where += ' AND SubscriptionHistories.createdAt >= "' + from_date + '"';
        }
        if (event?.query?.to_date) {
            to_date = event.query.to_date;
            where += ' AND SubscriptionHistories.createdAt <= "' + to_date + '"';
        }
        console.log(event.query.filter_type);
        if (event?.query?.filter_type == 'activationFee') {
            where += ` AND SubscriptionHistories.plan_type = 'activationFee' AND SubscriptionHistories.price > 0`
        } else if (event?.query?.filter_type == 'activationFeeWaived') {
            where += ` AND SubscriptionHistories.plan_type = 'activationFee' AND SubscriptionHistories.price = 0`
        }

        /**Sort**/
        if (query.sort == "false") {
            sortQuery += ` ORDER BY SubscriptionHistories.createdAt DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            if (query?.sort?.column != "all" && query?.sort?.column) {
                sortQuery += ` ORDER BY ${query?.sort?.column}`;
            }
            if (query?.sort?.type != "all" && query?.sort?.type) {
                sortQuery += ` ${query?.sort?.type}`;
            }
        }

        /**Search**/
        if (query?.search != "false" && query?.search != "") {
            searchQuery = ` SubscriptionHistories.order_date LIKE '%${query?.search}%' OR SubscriptionHistories.subscribed_by LIKE '%${query?.search}%' OR SubscriptionHistories.price LIKE '%${query?.search}%' OR SubscriptionHistories.subscription_valid_till LIKE '%${query?.search}%' OR SubscriptionHistories.payment_type LIKE '%${query?.search}%' OR SubscriptionHistories.status LIKE '%${query?.search}%' OR Practices.name LIKE '%${query?.search}%' `;
        }

        codeSnip += where;
        codeSnip2 += where;

        if (searchQuery != "" && sortQuery != "") {
            codeSnip += " AND (" + searchQuery + ")" + sortQuery;
            codeSnip2 += " AND (" + searchQuery + ")" + sortQuery;
        } else if (searchQuery == "" && sortQuery != "") {
            codeSnip += sortQuery;
            codeSnip2 += sortQuery;
        } else if (searchQuery != "" && sortQuery == "") {
            codeSnip += " AND (" + searchQuery + ")";
            codeSnip2 += " AND (" + searchQuery + ")";
        }

        if(!event?.query?.download){
            codeSnip+=` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }
        let Query = "SELECT SubscriptionHistories.id,Practices.name AS practice_name,SubscriptionHistories.order_date, " +
            "SubscriptionHistories.practice_id,SubscriptionHistories.user_id,SubscriptionHistories.subscribed_by,SubscriptionHistories.price," +
            "SubscriptionHistories.subscribed_on,SubscriptionHistories.plan_id,SubscriptionHistories.plan_type," +
            "SubscriptionHistories.stripe_subscription_id,SubscriptionHistories.subscription_valid_start,SubscriptionHistories.subscription_valid_till," +
            "SubscriptionHistories.cancel_at,SubscriptionHistories.cancel_at_period_end,SubscriptionHistories.canceled_at," +
            "SubscriptionHistories.esquiretek_activity_type,SubscriptionHistories.stripe_subscription_data,SubscriptionHistories.stripe_product_id," +
            "SubscriptionHistories.status,SubscriptionHistories.payment_type,SubscriptionHistories.switched_plan," +
            "SubscriptionHistories.stripe_latest_activity,SubscriptionHistories.is_reminder_send,SubscriptionHistories.createdAt," +
            "SubscriptionHistories.updatedAt FROM SubscriptionHistories INNER JOIN Practices ON SubscriptionHistories.practice_id = Practices.id ";

        let sqlQueryWithPagination = Query + codeSnip;
        let sqlQueryForAllData = Query + codeSnip2;
        console.log(sqlQueryForAllData);
        const dataWithPagination = await sequelize.query(sqlQueryWithPagination, {
            type: QueryTypes.SELECT,
        });

        const getAllData = await sequelize.query(sqlQueryForAllData, {
            type: QueryTypes.SELECT,
        });

        for (let i = 0; i < dataWithPagination.length; i++) {
            if (dataWithPagination[i].plan_type) {

                let monthly_plan = ['monthly', 'responding_monthly_349', 'responding_monthly_495'];
                let yearly_plan = ['yearly', 'responding_yearly_3490', 'responding_yearly_5100'];

                if (dataWithPagination[i].plan_type == 'free_trial') {
                    dataWithPagination[i].plan_type = 'FREE TRIAL';
                } else if (dataWithPagination[i].plan_type == 'pay_as_you_go') {
                    dataWithPagination[i].plan_type = 'TEK AS-YOU-GO';
                } else if (dataWithPagination[i].plan_type == 'propounding_monthly_199') {
                    dataWithPagination[i].plan_type = 'PROPOUNDING MONTHLY';
                } else if (dataWithPagination[i].plan_type == 'propounding_yearly_2199') {
                    dataWithPagination[i].plan_type = 'PROPOUNDING YEARLY';
                } else if (monthly_plan.includes(dataWithPagination[i].plan_type)) {
                    dataWithPagination[i].plan_type = 'MONTHLY';
                } else if (yearly_plan.includes(dataWithPagination[i].plan_type)) {
                    dataWithPagination[i].plan_type = 'YEARLY';
                }
            };
        }

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": getAllData.length,
            },
            body: JSON.stringify(dataWithPagination),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the subscription.' }),
        };
    }
};

const getAllActiveSubscriptions = async (event) => {
    try {
        const query = event.headers;
        let searchKey= '';
        let codeSnip2 ='';
        let sortQuery = '';
        let searchQuery = '';
        let codeSnip = '';
        const sortKey = {};
        const { sequelize, Plans, Op } = await connectToDatabase();

        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;

        let filter = event?.query?.filter;
        
        let monthly_plans = "('monthly','propounding_monthly_199','responding_monthly_349','responding_monthly_495')";
        let yearly_plans = "('yearly','propounding_yearly_2199','responding_yearly_3490','responding_yearly_5100')";

        let where = `WHERE SubscriptionHistories.status = 'Success' `+
        `AND SubscriptionHistories.payment_type != 'Subscription Canceled' AND SubscriptionHistories.plan_category IS NOT NULL AND Subscriptions.subscribed_valid_till >= CURRENT_DATE() `+
            `AND Subscriptions.practice_id != '4a5daca9-869c-48c4-b474-45732f8d36fa' `;
        /* Dashboard */
        if (filter) {
            let dates = customDateandTime(event.query.filter);
            where += ' AND SubscriptionHistories.subscribed_on >= "' + dates.from_date + '" AND SubscriptionHistories.subscribed_on <= "' + dates.to_date + '"';
        }
        if (event?.query?.from_date) {
            from_date = event.query.from_date;
            where += ' AND SubscriptionHistories.subscribed_on >= "'+from_date+'"';
        }
        
        if (event?.query?.to_date) {
            to_date = event.query.to_date;
            where += ' AND SubscriptionHistories.subscribed_on <= "'+to_date+'"';
        }
        if(event?.query?.filter_type == 'monthly'){
            where += ` AND SubscriptionHistories.plan_type IN ${monthly_plans} `
        }else if(event?.query?.filter_type == 'yearly'){
            where += ` AND SubscriptionHistories.plan_type IN ${yearly_plans} `
        }


        /**Sort**/
        sortQuery = `GROUP BY SubscriptionHistories.practice_id , SubscriptionHistories.plan_category `;
        if (query.sort == "false") {
            sortQuery += ` ORDER BY SubscriptionHistories.subscribed_on DESC`;
        } else if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != "all" && sortKey.column != "" && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != "all" && sortKey.type != "" && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != "false" && searchKey != "") {
            searchQuery = ` SubscriptionHistories.subscribed_on LIKE '%${searchKey}%' OR  Practices.name LIKE '%${searchKey}%' OR SubscriptionHistories.price LIKE '%${searchKey}%' OR SubscriptionHistories.payment_type LIKE '%${searchKey}%' `;
        }

        codeSnip += where;
        codeSnip2 += where;
        
        if (searchQuery != "" && sortQuery != "") {
            codeSnip += " AND (" + searchQuery + ")" + sortQuery;
            codeSnip2 +=" AND (" + searchQuery + ")" + sortQuery;
        } else if (searchQuery == "" && sortQuery != "") {
            codeSnip += sortQuery;
            codeSnip2 += sortQuery;
        } else if (searchQuery != "" && sortQuery == "") {
            codeSnip += " AND (" + searchQuery + ")";
            codeSnip2 += " AND (" + searchQuery + ")";
        }

        if(!event?.query?.download){
            codeSnip+=` LIMIT ${query.limit} OFFSET ${query.offset}`;
        }

        let Query =  "SELECT SubscriptionHistories.practice_id, Practices.name AS practice_name, MAX(SubscriptionHistories.order_date) AS order_date, "+
        "SubscriptionHistories.subscribed_by AS subscribed_by, SubscriptionHistories.plan_type, SubscriptionHistories.price, SubscriptionHistories.subscription_valid_till, "+
        "SubscriptionHistories.status, SubscriptionHistories.payment_type, SubscriptionHistories.plan_category FROM SubscriptionHistories INNER JOIN "+
        "Subscriptions ON SubscriptionHistories.practice_id = Subscriptions.practice_id AND SubscriptionHistories.stripe_subscription_id = Subscriptions.stripe_subscription_id "+
        "INNER JOIN Practices ON SubscriptionHistories.practice_id = Practices.id ";
        let sqlQueryWithPagination = Query + codeSnip ;
        let sqlQueryForAllData = Query + codeSnip2 ;

        console.log(sqlQueryWithPagination);

        const dataWithPagination = await sequelize.query(sqlQueryWithPagination, {
            type: QueryTypes.SELECT,
        });

        const getAllData = await sequelize.query(sqlQueryForAllData, {
            type: QueryTypes.SELECT,
        });

        for (let i = 0; i < dataWithPagination.length; i++) {
            if (dataWithPagination[i].plan_type) {

                let monthly_plan = ['monthly', 'responding_monthly_349', 'responding_monthly_495'];
                let yearly_plan = ['yearly', 'responding_yearly_3490', 'responding_yearly_5100'];

                if (dataWithPagination[i].plan_type == 'free_trial') {
                    dataWithPagination[i].plan_type = 'FREE TRIAL';
                } else if (dataWithPagination[i].plan_type == 'pay_as_you_go') {
                    dataWithPagination[i].plan_type = 'TEK AS-YOU-GO';
                } else if (dataWithPagination[i].plan_type == 'propounding_monthly_199') {
                    dataWithPagination[i].plan_type = 'PROPOUNDING MONTHLY';
                } else if (dataWithPagination[i].plan_type == 'propounding_yearly_2199') {
                    dataWithPagination[i].plan_type = 'PROPOUNDING YEARLY';
                } else if (monthly_plan.includes(dataWithPagination[i].plan_type)) {
                    dataWithPagination[i].plan_type = 'MONTHLY';
                } else if (yearly_plan.includes(dataWithPagination[i].plan_type)) {
                    dataWithPagination[i].plan_type = 'YEARLY';
                }
            };
        }

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Expose-Headers": "totalPageCount",
                "totalPageCount": getAllData.length,
            },
            body: JSON.stringify(dataWithPagination),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the subscription.' }),
        };
    }
};

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.getAllHistory = getAllHistory;
module.exports.adminGetAllHistory = adminGetAllHistory;
module.exports.createPropoundingSubscription = createPropoundingSubscription;
module.exports.cancelPropoundingSubscription = cancelPropoundingSubscription;
module.exports.getAllActivationFeeAndFeeWaived = getAllActivationFeeAndFeeWaived;
module.exports.getAllActiveSubscriptions = getAllActiveSubscriptions;