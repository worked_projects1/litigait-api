const axios = require("axios");
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const decode = require("urldecode");
const jwt = require('jsonwebtoken');
const uuid = require("uuid");
const { sendEmail } = require("../../utils/mailModule");
const { generateRandomString } = require('../../utils/randomStringGenerator');

const fetchSigninDetails = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;

        const { Users, Op } = await connectToDatabase();

        const accessToken = input.googleAccessToken;
        const googleUrl = process.env.GOOGLE_USERINFO_URL;
        let result;

        await axios.get(googleUrl, { headers: { "Authorization": `Bearer ${accessToken}` } }).then((response) => {
            result = response.data;
        });

        const email = result.email;

        const UserData = await Users.findOne({
            where: {
                email,
                is_deleted: { [Op.not]: true }
            },
        });

        if (!UserData) throw new HTTPError(404, 'User does not exist');

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(result),
        };
    }
    catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || "invalid token" }),
        };
    }
}

const fetchSignUpDetails = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;

        const { Users, Op } = await connectToDatabase();

        const accessToken = input.googleAccessToken;
        const googleUrl = process.env.GOOGLE_USERINFO_URL;
        let result;

        await axios.get(googleUrl, { headers: { "Authorization": `Bearer ${accessToken}` } }).then((response) => {
            result = response.data;
        });

        const email = result.email;

        const UserData = await Users.findOne({
            where: {
                email,
                is_deleted: { [Op.not]: true }
            },
        });

        if (UserData) throw new HTTPError(404, 'User already exist');

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(result),
        };
    }
    catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || "invalid token" }),
        };
    }
}


const GoogleSignIn = async (UserData) => {
    try {
        const { Settings, Users, Op } = await connectToDatabase();

        const tokenUser = {
            id: UserData.id,
            role: UserData.role,
        };
        const token = jwt.sign(
            tokenUser,
            process.env.JWT_SECRET,
            {
                expiresIn: process.env.JWT_EXPIRATION_TIME,
            }
        );

        const settingsObject = await Settings.findOne({
            where: {
                key: 'global_settings',
            }
        });

        const responseData = {
            //authToken: `JWT ${token}`,
            user: {
                name: UserData.name,
                role: UserData.role,
                email: UserData.email,
            },
        };

        if (UserData.twofactor_status) {
            const otpSecret = generateRandomString(4);
            const userotpUpdate = await Users.update({ twofactor_otp: otpSecret }, {
                where: {
                    email:responseData.user.email,
                    is_deleted: { [Op.not]: true }
                }
            });
            await sendEmail(responseData.user.email, 'EsquireTek Two Factor Authentication', ` Your OTP code is <b>${otpSecret}</b>`);
            responseData.user.otpstatus = 'Otp code send Successfully';

            responseData.user.two_factor = true;
            responseData.authToken = `JWT ${token}`;
        } else {
            responseData.authToken = `JWT ${token}`;
        }
        responseData.user.newUserLogin = false;
        responseData.user.signingMethod = 'signin';
        responseData.user.user_from = 'google';

        if (settingsObject) {
            const plainSettingsObject = settingsObject.get({ plain: true });
            const settings = JSON.parse(plainSettingsObject.value);
            responseData.session_timeout = settings.session_timeout;
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(responseData),
        };
    }
    catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || "invalid token" }),
        };
    }
}

const googleAuthentication = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;

        const code = decode(input.code);

        const { Users, Op } = await connectToDatabase();

        let token_result, userInfo_result, client_id, client_secret, token_url, redirect_uri;

        client_id = process.env.GOOGLE_CLIENT_ID;
        client_secret = process.env.GOOGLE_CLIENT_SECRET;
        token_url = process.env.GOOGLE_TOKEN_URL;
        userinfo_url = process.env.GOOGLE_USERINFO_URL;
        redirect_uri = `${process.env.APP_URL}/google-oauth2`;

        let data = { code, grant_type: "authorization_code", redirect_uri, client_id, client_secret };

        await axios.request({ url: token_url, method: "post", data }).then((response) => {
            token_result = response.data;
        });
        const access_token = token_result.access_token;

        let headers = {
            Authorization: "Bearer " + access_token,
        };

        await axios.request({ url: userinfo_url, method: "get", headers }).then((response) => {
            userInfo_result = response.data;
        });

        const userdetails = {
            name: userInfo_result.name,
            email: userInfo_result.email,
        }

        if (process.env.NODE_ENV === "test" && event.body.id) {
            userdetails.id = event.body.id;
        }

        const UserData = await Users.findOne({
            where: {
                email: userdetails.email,
                is_deleted: { [Op.not]: true }
            },
        });

    const responseData = {
        user: {
            name: userdetails.name,
            email: userdetails.email,
            newUserLogin: true,
            signingMethod: 'signup',
            user_from: 'google',
        }
    }

        if (UserData) {
            UserData.last_login_ts = new Date();
            UserData.device_id = input.device_id;
            await UserData.save();

            return GoogleSignIn(UserData);

        } else {
            return {
                statusCode: 200, headers: {
                    "Content-Type": "text/plain",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": true,
                }, body: JSON.stringify(responseData),
            };
        }
    }
    catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || "invalid request" }),
        };
    }
}

module.exports.fetchSigninDetails = fetchSigninDetails;
module.exports.fetchSignUpDetails = fetchSignUpDetails;
module.exports.googleAuthentication = googleAuthentication;
