module.exports = (sequelize, type) => sequelize.define('Feewaiver', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    email: type.STRING,
    expiry_date: type.DATE,
});