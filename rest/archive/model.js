module.exports = (sequelize, type) => sequelize.define('CaseArchive', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    client_id: type.STRING,
    case_id: type.STRING,
    case_title: type.STRING,
    case_number: type.STRING,
    form_data: type.TEXT('long'),
    is_deleted: type.BOOLEAN,
    is_unarchived: type.BOOLEAN,
    state: type.STRING
});
