const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateArchiveCases = function (data) {
    const rules = {
        id: 'required',
        practice_id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
exports.validateUnarchiveCases = function (data) {
    const rules = {
        id: 'required',
        practice_id: 'required',
        client_id: 'required',
        case_id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};