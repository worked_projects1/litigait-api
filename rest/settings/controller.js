const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');


const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const authMiddleware = require('../../auth');


const getSettings = async (event) => {
    try {
        const {Settings} = await connectToDatabase();
        const settingsObject = await Settings.findOne({
            where: {
                key: 'global_settings',
            }
        });
        let settings;
        if (settingsObject) {
            const plainSettingsObject = settingsObject.get({plain: true});
            settings = JSON.parse(plainSettingsObject.value);
            if (!settings.medical_history_per_page_price) {
                settings.medical_history_per_page_price = 0;
            }
            if (!settings.medical_history_pricing_tier) {
                settings.medical_history_pricing_tier = [];
            }
            if (!settings.no_of_medical_history_free_pages) {
                settings.no_of_medical_history_free_pages = 0;
            }
            if (!settings.medical_history_minimum_pricing) {
                settings.medical_history_minimum_pricing = 0;
            }
        } else {
            settings = {
                no_of_docs_free_tier: 0,
                template_generation_price: {
                    FROGS: 0,
                    SPROGS: 0,
                    RFPD: 0,
                    RFA: 0,
                },
                final_doc_generation_price: {
                    FROGS: 0,
                    SPROGS: 0,
                    RFPD: 0,
                    RFA: 0,
                },
                medical_history_per_page_price: 0,
                no_of_medical_history_free_pages: 0,
                medical_history_minimum_pricing: 0,
                session_timeout: 0,
            };
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(settings),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the Practices.'}),
        };
    }
};


const setSettings = async (event) => {
    try {
        const input = JSON.parse(event.body);
        const {Settings} = await connectToDatabase();
        const settingsObject = await Settings.findOne({
            where: {
                key: 'global_settings',
            }
        });
        if (settingsObject) {
            settingsObject.value = JSON.stringify(input);
            await settingsObject.save();
        } else {
            const dataObject = Object.assign({
                key: 'global_settings',
                value: JSON.stringify(input),
            }, {id: uuid.v4()});
            await Settings.create(dataObject);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(input),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not update the Practices.'}),
        };
    }
};

module.exports.getSettings = middy(getSettings).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.setSettings = middy(setSettings).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
