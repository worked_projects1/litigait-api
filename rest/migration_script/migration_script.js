const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');


const stateNewObjection = async (event) => {
    try {
        let id;
        const inputBody = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {AdminObjections, CustomerObjections, Practices, Op,} = await connectToDatabase();
        for (let i = 0; i < inputBody.length; i++) {
            const input = inputBody[i];
            if (event.user.id) {
                input.created_by_admin_id = event.user.id;
            }
            if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
            if (!input.objection_title || !input.objection_text) throw new HTTPError(403, `Objection Title or Objection Text cannot be emapty`);
            const dataObject = Object.assign(input, {id: id || uuid.v4()});
            const internalRoles = ['superAdmin', 'manager', 'operator'];
            console.log(event.user.role);
            if (!internalRoles.includes(event.user.role)) throw new HTTPError(403, `cannot create migration objection for this user`);
            const adminObjectionss = await AdminObjections.create(dataObject);
            const adminObjectionssPlain = await AdminObjections.findOne({where: {id: adminObjectionss.id}});

            const newObjection = adminObjectionssPlain;
            const processedObjections = [];

            const allPractices = await Practices.findAll({
                where: {is_deleted: {[Op.not]: true}},
                raw: true,
                attributes: ['id']
            });
            for (let p = 0; p < allPractices.length; p += 1) {
                const practice = allPractices[p];
                if (!newObjection.objection_title || !newObjection.objection_text) throw new HTTPError(403, `Objection Title or Objection Text cannot be emapty`);
                const objectionItem = {};
                objectionItem.id = `${uuid.v4()}${practice.id}`;
                objectionItem.adminobjection_id = newObjection.id;
                objectionItem.practice_id = practice.id;
                objectionItem.objection_title = newObjection.objection_title;
                objectionItem.objection_text = newObjection.objection_text;
                objectionItem.objection_view = true;
                objectionItem.state = newObjection.state;
                processedObjections.push(objectionItem);
            }
            if (processedObjections.length) {
                await CustomerObjections.bulkCreate(processedObjections);
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Objection Added Successfully',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the adminObjectionss.'}),
        };
    }
}
const updateDiscountAmount = async (event) => {
    try {
        const { DiscountHistory, Op } = await connectToDatabase();
        const historyObj = await DiscountHistory.findAll({
            where:{[Op.or]:[
                {base_price:5100},
                {base_price:495},
                {base_price:199},
                {base_price: 2199}
            ]},
            logging:console.log,
            raw: true
        });

        for (let i = 0; i < historyObj.length; i++) {
            console.log(i);
            const discount_percentage = historyObj[i].discount_percentage;
            let percentage = 1 - (discount_percentage / 100);
            percentage = parseFloat(percentage.toFixed(2));
            const base_price = historyObj[i].base_price;
            let discount_price = (percentage * base_price).toFixed(2);
            discount_price = parseFloat(discount_price);
            let discount_amount = base_price - discount_price;
            discount_amount = parseFloat(discount_amount.toFixed(2));
            discount_amount = discount_amount.toFixed(2);
            console.log('percentage : '+percentage);
            console.log('base_price : '+base_price);
            console.log('discounted_price : '+discount_price);
            console.log('discount_amount : '+discount_amount);

            await DiscountHistory.update({ discounted_price: discount_price, discount_amount: discount_amount }, { where: { id: historyObj[i].id } });
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: ' updated Successfully',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the discount history.'}),
        };
    }
}
const updateDiscountAmountActivation = async (event) => {
    try {
        const { DiscountHistory, Op } = await connectToDatabase();
        const historyObj = await DiscountHistory.findAll({
            where:{base_price:{[Op.notIn]:[5100, 495 , 199,  2199]},discount_percentage:{[Op.not]:null}},
            logging:console.log,
            raw: true
        });

        for (let i = 0; i < historyObj.length; i++) {
            const discountPercentage = historyObj[i].discount_percentage;
            let percentage = 1 - (discountPercentage / 100);
            percentage = parseFloat(percentage.toFixed(2));
            const basePrice = historyObj[i].base_price;
            const discountPrice = parseFloat((Math.ceil(percentage * basePrice)).toFixed(2));
            const discountAmount = parseFloat((basePrice - discountPrice).toFixed(2));
            console.log(' i : '+ i);
            console.log(' percentage : '+ percentage);
            console.log(' basePrice : '+ basePrice);
            console.log(' discountPrice : '+discountPrice);
            console.log(' discountAmount : '+discountAmount);
            
            await DiscountHistory.update({ discounted_price: discountPrice, discount_amount: discountAmount.toFixed(2) }, { where: { id: historyObj[i].id } });
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: ' updated Successfully',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the discount history.'}),
        };
    }
}
module.exports.stateNewObjection = stateNewObjection;
module.exports.updateDiscountAmount = updateDiscountAmount;
module.exports.updateDiscountAmountActivation = updateDiscountAmountActivation;

