/* eslint-disable indent */

const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY, {apiVersion: ''});
const {sendEmail} = require('../../utils/mailModule');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');
const authMiddleware = require('../../auth');


const stripeWebHook = async (event) => {
    const input = typeof event === 'string' ? JSON.parse(event) : event;
    const {Subscriptions} = await connectToDatabase();
    if (input && input.data && input.data.object && input.data.object.object === 'subscription') {
        const existingSubscriptionDetails = await Subscriptions.findOne({
            where: {stripe_subscription_id: input.data.object.id}, order: [['createdAt', 'DESC']],
        });
        if (existingSubscriptionDetails) {
            const subscription = await stripe.subscriptions.retrieve(
                input.data.object.id
            );
            if (subscription) {
                existingSubscriptionDetails.stripe_subscription_data = JSON.stringify(subscription);
                await existingSubscriptionDetails.save();
            }
        }
    }
    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'text/plain',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body: JSON.stringify({
            status: 'ok',
        }),
    };
};
const fetchBillingStatus = async (event) => {
    try {
        const input = JSON.parse(event.body);
        const {
            Practices, Settings, Orders, MedicalHistory, MedicalHistorySummery, Op, Cases, Clients, PracticeSettings,
            Subscriptions, Users, Plans } = await connectToDatabase();
        const practiceObject = await Practices.findOne({where: {id: event.user.practice_id}});
        const usersObject = await Users.findOne({where: {id: event.user.id}});
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        const document_type = input.document_type;
        const document_generation_type = input.document_generation_type;
        
        const settingsObject = await Settings.findOne({ where: { key: 'global_settings' }, raw: true, });

        if (!settingsObject) throw new HTTPError(400, 'Settings data not found');
        let settings = JSON.parse(settingsObject.value);

        const practiceSettings = await PracticeSettings.findOne({ where: { practice_id: event.user.practice_id, }, raw: true, logging: console.log });
        if (practiceSettings) {
            const practiceSettingsObject = JSON.parse(practiceSettings.value);
            settings = Object.assign(settings, practiceSettingsObject);
        }
        let validSubscriptionFeatures = [];
        let plan_category = document_generation_type == 'propound_doc' ? 'propounding' : 'responding';
        
        const existingSubscriptionDetails = await Subscriptions.findOne({
            where: {practice_id: event.user.practice_id, plan_category},
            order: [['createdAt', 'DESC']], logging: console.log
        });
 

        let existingSubscriptionDetailsPlain = undefined;

        if (existingSubscriptionDetails?.existingSubscriptionDetails?.stripe_subscription_id.startsWith('sub_') && existingSubscriptionDetails.plan_id ) {
            const subscription = await stripe.subscriptions.retrieve(existingSubscriptionDetails.stripe_subscription_id);
            if (subscription) {
                existingSubscriptionDetails.stripe_subscription_data = JSON.stringify(subscription);
                const updateubscriptionDetails = await existingSubscriptionDetails.save();
                existingSubscriptionDetailsPlain = updateubscriptionDetails.get({plain: true});
            }
        }else if(existingSubscriptionDetails?.existingSubscriptionDetails?.stripe_subscription_id.startsWith('pi_')){
            existingSubscriptionDetailsPlain = existingSubscriptionDetails.get({plain: true});
        }
        let planDetails = undefined;
        if (existingSubscriptionDetails && existingSubscriptionDetails.stripe_subscription_data && existingSubscriptionDetails.plan_id ) {
            let subscriptionValidity;
            if(existingSubscriptionDetails?.stripe_subscription_id.startsWith('sub_')){
                const stripeSubscriptionDataObject = JSON.parse(existingSubscriptionDetails.stripe_subscription_data);
                subscriptionValidity = new Date(parseInt(stripeSubscriptionDataObject.current_period_end) * 1000);
            }else if(existingSubscriptionDetails?.stripe_subscription_id.startsWith('pi_')){
                subscriptionValidity = new Date(existingSubscriptionDetails.subscribed_valid_till);
            }
            const today = new Date();
            if (subscriptionValidity > today) {
                planDetails = await Plans.findOne({
                    raw: true,
                    where: {stripe_product_id: existingSubscriptionDetails.stripe_product_id},
                });
                validSubscriptionFeatures = planDetails.features_included.split(',');
            }
        }


        let free_tier_available = true;

        let price = 0;
        let custom_quote_needed = false;
        let pagesCount = 0;
        let pricing_tier = {};
        let uneffectedPrice = 0;
        let minimumPrice = 0;
        let medicalHistory_previous_price = 0;
        let previous_uploaded_docs_pagesCount = 0;
        let total_uploaded_docs_pagesCount = 0;
        let medical_history_total_price = 0;

        let medicalhistory_first_set = true;
        const availableFormDocumenttype = ['FROGS', 'SPROGS', 'RFPD', 'RFA'];
        if (document_type === 'TEKSIGN') {

            console.log(settings);
            if (settings.teksign) {
                price = parseFloat(settings.teksign, 10);
            } else {
                price = 0;
            }
            free_tier_available = false;
            if (validSubscriptionFeatures.includes('teksign')) {
                price = 0;
                free_tier_available = true;
            }

            const existOrderModel = await Orders.findAll({
                where: {
                    status: 'completed',
                    practice_id: event.user.practice_id,
                    client_id: input.client_id,
                    legalforms_id: input.legalforms_id,
                    document_type: 'TEKSIGN',
                    document_generation_type: 'teksign'
                }, logging: console.log
            });
            if (existOrderModel.length != 0 || (planDetails && planDetails.plan_id != 'free_trial')) {
                price = 0;
                free_tier_available = true;
            }

        } else if ((document_generation_type == 'pos' || document_generation_type == 'POS') && availableFormDocumenttype.includes(document_type)) {
            const posFreeQuotaUseageCount = await Orders.count({
                where: {
                    practice_id: event.user.practice_id,
                    document_type: {[Op.in]: ['FROGS', 'SPROGS', 'RFPD', 'RFA']},
                    amount_charged: 0,
                }
            });
            if (posFreeQuotaUseageCount >= parseInt(settings.no_of_docs_free_tier, 10)) {
                free_tier_available = false;
            }
            price = parseFloat(settings[document_generation_type], 10);
            if (validSubscriptionFeatures.includes('pos')) {
                price = 0;
                free_tier_available = true;
            }

        } else if (availableFormDocumenttype.includes(document_type) && document_generation_type != 'pos' && document_generation_type != 'propound_doc') {
            console.log('Inside documents generate');
            const freeQuotaUseageCount = await Orders.count({
                where: {
                    practice_id: event.user.practice_id,
                    document_type: {[Op.in]: ['FROGS', 'SPROGS', 'RFPD', 'RFA']},
                    amount_charged: 0,
                }
            });
            console.log(settings.no_of_docs_free_tier);
            console.log('freeQuotaUseageCount : '+freeQuotaUseageCount);
            if (freeQuotaUseageCount >= parseInt(settings.no_of_docs_free_tier, 10)) {
                free_tier_available = false;
            }            
            price = parseFloat(settings[`${document_generation_type}_generation_price`][document_type], 10);
            if (input.document_generation_type == 'template' && validSubscriptionFeatures.includes('shell')) {
            // if (validSubscriptionFeatures.includes('shell')) {
                price = 0;
                free_tier_available = true;
            }
            console.log('validSubscriptionFeatures : '+validSubscriptionFeatures);
            if (input.document_generation_type == 'final_doc' && validSubscriptionFeatures.includes('discovery')) {
                price = 0;
                free_tier_available = true;
            }
        } else if (document_type === 'MEDICAL_HISTORY') {
            const MedicalHistoryRecords = await MedicalHistory.findAll({
                where: {case_id: input.case_id},
                order: [
                    ['createdAt', 'DESC'],
                ],
                raw: true
            });

            const MedicalSummeryRecords = await MedicalHistorySummery.findOne({
                where: {case_id: input.case_id},
                order: [
                    ['createdAt', 'DESC'],
                ],
                raw: true
            });
            if (MedicalSummeryRecords) {
                medicalhistory_first_set = false;
                medicalHistory_previous_price = parseFloat(MedicalSummeryRecords.amount_charged);
                previous_uploaded_docs_pagesCount = MedicalSummeryRecords.pages;
            }
            for (let i = 0; i < MedicalHistoryRecords.length; i += 1) {
                if (MedicalHistoryRecords[i].pages) {
                    pagesCount += MedicalHistoryRecords[i].pages || 1;
                }
            }
            console.log('Page Count ' + pagesCount);
            console.log(settings.medical_history_pricing_tier);
            console.log(settings.medical_history_minimum_pricing);
            if (settings.medical_history_pricing_tier && settings.medical_history_pricing_tier) {

                for (let i = 0; i < settings.medical_history_pricing_tier.length; i += 1) {
                    const pricingPLan = settings.medical_history_pricing_tier[i];
                    if (pagesCount > pricingPLan.from && pricingPLan.custom_quote_needed) {
                        pricing_tier = pricingPLan;
                        custom_quote_needed = true;
                    } else if (pagesCount >= pricingPLan.from && pagesCount <= pricingPLan.to) {
                        pricing_tier = pricingPLan;
                        if (!pricingPLan.price) {
                            free_tier_available = true;
                        } else {
                            free_tier_available = false;
                        }
                        console.log(pricingPLan.price);
                        price = parseFloat(pricingPLan.price) * pagesCount;
                        uneffectedPrice = price;
                    }
                }

                console.log('Price ' + price);
                console.log('Uneffected Price ' + uneffectedPrice);

                if (settings.medical_history_minimum_pricing) {
                    const minimumPricing = parseFloat(settings.medical_history_minimum_pricing);
                    if (price < minimumPricing) {
                        price = minimumPricing;
                        minimumPrice = price;
                        console.log('Inside Minimum Price');
                        console.log('If Price Price ' + price);
                        console.log('Minimum Price ' + minimumPrice);
                    }
                }

                if (price) {
                    price = parseFloat(price.toFixed(2));
                    console.log(price);
                    free_tier_available = false;
                } else {
                    free_tier_available = true;
                }
            } else {
                return {
                    statusCode: 400,
                    headers: {
                        'Content-Type': 'text/plain',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Credentials': true
                    },
                    body: JSON.stringify({
                        status: 'errpr',
                        message: 'Pricing tier data not available',
                    }),
                };
            }


            console.log('Final---');
            console.log('Total Page Count  ' + pagesCount);
            console.log('Previous Upload Page Count  ' + previous_uploaded_docs_pagesCount);
            console.log('Valid Subscription Features');
            console.log(validSubscriptionFeatures);
            if (!medicalhistory_first_set) {
                if (validSubscriptionFeatures.length && price) {
                    price *= 0.90; //10 % discound for Yearly Subscription
                }
                price = parseFloat(price).toFixed(2);
                price = parseFloat(price);
                medical_history_total_price = price;
                total_uploaded_docs_pagesCount = pagesCount;
                console.log('second set price ' + price);
                pagesCount = pagesCount - previous_uploaded_docs_pagesCount;
                if (pagesCount < 0) {
                    pagesCount = pagesCount * -1;
                } else if (pagesCount == 0) {
                    pagesCount = previous_uploaded_docs_pagesCount
                }
                if (price > medicalHistory_previous_price) {
                    price = price - medicalHistory_previous_price;
                    price = parseFloat(price).toFixed(2);
                    console.log('second set price inside if ' + price);
                } else {
                    price = 0;
                }
            } else {
                if (validSubscriptionFeatures.length && price) {
                    price *= 0.90; //10 % discound for Yearly Subscription
                }
                price = parseFloat(price).toFixed(2);
                price = parseFloat(price);
                medical_history_total_price = price;
                total_uploaded_docs_pagesCount = pagesCount;
            }
            console.log("-----------")
            console.log('Total Page Count  ' + total_uploaded_docs_pagesCount);
            console.log('Previous Upload Page Count  ' + previous_uploaded_docs_pagesCount);
            console.log('Current Document Page Count  ' + pagesCount);

            // console.log('After parse price '  + price);
            // if (validSubscriptionFeatures.includes('medicalHistory')) {
            //   price = 0;
            //   free_tier_available = true;
            // } else if (validSubscriptionFeatures.length && price) {
            //   price *= 0.90; //10 % discound for Yearly Subscription
            //   price = parseFloat(price).toFixed(2);
            // }
        } else if (document_type === 'custom_template') {
            if (settings.custom_template) {
                price = parseFloat(settings.custom_template, 10);
            } else {
                /* price = 0; */
                price = 300;
            }
            /* free_tier_available = false; */
            free_tier_available = false;
            if (validSubscriptionFeatures.includes('custom_template')) {
                /* price = 0; */
                price = 300;
                free_tier_available = false;
            }
        } else if (document_generation_type === 'propound_doc') {
            console.log('inside propound if');
            // if (settings.propound_doc) { 
            //     price = parseFloat(settings.propound_doc, 10);
            // } else {
            //     price = 0;
            // }
            const propoundDocumentCount = await Orders.count({where: {
                practice_id: event.user.practice_id,
                document_type: input.document_type,
                document_generation_type: 'propound_doc'
                }});
                console.log('propoundDocumentCount : '+propoundDocumentCount);
                console.log('settings.no_of_docs_free_tier : '+settings.no_of_docs_free_tier);
                if(propoundDocumentCount >= parseInt(settings.no_of_docs_free_tier, 10)){
                    free_tier_available = false;
                }else{
                    free_tier_available = true;
                }
                console.log(validSubscriptionFeatures);
            if (validSubscriptionFeatures.includes('propound_doc')) {
                price = 0;
                free_tier_available = true;
            }

            const existOrderModel = await Orders.findAll({
                where: {
                    status: 'completed',
                    practice_id: event.user.practice_id,
                    client_id: input.client_id,
                    propoundforms_id: input.propoundforms_id,
                    document_type: input.document_type,
                    document_generation_type: 'propound_doc'
                }, logging: console.log
            });
            console.log('existOrderModel : '+existOrderModel.length);
            if (existOrderModel.length != 0 && (planDetails && planDetails.plan_id != 'free_trial')) {
                price = 0;
                free_tier_available = true;
            }

        }


        let credit_card_details_available = false;

        let stripe_payment_methods = [];

        if (practiceObject && practiceObject.stripe_customer_id) {
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });
            stripe_payment_methods = paymentMethods.data || [];
            if (paymentMethods.data.length) {
                credit_card_details_available = true;
            }
        }
        const username = event?.user?.name;
        if (custom_quote_needed === true) {
            const caseObject = await Cases.findOne({where: {id: input.case_id}});
            const clientObject = await Clients.findOne({where: {id: caseObject.client_id}});
            let template = 'Total pages exceeded'
            if(event?.user?.name){ template = template + ' by '+ event.user.name}
            await sendEmail(process.env.SUPPORT_EMAIL_RECEIPIENT, `${template}`, `
      User Email: ${event.user.email} <br/>
      Practice Name : ${practiceObject.name} <br/>
      Client Name : ${clientObject.name} <br/>
      Case Title : ${caseObject.case_title} <br/>
      Case Number : ${caseObject.case_number} <br/>
      Document Type: Medical History <br/>
      Total Pages: ${pagesCount} <br/>
      `);
        }

        if (price && price > 0 && price < 0.50) {
            price = 0.50;
        }

        if (document_type === 'MEDICAL_HISTORY') {
            free_tier_available = false;
        }
        /* previous_price_amount = medicalHistory_previous_price; */
        medical_history_total_price = medical_history_total_price.toFixed(2);
        uneffectedPrice = uneffectedPrice.toFixed(2);
        medicalHistory_previous_price = medicalHistory_previous_price.toFixed(2);
        const data = {
            free_tier_available,
            credit_card_details_available,
            price,
            stripe_payment_methods,
            custom_quote_needed,
            pagesCount,
            pricing_tier,
            validSubscriptionFeatures,
            uneffectedPrice,
            minimumPrice,
            medicalHistory_previous_price,
            previous_uploaded_docs_pagesCount,
            total_uploaded_docs_pagesCount,
            medical_history_total_price
            /* previous_price_amount */
        };

        if(usersObject?.notification_status){
            data.is_tekasyougo = usersObject.notification_status;
        }else{
            data.is_tekasyougo = false;
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(data),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the practices.'}),
        };
    }
};
const setUpPaymentIntent = async (event) => {
    try {
        const {Practices} = await connectToDatabase();
        const practiceObject = await Practices.findOne({where: {id: event.user.practice_id}});
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        if (!practiceObject.stripe_customer_id) {
            const customer = await stripe.customers.create();
            console.log('Customer ID ' + customer.id);
            practiceObject.stripe_customer_id = customer.id;
        }

        const intent = await stripe.setupIntents.create({
            customer: practiceObject.stripe_customer_id,
        });

        await practiceObject.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                client_secret: intent.client_secret,
            }),
        };
    } catch (err) {
        console.log('Stripe error : payment intent');
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the practices.'}),
        };
    }
};
const testPayment = async (event) => {
    try {
        const {Practices} = await connectToDatabase();
        const practiceObject = await Practices.findOne({where: {id: event.user.practice_id}});
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        const paymentMethods = await stripe.paymentMethods.list({
            customer: practiceObject.stripe_customer_id,
            type: 'card',
        });
        if (!paymentMethods.data.length) {
            throw new HTTPError(400, 'Payment method not added');
        }

        const paymentMethod = paymentMethods.data[0];
        const paymentIntent = await stripe.paymentIntents.create({
            amount: 1099,
            currency: 'usd',
            customer: practiceObject.stripe_customer_id,
            payment_method: paymentMethod.id,
            off_session: true,
            confirm: true,
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                paymentIntent,
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the practices.'}),
        };
    }
};
const saveCardData = async (event) => {
    try {
        const input = JSON.parse(event.body);
        const {Practices} = await connectToDatabase();
        const practiceObject = await Practices.findOne({where: {id: event.user.practice_id}});
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        if (!input) {
            throw new HTTPError(400, 'fields missing');
        }
        if (!input.payment_method) {
            throw new HTTPError(400, 'Stripe Token missing');
        }
        if (practiceObject.stripe_customer_id) {
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });
            if (paymentMethods.data.length) {
                for (let i = 0; i < paymentMethods.data.length; i += 1) {
                    if (paymentMethods.data[i].id != input.payment_method) {
                        await stripe.paymentMethods.detach(paymentMethods.data[i].id);
                    }
                }
            }
            await stripe.paymentMethods.attach(
                input.payment_method,
                {customer: practiceObject.stripe_customer_id}
            );
            await stripe.customers.update(
                practiceObject.stripe_customer_id,
                {
                    invoice_settings: {default_payment_method: input.payment_method},
                    email: event.user.email,
                    name: practiceObject.name,
                    metadata: {
                        practice_id: event.user.practice_id,
                    },
                }
            );
        } else {
            const stripeCustomer = await stripe.customers.create(
                {
                    description: ' Added Stripe Data',
                    email: event.user.email,
                    name: practiceObject.name,
                    metadata: {
                        practice_id: event.user.practice_id,
                    },
                }
            );
            practiceObject.stripe_customer_id = stripeCustomer.id;
            await practiceObject.save();
            await stripe.paymentMethods.attach(
                input.payment_method,
                {customer: practiceObject.stripe_customer_id}
            );
            await stripe.customers.update(
                practiceObject.stripe_customer_id,
                {invoice_settings: {default_payment_method: input.payment_method}}
            );
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'card data saved',
            }),
        };
    } catch (err) {
        console.log('Stripe error : saving card data');
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not create the practices.'}),
        };
    }
};

module.exports.saveCardData = middy(saveCardData).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.fetchBillingStatus = middy(fetchBillingStatus).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.setUpPaymentIntent = middy(setUpPaymentIntent).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.testPayment = middy(testPayment).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.stripeWebHook = stripeWebHook;