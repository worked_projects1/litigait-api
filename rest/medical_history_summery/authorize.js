const {HTTPError} = require('../../utils/httpResp');

const adminInternalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const practiceRoles = ['lawyer', 'paralegal'];
const internalRoles = ['superAdmin', 'manager', 'operator', 'medicalExpert', 'QualityTechnician'];
const superAdminRole = ['superAdmin', 'medicalExpert', 'QualityTechnician'];
const normalUserRole = ['superAdmin', 'manager', 'operator'];

exports.authorizeCreate = function (user) {
    if (!superAdminRole.includes(user.role) && !user.is_admin) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeGetOne = function (user) {
    if (!superAdminRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};
exports.authorizeGetAll = function (user) {
    if (!superAdminRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeUpdate = function (user) {
    if (!superAdminRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeDestroy = function (user) {
    if (!superAdminRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};
