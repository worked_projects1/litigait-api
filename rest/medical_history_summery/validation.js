const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateCreateMedicalHistorySummery = function (data) {
    const rules = {
        // client_id: 'required|min:4|max:64',
        // case_id: 'required|min:4|max:64',
        // start_date: 'date',
        // case_title: 'required|min:4|max:64',
        // case_number: 'required|min:1|max:64',
        // status: 'min:3|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateGetOneMedicalHistorySummery = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateUpdateMedicalHistorySummery = function (data) {
    const rules = {
        // s3_file_key: 'min:2',
        // comment: 'min:2',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDeleteMedicalHistorySummery = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateReadMedicalHistorySummery = function (data) {
    const rules = {
        offset: 'numeric',
        limit: 'numeric',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};
