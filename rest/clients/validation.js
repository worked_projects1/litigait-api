const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateCreateClients = function (data) {
    const rules = {
        name: 'min:4|max:200',
        email: 'email|min:4|max:64',
        phone: 'required|numeric',
        address: 'min:4|max:64',
        hipaa_acceptance_status: 'boolean',
        hipaa_acceptance_date: 'date',
        fee_acceptance_status: 'boolean',
        fee_acceptance_date: 'date',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateGetOneClient = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateUpdateClients = function (data) {
    const rules = {
        name: 'min:4|max:64',
        email: 'email|min:4|max:64',
        phone: 'numeric',
        address: 'min:4|max:64',
        hipaa_acceptance_status: 'boolean',
        hipaa_acceptance_date: 'date',
        fee_acceptance_status: 'boolean',
        fee_acceptance_date: 'date',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateDeleteClient = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateReadClients = function (data) {
    const rules = {
        offset: 'numeric',
        limit: 'numeric',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};


exports.validateTermsAcceptance = function (data) {
    if (data.hipaa_acceptance_status === false && data.fee_acceptance_status === false) {
        throw new HTTPError(400, 'invalid params, both status cannot be false');
    }
};
