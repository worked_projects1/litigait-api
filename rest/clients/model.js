module.exports = (sequelize, type) => sequelize.define('Clients', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    name: type.STRING,
    first_name: type.STRING,
    middle_name: type.STRING,
    last_name: type.STRING,
    email: type.STRING,
    phone: type.STRING,
    // phone_country_code: type.STRING,
    address: type.STRING,
    street: type.STRING,
    city: type.STRING,
    state: type.STRING,
    zip_code: type.STRING,
    country: type.STRING,
    dob: type.DATE,
    client_from: type.STRING, // To identify whether the client is from quick_create, filevine, mycase or clio.
    integration_client_id: type.STRING, // integrations - filevine, mycase, clio.
    hipaa_acceptance_status: type.BOOLEAN,
    hipaa_acceptance_date: type.DATE,
    fee_acceptance_status: type.BOOLEAN,
    fee_acceptance_date: type.DATE,
    added_by_user: type.STRING,
    practice_id: type.STRING,
    archived: type.BOOLEAN,
    terms_sent_by: type.STRING,
    is_deleted: type.BOOLEAN,
},
{
    indexes: [
        {
            name: 'is_deleted',
            fields: ['is_deleted']
        },
        {
            name: 'createdAt',
            fields: ['createdAt']
        },
    ]
});
