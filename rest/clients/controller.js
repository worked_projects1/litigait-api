const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const { sendEmail } = require('../../utils/mailModule');
const { sendSMS } = require('../../utils/smsModule');
const authMiddleware = require('../../auth');
const {
    validateCreateClients, validateGetOneClient, validateUpdateClients, validateDeleteClient, validateReadClients, validateTermsAcceptance,
} = require('./validation');
const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION || 'us-east-1' });
const s3 = new AWS.S3();

const { generateRandomString } = require('../../utils/randomStringGenerator');
const { QueryTypes } = require('sequelize');
const create = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        if (event.user.practice_id) {
            input.practice_id = event.user.practice_id;
        }
        let name = '', address;
        if(input?.first_name){
            name = input?.first_name;
        }
        if (input.middle_name) { 
            name = name + ' ' + input.middle_name; 
        }
        if (input.last_name) { 
            name = name + ' ' + input.last_name; 
        }
        if(input.street){
        address = input.street + ', ' + input.city + ', ' + input.state + ' - ' + input.zip_code;
        }
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4(), name:name, address:address });
        validateCreateClients(dataObject);
        const { Clients } = await connectToDatabase();
        const clients = await Clients.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(clients),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the clients.' }),
        };
    }
};

const getOne = async (event) => {
    try {
        validateGetOneClient(event.pathParameters);
        const { Clients, Cases } = await connectToDatabase();
        const clients = await Clients.findOne({ where: { id: event.pathParameters.id } });
        if (!clients) throw new HTTPError(404, `Clients with id: ${event.pathParameters.id} was not found`);
        const plainClient = clients.get({ plain: true });
        plainClient.totalCases = await Cases.count({ where: { client_id: event.pathParameters.id } });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainClient),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Clients.' }),
        };
    }
};

/*
list optimize all -start
 */
// const getAll = async (event) => {
//   try {
//     const { sequelize } = await connectToDatabase();
//     let searchKey ='';
//     const sortKey = {};
//     let sortQuery = '';
//     let searchQuery = '';
//     const query = event.headers;
//     if( !query.limit && !query.offset ){
//       let casesres = getAllclientsForCases(event);
//       return casesres;
//     }
//     if (query.offset) query.offset = parseInt(query.offset, 10);
//     if (query.limit) query.limit = parseInt(query.limit, 10);
//     if (query.search) searchKey = query.search;
//     /* query.where.practice_id = event.user.practice_id; */
//     let practice_id = event.user.practice_id;
//     if(!practice_id) throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
//     let codeSnip = '';
//     let codeSnip2 = '';
//     let where = `WHERE is_deleted IS NOT true AND practice_id = '${practice_id}'`;

//     /**Sort**/
//     if(query.sort == 'false'){
//       sortQuery += ` ORDER BY createdAt DESC`
//     }else if (query.sort != 'false'){
//       query.sort = JSON.parse(query.sort);
//       sortKey.column = query.sort.column;
//       sortKey.type = query.sort.type;
//       if(sortKey.column != 'all' && sortKey.column != '' && sortKey.column){
//         sortQuery += ` ORDER BY ${sortKey.column}`;
//       }
//       if(sortKey.type != 'all' && sortKey.type != '' && sortKey.type){
//         sortQuery += ` ${sortKey.type}`;
//       }
//     }

//     /**Search**/
//     if( searchKey != 'false'  && searchKey.length>=1 && searchKey !='' ){
//       searchQuery = ` name LIKE '%${searchKey}%' OR email LIKE '%${searchKey}%' OR phone LIKE '%${searchKey}%' OR address LIKE '%${searchKey}%' OR dob LIKE '%${searchKey}%'`;
//     }

//     if( searchQuery != '' && sortQuery !=''){
//       codeSnip = where + ' AND ('+searchQuery+')' + sortQuery +` LIMIT ${query.limit} OFFSET ${query.offset}`;
//       codeSnip2 = where + ' AND ('+searchQuery+')' + sortQuery ;
//     }else if ( searchQuery == '' && sortQuery !=''){
//       codeSnip = where + sortQuery +` LIMIT ${query.limit} OFFSET ${query.offset}`;
//       codeSnip2 = where + sortQuery ;
//     }else if( searchQuery != '' && sortQuery =='' ){
//       codeSnip = where +' AND ('+searchQuery+')'+ ` LIMIT ${query.limit} OFFSET ${query.offset}`;
//       codeSnip2 = where +' AND ('+searchQuery+')';
//     }else if( searchQuery =='' && sortQuery =='' ){
//       codeSnip = where + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
//       codeSnip2 = where ;
//     }


//     let sqlQuery = 'select * from Clients '+ codeSnip ;

//     let sqlQueryCount = 'select * from Clients '+ codeSnip2 ;

//     console.log(sqlQuery);
//     const serverData = await sequelize.query(sqlQuery, {
//       type: QueryTypes.SELECT
//     });

//     const TableDataCount = await sequelize.query(sqlQueryCount, {
//       type: QueryTypes.SELECT
//     });


//     return {
//       statusCode: 200,
//       headers: { 'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true,
//         'Access-Control-Expose-Headers': 'totalPageCount',
//         'totalPageCount':TableDataCount.length
//       },
//       body: JSON.stringify(serverData),
//     };
//   } catch (err) {
//     console.log(err);
//     return {
//       statusCode: err.statusCode || 500,
//       headers: { 'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true },
//       body: JSON.stringify({ error: err.message || 'Could not fetch the clientss.' }),
//     };
//   }
// };

// const getAllclientsForCases = async (event) => {
//   try {
//     const query = event.queryStringParameters || {};
//     if (query.offset) query.offset = parseInt(query.offset, 10);
//     if (query.limit) query.limit = parseInt(query.limit, 10);
//     if (query.where) query.where = JSON.parse(query.where);
//     if (!query.where) {
//       query.where = {};
//     }
//     query.where.practice_id = event.user.practice_id;
//     query.order = [
//       ['createdAt', 'DESC'],
//     ];
//     validateReadClients(query);
//     const { Clients, Op } = await connectToDatabase();
//     query.where.is_deleted = { [Op.not]: true };
//     let clientss = await Clients.findAll(query);
//     return {
//       statusCode: 200,
//       headers: { 'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true },
//       body: JSON.stringify(clientss),
//     };
//   }catch (err){
//     console.log(err);
//     return {
//       statusCode: err.statusCode || 500,
//       headers: { 'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true },
//       body: JSON.stringify({ error: err.message || 'Could not create the sign up link.' }),
//     };
//   }

// };

/*
list all - end
 */
const getAll = async (event) => {
    try {
        const query = event.queryStringParameters || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query.order = [
            ['createdAt', 'DESC'],
        ];
        validateReadClients(query);
        const { Clients, Op } = await connectToDatabase();
        query.where.is_deleted = { [Op.not]: true };
        let clientss = await Clients.findAll(query);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(clientss),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the clientss.' }),
        };
    }
};

// const getAll = async (event) => {
//   try {
//     const { sequelize } = await connectToDatabase();
//       let searchKey ='';
//       const sortKey = {};
//       let sortQuery = '';
//       let searchQuery = '';
//       const query = event.headers;
//       if (query.offset) query.offset = parseInt(query.offset, 10);
//       if (query.limit) query.limit = parseInt(query.limit, 10);
//       if (query.search) searchKey = query.search;
//       /* query.where.practice_id = event.user.practice_id; */
//       let practice_id = event.user.practice_id;
//       if(!practice_id) throw new HTTPError(404, `Practice id: ${practice_id} was not found`);
//       let codeSnip = '';
//       let codeSnip2 = '';
//       let where = `WHERE is_deleted IS NOT true AND practice_id = '${practice_id}'`;

//       /**Sort**/
//       if(query.sort == 'false'){
//           sortQuery += ` ORDER BY createdAt DESC`
//       }else if (query.sort != 'false'){
//         query.sort = JSON.parse(query.sort);
//         sortKey.column = query.sort.column;
//         sortKey.type = query.sort.type;
//         if(sortKey.column != 'all' && sortKey.column != '' && sortKey.column){
//           sortQuery += ` ORDER BY ${sortKey.column}`;   
//         }
//         if(sortKey.type != 'all' && sortKey.type != '' && sortKey.type){
//           sortQuery += ` ${sortKey.type}`; 
//         }
//       }

//       /**Search**/
//       if( searchKey != 'false'  && searchKey.length>=1 && searchKey !='' ){
//         searchQuery = ` name LIKE '%${searchKey}%' OR email LIKE '%${searchKey}%' OR phone LIKE '%${searchKey}%' OR address LIKE '%${searchKey}%' OR dob LIKE '%${searchKey}%'`;
//       }

//       if( searchQuery != '' && sortQuery !=''){
//         codeSnip = where + ' AND ('+searchQuery+')' + sortQuery +` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where + ' AND ('+searchQuery+')' + sortQuery ;
//       }else if ( searchQuery == '' && sortQuery !=''){
//         codeSnip = where + sortQuery +` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where + sortQuery ;
//       }else if( searchQuery == '' && sortQuery !='' ){
//         codeSnip = where + sortQuery +` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where + sortQuery ;
//       }else if( searchQuery != '' && sortQuery !='' ){
//         codeSnip = where +' AND'+ searchQuery + sortQuery +` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where +' AND'+ searchQuery + sortQuery ;
//       }else if( searchQuery != '' && sortQuery =='' ){
//         codeSnip = where +' AND'+ searchQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where +' AND'+ searchQuery ;
//       }else if( searchQuery != '' && sortQuery =='' ){
//         codeSnip = where + ' AND ('+searchQuery+')' +` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where + ' AND ('+searchQuery+')' ;
//       }else if( searchQuery =='' && sortQuery =='' ){
//         codeSnip = where + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where ;
//       }else if( searchQuery == '' && sortQuery =='' ){
//         codeSnip = where +` LIMIT ${query.limit} OFFSET ${query.offset}`;
//         codeSnip2 = where;
//       }


//       let sqlQuery = 'select * from Clients '+ codeSnip ;

//       let sqlQueryCount = 'select * from Clients '+ codeSnip2 ;

//       console.log(sqlQuery);
//     const serverData = await sequelize.query(sqlQuery, {
//       type: QueryTypes.SELECT 
//     });

//     const TableDataCount = await sequelize.query(sqlQueryCount, {
//       type: QueryTypes.SELECT 
//     });


//     return {
//       statusCode: 200,
//       headers: { 'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true,
//         'Access-Control-Expose-Headers': 'totalPageCount',
//         'totalPageCount':TableDataCount.length
//       },
//       body: JSON.stringify(serverData),
//     };
//   } catch (err) {
//     console.log(err);
//     return {
//       statusCode: err.statusCode || 500,
//       headers: { 'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true },
//       body: JSON.stringify({ error: err.message || 'Could not fetch the clientss.' }),
//     };
//   }
// };

const update = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateUpdateClients(input);
        const { Clients } = await connectToDatabase();
        const clients = await Clients.findOne({ where: { id: event.pathParameters.id } });
        if (!clients) throw new HTTPError(404, `Clients with id: ${event.pathParameters.id} was not found`);
        let address;
        let name = '';
        if(input?.first_name){
            name = input?.first_name;
        }
        if (input.middle_name) { 
            name = name + ' ' + input.middle_name; 
        }
        if (input.last_name) { 
            name = name + ' ' + input.last_name; 
        }
        if(input.street){
            address = input.street + ', ' + input.city + ', ' + input.state + ' - ' + input.zip_code;
            }
        input.name = name;
        input.address = address;
        const updatedModel = Object.assign(clients, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Clients.' }),
        };
    }
};

const destroy = async (event) => {
    try {
        validateDeleteClient(event.pathParameters);
        const { Clients, Cases, Forms, LegalForms, Orders, Formotp } = await connectToDatabase();
        const clientDetails = await Clients.findOne({ where: { id: event.pathParameters.id } });
        if (!clientDetails) throw new HTTPError(404, `Clients with id: ${event.pathParameters.id} was not found`);

        const cases = await Cases.findAll({
            where: {
                client_id: clientDetails.id,
            },
            raw: true
        });

        if (cases && cases.length) {
            for (let i = 0; i < cases.length; i += 1) {
                const caseDetails = cases[i];
                const legalFormsObj = await LegalForms.findAll({ where: { case_id: caseDetails.id }, raw: true });
                for (let index = 0; index < legalFormsObj.length; index++) {
                    if (legalFormsObj[index]?.pdf_s3_file_key) await s3.deleteObject({ Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS, Key: legalFormsObj[index].pdf_s3_file_key }).promise();
                    if (legalFormsObj[index]?.final_document_s3_key) await s3.deleteObject({ Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS, Key: legalFormsObj[index].final_document_s3_key }).promise();
                    if (legalFormsObj[index]?.s3_file_key) await s3.deleteObject({ Bucket: process.env.S3_BUCKET_FOR_FORMS, Key: legalFormsObj[index].s3_file_key }).promise();
                    if (legalFormsObj[index]?.generated_document_s3_key) await s3.deleteObject({ Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS, Key: legalFormsObj[index].generated_document_s3_key }).promise();
                    if (legalFormsObj[index]?.pos_document_s3_key) await s3.deleteObject({ Bucket: process.env.S3_BUCKET_FOR_DOCUMENTS, Key: legalFormsObj[index].pos_document_s3_key }).promise();
                }
                await Forms.destroy({ where: { case_id: caseDetails.id } });
                await LegalForms.destroy({ where: { case_id: caseDetails.id } });
                await Cases.destroy({ where: { id: caseDetails.id } });
                await Orders.update({ is_deleted: true }, { where: { case_id: caseDetails.id } });
            }
        }
        await Formotp.destroy({ where: { client_id: clientDetails.id } });
        clientDetails.is_deleted = true;
        await clientDetails.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(clientDetails),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy fetch the Clients.' }),
        };
    }
};

const createSignUpLink = async (event) => {
    try {
        let id;
        const input = JSON.parse(event.body);
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, { id: id || uuid.v4() });
        validateCreateClients(dataObject);
        dataObject.added_by_user = event.user.id;
        const { Clients } = await connectToDatabase();
        let name = '', address;
            if(input?.first_name){
                name = input?.first_name;
            }
            if (input.middle_name) { 
                name = name + ' ' + input.middle_name; 
            }
            if (input.last_name) { 
                name = name + ' ' + input.last_name; 
            }
            dataObject.name = name;
            if(input.street){
            address = input.street + ', ' + input.city + ', ' + input.state + ' - ' + input.zip_code;
            dataObject.address = address;
            }
        const clients = await Clients.create(dataObject);
        // email sending logic here
        sendEmail(clients.email, 'Please sign-up', 'Please sign-up using provided link');
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(clients),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the sign up link.' }),
        };
    }
};

const sendTermsToClient = async (event) => {
    try {
        const { Clients, Practices } = await connectToDatabase();
        const clientDetails = await Clients.findOne({ where: { id: event.pathParameters.id } });
        if (!clientDetails) throw new HTTPError(404, `Clients with id: ${event.pathParameters.id} was not found`);
        let practiceName = '';
        const practiceDetails = await Practices.findOne({ where: { id: clientDetails.practice_id } });
        if (practiceDetails) {
            practiceName = practiceDetails.name;
        }

        await sendEmail(clientDetails.email, 'Important: Please Accept Terms for your lawsuit', `Your attorney has sent you Fee & HIPAA Form. Please review and accept them at your earliest convenience by clicking on the link: ${process.env.CARE_URL}/terms/${event.pathParameters.id}`, practiceName);

        if (clientDetails && clientDetails.phone) {
            await sendSMS(`1${clientDetails.phone}`, 'Important: Please Accept Terms for your lawsuit', `Your attorney has sent you Fee & HIPAA Form. Please review and accept them at your earliest convenience by clicking on the link: ${process.env.CARE_URL}/terms/${event.pathParameters.id}`);
        }

        clientDetails.terms_sent_by = event.user.id;
        await clientDetails.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                message: 'Terms sent to client',
                clientDetails,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the sign up link.' }),
        };
    }
};

const sendHipaaTermsToClient = async (event) => {
    try {
        const { Clients, Cases, Practices } = await connectToDatabase();
        const clientId = event.params.clientId;
        const clientDetails = await Clients.findOne({ where: { id: clientId } });
        const caseId = event.params.caseId;

        let practiceName = '';

        if (clientDetails && clientDetails.practice_id) {
            const practiceDetails = await Practices.findOne({ where: { id: clientDetails.practice_id } });
            if (practiceDetails) {
                practiceName = practiceDetails.name;
            }
        }
        if (!clientDetails) throw new HTTPError(404, `Clients with id: ${clientId} was not found`);
        await sendEmail(clientDetails.email, 'Important: Please Accept HIPAA Form for your lawsuit', `
    ${practiceName} is requesting you sign the HIPPA release. Please click the following link to review and sign. Thank you. <br/>
    ${process.env.CARE_URL}/hipaa-terms/${clientId}/${caseId}`);

        if (clientDetails && clientDetails.phone) {
            await sendSMS(`1${clientDetails.phone}`, 'Important: Please Accept HIPAA Form for your lawsuit', `${practiceName} is requesting you sign the HIPPA release. Please click the following link to review and sign. Thank you. ${process.env.CARE_URL}/hipaa-terms/${clientId}/${caseId}`, practiceName);
        }
        clientDetails.hipaa_acceptance_status = 0;
        if (caseId) {
            const caseDetails = await Cases.findOne({ where: { id: caseId } });
            caseDetails.hipaa_acceptance_status = 0;
            await caseDetails.save();
        }
        clientDetails.terms_sent_by = event.user.id;
        await clientDetails.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                message: 'Terms sent to client',
                clientDetails,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the sign up link.' }),
        };
    }
};

const sendFeeTermsToClient = async (event) => {
    try {
        const { Clients, Cases, Practices } = await connectToDatabase();
        const clientId = event.params.clientId;
        const clientDetails = await Clients.findOne({ where: { id: clientId } });
        const caseId = event.params.caseId;
        let practiceName = '';

        if (clientDetails && clientDetails.practice_id) {
            const practiceDetails = await Practices.findOne({ where: { id: clientDetails.practice_id } });
            if (practiceDetails) {
                practiceName = practiceDetails.name;
            }
        }

        if (!clientDetails) throw new HTTPError(404, `Clients with id: ${event.pathParameters.id} was not found`);
        await sendEmail(clientDetails.email, 'Important: Please Accept Fee Agreement for your lawsuit', `
    ${practiceName} is requesting you sign the fee agreement. Please click the following link to review and sign. Thank you. <br/>
    ${process.env.CARE_URL}/fee-terms/${clientId}/${caseId}`, practiceName);

        if (clientDetails && clientDetails.phone) {
            await sendSMS(`1${clientDetails.phone}`, 'Important: Please Accept Fee Agreement for your lawsuit', `${practiceName} is requesting you sign the fee agreement. Please click the following link to review and sign. Thank you. ${process.env.CARE_URL}/fee-terms/${clientId}/${caseId}`);
        }


        clientDetails.fee_acceptance_status = 0;
        if (caseId) {
            const caseDetails = await Cases.findOne({ where: { id: caseId } });
            caseDetails.fee_acceptance_status = 0;
            await caseDetails.save();
        }

        clientDetails.terms_sent_by = event.user.id;
        await clientDetails.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                message: 'Terms sent to client',
                clientDetails,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the sign up link.' }),
        };
    }
};

const sendSms = async (event) => {
    try {
        await sendSMS('919038647057', 'test sms', 'sms test');
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                message: 'sms sent',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the sign up link.' }),
        };
    }
};

const acceptTerms = async (event) => {
    try {
        const input = JSON.parse(event.body);
        validateTermsAcceptance(input);
        const { Users, Clients, Cases, Op, Practices } = await connectToDatabase();
        const clientDetails = await Clients.findOne({ where: { id: event.pathParameters.id } });
        if (!clientDetails) throw new HTTPError(404, `Clients with id: ${event.pathParameters.id} was not found`);
        let practiceName = '';
        const practiceDetails = await Practices.findOne({ where: { id: clientDetails.practice_id } });
        if (practiceDetails) {
            practiceName = practiceDetails.name;
        }
        if (clientDetails.hipaa_acceptance_status &&
            clientDetails.fee_acceptance_status) {
            throw new HTTPError(400, 'Terms are already accecpted');
        }

        if (input.hipaa_acceptance_status === true) {
            clientDetails.hipaa_acceptance_status = true;
            clientDetails.hipaa_acceptance_date = new Date();
        }
        if (input.fee_acceptance_status === true) {
            clientDetails.fee_acceptance_status = true;
            clientDetails.fee_acceptance_date = new Date();
        }
        if (input.case_id) {
            const caseDetails = await Cases.findOne({ where: { id: input.case_id } });
            if (caseDetails) {
                if (input.hipaa_acceptance_status === true) {
                    caseDetails.hipaa_acceptance_status = true;
                    caseDetails.hipaa_acceptance_date = new Date();
                }
                if (input.fee_acceptance_status === true) {
                    caseDetails.fee_acceptance_status = true;
                    caseDetails.fee_acceptance_date = new Date();
                }
            }
            await caseDetails.save();
        }

        await clientDetails.save();

        if (clientDetails.hipaa_acceptance_status &&
            clientDetails.fee_acceptance_status &&
            clientDetails.terms_sent_by
        ) {
            /* const user = await Users.findOne({where:{id:clientDetails.terms_sent_by); }}*/
            const user = await Users.findOne({ where: { id: clientDetails.terms_sent_by, is_deleted: { [Op.not]: true } } });
            if (user) {
                let template = 'Hi';
                if (user?.name) template = template + ' ' + user.name;
                sendEmail(user.email, `Terms accepted by ${clientDetails.name}`, `
                ${template}, <br/><br/>

      Your client, ${clientDetails.name}, has accepted HIPAA and Fee Agreement. <br/><br/>

      Notification from ${practiceName}`, practiceName);
            } else {
                throw new HTTPError(400, 'User ID not found for this Client ');
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                message: 'Thanks for accpetaning the terms',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the sign up link.' }),
        };
    }
};

const stepByStepSignup = async (event) => {
    try {
        const input = JSON.parse(event.body);
        const { User, Clients } = await connectToDatabase();
        const clients = await Clients.findOne({ where: { id: event.pathParameters.id } });
        if (!clients) throw new HTTPError(404, `Clients with id: ${event.pathParameters.id} was not found`);
        const updatedModel = Object.assign(clients, input);
        await updatedModel.save();
        if (updatedModel.name &&
            updatedModel.email &&
            updatedModel.phone &&
            updatedModel.address &&
            updatedModel.hipaa_acceptance_status &&
            updatedModel.fee_acceptance_status
        ) {
            // notify the lawyer via email here
            const user = await User.findOne({ where: { id: clients.added_by_user } });
            sendEmail(user.email, 'one client had completed Sign-up', `${clients.name} sign-up process completed`);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Clients.' }),
        };
    }
};

const sendOtp = async (event) => {
    try {
        const { Formotp, Op, Clients } = await connectToDatabase();
        const input = JSON.parse(event.body);
        const otp_code = input.otp_code;
        const TargetLanguageCode = input.TargetLanguageCode;
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code query parameter');

        const otpDetails = await Formotp.findOne({
            where: { otp_code },
        });
        if (!otpDetails) throw new HTTPError(404, 'Invalid OTP code');

        otpDetails.otp_secret = generateRandomString(4);
        await otpDetails.save();

        const ClientDetails = await Clients.findOne({
            where: { id: otpDetails.client_id },
        });
        if (ClientDetails && ClientDetails.phone) {
            const phoneCountryCode = process.env.PHONE_COUNTRY_CODE || '1';
            if (TargetLanguageCode == 'en' || TargetLanguageCode == '') {
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'Required OTP', `${otpDetails.otp_secret} is your SECRET One Time Password(OTP). Please do not share it with anyone.`);
            } else if (TargetLanguageCode == 'es') {
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'OTP requerido', `${otpDetails.otp_secret} es su contraseña secreta de un solo uso (OTP). Por favor, no lo compartas con nadie.`);
            } else if (TargetLanguageCode == 'vi') {
                await sendSMS(`${phoneCountryCode}${ClientDetails.phone}`, 'OTP bắt buộc', `${otpDetails.otp_secret} là Mật khẩu dùng một lần BÍ MẬT (OTP) của bạn. Vui lòng không chia sẻ nó với bất kỳ ai.`);
            }
        } else {
            throw new HTTPError(400, 'Client phone number not available');
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'OTP sent to your mobile number',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the forms' }),
        };
    }
};

const validateOtp = async (event) => {
    try {
        const { Formotp, OtherPartiesFormsOtp, Users, Practices, LegalForms } = await connectToDatabase();
        const input = JSON.parse(event.body);
        const otp_code = input.otp_code;
        const otp_secret = input.otp_secret;
        if (!otp_code) throw new HTTPError(400, 'Missing OTP code');
        if (!otp_secret) throw new HTTPError(400, 'Missing OTP secret');
        let practicename = '';
        let attorneyname = '';
        const checkotpCode = await Formotp.findOne({ where: { otp_code: otp_code } });
        const checkOtherPartiesotpCode = await OtherPartiesFormsOtp.findOne({ where: { otp_code: otp_code } });
        if (!checkotpCode && !checkOtherPartiesotpCode) throw new HTTPError(400, 'This url is no longer valid.');
        const otpDetails = await Formotp.findOne({
            where: { otp_code, otp_secret },
        });
        const otherPartiesotpDetails = await OtherPartiesFormsOtp.findOne({
            where: { otp_code, otp_secret },
        });
        if (otpDetails) {
            const legalformsObj = await LegalForms.findAll({ where: { id: otpDetails.legalforms_id }, raw: true });
            const practicessObj = await Practices.findAll({ where: { id: legalformsObj[0].practice_id }, raw: true });
            const usersObj = await Users.findAll({ where: { id: otpDetails.sent_by }, raw: true });
            practicename = practicessObj[0].name;
            attorneyname = usersObj[0].name;
        } else if (otherPartiesotpDetails) {
            const legalformsObj = await LegalForms.findAll({
                where: { id: otherPartiesotpDetails.legalforms_id },
                raw: true
            });
            const practicessObj = await Practices.findAll({ where: { id: legalformsObj[0].practice_id }, raw: true });
            const usersObj = await Users.findAll({ where: { id: otherPartiesotpDetails.sent_by }, raw: true });
            practicename = practicessObj[0].name;
            attorneyname = usersObj[0].name;
        }
        if (!otpDetails && !otherPartiesotpDetails) throw new HTTPError(400, 'Invalid OTP');
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'OTP verified',
                otp_verified: true,
                practice_name: practicename,
                attorney_name: attorneyname,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the forms' }),
        };
    }
};

module.exports.create = middy(create).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = middy(getAll).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.createSignUpLink = middy(createSignUpLink).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.stepByStepSignup = stepByStepSignup;
module.exports.sendTermsToClient = middy(sendTermsToClient).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.acceptTerms = acceptTerms;
module.exports.sendSms = sendSms;
module.exports.sendOtp = sendOtp;
module.exports.validateOtp = validateOtp;
module.exports.sendHipaaTermsToClient = sendHipaaTermsToClient;
module.exports.sendFeeTermsToClient = sendFeeTermsToClient;

