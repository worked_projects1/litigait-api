const {sendEmail} = require('../../utils/mailModule');

const {
    supportEmailValidation,
    demoEmailValidation,
} = require('./validation');

const supportMail = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const dataObject = input;
        supportEmailValidation(dataObject);
        await sendEmail(process.env.SUPPORT_EMAIL_RECEIPIENT, `Support requested by ${dataObject.first_name}`, `
    First Name - ${dataObject.first_name}<br/>
    Last Name - ${dataObject.last_name}<br/>
    Email - ${dataObject.email}<br/>
    Message - ${dataObject.message}<br/>
    `);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Support Request Sent',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not resolve.'}),
        };
    }
};


const demoMail = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const dataObject = input;
        demoEmailValidation(dataObject);
        await sendEmail(process.env.DEMO_EMAIL_RECEIPIENT, `Demo requested by ${dataObject.first_name}`, `
    First Name - ${dataObject.first_name}<br/>
    Last Name - ${dataObject.last_name}<br/>
    Email - ${dataObject.email}<br/>
    Phone Number - ${dataObject.phone}<br/>
    `);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Demo Request Sent',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not resolve.'}),
        };
    }
};


module.exports.supportMail = supportMail;
module.exports.demoMail = demoMail;
