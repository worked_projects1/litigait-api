const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.supportEmailValidation = function (data) {
    const rules = {
        first_name: 'required|min:2|max:64',
        last_name: 'required|min:2|max:64',
        email: 'required|email|min:2|max:64',
        message: 'min:2',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.demoEmailValidation = function (data) {
    const rules = {
        first_name: 'required|min:2|max:64',
        last_name: 'required|min:2|max:64',
        email: 'required|email|min:2|max:64',
        phone: 'required|min:2|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

