const connectToDataBase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');



const getOne = async (event) => {
    try {
        const input = event.pathParameters || event.params;
        const { FormsResponseHistory } = await connectToDataBase();
        const formsResponseHistory = await FormsResponseHistory.findOne({
            where: { id: input.id }
        })
        if (!formsResponseHistory) throw new HTTPError(404, `FormsResponseHistory with id ${input.id} was not found`);
        formsResponseHistory.response = typeof formsResponseHistory.response == 'string' ? JSON.parse(formsResponseHistory.response) : formsResponseHistory.response
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(formsResponseHistory)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || `Unable to fetch records.` })
        }
    }
}

const getAll = async (event) => {
    try {
        const input = event.pathParameters || event.params;
        const { FormsResponseHistory } = await connectToDataBase();

        let query = { where: {} };
        query.where.practice_id = event.user.practice_id;

        if (input.form_id) query.where.form_id = input.form_id;
        if (input.client_id) query.where.client_id = input.client_id;
        if (input.legalforms_id) query.where.legalforms_id = input.legalforms_id;

        const formsResponseHistory = await FormsResponseHistory.findAll(query)
        formsResponseHistory.forEach((historyObj) => {
            historyObj.response = typeof historyObj.response == 'string' ? JSON.parse(historyObj.response) : historyObj.response;
        })

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(formsResponseHistory)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || `Unable to fetch records.` })
        }
    }
}

const destroy = async (event) => {
    try {
        const input = event.pathParameters || event.params;
        const { FormsResponseHistory } = await connectToDataBase();
        const formsResponseHistory = await FormsResponseHistory.findOne({
            where: { id: input.id }
        })
        if (!formsResponseHistory) throw new HTTPError(404, `FormsResponseHistory with id ${input.id} was not found`);
        await formsResponseHistory.destroy();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(formsResponseHistory)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || `Unable to fetch records.` })
        }
    }
}

module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.destroy = destroy;