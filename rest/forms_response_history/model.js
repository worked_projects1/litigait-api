module.exports = (sequelize, type) => sequelize.define('FormsResponseHistory', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    case_id: type.STRING,
    form_id: type.STRING,
    legalforms_id: type.STRING,
    document_type: type.STRING,
    response: type.TEXT('long')
});
