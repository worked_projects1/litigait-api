module.exports = (sequelize, type) => sequelize.define('PropoundForms', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    case_id: type.STRING,
    propound_template_id: type.STRING,
    file_name: type.STRING,
    document_type: type.STRING,
    questions: type.TEXT('long'),
    number_of_questions: type.STRING,
    number_of_questions_sent: type.STRING,
    status: type.STRING,
    is_deleted: type.BOOLEAN,
    s3_file_key: type.TEXT,
    pdf_s3_file_key: type.TEXT,
    email_sent_date: type.DATE,
    propounder_serving_attorney_name: type.STRING,
    propounder_serving_attorney_street: type.STRING,
    propounder_serving_attorney_city: type.STRING,
    propounder_serving_attorney_state: type.STRING,
    propounder_serving_attorney_zip_code: type.STRING,
    propounder_serving_attorney_email: type.STRING,
    propounder_serving_date: type.DATE,
    generated_document_questions: type.TEXT('long'),
    attach_documents: type.TEXT('long')
});