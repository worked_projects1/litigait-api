const uuid = require('uuid');
const connectToDatabase = require('../../db');
const {HTTPError} = require('../../utils/httpResp');

const getStatus = async (event) => {
    try {
        console.log('Request Start '+new Date().getTime());
        console.log(event);
        const input = event.pathParameters || event.queryParams || event.query || event.queryStringParameters;
        console.log(input);
        const {PropoundDocumentUploadProgress, Op} = await connectToDatabase();
        const propound_template_id = input.propound_template_id;
        if (!propound_template_id) throw new HTTPError(404, `Propound template id: ${propound_template_id} was not found`);
        const uploadProgressStatusObject = await PropoundDocumentUploadProgress.findOne({
            where: {
                practice_id: input.practice_id || event.user.practice_id,
                document_type: input.document_type,
                propound_template_id: input.propound_template_id
            },
            logging: console.log,
            raw: true
        });
        console.log('*************GET DATA**********')
        console.log(uploadProgressStatusObject);
        console.log('Request End '+new Date().getTime());

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({status: 'ok', data: uploadProgressStatusObject, input}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Can not get status'}),
        };
    }
};

const setStatus = async (event) => {
    try {
        console.log('Request Start '+new Date().getTime());
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {PropoundDocumentUploadProgress, PropoundTemplates} = await connectToDatabase();
        console.log(input);
        const propound_template_id = input.propound_template_id;
        const status = input.status;
        if (!propound_template_id) throw new HTTPError(404, `PropoundTemplates id: ${propound_template_id} was not found`);
        let uploadProgressStatusObject = await PropoundDocumentUploadProgress.findOne({
            where: {
                document_type: input.document_type,
                propound_template_id: input.propound_template_id,
                practice_id: input.practice_id,
            },
            logging: console.log
        });
        if (status == 'cancelled' || status == 'Cancelled') {
            console.log('inside cancel');
            const PropoundTemplatesObj = await PropoundTemplates.findOne({where: {id: propound_template_id}});
            if (!PropoundTemplatesObj) throw new HTTPError(404, `PropoundTemplates with id: ${propound_template_id} was not found`);
            await PropoundTemplatesObj.destroy();
        }
        if (uploadProgressStatusObject) {
            console.log('inside update');
            const updatedModel = Object.assign(uploadProgressStatusObject, input);
            console.log(updatedModel);
            await updatedModel.save();
        } else {
            if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
            console.log('inside create');
            const dataObject = Object.assign(input, {id: id || uuid.v4()});
            console.log(dataObject)
            let progressResponse = await PropoundDocumentUploadProgress.create(dataObject);
            const plainResponse = progressResponse.get({plain: true});
            uploadProgressStatusObject = {};
            uploadProgressStatusObject = Object.assign({},plainResponse);
        }
        console.log('out side if');
        console.log(uploadProgressStatusObject);
        console.log('Request End '+new Date().getTime());
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({status: 'ok', data: uploadProgressStatusObject}),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'status not stored'}),
        };
    }
};


module.exports.setStatus = setStatus;
module.exports.getStatus = getStatus;