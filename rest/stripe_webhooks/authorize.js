const {HTTPError} = require('../../utils/httpResp');

const superAdminRole = ['superAdmin'];

exports.authorizeGetBillingDetails = function (user) {
    if (!superAdminRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};

exports.authorizeSubscriptionCancel = function (user) {
    if (!superAdminRole.includes(user.role)) {
        throw new HTTPError(403, 'forbidden');
    }
};