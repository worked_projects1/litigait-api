
const uuid = require('uuid');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY, {apiVersion: ''});
const connectToDatabase = require('../../db');
const fs = require('fs');
const {sendEmail} = require('../../utils/mailModule');
const { unixTimeStamptoDateTime, unixTimeStamptoDate }  = require('../../utils/timeStamp');
const {QueryTypes} = require('sequelize');
const {HTTPError} = require('../../utils/httpResp');
/* Charges */
const stripeChargeWebhook = async (event) => {
    try {
        console.log("Inside Charges Webhook");
        const endpointSecret = process.env.STRIPE_CHARGES_WEBHOOK_SECRET;
        const signature = event.headers['Stripe-Signature'];
        const stripeEvent = stripe.webhooks.constructEvent(event.body, signature, endpointSecret);
        switch (stripeEvent.type) {
            case 'charge.succeeded':
                await stripeChargeEvent(stripeEvent);
                break;
            case 'charge.refunded':
                await stripeChargeEvent(stripeEvent);
                break;
            case 'charge.failed':
                await stripeChargeEvent(stripeEvent);
                break;
            default:
                console.log(`Unhandled event type ${stripeEvent.type}`);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };


    } catch (err) {
        console.log(`Webhook signature verification failed.`, err.message);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}
const stripeChargeEvent = async (event) => {
    try {
        let body = '';
        
          let action = undefined;
        if (process.env.CODE_ENV == 'local' && event.body &&  event.body.data) {
            body = typeof event.body.data === 'string' ? JSON.parse(event.body.data) : event.body.data;
            action = event.body.type;
        } else {
               body = typeof event.data === 'string' ? JSON.parse(event.data) : event.data;
               action = event.type;
        }

        const { Op, Users, Plans, Practices, StripeBillingHistory, } = await connectToDatabase();

        /* Retrive Subscripe plan from invoice.*/
        let invoiceObj =  undefined;
        let invoiceData = undefined;

        body.object.invoice ? invoiceObj = await stripe.invoices.retrieve(body.object.invoice) : invoiceObj = undefined;
        invoiceObj ? invoiceData = invoiceObj.lines.data[0] : invoiceData = undefined;

        /* Retrive Subscription Details from Subscription */
        let subscriptionObj = undefined;
        if( invoiceData && invoiceData.subscription){
            subscriptionObj = await stripe.subscriptions.retrieve(invoiceData.subscription);
        }

        const practicesobj = await Practices.findOne({ where: { stripe_customer_id: body.object.customer, is_deleted: {[Op.not]: true}}, raw: true,});
        const usersobj = await Users.findOne({
            where: { practice_id: practicesobj.id, is_deleted: {[Op.not]: true}, role: 'lawyer', is_admin: true },
            order: [['createdAt', 'DESC']],
            raw: true
        });
        


        const billingHistoryObj = {};

        billingHistoryObj.id = uuid.v4();
        billingHistoryObj.webhook_action = action;
        billingHistoryObj.payment_date = new Date();
        billingHistoryObj.practice_id = practicesobj.id;
        billingHistoryObj.user_id = usersobj.id;

        if(invoiceData && invoiceData.plan && invoiceData.plan.product){
            const plansObj = await Plans.findOne({ where: { stripe_product_id: invoiceData.plan.product, active: true, }, raw: true });
            billingHistoryObj.plan_type = plansObj.plan_type;
            billingHistoryObj.plan_price = plansObj.price;
        }


        body.object.amount_captured ? billingHistoryObj.amount_captured = (body.object.amount_captured)/100 : billingHistoryObj.amount_captured = 0 ;
        if(body && body.object){
            const refundsObj = body.object.refunds.data[0];
            if(body.object.amount_captured && !body.object.amount_refunded && body.object.status == 'succeeded'){
                billingHistoryObj.payment_status = 'Success';
                billingHistoryObj.activity_type = body.object.description;
            }else if (!body.object.amount_captured && !body.object.amount_refunded && body.object.status == 'failed'){
                billingHistoryObj.payment_status = body.object.failure_message; // Card Decline , Insufficient funds
                billingHistoryObj.activity_type = body.object.description;
            }else if (body.object.refunded && refundsObj.status == 'succeeded' && body.object.amount_refunded ){ //Refund
                billingHistoryObj.amount_captured = 0;
                billingHistoryObj.payment_status = 'Success';
                billingHistoryObj.refund_amount = (body.object.amount_refunded)/100;
                let refund_reason = refundsObj.reason.split('_');
                refund_reason = refund_reason.map((el)=>{
                    return el.charAt(0).toUpperCase() + el.slice(1).toLowerCase();
                });
                refund_reason = refund_reason.join(' ');
                billingHistoryObj.refund_reason = refund_reason;
                billingHistoryObj.activity_type = 'Refund';
            }
            invoiceData && invoiceData.subscription ? billingHistoryObj.stripe_subscription_id = invoiceData.subscription : billingHistoryObj.stripe_subscription_id = undefined;
            if(invoiceData && invoiceData.period && invoiceData.period.start) {
                billingHistoryObj.subscription_start = unixTimeStamptoDateTime(invoiceData.period.start);
            }
            if(invoiceData && invoiceData.period && invoiceData.period.end) {
                billingHistoryObj.subscription_end = unixTimeStamptoDateTime(invoiceData.period.end);
            }
            
        }
        subscriptionObj ? billingHistoryObj.subscription_stripe_obj = JSON.stringify(subscriptionObj) : billingHistoryObj.subscription_stripe_obj = undefined;
        billingHistoryObj.charges_stripe_obj = JSON.stringify(body);
        invoiceObj ? billingHistoryObj.invoice_stripe_obj = JSON.stringify(invoiceObj) : billingHistoryObj.invoice_stripe_obj = undefined;
        const saveBillingHistory = await StripeBillingHistory.create(billingHistoryObj);
        const plainText = saveBillingHistory.get({plain: true});

          return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(plainText),
        };
    }catch(err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}

/* Invoice */
const invoiceItemWebHook = async (event) =>{
    try {
        console.log("Inside invoice items Webhook");
        const endpointSecret = process.env.STRIPE_INVOICEITEM_WEBHOOK_SECRET;
        const signature = event.headers['Stripe-Signature'];
        const stripeEvent = stripe.webhooks.constructEvent(event.body, signature, endpointSecret);
        switch (stripeEvent.type) {
            case 'invoiceitem.created':
                await invoiceItemEvent(stripeEvent);
                break;
            case 'invoiceitem.deleted':
                await invoiceItemEvent(stripeEvent);
                break;
            case 'invoiceitem.updated':
                await invoiceItemEvent(stripeEvent);
                break;        
            default:
                console.log(`Unhandled event type ${stripeEvent.type}`);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };


    } catch (err) {
        console.log(`Webhook signature verification failed.`, err.message);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    } 
}
const invoiceItemEvent = async (event) => {
    try {
        let body = '';
        
          let action = undefined;
        if (process.env.CODE_ENV == 'local' && event.body &&  event.body.data) {
            body = typeof event.body.data === 'string' ? JSON.parse(event.body.data) : event.body.data;
            action = event.body.type;
        } else {
               body = typeof event.data === 'string' ? JSON.parse(event.data) : event.data;
               action = event.type;
        }

        const { Op, Users, Plans, Practices, StripeBillingHistory, } = await connectToDatabase();

        /* Retrive Subscripe plan from invoice.*/
        const invoiceObj = await stripe.invoices.retrieve(body.object.invoice);
        const invoiceData = invoiceObj.lines.data[0];
        /* Retrive Subscription Details from Subscription */
        let subscriptionObj = undefined;
        if( invoiceData && invoiceData.subscription){
            subscriptionObj = await stripe.subscriptions.retrieve(invoiceData.subscription);
        }

        const practicesobj = await Practices.findOne({
            where: { stripe_customer_id: body.object.customer,
                is_deleted: {[Op.not]: true}
            }, raw: true,
        });
        const usersobj = await Users.findOne({
            where: {
                practice_id: practicesobj.id,
                is_deleted: {[Op.not]: true},
                role: 'lawyer',
                is_admin: true
            },
            order: [
                ['createdAt', 'DESC'],
            ],
            raw: true
        });
        
        const billingHistoryObj = {};

        billingHistoryObj.id = uuid.v4();
        billingHistoryObj.webhook_action = action;
        billingHistoryObj.payment_date = new Date();
        billingHistoryObj.practice_id = practicesobj.id;
        billingHistoryObj.user_id = usersobj.id;

        if(invoiceData && invoiceData.plan && invoiceData.plan.product){
            const plansObj = await Plans.findOne({
                where: {
                    stripe_product_id: invoiceData.plan.product,
                    active:true,
                },
                raw: true
            });
            billingHistoryObj.plan_type = plansObj.plan_type;
            billingHistoryObj.plan_price = plansObj.price;
        }


        body.object.amount_captured ? billingHistoryObj.amount_captured = (body.object.amount_captured)/100 : billingHistoryObj.amount_captured = 0 ;

            if(body.object.proration && body.object.unit_amount < 0){ //Credit Balance Added.
                billingHistoryObj.payment_status = 'Amount added into credit balance.';                
                billingHistoryObj.activity_type = 'Invoice item';
            }else if(!body.object.proration && body.object.unit_amount){
                    billingHistoryObj.payment_status = 'New product added into invoice';
                    billingHistoryObj.activity_type = 'Invoice item';
            }
            if(action == 'invoiceitem.updated') {
                billingHistoryObj.payment_status = undefined;
                billingHistoryObj.activity_type = 'Invoice item updated';
            }else if(action == 'invoiceitem.deleted'){
                billingHistoryObj.payment_status = undefined;
                billingHistoryObj.activity_type = 'Invoice item deleted';
            }
            billingHistoryObj.invoiceitem_reason = body.object.description;
            billingHistoryObj.invoiceitem_amount = (body.object.unit_amount)/100;
            invoiceData && invoiceData.subscription ? billingHistoryObj.stripe_subscription_id = invoiceData.subscription : billingHistoryObj.stripe_subscription_id = undefined;
        subscriptionObj ? billingHistoryObj.subscription_stripe_obj = JSON.stringify(subscriptionObj) : billingHistoryObj.subscription_stripe_obj = undefined;
        billingHistoryObj.charges_stripe_obj = JSON.stringify(body);
        billingHistoryObj.invoice_stripe_obj = JSON.stringify(invoiceObj);
        const saveBillingHistory = await StripeBillingHistory.create(billingHistoryObj);
        const plainText = saveBillingHistory.get({plain: true});

          return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(plainText),
        };
    }catch(err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}

/* Subscription */
const subscriptionWebHook = async (event) => {
    try {
        console.log("Inside Subscription Webhook");
        const endpointSecret = process.env.STRIPE_SUBSCRIPTION_WEBHOOK_SECRET;
        const signature = event.headers['Stripe-Signature'];
        const stripeEvent = stripe.webhooks.constructEvent(event.body, signature, endpointSecret);
        switch (stripeEvent.type) {
            case 'customer.subscription.created':
                await updateSubscriptionWebHookStatus(stripeEvent);
                break;
            case 'customer.subscription.deleted':
                await updateSubscriptionWebHookStatus(stripeEvent);
                break;
            case 'customer.subscription.updated':
                await updateSubscriptionWebHookStatus(stripeEvent);
                break;
            default:
                console.log(`Unhandled event type ${stripeEvent.type}`);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };


    } catch (err) {
        console.log(`Webhook signature verification failed.`, err.message);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}
const updateSubscriptionWebHookStatus = async (event) => {
    try {
        const { Op, Users, Plans, Practices, SubscriptionHistoryWebhook, SubscriptionHistory, Subscriptions, DiscountCode, DiscountHistory } = await connectToDatabase();
        let body = '';
        if (process.env.CODE_ENV == 'local' && event.body &&  event.body.data) {
            body = typeof event.body.data === 'string' ? JSON.parse(event.body.data) : event.body.data;
        } else {
            body = typeof event.data === 'string' ? JSON.parse(event.data) : event.data;
        }
        const monthly_plans = ['monthly', 'responding_monthly_495', 'responding_monthly_349', 'propounding_monthly_199'];
        const yearly_plans = ['yearly', 'responding_yearly_5100', 'responding_yearly_3490', 'propounding_yearly_2199'];
        const customer_id = body.object.customer;
        const practicesobj = await Practices.findOne({ where: { stripe_customer_id: customer_id, is_deleted: { [Op.not]: true } }, raw: true });
        if (!practicesobj) throw new HTTPError(404, `Invalid stripe customer id ${customer_id}`);
        /* Find Initially created user */
        const usersobj = await Users.findOne({
          where: { practice_id: practicesobj.id, is_deleted: { [Op.not]: true }, role: 'lawyer', is_admin: true },
          order: [['createdAt', 'ASC']],
          raw: true,
        });
        /* Find Plans details */
        const planObject = body.object.metadata.plan_id
            ? await Plans.findOne({ where: { plan_id: body.object.metadata.plan_id }, raw: true })
            : await Plans.findOne({ where: { stripe_product_id: body.object.plan.product }, raw: true });
        /* Find previous subscription history */
        const currentSubscriptionObj = await SubscriptionHistory.findOne({
          where: { practice_id: practicesobj.id, plan_category: planObject.plan_category },
          order: [['createdAt', 'DESC']],
          raw: true,
        });
    
        let subscriptionValidityStart = new Date(body.object.current_period_start * 1000);
        let subscriptionValidity = new Date(body.object.current_period_end * 1000);
        let latest_actvity, switchedPlan, esquiretek_activity_type, payment_type, discountCodeObj, discount_percentage, discount_price, discount_code, discounted_price, discount_amount, base_price;
        let status = 'Success';
        
        /* Find Payment type */
        if (event.type === 'customer.subscription.created') {
            payment_type = 'New';
        } else if (event.type === 'customer.subscription.updated') {
            if (
                currentSubscriptionObj?.plan_type &&
                ((monthly_plans.includes(currentSubscriptionObj.plan_type) && monthly_plans.includes(planObject.plan_type)) ||
                    (yearly_plans.includes(currentSubscriptionObj.plan_type) && yearly_plans.includes(planObject.plan_type)))
            ) {
                payment_type = 'Renewal';
            } else {
                payment_type = 'New';
            }
        } else if (event.type === 'customer.subscription.deleted') {
            payment_type = 'Subscription Canceled';
            subscriptionValidity = new Date();
        }
        /* Find latest_actvity,payment_type,switchedPlan,esquiretek_activity_type for canceled subscription*/
        if (body.object.cancel_at_period_end && body.object.canceled_at && body.object.cancel_at && ['active', 'trialing'].includes(body.object.status)) {
            latest_actvity = 'downgrade';
            payment_type = 'Subscription Canceled';
            switchedPlan = 'subscription_cancel';
            esquiretek_activity_type = 'DOWNGRADE';
          } else {
            if (body.object.metadata.plan_id && body.object.metadata.latest_actvity) {
              latest_actvity = body.object.metadata.latest_actvity;
            }
            switchedPlan = planObject.plan_type;
            const historyCount = await SubscriptionHistory.count({ where: { practice_id: practicesobj.id } });
            esquiretek_activity_type = historyCount <= 0 ? 'NEW_SUBSCRIPTION' : 'UPGRADED';
          }
          
          esquiretek_activity_type = !['active', 'trialing'].includes(body.object.status) ? undefined : esquiretek_activity_type;
        
        /* Find payment_type,esquiretek_activity_type for subscription create*/  
        if (event.type === 'customer.subscription.created' && !['active', 'trialing'].includes(body.object.status)) {
            latest_actvity = 'Subscription Creation';
            payment_type = 'Failed';
            esquiretek_activity_type = 'UPGRADE';
            status = "Payment Failed";
        }
        
        /* Find payment_type,esquiretek_activity_type for subscription update*/
        if (event.type === 'customer.subscription.updated' && !['active', 'trialing'].includes(body.object.status)) {
            payment_type = 'Failed';
            esquiretek_activity_type = 'UPGRADE';
            status = "Subscription Canceled";
        }
          
          const cancel_at = body.object.cancel_at ? new Date(body.object.cancel_at * 1000) : null;
          const canceled_at = body.object.canceled_at ? new Date(body.object.canceled_at * 1000) : null;
          
          let price = planObject.price;
          
          /* Adding price details */
          if ((event.type === 'customer.subscription.created' || event.type === 'customer.subscription.updated') && body.object.discount?.coupon?.id && ['active', 'trialing'].includes(body.object.status)) {
            discount_code = body.object.discount.coupon.name;
            discountCodeObj = await DiscountCode.findOne({ where: { discount_code } });
            discount_percentage = discountCodeObj?.discount_percentage;
            const percentage = 1 - (discount_percentage / 100);
            discounted_price = base_price = price = (percentage * price).toFixed(2);
            discount_amount = planObject.price - parseFloat(discounted_price);
            discount_amount = discount_amount.toFixed(2)
/*             const invoiceObj = await stripe.invoices.retrieve(body?.object?.latest_invoice);
             discounted_price = invoiceObj?.total_discount_amounts[0]?.amount;
             discounted_price = discounted_price / 100;
             discount_amount = invoiceObj?.total / 100; */

          } else if (event.type === 'customer.subscription.deleted') {
            price = currentSubscriptionObj.price;
          }
          /* Create subscription history */
          const subscriptionHistoryData = {
            id: uuid.v4(),
            order_date: new Date(),
            practice_id: practicesobj.id,
            user_id: usersobj.id,
            subscribed_by: usersobj.name,
            price: price,
            plan_type: planObject.plan_type,
            subscribed_on: new Date(),
            plan_id: planObject.plan_id,
            stripe_subscription_id: body.object.id,
            subscription_valid_start: subscriptionValidityStart,
            subscription_valid_till: subscriptionValidity,
            stripe_subscription_data: JSON.stringify(body.object),
            cancel_at: cancel_at,
            cancel_at_period_end: body.object.cancel,
            canceled_at: canceled_at,
            esquiretek_activity_type: esquiretek_activity_type,
            payment_type: payment_type,
            switched_plan: switchedPlan,
            stripe_latest_activity: latest_actvity,
            stripe_product_id: planObject.stripe_product_id,
            status: status,
            event_type: event.type,
            plan_category: planObject.plan_category,
        };
          
          await SubscriptionHistory.create(subscriptionHistoryData);
          await SubscriptionHistoryWebhook.create(subscriptionHistoryData);

        /* Insert Discount History details. */
        if (body.object.discount?.coupon?.id && ['active', 'trialing'].includes(body.object.status) && !body.object.cancel_at_period_end) {
            const plan_type = planObject.plan_type.split('_').slice(0, planObject.plan_type.length - 1);
            const discount_for = plan_type.slice(0, plan_type.length - 1).join('_').toLowerCase();
            const subscriptionObj = await Subscriptions.findOne({
                where: { practice_id: practicesobj.id, plan_category: planObject.plan_category },
                order: [['createdAt', 'DESC']],
            });
            const discount_history = {
                id: uuid.v4(),
                practice_id: practicesobj.id,
                plan_type: planObject.plan_type,
                plan_category: planObject.plan_category,
                discount_for,
                promocode: discountCodeObj.name,
                discount_percentage,
                base_price: planObject.price,
                discounted_price,
                discount_amount
            };
            console.log(discount_history);
            await DiscountHistory.create(discount_history);
        }
        /* Subscribe Propounding Plan in stripe */
        const checkPropoundingSubscription = await Subscriptions.findOne({
            where: { practice_id: practicesobj.id, plan_category: 'propounding' },
            raw: true,
        });

        if (checkPropoundingSubscription && checkPropoundingSubscription.stripe_subscription_id.startsWith('pi_') && !practicesobj.is_propounding_canceled) {
            let propoundingPlan;
            if (planObject.plan_type && ['monthly', 'responding_monthly_349', 'responding_monthly_495'].includes(planObject.plan_type)) propoundingPlan = 'propounding_monthly_199';
            if (planObject.plan_type && ['yearly', 'responding_yearly_5100', 'responding_yearly_3490'].includes(planObject.plan_type)) propoundingPlan = 'propounding_yearly_2199';
            const propoundingPlanObj = await Plans.findOne({ where: { plan_type: propoundingPlan, active: true } });

            const subscriptionCreateObj = {
                customer: practicesobj.stripe_customer_id,
                items: [
                    { price: propoundingPlanObj.plan_id },
                ],
                metadata: {
                    practice_id: practicesobj.id,
                    user_email: usersobj.email,
                    plan_id: propoundingPlanObj.plan_id,
                    stripe_product_id: propoundingPlanObj.stripe_product_id,
                },
            };

            if (body.object.discount?.coupon?.name) {
                const propound_discountObj = await DiscountCode.findOne({ where: { discount_code: discount_code }, raw: true });
                if (propound_discountObj?.id) {
                    const is_eligible_for = propound_discountObj.plan_type.split(',');
                    if (is_eligible_for.includes(propoundingPlanObj.plan_type)) {
                        const stripe_couponcode_obj = JSON.parse(propound_discountObj.stripe_obj);
                        subscriptionCreateObj.coupon = stripe_couponcode_obj.id;
                    }
                }
            }

            const propoundingSubscription = await stripe.subscriptions.create(subscriptionCreateObj);

            if (propoundingSubscription?.status === 'incomplete') {
                await stripe.subscriptions.del(propoundingSubscription.id);
            }

            const subscriptionValidity = new Date(parseInt(propoundingSubscription.current_period_end) * 1000);
            /* Update propounding subscription in db */
            const updateColumn = {
                subscribed_on: new Date(),
                plan_id: propoundingPlanObj.plan_id,
                stripe_subscription_id: propoundingSubscription.id,
                subscribed_valid_till: subscriptionValidity,
                stripe_subscription_data: JSON.stringify(propoundingSubscription),
                stripe_product_id: propoundingPlanObj.stripe_product_id,
                plan_category: propoundingPlanObj.plan_category,
            };
                await Subscriptions.update(updateColumn, { where: {id: checkPropoundingSubscription.id}});
        }
        /*  */
        if (checkPropoundingSubscription && checkPropoundingSubscription.stripe_subscription_id.startsWith('pi_') && practicesobj.is_propounding_canceled) {
            await Practices.update({ is_propounding_canceled: null }, { where: { id: practicesobj.id } });
            await Subscriptions.destroy({ where: { practice_id: practicesobj.id, plan_category: 'propounding' } });
        }
        return;
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}
const new_version = async (event) => {
    try {
        const { Op, Users, Plans, Practices, SubscriptionHistoryWebhook, SubscriptionHistory, Subscriptions, DiscountCode, DiscountHistory, } = await connectToDatabase();
        let body = '';
        if (process.env.CODE_ENV === 'local' && event.body && event.body.data) {
            body = JSON.parse(event.body.data);
        } else {
            body = JSON.parse(event.data);
        }

        const propounding_plans = ['propounding_monthly_199', 'propounding_yearly_2199'];
        const monthly_plans = ['monthly', 'responding_monthly_495', 'responding_monthly_349', 'propounding_monthly_199'];
        const yearly_plans = ['yearly', 'responding_yearly_5100', 'responding_yearly_3490', 'propounding_yearly_2199'];

        const customer_id = body.object.customer;
        const practicesobj = await Practices.findOne({
            where: { stripe_customer_id: customer_id, is_deleted: { [Op.not]: true } },
            raw: true,
        });
        if (!practicesobj) throw new HTTPError(404, `Invalid stripe customer id ${customer_id}`);
        const usersobj = await Users.findOne({
            where: { practice_id: practicesobj.id, is_deleted: { [Op.not]: true }, role: 'lawyer', is_admin: true },
            order: [['createdAt', 'ASC']],
            raw: true,
        });

        const planObject = body.object.metadata.plan_id
            ? await Plans.findOne({ where: { plan_id: body.object.metadata.plan_id }, raw: true })
            : await Plans.findOne({ where: { stripe_product_id: body.object.plan.product }, raw: true });

        const currentSubscriptionObj = await SubscriptionHistory.findOne({
            where: { practice_id: practicesobj.id, plan_category: planObject.plan_category },
            order: [['createdAt', 'DESC']],
            raw: true,
        });

        let subscriptionValidityStart = new Date(parseInt(body.object.current_period_start) * 1000);
        let subscriptionValidity = new Date(parseInt(body.object.current_period_end) * 1000);
        let latest_actvity,
            switchedPlan,
            esquiretek_activity_type,
            payment_type = '';
        if (event.type === 'customer.subscription.created') {
            payment_type = 'New';
        } else if (event.type === 'customer.subscription.updated') {
            if (
                currentSubscriptionObj?.plan_type &&
                ((monthly_plans.includes(currentSubscriptionObj.plan_type) && monthly_plans.includes(planObject.plan_type)) ||
                    (yearly_plans.includes(currentSubscriptionObj.plan_type) && yearly_plans.includes(planObject.plan_type)))
            ) {
                payment_type = 'Renewal';
            } else {
                payment_type = 'New';
            }
        } else if (event.type === 'customer.subscription.deleted') {
            payment_type = 'Subscription Canceled';
            subscriptionValidity = new Date();
        }

        if (body.object.cancel_at_period_end && body.object.canceled_at && body.object.cancel_at && ['active', 'trialing'].includes(body.object.status)) {
            latest_actvity = 'downgrade';
            payment_type = 'Subscription Canceled';
            switchedPlan = 'subscription_cancel';
            esquiretek_activity_type = 'DOWNGRADE';
        } else {
            if (body.object.metadata.plan_id && body.object.metadata.latest_actvity) {
                latest_actvity = body.object.metadata.latest_actvity;
            }
            switchedPlan = planObject.plan_type;
            const historyCount = await SubscriptionHistory.count({ where: { practice_id: practicesobj.id } });
            esquiretek_activity_type = historyCount <= 0 ? 'NEW_SUBSCRIPTION' : 'UPGRADED';
        }

        esquiretek_activity_type = !['active', 'trialing'].includes(body.object.status) ? undefined : esquiretek_activity_type;

        if (event.type === 'customer.subscription.created' && !['active', 'trialing'].includes(body.object.status)) {
            latest_actvity = 'Subscription Creation';
            payment_type = 'Failed';
            esquiretek_activity_type = 'UPGRADE';
        }

        if (event.type === 'customer.subscription.updated' && !['active', 'trialing'].includes(body.object.status)) {
            payment_type = 'Failed';
            esquiretek_activity_type = 'UPGRADE';
        }

        let cancel_at, canceled_at;
        if (body.object.cancel_at != null) cancel_at = new Date(parseInt(body.object.cancel_at) * 1000);
        if (body.object.canceled_at != null) canceled_at = new Date(parseInt(body.object.canceled_at) * 1000);

        let price = planObject.price;
        let discountCodeObj, discount_percentage, discount_price, discount_code;
        if (
            (event.type === 'customer.subscription.created' || event.type === 'customer.subscription.updated') &&
            body?.object?.discount?.coupon?.id &&
            body?.object?.status === 'active'
        ) {
            discount_code = body.object.discount.coupon.name;
            discountCodeObj = await DiscountCode.findOne({ where: { discount_code: discount_code } });
            discount_percentage = discountCodeObj.discount_percentage;
            const percentage = 1 - discount_percentage / 100;
            discount_price = price = (percentage * price).toFixed(2);
        } else if (event.type === 'customer.subscription.deleted') {
            price = currentSubscriptionObj.price;
        }

        const subscriptionHistoryData = {
            id: uuid.v4(),
            order_date: new Date(),
            practice_id: practicesobj.id,
            user_id: usersobj.id,
            subscribed_by: usersobj.name,
            price: price,
            plan_type: planObject.plan_type,
            subscribed_on: new Date(),
            plan_id: planObject.plan_id,
            stripe_subscription_id: body.object.id,
            subscription_valid_start: subscriptionValidityStart,
            subscription_valid_till: subscriptionValidity,
            stripe_subscription_data: JSON.stringify(body.object),
            cancel_at: cancel_at,
            cancel_at_period_end: body.object.cancel,
            canceled_at: canceled_at,
            esquiretek_activity_type: esquiretek_activity_type,
            payment_type: payment_type,
            switched_plan: switchedPlan,
            stripe_latest_activity: latest_actvity,
            stripe_product_id: planObject.stripe_product_id,
            status: body.object.status === 'active' || body.object.status === 'trialing' ? 'Success' : 'Payment Failed',
            event_type: event.type,
            plan_category: planObject.plan_category,
        };
        await SubscriptionHistory.create(subscriptionHistoryData);
        await SubscriptionHistoryWebhook.create(subscriptionHistoryData);

        if (body?.object?.discount?.coupon?.id && body?.object?.status === 'active' && !body?.object?.cancel_at_period_end) {
            let plan_type = planObject.plan_type.split('_').slice(0, planObject.plan_type.length - 1);
            let discount_for = plan_type.slice(0, plan_type.length - 1).join('_').toLowerCase();
            let subscriptionObj = await Subscriptions.findOne({
                where: { practice_id: practicesobj.id, plan_category: planObject.plan_category },
                order: [['createdAt', 'DESC']],
            });
            let discount_history = {
                id: uuid.v4(),
                practice_id: practicesobj.id,
                subscription_id: subscriptionObj.id,
                plan_type: planObject.plan_type,
                plan_category: planObject.plan_category,
                discount_for,
                promocode: discountCodeObj.name,
                discount_percentage,
                actual_price: planObject.price,
                discount_price,
            };
            console.log(discount_history);
            await DiscountHistory.create(discount_history);
        }

        /* Subscribe Propounding Plans */
        const checkPropoundingSubscription = await Subscriptions.findOne({
            where: { practice_id: practicesobj.id, plan_category: 'propounding' },
            raw: true,
        });
        if (checkPropoundingSubscription && checkPropoundingSubscription.stripe_subscription_id.startsWith('pi_') && !practicesobj.is_propounding_canceled) {
            let propoundingPlan;
            if (planObject?.plan_type && ['monthly', 'responding_monthly_349', 'responding_monthly_495'].includes(planObject.plan_type)) propoundingPlan = 'propounding_monthly_199';
            if (planObject?.plan_type && ['yearly', 'responding_yearly_5100', 'responding_yearly_3490'].includes(planObject.plan_type)) propoundingPlan = 'propounding_yearly_2199';
            const propoundingPlanObj = await Plans.findOne({ where: { plan_type: propoundingPlan, active: true } });

            let subscriptionCreateObj = {
                customer: practicesobj.stripe_customer_id,
                items: [{ price: propoundingPlanObj.plan_id }],
                metadata: {
                    practice_id: practicesobj.id,
                    user_email: usersobj.email,
                    plan_id: propoundingPlanObj.plan_id,
                    stripe_product_id: propoundingPlanObj.stripe_product_id,
                },
            };
            if (body?.object?.discount?.coupon?.name) {
                const propound_discountObj = await DiscountCode.findOne({ where: { discount_code: discount_code }, raw: true });
                if (propound_discountObj?.id) {
                    const is_eligible_for = propound_discountObj.plan_type.split(',');
                    if (is_eligible_for.includes(propoundingPlanObj.plan_type)) {
                        let stripe_couponcode_obj = JSON.parse(propound_discountObj.stripe_obj);
                        subscriptionCreateObj.coupon = stripe_couponcode_obj.id;
                    }
                }
            }
            const propoundingSubscription = await stripe.subscriptions.create(subscriptionCreateObj);
            if (propoundingSubscription?.status === 'incomplete') {
                await stripe.subscriptions.del(propoundingSubscription.id);
            }
            const subscriptionValidity = new Date(parseInt(propoundingSubscription.current_period_end) * 1000);
            const updateColumn = {
                subscribed_on: new Date(),
                plan_id: propoundingPlanObj.plan_id,
                stripe_subscription_id: propoundingSubscription.id,
                subscribed_valid_till: subscriptionValidity,
                stripe_subscription_data: JSON.stringify(propoundingSubscription),
                stripe_product_id: propoundingPlanObj.stripe_product_id,
                plan_category: propoundingPlanObj.plan_category,
            };
            await Subscriptions.update(updateColumn, { where: { id: checkPropoundingSubscription.id } });
        }
        if (checkPropoundingSubscription && checkPropoundingSubscription.stripe_subscription_id.startsWith('pi_') && practicesobj.is_propounding_canceled) {
            await Practices.update({ is_propounding_canceled: null }, { where: { id: practicesobj.id } });
            await Subscriptions.destroy({ where: { practice_id: practicesobj.id, plan_category: 'propounding' } });
        }
        return;
    } catch (err) {

    }
}
const updateSubscriptionWebHookStatus_old = async (event) => {
    try {
        let body = '';
        if (process.env.CODE_ENV == 'local' && event.body &&  event.body.data) {
            body = typeof event.body.data === 'string' ? JSON.parse(event.body.data) : event.body.data;
        } else {
            body = typeof event.data === 'string' ? JSON.parse(event.data) : event.data;
        }
        const propounding_plans = ['propounding_monthly_199','propounding_yearly_2199'];
        const monthly_plans = ['monthly','responding_monthly_495','responding_monthly_349','propounding_monthly_199'];
        const yearly_plans = ['yearly','responding_yearly_5100','responding_yearly_3490','propounding_yearly_2199'];
        const { Op, Users, Plans, Practices, SubscriptionHistoryWebhook, SubscriptionHistory, Subscriptions,
            DiscountCode, DiscountHistory } = await connectToDatabase();

        const customer_id = body.object.customer;
        const practicesobj = await Practices.findOne({ where: { stripe_customer_id: customer_id, is_deleted: { [Op.not]: true } }, raw: true });
        if(!practicesobj) throw new HTTPError(404, 'Invalid stripe customer id '+customer_id); 
        const usersobj = await Users.findOne({
            where: {
                practice_id: practicesobj.id, is_deleted: { [Op.not]: true }, role: 'lawyer', is_admin: true
            }, order: [['createdAt', 'ASC']], raw: true
        });

        let planObject;
        if (body.object.metadata.plan_id) {
            planObject = await Plans.findOne({ where: { plan_id: body.object.metadata.plan_id }, raw: true, logging: console.log });
        } else {
            planObject = await Plans.findOne({ where: { stripe_product_id: body.object.plan.product }, raw: true, logging: console.log });
        }

        const currentSubscriptionObj = await SubscriptionHistory.findOne({ where: { practice_id: practicesobj.id, plan_category: planObject.plan_category }, order: [['createdAt', 'DESC']], raw: true, logging:console.log });
        
        let subscriptionValidityStart = new Date(parseInt(body.object.current_period_start) * 1000);
        let subscriptionValidity = new Date(parseInt(body.object.current_period_end) * 1000);
        let latest_actvity, switchedPlan, esquiretek_activity_type, payment_type;
        let status = 'Success';
        if (event.type == 'customer.subscription.created') {
            payment_type = 'New';
        } else if (event.type == 'customer.subscription.updated') {
            payment_type = 'Renewal';
            if (currentSubscriptionObj?.plan_type && ((monthly_plans.includes(currentSubscriptionObj.plan_type) && monthly_plans.includes(planObject.plan_type)) ||
                (yearly_plans.includes(currentSubscriptionObj.plan_type) && yearly_plans.includes(planObject.plan_type)))) {
                payment_type = 'Renewal';
            } else {
                payment_type = 'New';
            }
        } else if (event.type == 'customer.subscription.deleted') {
            payment_type = 'Subscription Canceled';
            subscriptionValidity = new Date();
        }
        if (body.object.cancel_at_period_end == true && body.object.canceled_at && body.object.cancel_at && ['active','trialing'].includes(body.object.status)) {
            latest_actvity = 'downgrade';
            payment_type = 'Subscription Canceled';
            switchedPlan = 'subscription_cancel';
            esquiretek_activity_type = 'DOWNGRADE';
        } else {
            if (body.object.metadata.plan_id) {
                if (body.object.metadata.latest_actvity) {
                    latest_actvity = body.object.metadata.latest_actvity;
                }
            }
            switchedPlan = planObject.plan_type;
            const historyCount = await SubscriptionHistory.count({where: {practice_id: practicesobj.id}});
            if (historyCount <= 0) {
                esquiretek_activity_type = 'NEW_SUBSCRIPTION';
            } else {
                esquiretek_activity_type = 'UPGRADED';
            }
        }
        if (!['active','trialing'].includes(body.object.status)) {
            esquiretek_activity_type = undefined;
        }

        if (event.type == 'customer.subscription.created' && !['active','trialing'].includes(body.object.status)) {
            status = "Payment Failed";
            latest_actvity = 'Subscription Creation';
            payment_type = 'Failed';
            esquiretek_activity_type = 'UPGRADE';
        }
        if (event.type == 'customer.subscription.updated' && !['active','trialing'].includes(body.object.status)) {
            status = "Subscription Canceled";
            payment_type = 'Failed';
            esquiretek_activity_type = 'UPGRADE';
        }

        let cancel_at, canceled_at;
        if (body.object.cancel_at != null) cancel_at = new Date(parseInt(body.object.cancel_at) * 1000);
        if (body.object.canceled_at != null) canceled_at = new Date(parseInt(body.object.canceled_at) * 1000);
       
        let price = planObject.price;
        let discountCodeObj, discount_percentage, discount_price, discount_code;
        if ((event.type == 'customer.subscription.created' || event.type == 'customer.subscription.updated') &&
        body?.object?.discount?.coupon?.id && body?.object?.status == 'active'){
                discount_code = body.object.discount.coupon.name;
                discountCodeObj = await DiscountCode.findOne({where:{discount_code:discount_code}});
                discount_percentage = discountCodeObj.discount_percentage;
                const percentage = 1 - (discount_percentage/100);
                discount_price = price = (percentage * price).toFixed(2);
        }else if(event.type == 'customer.subscription.deleted'){
            price = currentSubscriptionObj.price;
        }

        const subscriptionHistoryData = {
            id: uuid.v4(), order_date: new Date(), practice_id: practicesobj.id, user_id: usersobj.id,
            subscribed_by: usersobj.name, price: price, plan_type: planObject.plan_type, subscribed_on: new Date(), plan_id: planObject.plan_id,
            stripe_subscription_id: body.object.id, subscription_valid_start: subscriptionValidityStart, subscription_valid_till: subscriptionValidity,
            stripe_subscription_data: JSON.stringify(body.object), cancel_at: cancel_at, cancel_at_period_end: body.object.cancel_at_period_end,
            canceled_at: canceled_at, esquiretek_activity_type: esquiretek_activity_type, payment_type: payment_type, switched_plan: switchedPlan,
            stripe_latest_activity: latest_actvity, stripe_product_id: planObject.stripe_product_id, status: status, event_type: event.type,
            plan_category: planObject.plan_category
        };
        await SubscriptionHistory.create(subscriptionHistoryData);
        await SubscriptionHistoryWebhook.create(subscriptionHistoryData);

        if(body?.object?.discount?.coupon?.id && body?.object?.status == 'active' && !body?.object?.cancel_at_period_end){
            let plan_type = planObject.plan_type.split('_').slice(0,planObject.plan_type.length-1);
            let discount_for = plan_type.slice(0,plan_type.length-1).join('_').toLowerCase();
            let subscriptionObj = await Subscriptions.findOne({
                where:{practice_id: practicesobj.id,plan_category:planObject.plan_category},
                order:[['createdAt','DESC']]
            });
            let discount_history = {
                id: uuid.v4(), practice_id: practicesobj.id, subscription_id: subscriptionObj.id,  plan_type: planObject.plan_type,
                plan_category: planObject.plan_category, discount_for, promocode: discountCodeObj.name, discount_percentage, 
                actual_price: planObject.price, discount_price,
            }
            console.log(discount_history);
            await DiscountHistory.create(discount_history);
        }
        /* Subscribe Propounding Plans */
        const checkPropoundingSubscription = await Subscriptions.findOne({where:{practice_id: practicesobj.id,plan_category:'propounding'},raw:true});
        if(checkPropoundingSubscription && checkPropoundingSubscription.stripe_subscription_id.startsWith('pi_') && !practicesobj.is_propounding_canceled)
        {
            let propoundingPlan;
            if(planObject?.plan_type && ['monthly','responding_monthly_349','responding_monthly_495'].includes(planObject.plan_type)) propoundingPlan = 'propounding_monthly_199';
            if(planObject?.plan_type && ['yearly','responding_yearly_5100','responding_yearly_3490'].includes(planObject.plan_type)) propoundingPlan = 'propounding_yearly_2199';
            const propoundingPlanObj = await Plans.findOne({ where: { plan_type: propoundingPlan, active: true } });

            let subscriptionCreateObj = {
                customer: practicesobj.stripe_customer_id,
                items: [
                    { price: propoundingPlanObj.plan_id },
                ],
                metadata: {
                    practice_id: practicesobj.id,
                    user_email: usersobj.email,
                    plan_id: propoundingPlanObj.plan_id,
                    stripe_product_id: propoundingPlanObj.stripe_product_id,
                },
            }
            if (body?.object?.discount?.coupon?.name) {
                const propound_discountObj = await DiscountCode.findOne({ where: { discount_code: discount_code }, raw: true });
                if (propound_discountObj?.id) {
                    const is_eligible_for = propound_discountObj.plan_type.split(',');
                    if (is_eligible_for.includes(propoundingPlanObj.plan_type)) {
                        let stripe_couponcode_obj = JSON.parse(propound_discountObj.stripe_obj);
                        subscriptionCreateObj.coupon = stripe_couponcode_obj.id;
                    }
                }
            }
                const propoundingSubscription = await stripe.subscriptions.create(subscriptionCreateObj);
                if (propoundingSubscription?.status === 'incomplete') {
                    await stripe.subscriptions.del(propoundingSubscription.id);
                }
                const subscriptionValidity = new Date(parseInt(propoundingSubscription.current_period_end) * 1000);
                const updateColumn = {
                    subscribed_on: new Date(), plan_id: propoundingPlanObj.plan_id, stripe_subscription_id: propoundingSubscription.id,
                    subscribed_valid_till: subscriptionValidity, stripe_subscription_data: JSON.stringify(propoundingSubscription),
                    stripe_product_id: propoundingPlanObj.stripe_product_id, plan_category: propoundingPlanObj.plan_category
                };
                await Subscriptions.update(updateColumn, { where: {id: checkPropoundingSubscription.id}});
        }
        if(checkPropoundingSubscription && checkPropoundingSubscription.stripe_subscription_id.startsWith('pi_') && practicesobj.is_propounding_canceled){
            await Practices.update({is_propounding_canceled:null}, { where: { id: practicesobj.id} });
            await Subscriptions.destroy({ where: {practice_id: practicesobj.id,plan_category:'propounding'} });
        }
        return;
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}
const listallCharges = async (event) => {
    try {
        // const charges = await stripe.charges.list({
        //     customer: 'cus_MQh7dRKc8KZVOZ',
        //   });
        let body = '';
        // let temp = JSON.stringify(event);

        // fs.writeFile('invoice_item_update.json', temp, function (err) {
        //     if (err) {
        //         console.log('write function error.');
        //         throw err;
        //     };
        //     console.log('Saved!');
        //   });

        let action = undefined;
        if (process.env.CODE_ENV == 'local' && event.body &&  event.body.data) {
            body = typeof event.body.data === 'string' ? JSON.parse(event.body.data) : event.body.data;
            action = event.body.type;
        } else {
               body = typeof event.data === 'string' ? JSON.parse(event.data) : event.data;
               action = event.type;
        }
        const { Op, Users, Plans, Practices, StripeBillingHistory, } = await connectToDatabase();

        /* Retrive Subscripe plan from invoice.*/
        const invoiceObj = await stripe.invoices.retrieve(body.object.invoice);
        const invoiceData = invoiceObj.lines.data[0];
        
        /* Retrive Subscription Details from Subscription */
        let subscriptionObj = undefined;
        if( invoiceData && invoiceData.subscription){
            subscriptionObj = await stripe.subscriptions.retrieve(invoiceData.subscription);
        }

        const practicesobj = await Practices.findOne({
            where: { stripe_customer_id: body.object.customer,
                is_deleted: {[Op.not]: true}
            }, raw: true,
        });
        const usersobj = await Users.findOne({
            where: {
                practice_id: practicesobj.id,
                is_deleted: {[Op.not]: true},
                role: 'lawyer',
                is_admin: true
            },
            order: [
                ['createdAt', 'DESC'],
            ],
            raw: true
        });
        
        let subscription_start = invoiceData.period.start;
        let subscription_end = invoiceData.period.end;


        const billingHistoryObj = {};

        billingHistoryObj.id = uuid.v4();
        billingHistoryObj.webhook_action = action;
        billingHistoryObj.payment_date = new Date();
        billingHistoryObj.practice_id = practicesobj.id;
        billingHistoryObj.user_id = usersobj.id;

        if(invoiceData.plan && invoiceData.plan.product){
            const plansObj = await Plans.findOne({
                where: {
                    stripe_product_id: invoiceData.plan.product,
                    active:true,
                },
                raw: true
            });
            billingHistoryObj.plan_type = plansObj.plan_type;
            billingHistoryObj.plan_price = plansObj.price;
        }


        body.object.amount_captured ? billingHistoryObj.amount_captured = (body.object.amount_captured)/100 : billingHistoryObj.amount_captured = 0 ;
        if(body && body.object && body.object.object != 'invoiceitem'){
            const refundsObj = body.object.refunds.data[0];
            if(body.object.amount_captured && !body.object.amount_refunded && body.object.status == 'succeeded'){
                billingHistoryObj.payment_status = 'Success';
                billingHistoryObj.activity_type = body.object.description;
            }else if (!body.object.amount_captured && !body.object.amount_refunded && body.object.status == 'failed'){
                billingHistoryObj.payment_status = body.object.failure_message; // Card Decline , Insufficient funds
                billingHistoryObj.activity_type = body.object.description;
            }else if (body.object.refunded && refundsObj.status == 'succeeded' && body.object.amount_refunded ){
                billingHistoryObj.amount_captured = 0;
                billingHistoryObj.payment_status = 'Success';
                billingHistoryObj.refund_amount = (body.object.amount_refunded)/100;
                let refund_reason = refundsObj.reason.split('_');
                refund_reason = refund_reason.map((el)=>{
                    return el.charAt(0).toUpperCase() + el.slice(1).toLowerCase();
                });
                refund_reason = refund_reason.join(' ');
                billingHistoryObj.refund_reason = refund_reason;
                billingHistoryObj.activity_type = 'Refund';
            }
            invoiceData.subscription ? billingHistoryObj.stripe_subscription_id = invoiceData.subscription : billingHistoryObj.stripe_subscription_id = undefined;            
            billingHistoryObj.subscription_start = unixTimeStamptoDateTime(subscription_start);
            billingHistoryObj.subscription_end = unixTimeStamptoDateTime(subscription_end);
        }else if (body.object.object == 'invoiceitem'){
            if(body.object.proration && body.object.unit_amount < 0){ //Credit Balance Added.
                billingHistoryObj.payment_status = 'Amount added into credit balance.';                
                billingHistoryObj.activity_type = 'Invoice item';
            }else if(!body.object.proration && body.object.unit_amount){
                    billingHistoryObj.payment_status = 'New product added into invoice';
                    billingHistoryObj.activity_type = 'Invoice item';
            }
            billingHistoryObj.invoiceitem_reason = body.object.description;
            billingHistoryObj.invoiceitem_amount = (body.object.unit_amount)/100;
            invoiceData.subscription ? billingHistoryObj.stripe_subscription_id = invoiceData.subscription : billingHistoryObj.stripe_subscription_id = undefined;
        }
        subscriptionObj ? billingHistoryObj.subscription_stripe_obj = JSON.stringify(subscriptionObj) : billingHistoryObj.subscription_stripe_obj = undefined;
        billingHistoryObj.charges_stripe_obj = JSON.stringify(body);
        billingHistoryObj.invoice_stripe_obj = JSON.stringify(invoiceObj);
        const saveBillingHistory = await StripeBillingHistory.create(billingHistoryObj);
        const plainText = saveBillingHistory.get({plain: true});

          return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(plainText),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}

const getBillingDetails = async (event) => {
    try {
        const {sequelize, StripeBillingHistory,  Practices, Users} = await connectToDatabase();
        let searchKey = '';
        const sortKey = {};
        let sortQuery = '';
        let searchQuery = '';
        let sqlQueryCount = 0;
        const query = event.headers;
        // if (!query.offset && !query.limit) {
        //     let practicessRes = getallforClients(event);
        //     return practicessRes;
        // }
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;

        let codeSnip = '';
        let codeSnip2 = '';
        // let where = `WHERE is_deleted IS NOT true`;

        /**Sort**/
        if (query.sort == false) {
            sortQuery += ` ORDER BY createdAt DESC`
        } else if (query.sort != false) {
            query.sort = JSON.parse(query.sort);
            sortKey.column = query.sort.column;
            sortKey.type = query.sort.type;
            if (sortKey.column != 'all' && sortKey.column != '' && sortKey.column) {
                sortQuery += ` ORDER BY ${sortKey.column}`;
            }
            if (sortKey.type != 'all' && sortKey.type != '' && sortKey.type) {
                sortQuery += ` ${sortKey.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` name LIKE '%${searchKey}%' OR street LIKE '%${searchKey}%' OR phone LIKE '%${searchKey}%' OR address LIKE '%${searchKey}%' OR createdAt LIKE '%${searchKey}%' OR city LIKE '%${searchKey}%' OR state LIKE '%${searchKey}%' OR zip_code LIKE '%${searchKey}%' OR fax LIKE '%${searchKey}%'`;
        }

        if (searchQuery && sortQuery) {
            console.log('If 1');
            codeSnip =+ 'WHERE (' + searchQuery + ')' + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 =+ 'WHERE (' + searchQuery + ')' + sortQuery;
        } else if (!searchQuery && sortQuery) {
            console.log('If 2');
            codeSnip =+ sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 =+ sortQuery;
        } else if (searchQuery && !sortQuery) {
            console.log('If 3');
            codeSnip =+ 'WHERE (' + searchQuery + ')' + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
            codeSnip2 =+ 'WHERE (' + searchQuery + ')';
        } else if (!searchQuery && !sortQuery) {
            console.log('If 4');
            console.log(query);
            codeSnip += 'ORDER BY createdAt DESC LIMIT '+ query.limit +' OFFSET '+query.offset;
            console.log(codeSnip);
        }


        let sqlQuery = 'select id, payment_date, practice_id, user_id, plan_type, plan_price, amount_captured, payment_status, stripe_subscription_id, subscription_start, subscription_end, activity_type, refund_amount, refund_reason, invoiceitem_amount, invoiceitem_reason, webhook_action from StripeBillingHistories ' + codeSnip;
console.log('sqlQuery',sqlQuery);
         if(codeSnip2){
            sqlQueryCount = 'select * from StripeBillingHistories ' + codeSnip2;
         }else{
            sqlQueryCount = 'select * from StripeBillingHistories ';
         }

        const serverData = await sequelize.query(sqlQuery, {
            type: QueryTypes.SELECT
        });
        for (let i = 0; i < serverData.length; i++) {
            const practiceObj = await Practices.findOne({where: {id: serverData[i].practice_id}, raw: true});
            serverData[i].practice_name = practiceObj.name;
            const UserObj = await Users.findOne({where:{id:serverData[i].user_id}, raw: true});
            serverData[i].user_name = UserObj.name;    
        }
        const TableDataCount = await sequelize.query(sqlQueryCount, {
            type: QueryTypes.SELECT
        });


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not fetch the billing histories.'}),
        };
    }
}

const subscriptionCancel = async (event) => {
    try {
        const params = event.query;
        console.log(params);
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const { ids , type } = input;
        if(!params.practice_id) throw new HTTPError(404, `Practice id was not found`);
        const {Practices, Users, Op, SubscriptionHistory, Subscriptions, Plans} = await connectToDatabase();
        const practiceObj = await Practices.findOne({where:{id:params.practice_id, is_deleted: {[Op.not]: true}}});
        if(!practiceObj) throw new HTTPError(404, `Invalid practice id`);
        let endDate;    
        for(let i = 0;i<ids.length;i++) {
            const subscriptionObj = await Subscriptions.findOne({ where: { practice_id: params.practice_id, id: ids[i] }, order: [['createdAt', 'DESC'],], raw: true });
            const plansObj = await Plans.findOne({ where: { plan_id: subscriptionObj.plan_id } });
            const usersObj = await Users.findOne({ where: { practice_id: practiceObj.id, is_deleted: { [Op.not]: true } }, order: [['createdAt', 'ASC']], logging: console.log, raw: true });
            const stripeSubscriptionObj = JSON.parse(subscriptionObj.stripe_subscription_data);
            
            let template = 'Hi'
            if(usersObj?.name){ template = template + ' ' + usersObj.name};

            if(subscriptionObj && subscriptionObj.stripe_subscription_id && subscriptionObj.stripe_subscription_id.startsWith('sub_') && !stripeSubscriptionObj.cancel_at_period_end){
                const cancelObj  = await stripe.subscriptions.update(subscriptionObj.stripe_subscription_id, {cancel_at_period_end: true});
                let response = await Subscriptions.update({ stripe_subscription_data : JSON.stringify(cancelObj)},{where:{practice_id:params.practice_id,id:ids[i]}});
                
                if(response.length != 0 && response[0] == 0 ){
                    throw new HTTPError(500,`Subscription details not updated`);
                }else{
                    let Plan_name = plansObj.plan_category == 'responding' ? 'Responding' : 'Propounding'
                    endDate = unixTimeStamptoDate(cancelObj.cancel_at);
                    let subject = `Your EsquireTek account will be active till ${endDate}`;
                    let message_body = template + ',<br><br>' +
                        'We have canceled the auto-renewal of your EsquireTek <b>' + Plan_name + '</b> subscription based on your request. ' +
                        'Your plan will remain active till the end of the current subscription period (' + endDate + '). There will be no further ' +
                        'subscription charges to your account.<br><br>' +
                        'Thank you for being a valuable customer. We are always looking to improve EsquireTek if you have any feedback for ' +
                        'us on how to improve our platform, or if this cancellation was in error, please email (support@esquiretek.com) with any ' +
                        'comments or concerns. ';
                    await sendEmail(usersObj.email, subject, message_body, 'EsquireTek');
                } 
            } else if (!stripeSubscriptionObj.cancel_at_period_end && (subscriptionObj.stripe_subscription_id.startsWith('pi_') || plansObj.plan_category == 'propounding')) {
                practiceObj.is_propounding_canceled = true;
                await practiceObj.save();
                cancelObj = new Date(subscriptionObj.subscribed_valid_till).getTime();
                endDate = unixTimeStamptoDate(cancelObj);

                let Plan_name = 'Propounding';
                let subject = `Your Propounding feature will be active till ${endDate}`;
                let message_body = template+',<br><br>'+
                'We have canceled the auto-renewal for Propounding feature based on your request. You will be able to use propounding feature till the end of the current subscription period '+endDate+'.<br><br>'+
                'Thank you for trying out the propounding feature. We are always looking to improve EsquireTek if you have any feedback for us on how to improve our platform, or if this cancellation was in error, please email (support@esquiretek.com) with any comments or concerns.';
                await sendEmail(usersObj.email, subject, message_body, 'EsquireTek');
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({status:'ok',message:'Subscription canceled successfully.'}),
        };

    } catch (err) {
        console.log(err);    
        
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not cancel subscription.'}),
        };
    }
}

const fetchActiveSubscriptionUsingCustomerID = async (event) => {
    try {
        const customer = await stripe.customers.retrieve('cus_JQg8CJdEkOfYzw');
        if(!customer) throw new HTTPError(404, `Invalid stripe customer id`); 
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify(customer),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not get customer details.'}),
        };
    }   
}
const fetchSubscriptionInfo = async (event) => {
    try {
        const subscription = await stripe.subscriptions.retrieve('sub_1Ll6nWABxGGVMf06DuNCnYwt');
        if(!subscription) throw new HTTPError(404, `Invalid stripe customer id`); 
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify(subscription),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({error: err.message || 'Could not get subscription details.'}),
        };
    } 
}
module.exports.stripeChargeWebhook = stripeChargeWebhook;
module.exports.listallCharges = listallCharges;
module.exports.invoiceItemWebHook = invoiceItemWebHook;
module.exports.subscriptionWebHook = subscriptionWebHook;
module.exports.updateSubscriptionWebHookStatus = updateSubscriptionWebHookStatus;
module.exports.getBillingDetails = getBillingDetails;
module.exports.subscriptionCancel = subscriptionCancel;
module.exports.fetchActiveSubscriptionUsingCustomerID = fetchActiveSubscriptionUsingCustomerID;
module.exports.fetchSubscriptionInfo = fetchSubscriptionInfo;