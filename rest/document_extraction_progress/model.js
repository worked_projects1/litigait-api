module.exports = (sequelize, type) => sequelize.define('DocumentExtractionProgress', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    user_id: type.STRING,
    session_user_id: type.STRING, /* No need */
    legalform_filename: type.STRING,
    pdf_filename: type.STRING,
    s3_file_key: type.STRING,
    set_number: type.STRING,
    state: type.STRING,
    document_type: type.STRING,
    status: type.STRING,// fileProgress,pending, complete,Failed
    document_data: type.TEXT('long'),
    legalforms_id: type.STRING,
});