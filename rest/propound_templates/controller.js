const uuid = require("uuid");
const connectToDatabase = require("../../db");
const {HTTPError} = require("../../utils/httpResp");
const AWS = require("aws-sdk");
AWS.config.update({region: process.env.REGION || "us-west-2"});
const s3 = new AWS.S3();
const {
    validateSavePropoundTemplatesData,
    validateGetPropoundTemplatesQuestions,
    validateGetOne
} = require('./validation');
const {sendEmail} = require('../../utils/mailModule');

const create = async (event) => {
    try {
        let id;
        const input =
            typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const questions = input.questions;
        const dataObject = Object.assign(input,
            {id: id || uuid.v4()},
            {questions: JSON.stringify(questions)},
            {number_of_questions: questions.length});
        const {PropoundTemplates, Op, PropoundHelpRequest, Users} = await connectToDatabase();
        const PropoundTemplatesObj = await PropoundTemplates.create(dataObject);
        const plainText = PropoundTemplatesObj.get({plain: true});
        if (input.propound_help_request_id) {
            await PropoundHelpRequest.destroy({where: {id:input.propound_help_request_id},logging: console.log});

            const usersObj = await Users.findOne({where: {email:input.user_email}, raw: true});
            let template = 'Hi';
            if(usersObj?.name) template = template+' '+usersObj.name;
            const Body =  template + ',<br><br>' + 'Your template, '+input.file_name+' has been updated with the questions.<br><br>' +
                'Notification from EsquireTek.';
            await sendEmail(input.user_email, 'Important: Questions updated for your template', Body);
        }
        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(PropoundTemplatesObj),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "Could not create the Propounding Details.",
            }),
        };
    }
};
const getOne = async (event) => {
    try {
        const input =
            typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        // validateGetOne(params);
        const {PropoundTemplates, Op} = await connectToDatabase();
        const PropoundTemplatesObj = await PropoundTemplates.findOne({
            where: {id: params.id},
        });
        if (!PropoundTemplatesObj)
            throw new HTTPError(
                404,
                ` Propound Template with id: ${params.id} was not found`
            );
        const plaintext = PropoundTemplatesObj.get({plain: true});
        if(plaintext && plaintext.questions){
            plaintext.questions = JSON.parse(plaintext.questions);
        }
        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(plaintext),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "Could not get the Propounding Details.",
            }),
        };
    }

};
const getAll = async (event) => {
    try {
        const query = event.queryStringParameters || {};
        const {PropoundTemplates, Op} = await connectToDatabase();
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }
        query.where.practice_id = event.user.practice_id;
        query.where.questions = {[Op.not]: null}
        query.where.is_deleted = {[Op.not]: true}
        query.order = [["createdAt", "DESC"]];
        query.raw = true;

        const plaintext = await PropoundTemplates.findAll(query);
        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(plaintext),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "Could not get the Propounding Details.",
            }),
        };
    }
};
const destroy = async (event) => {
    try {
        const input =
            typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        const {PropoundTemplates, Op} = await connectToDatabase();
        const PropoundTemplatesObj = await PropoundTemplates.findOne({
            where: {id: params.id},
        });
        if (!PropoundTemplatesObj)
            throw new HTTPError(
                404,
                ` Propound Template with id: ${params.id} was not found`
            );
        PropoundTemplatesObj.is_deleted = true;
        await PropoundTemplatesObj.save();

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                status: "Ok",
                message: "Propound Template Removed Successfully.",
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error:
                    err.message || "Could not delete the Propound Template Details.",
            }),
        };
    }
};
const createPropoundTemplates = async (event) => {
    try {
        let id;
        const input =
            typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        const questions = input.questions;
        if (process.env.NODE_ENV === "test" && input.id) id = input.id;
        const dataObject = Object.assign(input, {
            id: id || uuid.v4(),
            created_by: event.user.id,
            practice_id: event.user.practice_id,
            status: "pending",
        });
        const {PropoundTemplates, Op} = await connectToDatabase();
        const existing_propound_template_id = input.propound_template_id;
        if (existing_propound_template_id) {
            const existingPropoundTemplates = await PropoundTemplates.findOne({
                where: {
                    id: existing_propound_template_id,
                    practice_id: dataObject.practice_id,
                },
                logging: console.log,
                raw: true,
            });
            if (existingPropoundTemplates) {
                await PropoundTemplates.destroy({
                    where: {
                        id: existing_propound_template_id,
                        practice_id: dataObject.practice_id,
                    },
                    logging: console.log,
                    raw: true,
                });
            }
        }
        if (dataObject.questions) {
            dataObject.questions = JSON.stringify(questions);
            dataObject.number_of_questions = questions.length;
        }
        const PropoundTemplatesObj = await PropoundTemplates.create(dataObject);
        const uploadURLObject = await getUploadURL(input, PropoundTemplatesObj);
        PropoundTemplatesObj.s3_file_key = uploadURLObject.s3_file_key;
        await PropoundTemplatesObj.save();

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                PropoundTemplatesObj,
                uploadURL: uploadURLObject.uploadURL,
                s3_file_key: uploadURLObject.s3_file_key,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error:
                    err.message || "Could not upload the Propound Template Details.",
            }),
        };
    }
};
const getUploadURL = async (input, PropoundTemplatesObj) => {
    let fileExtention = "";
    let documentType = "";
    if (input.upload_document_name) {
        fileExtention = `.${input.upload_document_name.split(".").pop()}`;
    }
    if (input.document_type) {
        documentType = `_${input.document_type}`;
    }
    const s3Params = {
        Bucket: process.env.S3_BUCKET_FOR_PROPOUND_SETTINGS_DOCUMENTS,
        Key:
            `${input.practice_id}/${input.upload_document_name}` + `${fileExtention}`,
        ContentType: input.content_type,
        ACL: "private",
        Metadata: {
            practice_id: PropoundTemplatesObj.practice_id,
            state: PropoundTemplatesObj.state,
            document_type: PropoundTemplatesObj.document_type,
        },
    };
    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl("putObject", s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
        });
    });
};
const savePropoundTemplatesData = async (event) => {
    try {
        const {PropoundTemplates, PropoundHelpRequest, Op} = await connectToDatabase();
        const input =
            typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        console.log(input);
        // validateSavePropoundTemplatesData(input);
        const questions = input.questions;
        const questions_count = questions.length;
        const queryParams = event.pathParameters || event.params;
        const dataObject = JSON.stringify(questions);

        await PropoundTemplates.update(
            {
                questions: dataObject,
                number_of_questions: questions_count
            },
            {
                where: {
                    id: input.propound_template_id
                    , practice_id: input.practice_id, document_type: input.document_type
                },
                logging: console.log,
            }
        );

        if(input.propound_help_request_id){
            await PropoundHelpRequest.destroy({where: {id:input.propound_help_request_id}});
        }
        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                message: "Propound template data processing completed",
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error:
                    err.message ||
                    "Could not upload the Propound Template Question Details.",
            }),
        };
    }
};
const getPropoundTemplatesQuestions = async (event) => {
    try {
        const {PropoundTemplates} = await connectToDatabase();
        const params = event.params || event.pathParameters;
        // validateGetPropoundTemplatesQuestions(params);
        const forms = await PropoundTemplates.findOne({
            where: {
                practice_id: event.user.practice_id,
                id: params.id,
                document_type: params.document_type,
            },logging:console.log
        });
        if (!forms || !forms?.questions) throw new HTTPError( `Question not found for this Propound template ${params.id}`);

        const data = JSON.parse(forms.questions);
        
        const compare = (x1, x2) => {
            let a = x1.question_number_text;
            let b = x2.question_number_text;
            let section_id_a = parseInt(x1.question_section_id);
            let section_id_b = parseInt(x2.question_section_id);
            if (a === b) {
                if (section_id_a < section_id_b) {
                    return -1;
                }
                if (section_id_a > section_id_b) {
                    return 1;
                }

                return 0;
            }
            const aArr = a.split("."),
                bArr = b.split(".");
            for (let i = 0; i < Math.min(aArr.length, bArr.length); i++) {
                if (parseInt(aArr[i]) < parseInt(bArr[i])) {
                    return -1;
                }
                if (parseInt(aArr[i]) > parseInt(bArr[i])) {
                    return 1;
                }
            }
            if (aArr.length < bArr.length) {
                return -1;
            }
            if (aArr.length > bArr.length) {
                return 1;
            }
            return 0;
        };
        await data.sort(compare);

        return {
            statusCode: 200,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(data),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error:
                    err.message ||
                    "Could not get the Propound Template Question Details.",
            }),
        };
    }
};

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.destroy = destroy;
module.exports.createPropoundTemplates = createPropoundTemplates;
module.exports.savePropoundTemplatesData = savePropoundTemplatesData;
module.exports.getPropoundTemplatesQuestions = getPropoundTemplatesQuestions;
