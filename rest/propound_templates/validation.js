const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');


exports.validateSavePropoundTemplatesData = function (data) {

    const rules = {
        propound_template_id: 'required|min:4|max:64',
        practice_id: 'required|min:4|max:64',
        document_type: 'required|min:3|max:64',
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateGetPropoundTemplatesQuestions = function (data) {

    const rules = {
        id: 'required|min:4|max:64',
        document_type: 'required|min:3|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateGetOne = function (data) {

    const rules = {
        id: 'required|min:4|max:64',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};