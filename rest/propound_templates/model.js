module.exports = (sequelize, type) => sequelize.define('PropoundTemplates', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    file_name: type.STRING,
    upload_document_name: type.STRING,
    questions: type.TEXT('long'),
    number_of_questions: type.STRING,
    document_type: type.STRING,
    state: type.STRING,
    is_deleted: type.BOOLEAN,
    s3_file_key: type.STRING,
    created_by: type.STRING,
});