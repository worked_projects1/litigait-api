const Validator = require('validatorjs');
const { HTTPError } = require('../../utils/httpResp');

exports.validateCreate = function (data) {
    const rules = {
        adminobjection_id: 'required',
        practice_id: 'required',
        user_id: 'required',
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all())
}

exports.validateUpdate = function (data) {
    const rules = {
        objection_title: 'min:2|max:64',
        objection_text: 'min:2',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateGetOne = function (data) {
    const rules = {
        id: 'required'
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all());
}
