const connectToDatabase = require('../../db');
const uuid = require('uuid');
const { validateCreate, validateUpdate, validateGetOne } = require('./validation');

const create = async (event) => {
    try {
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        validateCreate(input)
        const { LawyerObjections } = await connectToDatabase();
        const dataObject = Object.assign(input, { id: uuid.v4(), practice_id: event.user.practice_id })
        const response = await LawyerObjections.create(dataObject);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(response)
        }

    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the Laywer Objections.' }),
        };
    }
}

const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        validateUpdate(input);
        const { LawyerObjections } = await connectToDatabase();
        const lawyerObjections = await LawyerObjections.findOne({ where: { id: params.id } });
        if (!lawyerObjections) throw new HTTPError(404, `LawyerObjections with id: ${params.id} was not found`);
        const updatedModel = Object.assign(lawyerObjections, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the LawyerObjections.' }),
        };
    }
};

const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateGetOne(params);
        const { LawyerObjections } = await connectToDatabase();
        const lawyerObjections = await LawyerObjections.findOne({ where: { id: params.id } });
        if (!lawyerObjections) throw new HTTPError(404, `LawyerObjections with id: ${params.id} was not found`);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(lawyerObjections)
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the LawyerObjections.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        let query = event.headers || {};
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        query.where = { practice_id: event.user.practice_id }
        query.order = [
            ['objection_title', 'ASC']
        ];

        const { LawyerObjections } = await connectToDatabase();
        const lawyerObjections = await LawyerObjections.findAll({
            where: { practice_id: event.user.practice_id },
            order: [
                ['objection_title', 'ASC']
            ]
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(lawyerObjections),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the LawyerObjections.' })
        };
    }
};

const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateDelete(params);
        const { LawyerObjections } = await connectToDatabase();
        const lawyerObjectionsDetails = await LawyerObjections.findOne({ where: { id: params.id } });
        if (!lawyerObjectionsDetails) throw new HTTPError(404, `LawyerObjections with id: ${params.id} was not found`);
        await lawyerObjectionsDetails.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(lawyerObjectionsDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could destroy fetch the LawyerObjections.' }),
        };
    }
};

module.exports.create = create;
module.exports.update = update;
module.exports.getOne = getOne;
module.exports.getAll = getAll;
module.exports.destroy = destroy;